### Data Sets ###
# You can try the code completion by starting to write a
# function and then press the tab button. Code completion
# also works for function arguments.

# You can press the Ctrl button and then click on a function
# to see its source code in a new window.

# Single lines or code selections can be sent to the console
# by pressing Ctrl + Enter.

# Let's explore the "cars" data from the "datasets" package
# by using the help pages
help(package="datasets")
?cars

# Here we load the data and explore the data directly by
# using built in R functions.
data(cars)

head(cars)
View(cars)

str(cars)
summary(cars)
summary(cars$dist)





### Visualization ###
# Static Plot
plot(cars$speed, cars$dist, main="Speed vs. Distance")

# Interactive Plot (Optional)
library(manipulate)
?manipulate

manipulate(
  plot(cars$speed, cars$dist, main="Speed vs. Distance",
       col = color,
       type = type,
       pch = pch,
       axes = axes,
       ann = label),
  color = picker("black","darkred","steelblue"),
  type = picker("p","l","b"),
  pch = slider(0,25,1),
  axes = checkbox(TRUE, "Draw Axes"),
  label = checkbox(TRUE, "Draw Labels")
)

# 3D Plotting
f <- function(x,y) {
  r <- sqrt(x^2+y^2)
  10 * sin(r)/r
}

x <- seq(-10, 10, length=30)
y <- x
z <- outer(x, y, f)

manipulate(
  persp(x, y, z,
        theta = theta, phi = 30, expand = 0.5,
        col = "lightblue",
        ltheta = 120, shade = 0.75,
        xlab = "X", ylab = "Y",
        zlab = "Sinc( r )"),
  theta = slider(0,90,30)
)

#mypower <- function(x,k=2) {
 # x^k
}
