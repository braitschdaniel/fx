###################################
###################################
# calculate the number of students taking choices A,B,C,D,E,F
###################################
###################################

setwd(dirname(rstudioapi::getActiveDocumentContext()$path));    #getwd()
library(Hmisc) # to get spss files

options(warn=-1)                  # to suppress irrelevant warning of spss.get
projectData <- spss.get("FAProjectDataV2.sav", use.value.labels=TRUE)
options(warn=0)                   # to reactivate warnings
#View(projectData[285:288])       # profile: science: yes or no

projectData$LP.Name <- as.character(projectData$LP.Name)    # change the class into char

preData <- projectData[369:417];                      # preData
preForm1        <- preData[1:216,];                   # subdivided into teaching form
preForm2        <- preData[217:425,];
preForm3        <- preData[426:607,];
preAll          <- preData[1:607,]


postData        <- projectData[320:368]               # postData
postForm1       <- postData[1:216,]                   # subdivided into teaching form
postForm2       <- postData[217:425,]
postForm3       <- postData[426:607,]
postAll         <- postData[1:607,]



#View(projectData[369:417])        # PRE Answers
#View(projectData[320:368])        # POST Answers

PREMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(PREMatrix)){
  PREMatrix[i,1] <- sum(preData[,i] == 1)
  PREMatrix[i,2] <- sum(preData[,i] == 2)
  PREMatrix[i,3] <- sum(preData[,i] == 3)
  PREMatrix[i,4] <- sum(preData[,i] == 4)
  PREMatrix[i,5] <- sum(preData[,i] == 5)
  PREMatrix[i,6] <- sum(preData[,i] == 6)
}
for (i in 1:nrow(PREMatrix)){
  for (j in 1:ncol(PREMatrix)){
    if (PREMatrix[i,j] == 0) {
      PREMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(PREMatrix, "PREData.xlsx")

POSTMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(POSTMatrix)){
  POSTMatrix[i,1] <- sum(postData[,i] == 1)
  POSTMatrix[i,2] <- sum(postData[,i] == 2)
  POSTMatrix[i,3] <- sum(postData[,i] == 3)
  POSTMatrix[i,4] <- sum(postData[,i] == 4)
  POSTMatrix[i,5] <- sum(postData[,i] == 5)
  POSTMatrix[i,6] <- sum(postData[,i] == 6)
}
for (i in 1:nrow(POSTMatrix)){
  for (j in 1:ncol(POSTMatrix)){
    if (POSTMatrix[i,j] == 0) {
      POSTMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(POSTMatrix, "POSTData.xlsx")


# Traditional, table
PREMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(PREMatrix)){
  PREMatrix[i,1] <- sum(preForm1[,i] == 1)
  PREMatrix[i,2] <- sum(preForm1[,i] == 2)
  PREMatrix[i,3] <- sum(preForm1[,i] == 3)
  PREMatrix[i,4] <- sum(preForm1[,i] == 4)
  PREMatrix[i,5] <- sum(preForm1[,i] == 5)
  PREMatrix[i,6] <- sum(preForm1[,i] == 6)
}
for (i in 1:nrow(PREMatrix)){
  for (j in 1:ncol(PREMatrix)){
    if (PREMatrix[i,j] == 0) {
      PREMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(PREMatrix, "PREDataTrad.xlsx")

POSTMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(POSTMatrix)){
  POSTMatrix[i,1] <- sum(postForm1[,i] == 1)
  POSTMatrix[i,2] <- sum(postForm1[,i] == 2)
  POSTMatrix[i,3] <- sum(postForm1[,i] == 3)
  POSTMatrix[i,4] <- sum(postForm1[,i] == 4)
  POSTMatrix[i,5] <- sum(postForm1[,i] == 5)
  POSTMatrix[i,6] <- sum(postForm1[,i] == 6)
}
for (i in 1:nrow(POSTMatrix)){
  for (j in 1:ncol(POSTMatrix)){
    if (POSTMatrix[i,j] == 0) {
      POSTMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(POSTMatrix, "POSTDataTrad.xlsx")


# Frequent testing, table
PREMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(PREMatrix)){
  PREMatrix[i,1] <- sum(preForm2[,i] == 1)
  PREMatrix[i,2] <- sum(preForm2[,i] == 2)
  PREMatrix[i,3] <- sum(preForm2[,i] == 3)
  PREMatrix[i,4] <- sum(preForm2[,i] == 4)
  PREMatrix[i,5] <- sum(preForm2[,i] == 5)
  PREMatrix[i,6] <- sum(preForm2[,i] == 6)
}
for (i in 1:nrow(PREMatrix)){
  for (j in 1:ncol(PREMatrix)){
    if (PREMatrix[i,j] == 0) {
      PREMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(PREMatrix, "PREDataFrequ.xlsx")

POSTMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(POSTMatrix)){
  POSTMatrix[i,1] <- sum(postForm2[,i] == 1)
  POSTMatrix[i,2] <- sum(postForm2[,i] == 2)
  POSTMatrix[i,3] <- sum(postForm2[,i] == 3)
  POSTMatrix[i,4] <- sum(postForm2[,i] == 4)
  POSTMatrix[i,5] <- sum(postForm2[,i] == 5)
  POSTMatrix[i,6] <- sum(postForm2[,i] == 6)
}
for (i in 1:nrow(POSTMatrix)){
  for (j in 1:ncol(POSTMatrix)){
    if (POSTMatrix[i,j] == 0) {
      POSTMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(POSTMatrix, "POSTDataFrequ.xlsx")


# Formative Assessement, table
PREMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(PREMatrix)){
  PREMatrix[i,1] <- sum(preForm3[,i] == 1)
  PREMatrix[i,2] <- sum(preForm3[,i] == 2)
  PREMatrix[i,3] <- sum(preForm3[,i] == 3)
  PREMatrix[i,4] <- sum(preForm3[,i] == 4)
  PREMatrix[i,5] <- sum(preForm3[,i] == 5)
  PREMatrix[i,6] <- sum(preForm3[,i] == 6)
}
for (i in 1:nrow(PREMatrix)){
  for (j in 1:ncol(PREMatrix)){
    if (PREMatrix[i,j] == 0) {
      PREMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(PREMatrix, "PREDataFA.xlsx")

POSTMatrix <- matrix(0, 49, 6)
for (i in 1:nrow(POSTMatrix)){
  POSTMatrix[i,1] <- sum(postForm3[,i] == 1)
  POSTMatrix[i,2] <- sum(postForm3[,i] == 2)
  POSTMatrix[i,3] <- sum(postForm3[,i] == 3)
  POSTMatrix[i,4] <- sum(postForm3[,i] == 4)
  POSTMatrix[i,5] <- sum(postForm3[,i] == 5)
  POSTMatrix[i,6] <- sum(postForm3[,i] == 6)
}
for (i in 1:nrow(POSTMatrix)){
  for (j in 1:ncol(POSTMatrix)){
    if (POSTMatrix[i,j] == 0) {
      POSTMatrix[i,j] <- "-"
    }
  }
}
write.xlsx(POSTMatrix, "POSTDataFA.xlsx")
