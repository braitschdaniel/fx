##########
# compare types
##########

# distinction by items:
# function compareConcThreeTypes
# function compareConcGender
# function compareSCPlotOfIndivTeach 
#   (works for items and one single concept)


compareSCPlotThreeTypes <- function(items, gender = "", Arrows = TRUE) {      #general form
  devTest <- 1; names(devTest) <- "null device"
  if (dev.cur() != devTest) {
    dev.off()
  }
  par(mfrow = c(1,3), cex = .5)
  ConcPlotDataTraditional <- SCPlot(Traditional, items, gender, Arrows, multiPlot = TRUE)
  ConcPlotDataFrequ       <- SCPlot(FrequTesting, items, gender, Arrows, multiPlot = TRUE)
  ConcPlotDataFA          <- SCPlot(FormAssess, items, gender, Arrows, multiPlot = TRUE)
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- "figuresMultiplePlots"
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  mainDir <- getwd()
  if (class (attributes(items)$c) != "character"){
    subDir2 <- "Items"
    if (length(items) != 49)
    {
      for (i in 1:length(items))
      {
        subDir2 <- paste(subDir2, items[i], sep= "_")
      }
    } else {
      subDir2 <- paste(subDir2, "All", sep= "")
    }
    if (file.exists(subDir2)){
      setwd(file.path(mainDir, subDir2))
    } else {
      dir.create(file.path(mainDir, subDir2))
      setwd(file.path(mainDir, subDir2))
    }
    pdfName <- paste("S-C-", gender, "PlotCompareTypes.pdf",sep="")
  } else{
    subDir2 <- "ItemsQuantisedRegions"
    #subDir2 <- paste(subDir2, attr(items, 'c'), sep= "_")
    if (file.exists(subDir2)){
      setwd(file.path(mainDir, subDir2))
    } else {
      dir.create(file.path(mainDir, subDir2))
      setwd(file.path(mainDir, subDir2))
    }
    pdfName <- paste(attr(items, 'c'),"-Items-S-C-", gender, "PlotCompareTypes.pdf",sep="")
  }
  dev.copy2pdf(file = pdfName)
  cat("pdfName: \t \t",pdfName, "\n")
  cat("saved in directory: \t",getwd(), "\n")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  #return()
}


##########
# compare gender
##########

compareSCPlotGender <- function(items, Arrows = TRUE) {  
  devTest <- 1; names(devTest) <- "null device"
  if (dev.cur() != devTest) {
    dev.off()
  }
  par(mfrow = c(1,2), cex = .5)
  ConcPlotDataTraditional <- SCPlot(Traditional, items, gender = "male", Arrows, multiPlot = TRUE)
  ConcPlotDataTraditional <- SCPlot(Traditional, items, gender = "female", Arrows, multiPlot = TRUE)
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- "figuresMultiplePlots"
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  mainDir <- getwd()
  subDir2 <- "Items"
  if (length(items) != 49)
  {
    for (i in 1:length(items))
    {
      subDir2 <- paste(subDir2, items[i], sep= "_")
    }
  } else {
    subDir2 <- paste(subDir2, "All", sep= "")
  }
  
  if (file.exists(subDir2)){
    setwd(file.path(mainDir, subDir2))
  } else {
    dir.create(file.path(mainDir, subDir2))
    setwd(file.path(mainDir, subDir2))
  }
  pdfName <- paste("S-C-PlotCompareGenderTraditional.pdf",sep="")
  dev.copy2pdf(file = pdfName)
  cat("pdfName: \t \t",pdfName, "\n")
  cat("saved in directory: \t",getwd(), "\n")
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  
  ConcPlotDataFrequ       <- SCPlot(FrequTesting, items, gender = "male", multiPlot = TRUE)
  ConcPlotDataFrequ       <- SCPlot(FrequTesting, items, gender = "female", multiPlot = TRUE)
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- "figuresMultiplePlots"
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  mainDir <- getwd()
  subDir2 <- "Items"
  if (length(items) != 49)
  {
    for (i in 1:length(items))
    {
      subDir2 <- paste(subDir2, items[i], sep= "_")
    }
  } else {
    subDir2 <- paste(subDir2, "All", sep= "")
  }
  
  if (file.exists(subDir2)){
    setwd(file.path(mainDir, subDir2))
  } else {
    dir.create(file.path(mainDir, subDir2))
    setwd(file.path(mainDir, subDir2))
  }
  pdfName <- paste("S-C-PlotCompareGenderFrequTesting.pdf",sep="")
  dev.copy2pdf(file = pdfName)
  cat("pdfName: \t \t",pdfName, "\n")
  cat("saved in directory: \t",getwd(), "\n")
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  
  ConcPlotDataFA          <- SCPlot(FormAssess, items, gender = "male", multiPlot = TRUE)
  ConcPlotDataFA          <- SCPlot(FormAssess, items, gender = "female", multiPlot = TRUE)
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- "figuresMultiplePlots"
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  mainDir <- getwd()
  subDir2 <- "Items"
  if (length(items) != 49)
  {
    for (i in 1:length(items))
    {
      subDir2 <- paste(subDir2, items[i], sep= "_")
      }
    } else {
      subDir2 <- paste(subDir2, "All", sep= "")
    }
    
    if (file.exists(subDir2)){
      setwd(file.path(mainDir, subDir2))
    } else {
      dir.create(file.path(mainDir, subDir2))
      setwd(file.path(mainDir, subDir2))
    }
  pdfName <- paste("S-C-PlotCompareGenderFormAssess.pdf",sep="")
  dev.copy2pdf(file = pdfName)
  cat("pdfName: \t \t",pdfName, "\n")
  cat("saved in directory: \t",getwd(), "\n")
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
}


##########
# calculate mean
##########





# compare all
compareSCPlotOfIndivTeach <- function(items, xAxis = c(-0.1,0.6), 
                                      yAxis = c(-0.3,0.6), combinedPlot = FALSE)  {
  if (combinedPlot == FALSE) {
    devTest <- 1; names(devTest) <- "null device"
    if (dev.cur() != devTest) {
      dev.off()
    }
  }
  
  TraditionalMeans <- matrix(nrow = length(allTradTeachers), ncol = 3)
  for (i in 1:length(allTradTeachers)){
    TradDataClassElement <-  SCPlot(allTradTeachers[[i]], items, calcMean = TRUE)
    TraditionalMeans[i,1] <- (TradDataClassElement@postMean[1] - TradDataClassElement@preMean[1])
    TraditionalMeans[i,2] <- (TradDataClassElement@postMean[2] - TradDataClassElement@preMean[2])
    TraditionalMeans[i,3] <- nrow(TradDataClassElement@pre)
  }
  
  TradNoStudMean <- mean(TraditionalMeans[,3])
  TradIntermedRes <- matrix(nrow = length(allTradTeachers), ncol = 2)
  for (i in 1:length(allTradTeachers)){
    TradIntermedRes[i,1] <- TraditionalMeans[i,1]*TraditionalMeans[i,3]
    TradIntermedRes[i,2] <- TraditionalMeans[i,2]*TraditionalMeans[i,3]
  }
  TraditionalAverage <- c(sum(TradIntermedRes[,1])/length(allTradTeachers)/TradNoStudMean,
                          sum(TradIntermedRes[,2])/length(allTradTeachers)/TradNoStudMean)
  
  FrequTestingMeans <- matrix(nrow = length(allFrequTestTeachers), ncol = 3)
  for (i in 1:length(allFrequTestTeachers)){
    FrequDataClassElement <-  SCPlot(allFrequTestTeachers[[i]], items, calcMean = TRUE)
    FrequTestingMeans[i,1] <- (FrequDataClassElement@postMean[1] - FrequDataClassElement@preMean[1])
    FrequTestingMeans[i,2] <- (FrequDataClassElement@postMean[2] - FrequDataClassElement@preMean[2])
    FrequTestingMeans[i,3] <- nrow(FrequDataClassElement@pre)
  }
  
  FrequTestingNoStudMean <- mean(FrequTestingMeans[,3])
  FrequTestingIntermedRes <- matrix(nrow = length(allFrequTestTeachers), ncol = 2)
  for (i in 1:length(allFrequTestTeachers)){
    FrequTestingIntermedRes[i,1] <- FrequTestingMeans[i,1]*FrequTestingMeans[i,3]
    FrequTestingIntermedRes[i,2] <- FrequTestingMeans[i,2]*FrequTestingMeans[i,3]
  }
  FrequAverage <- c(sum(FrequTestingIntermedRes[,1])/
                      length(allFrequTestTeachers)/FrequTestingNoStudMean,
                    sum(FrequTestingIntermedRes[,2])
                    /length(allFrequTestTeachers)/FrequTestingNoStudMean)
  
  FAMeans <- matrix(nrow = length(allFATeachers), ncol = 3)
  for (i in 1:length(allFATeachers)){
    faDataClassElement <-  SCPlot(allFATeachers[[i]], items, calcMean = TRUE)
    FAMeans[i,1] <- (faDataClassElement@postMean[1] - faDataClassElement@preMean[1])
    FAMeans[i,2] <- (faDataClassElement@postMean[2] - faDataClassElement@preMean[2])
    FAMeans[i,3] <- nrow(faDataClassElement@pre)
  }
  
  FANoStudMean <- mean(FAMeans[,3])
  FAIntermedRes <- matrix(nrow = length(allFATeachers), ncol = 2)
  for (i in 1:length(allFATeachers)){
    FAIntermedRes[i,1] <- FAMeans[i,1]*FAMeans[i,3]
    FAIntermedRes[i,2] <- FAMeans[i,2]*FAMeans[i,3]
  }
  FAAverage <- c(sum(FAIntermedRes[,1])/length(allFATeachers)/FANoStudMean,
                 sum(FAIntermedRes[,2])/length(allFATeachers)/FANoStudMean)
  
  
  ######            
  # Plotting
  ######
  
  par(pty="s")
  plot(TraditionalAverage[1], TraditionalAverage[2], type="p", lty=2, 
       pch = 17, cex.lab = 1.1, col = "Blue", 
       cex = 2.2, xlab = "", ylab = "", xaxs="i", yaxs="i",
       axes = TRUE,
       xlim = c(xAxis[1], xAxis[2]), ylim = c(yAxis[1], yAxis[2]))
  points(FrequAverage[1], FrequAverage[2], 
         pch = 17, cex = 2.2, col = "Red" )
  points(FAAverage[1], FAAverage[2], 
         pch = 17, cex = 2.2, col = "Forestgreen" )
  titleContent <- "Comparison of PRE to POST devel."
  title(main = titleContent, col.main = "black", xlab = expression(paste(Delta,"S")), 
        ylab = expression(paste(Delta,"C")), cex.main = 1.3)
  if (class (attributes(items)$colour) != "character"){
    subtitleContent <- "for C of individual school classes"
  } else {
    subtitleContent <- paste("for C of", attr(items, 'concept'), "of individual school classes")
  }
  if (combinedPlot == TRUE) {
    mtext(subtitleContent, side=3, line=0.4, cex=0.5)
  } else {
    mtext(subtitleContent, side=3, line=0.4, cex=0.8)
  }  
  grid(col = "darkgrey")
  abline(h = 0.0, col = "black", lwd = 1)
  abline(v = 0.0, col = "black", lwd = 1)
  
  
  
  # plotting DataPoints of single questions
  for(i in 1:nrow(TraditionalMeans)) {
    points(TraditionalMeans[i,1], TraditionalMeans[i,2], 
           pch = 21, cex = 1.5, col = "Blue" )
  }
  for(i in 1:nrow(FrequTestingMeans)) {
    points(FrequTestingMeans[i,1], FrequTestingMeans[i,2], 
           pch = 22, cex = 1.5, col = "Red" )
  }
  for(i in 1:nrow(FAMeans)) {
    points(FAMeans[i,1], FAMeans[i,2], 
           pch = 23, cex = 1.5, col = "Forestgreen" )
  }
  legend("topleft", legend=c(
    "Traditional", 
    "Frequent Testing", 
    "Formative Assessment"),
    y.intersp = 1.2, pch= c(21,22,23), bty = "o",
    col = c("blue", "red", "forestgreen"), cex=1)
  
  if (combinedPlot == FALSE){
    mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
    subDir1 <- "figuresCompareTeacherDevel"
    if (file.exists(subDir1)){
      setwd(file.path(mainDir, subDir1))
      } else {
        dir.create(file.path(mainDir, subDir1))
        setwd(file.path(mainDir, subDir1))
        }
    mainDir <- getwd()
    if (class (attributes(items)$colour) != "character"){
      subDir2 <- "Items"
      if (length(items) != 49)
        {
        for (i in 1:length(items))
          {
          subDir2 <- paste(subDir2, items[i], sep= "_")
          }
        } else {
          subDir2 <- paste(subDir2, "All", sep= "")
          }
      if (file.exists(subDir2)){
        setwd(file.path(mainDir, subDir2))
        } else {
          dir.create(file.path(mainDir, subDir2))
          setwd(file.path(mainDir, subDir2))
          }
      pdfName <- "ScoreConcentrAverageDevel.pdf"
      } else{
      subDir2 <- "Concepts"
      #subDir2 <- paste(subDir2, attr(items, 'c'), sep= "_")
      if (file.exists(subDir2)){
        setwd(file.path(mainDir, subDir2))
        } else {
        dir.create(file.path(mainDir, subDir2))
        setwd(file.path(mainDir, subDir2))
        }
      pdfName <- paste("Concept", attr(items, 'c'), "ScoreConcentrAverageDevel.pdf", sep="")
      }
    dev.copy2pdf(file = pdfName)
    cat("pdfName: \t \t",pdfName, "\n")
    cat("saved in directory: \t",getwd(), "\n")
    setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  }
}






