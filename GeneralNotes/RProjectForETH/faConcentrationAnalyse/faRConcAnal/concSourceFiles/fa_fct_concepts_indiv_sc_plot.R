##########
# compare concepts for individual teachers/ individual teaching types
##########

# function conceptsSCPlot
# function AllConceptsSCPlot

# function conceptsSCPlotForCombinedPlot
# function AllConceptsSCPlotForCombinedPlot

source("fa_header_add_legend.R")
source("fa_header_add_concept_sc_plot.R")

conceptsSCPlot <- function(teacher, itemsCNo1, itemsCNo2 = FALSE, itemsCNo3 = FALSE,
                           itemsCNo4 = FALSE, itemsCNo5 = FALSE, itemsCNo6 = FALSE,
                           itemsCNo7 = FALSE, gender = "", smallDataPoints = FALSE, 
                           Arrows = FALSE, multiPlot = FALSE) {                
  if (multiPlot == FALSE){
    devTest <- 1; names(devTest) <- "null device"
    if (dev.cur() != devTest) {
      dev.off()
    }
  }

  ConcPlotData <- SCPlot(teacher, itemsCNo1, gender, smallDataPoints, Arrows, combinedPlot = FALSE, multiPlot = TRUE)
  if (itemsCNo2[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo2, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  if (itemsCNo3[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo3, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  if (itemsCNo4[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo4, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  if (itemsCNo5[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo5, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  if (itemsCNo6[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo6, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  if (itemsCNo7[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo7, gender, smallDataPoints, Arrows, combinedPlot = FALSE)
  }
  dirNames <- addLegend(itemsCNo1, itemsCNo2, itemsCNo3, itemsCNo4, itemsCNo5, itemsCNo6, itemsCNo7)
  
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- "figuresSinglePlots"
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  mainDir <- getwd()
  subDir2 <- "Concepts"
  if (length(dirNames) != 7)
  {
    for (i in 1:length(dirNames))
    {
      subDir2 <- paste(subDir2, dirNames[i], sep= "_")
    }
  } else {
    subDir2 <- paste(subDir2, "All", sep= "")
  }
  
  if (file.exists(subDir2)){
    setwd(file.path(mainDir, subDir2))
  } else {
    dir.create(file.path(mainDir, subDir2))
    setwd(file.path(mainDir, subDir2))
  }
  if (gender == "male") {
    pdfName <- paste("S-C-Male-ByPlotConceptsOf", teacher@teacher,".pdf",sep="")
  }
  if (gender == "female") {
    pdfName <- paste("S-C-Female-ByPlotConceptsOf", teacher@teacher,".pdf",sep="")
  }
  pdfName <- paste("S-C-PlotByConceptsOf", teacher@teacher,".pdf",sep="")
  dev.copy2pdf(file = pdfName)
  cat("pdfName: \t \t",pdfName, "\n")
  cat("saved in directory: \t",getwd(), "\n")
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
}

allConceptsSCPlot <- function(teacher, gender = "", smallDataPoints = FALSE, 
                                               Arrows = FALSE) {
  conceptsSCPlot(teacher, itemsC1, itemsC2, itemsC3, itemsC4, itemsC5, itemsC6,
                 itemsC7, gender, smallDataPoints, Arrows)
}




conceptsSCPlotForCombinedPlot <- function(teacher, itemsCNo1, itemsCNo2 = FALSE, itemsCNo3 = FALSE,
                           itemsCNo4 = FALSE, itemsCNo5 = FALSE, itemsCNo6 = FALSE,
                           itemsCNo7 = FALSE, gender = "", smallDataPoints = FALSE, 
                           Arrows = FALSE) {                
  
  ConcPlotData <- SCPlot(teacher, itemsCNo1, gender, smallDataPoints, Arrows, combinedPlot = TRUE, multiPlot = TRUE)
  if (itemsCNo2[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo2, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  if (itemsCNo3[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo3, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  if (itemsCNo4[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo4, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  if (itemsCNo5[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo5, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  if (itemsCNo6[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo6, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  if (itemsCNo7[1] != FALSE) {
    ConcPlotData <- AddConceptSCPlot(teacher, itemsCNo7, gender, smallDataPoints, Arrows, combinedPlot = TRUE)
  }
  dirNames <- addLegend(itemsCNo1, itemsCNo2, itemsCNo3, itemsCNo4, itemsCNo5, itemsCNo6, itemsCNo7)
  
  return(dirNames)
}

allConceptsSCPlotForCombinedPlot <- function(teacher, gender = "", smallDataPoints = FALSE, 
                              Arrows = FALSE) {
  conceptsSCPlotForCombinedPlot(teacher, itemsC1, itemsC2, itemsC3, itemsC4, itemsC5, itemsC6,
                 itemsC7, gender, smallDataPoints, Arrows)
}
