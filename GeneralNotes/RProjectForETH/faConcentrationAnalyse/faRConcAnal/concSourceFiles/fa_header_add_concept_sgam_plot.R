##########
# add a new concept to an already created S-Gam-plot
##########

setGeneric(name="AddConceptSGamPlot",
           def=function(theObject, items, gender = "", smallDataPoints = TRUE, 
                        Arrows = TRUE, calcMean = FALSE, combinedPlot = FALSE)
           {
             standardGeneric("AddConceptSGamPlot")
           }
)

setMethod(f="AddConceptSGamPlot",
          signature="faData",
          definition=function(theObject, items, gender, smallDataPoints, Arrows, calcMean, combinedPlot)
          {
            ######            
            # set the output parameters
            # consider only the data of chosen items
            
            # validity test gender
            ######
            
            #if (gender != "male" && gender != "female" && gender != "") 
            #  {
            #  print("the optional input gender takes male or female")
            #  return()
            #  }
            
            theObject@items <- items
            
            
            if (gender == "male" || gender == "female") {
              preMale     <- data.frame()
              preFemale   <- data.frame()
              postMale    <- data.frame()
              postFemale  <- data.frame()
              
              for (i in 1:length(theObject@gender)) 
              {
                if (theObject@gender[[i]] == 1){
                  preMale  <- rbind(preMale,theObject@pre[i,])
                  postMale  <- rbind(postMale,theObject@post[i,])
                } else {
                  preFemale  <- rbind(preFemale,theObject@pre[i,])
                  postFemale  <- rbind(postFemale,theObject@post[i,])
                }
              }
            }
            
            if (gender == "male") {
              theObject@pre <- preMale
              theObject@post <- postMale
            }
            if (gender == "female") {
              theObject@pre <- preFemale
              theObject@post <- postFemale
            }
            
            preInt <- data.frame(matrix(NA, nrow= nrow(theObject@pre), ncol=length(theObject@items)))
            for (i in 1:length(theObject@items)) {
              preInt[,i] <- theObject@pre[,theObject@items[i]]
              colnames(preInt)[i] <- paste("X.",theObject@items[i],"Pre",sep="")
            }
            theObject@pre <- preInt
            
            postInt <- data.frame(matrix(NA, nrow= nrow(theObject@post), ncol=length(theObject@items)))
            for (i in 1:length(theObject@items)) {
              postInt[,i] <- theObject@post[,theObject@items[i]]
              colnames(postInt)[i] <- paste("X.",theObject@items[i],"Post",sep="")
            }
            theObject@post <- postInt
            
            sol <- data.frame(matrix(NA, nrow=1, ncol=length(theObject@items)))
            for (i in 1:length(theObject@items)) {
              sol[1,i] <- theObject@solutions[theObject@items[i]]
              colnames(sol)[i] <- paste("solution",theObject@items[i],sep="")
            }
            theObject@solutions <- sol
            
            num <- data.frame(matrix(NA, nrow=1, ncol=length(theObject@items)))
            for (i in 1:length(theObject@items)) {
              num[1,i] <- theObject@numberOfChoices[theObject@items[i]]
              colnames(num)[i] <- paste("numOfChoices",theObject@items[i],sep="")
            }
            theObject@numberOfChoices <- num
            
            ######
            # generate data points:
            ######
            
            m <- vector(mode="numeric", length=length(theObject@items))
            prefactor <- vector(mode="numeric", length=length(theObject@items))
            for(i in 1:length(theObject@items)) {
              m[i] <- theObject@numberOfChoices[1,i]
              prefactor[i] <- m[i]^0.5/(m[i]^0.5-1)
            }
            
            # score
            preScore <- vector(mode="numeric", length=0)
            postScore <- vector(mode="numeric", length=0)
            DataPointsPre <- matrix(nrow = length(theObject@items), ncol = 2)
            DataPointsPost <- matrix(nrow = length(theObject@items), ncol = 2)
            
            for(i in 1:length(theObject@solutions)) {
              DataPointsPre[i,1] <- sum(theObject@pre[,i] == 
                                          theObject@solutions[1,i])/nrow(theObject@pre)
              DataPointsPost[i,1] <- sum(theObject@post[,i] == 
                                           theObject@solutions[1,i])/nrow(theObject@post)
            }
            meanScorePre <-mean(DataPointsPre[,1])
            meanScorePost <-mean(DataPointsPost[,1])
            
            ######            
            # gamma
            ######
            
            # gamma of PRE
            prePortionSquared <- vector(mode="numeric", length = length(theObject@items))
            for(i in 1:length(theObject@items)) {
              prePortionCounter <- vector(mode="numeric", 
                                          length = theObject@numberOfChoices[1,i])
              for (j in 1:theObject@numberOfChoices[1,i]){
                prePortionCounter[j] <- sum(theObject@pre[,i] == j)
              }
              prePortionSquared[i] <- (sum(prePortionCounter^2) 
                                       - (nrow(theObject@pre)*DataPointsPre[i,1])^2)^(1/2)
              if (DataPointsPre[i,1] != 1) {
                DataPointsPre[i,2] <- prefactor[i]*(prePortionSquared[i]
                                                    /(nrow(theObject@pre)*(1-DataPointsPre[i,1])) 
                                                    - (m[i]-1)^(-1/2))
              } else {DataPointsPre[i,2] <- 0}
            }
            
            # gamma of POST
            postPortionSquared <- vector(mode="numeric", length = length(theObject@items))
            for(i in 1:length(theObject@items)) {
              postPortionCounter <- vector(mode="numeric", length = theObject@numberOfChoices[1,i])
              for (j in 1:theObject@numberOfChoices[1,i]){
                postPortionCounter[j] <- sum(theObject@post[,i] == j)
              }
              postPortionSquared[i] <- (sum(postPortionCounter^2) 
                                        - (nrow(theObject@post)*DataPointsPost[i,1])^2)^(1/2)
              if (DataPointsPost[i,1] != 1) {
                DataPointsPost[i,2] <- prefactor[i]*(postPortionSquared[i]
                                                     /(nrow(theObject@post)*(1-DataPointsPost[i,1])) 
                                                     - (m[i]-1)^(-1/2))
              } else {DataPointsPost[i,2] <- 0}
            }
            meanGammaPre <-mean(DataPointsPre[,2])
            meanGammaPost <-mean(DataPointsPost[,2])
            theObject@preMean <- c(meanScorePre,meanGammaPre)
            theObject@postMean <- c(meanScorePost,meanGammaPost)
            
            ######            
            # Plotting
            ######
            
            if (calcMean == FALSE) {
              
              if (theObject@teacher != "Traditional" && 
                  theObject@teacher != "FrequentTesting" &&
                  theObject@teacher != "FormativeAssessment")
              {
                titleContent <- paste("teacher: ", theObject@teacher, " (", nrow(theObject@pre), " ",
                                      gender, " stud.), type: ", theObject@form, sep = "")
              } 
              if (gender == "") {
                titleContent <- paste(theObject@teacher, " (", nrow(theObject@pre), " stud.)", sep = "")
              }
              else {
                titleContent <- paste(theObject@teacher, " (", nrow(theObject@pre), " ", 
                                      gender, " stud.)", sep = "")
              }
              title(main = titleContent, col.main = "black", xlab = "S", 
                    ylab = expression(paste(Gamma)), cex.main = 1.1)
              if (theObject@teacher != "Traditional" && theObject@teacher != "FrequentTesting"
                  && theObject@teacher != "FormativeAssessment") {
                if (theObject@form == 1) {
                  subtitleContent <- paste("teaching type: Traditional")
                }
                if (theObject@form == 2) {
                  subtitleContent <- paste("teaching type: Frequent testing")
                }
                if (theObject@form == 3) {
                  subtitleContent <- paste("teaching type: Formative Assessment")
                }
                if (combinedPlot == TRUE) {
                  mtext(subtitleContent, side=3, line=0.4, cex=0.5)
                } else {
                  mtext(subtitleContent, side=3, line=0.4, cex=0.8)
                }
              }
              
              for(i in 1:length(theObject@items)) {
                if (smallDataPoints == TRUE){
                  points(DataPointsPre[i,1], DataPointsPre[i,2], 
                         pch = 18, cex = 0.8, col = attr(theObject@items,"colour"))
                  points(DataPointsPost[i,1], DataPointsPost[i,2], 
                         pch = 22, cex = 0.8, col = attr(theObject@items,"colour"))
                }
                
                # plotting DataPoints of the mean value
                if (
                  (DataPointsPre[i,1] != DataPointsPost[i,1]) && 
                  (DataPointsPre[i,2] != DataPointsPost[i,2])) {
                  if(Arrows != FALSE) {
                    arrows(DataPointsPre[i,1], DataPointsPre[i,2], 
                           DataPointsPost[i,1], DataPointsPost[i,2],
                           length = 0.05, angle = 30, code = 2, col = attr(theObject@items,"colour"))
                  }
                }
              }
              points(meanScorePre, meanGammaPre, 
                     pch = 18, cex = 1.4, col = attr(theObject@items,"colour") )
              points(meanScorePost, meanGammaPost, 
                     pch = 25, cex = 1.2, col = attr(theObject@items,"colour") )
              if (
                (meanScorePre[1] != meanScorePost[1]) && 
                (meanGammaPre[1] != meanGammaPost[1])) {
                arrows(meanScorePre, meanGammaPre, meanScorePost, meanGammaPost, 
                       length = 0.1, angle = 30,
                       col = attr(theObject@items,"colour"),
                       #arr.type = "triangle",
                       code = 2, lwd = 1.5)
              }
            }
            validObject(theObject)
            return(theObject)
            #return()
          }
)