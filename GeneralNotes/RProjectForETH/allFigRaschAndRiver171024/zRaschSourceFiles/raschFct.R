# mainDirectory <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

convertDataToBinary <- function(rawData){
  # convert 49 or 2x49 items to binary
  solutions <- data.frame(sol = t(c(5,3,5,2,4,3,3,3,2,2,
                                    5,4,3,5,5,2,2,3,3,4,
                                    4,3,1,4,3,1,4,3,2,1,
                                    4,2,2,4,5,3,4,5,2,5,
                                    4,2,5,5,3,5,4,4,2)))
   # if we compare PRE and POST results
  if (nrow(rawData) > 49) {solutions <- cbind(solutions,solutions)}
  for (i in 1:length(rawData)){
    for (j in 1:nrow(rawData)){
      rawData[j,i] <- as.integer(rawData[j,i] == solutions[i])
    }
    colnames(rawData)[i] <- paste("",i,sep="")
  }
  return(rawData)
}

getRMItemStatTab <- function(Data, items, name =""){
  # get stat. info out of binary data
  # sheet 1: items	itemLogit	raschSE	infitMSQ	outfitMSQ
  # sheet 2: num of items being solved by each student (of the item sample)
  # sheet 3: total student score per item
  # sheet 4: mean person Logit (with/without extremes)
  
  library(eRm);
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("xlsTabItemStatTab")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  
  binary <- convertDataToBinary(Data$postIt)
  
  # absolute number of students which solved a certain item of student subsample
  totalStudentScore <- matrix(colSums(binary[items]))
  colnames(totalStudentScore) <- paste("totalStudentScore of ",nrow(Data$postIt)," stud",sep="")
  rownames(totalStudentScore) <- paste("item",items)

  # gives absolute number of correctly solved items of subsample
  numbOfCorrect <- rowSums(binary[items])
  corrPerItem <- matrix(nrow = (length(items)+1),ncol = 1)
  
  colnames(corrPerItem) <- paste("numOfCorrectlySolvedItems")
  rownames(corrPerItem) <- paste(0:length(items),"items solved")

  for (i in 0:(length(items))){
    corrPerItem[i+1,1] <- sum(numbOfCorrect==i)
  }

  raschData <- RM(binary[items], sum0 = TRUE) 
  # raschData includes a lot of info (std. error,aso)
  personRaschData <- person.parameter(raschData)    # person parameters
  personLogits <- data.frame(personRaschData[11])   # person abilities
  colnames(personLogits) <- paste("personLogit")
  
  sumOfFullOne  <- corrPerItem[length(corrPerItem)]*log((length(items)-0.5)/0.5)
  sumOfFullZero <- corrPerItem[length(1)]*log(0.5/(length(items)-0.5))
  newMean <- (sum(personLogits) + sumOfFullZero + sumOfFullOne)/(sum(corrPerItem))

  meanPersonLogit <- matrix(nrow = 2, ncol = 1)
  meanPersonLogit[1,1] <- mean(personLogits$personLogit)
  meanPersonLogit[2,1] <- newMean
  rownames(meanPersonLogit) <- c("meanPersonLogitWithoutExtremes","meanPersonLogitWithExtremes")
  
  itemLogits <- -data.frame(raschData[11])          # item difficulty = minus item easiness
  colnames(itemLogits) <- paste("itemLogit")
  itemFitData   <- itemfit(personRaschData)         # includes all the itemfit-data
  stdErr        <- data.frame(raschData$se.beta)

  outfitMSQ     <- data.frame(itemFitData$i.outfitMSQ)
  infitMSQ      <- data.frame(itemFitData$i.infitMSQ)
  totalScore    <- data.frame(colSums(binary))

  colnames(stdErr) <- paste("raschSE")
  colnames(outfitMSQ) <- paste("outfitMSQ")
  colnames(infitMSQ) <- paste("infitMSQ")

  dataForReturn <- data.frame(items = items,
                              itemLogits = itemLogits,
                              stdErr = stdErr,
                              infitMSQ = infitMSQ,
                              outfitMSQ = outfitMSQ)
  
  filenameStatInfo      <- paste("itemStatTab",name,".xlsx", sep = "")
  write.xlsx(dataForReturn, filenameStatInfo, sheetName="statisticTable")
  write.xlsx(corrPerItem, filenameStatInfo, sheetName="numSolvedItems",append=TRUE)
  write.xlsx(totalStudentScore, filenameStatInfo, sheetName="totalStudentScore",append=TRUE)
  write.xlsx(meanPersonLogit, filenameStatInfo, sheetName="meanPersonLogit",append=TRUE)

  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  return()
} 

getRMPersonStatTab <- function(Data, DataAll, items, name=""){
  # get stat. info out of binary data
  # sheet 1: lp.grp	lp.code sus.code personLogit raschSE infitMSQ outfitMSQ
  # sheet 2: total student score of the 583 students
  # sheet 3: meanPersonLogitWithoutExtremes
  # sheet 3: meanPersonLogitWithExtremes
  
  library(eRm);
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("xlsTabPersonStatTab")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  
  binary <- convertDataToBinary(Data$postIt)
  raschData <- RM(binary[items], sum0 = TRUE)     # the sum of the item logits is norm. to zero
  personRaschData <- person.parameter(raschData)  # person parameters
  
  # absolute number of students which solved a certain item of student subsample
  totalStudentScore <- matrix(colSums(binary[items]))
  colnames(totalStudentScore) <- paste("totalStudentScore of ",nrow(Data$postIt)," stud",sep="")
  rownames(totalStudentScore) <- paste("item",items)
  
  personFitData <- personfit(personRaschData)
  stdErr        <- data.frame(personRaschData$se.theta)
  outfitMSQ     <- data.frame(personFitData$p.outfitMSQ)
  infitMSQ      <- data.frame(personFitData$p.infitMSQ)
  
  colnames(stdErr)    <- paste("raschSE")
  colnames(outfitMSQ) <- paste("outfitMSQ")
  colnames(infitMSQ)  <- paste("infitMSQ")
  
  personLogits <- data.frame(personRaschData[11])   # person abilities
  colnames(personLogits) <- paste("personLogit")
  
  # gives absolute number of correctly solved items of subsample
  numbOfCorrect <- rowSums(binary[items])
  corrPerItem <- matrix(nrow = (length(items)+1),ncol = 1)
  
  colnames(corrPerItem) <- paste("numOfCorrectlySolvedItems")
  rownames(corrPerItem) <- paste(0:length(items),"items solved")
  
  for (i in 0:(length(items))){
    corrPerItem[i+1,1] <- sum(numbOfCorrect==i)
  }
  
  sumOfFullOne  <- corrPerItem[length(corrPerItem)]*log((length(items)-0.5)/0.5)
  sumOfFullZero <- corrPerItem[length(1)]*log(0.5/(length(items)-0.5))
  newMean <- (sum(personLogits) + sumOfFullZero + sumOfFullOne)/(sum(corrPerItem))
  
  meanPersonLogit <- matrix(nrow = 2, ncol = 1)
  meanPersonLogit[1,1] <- mean(personLogits$personLogit)
  meanPersonLogit[2,1] <- newMean
  rownames(meanPersonLogit) <- c("meanPersonLogitWithoutExtremes","meanPersonLogitWithExtremes")
  
  addDataIni <- matrix(nrow = 604,ncol = 3)

  for (i in as.numeric(substr(rownames(personLogits), 5, 7))){
    addDataIni[i,1] <- DataAll$LP.Grp[i]
    addDataIni[i,2] <- DataAll$LP.Code[i]
    addDataIni[i,3] <- DataAll$SuS.Code[i]
  }
  
  addDataIni   <- addDataIni[complete.cases(addDataIni),] 
  dataForReturn <- data.frame(lp.grp = addDataIni[,1],
                              lp.code = addDataIni[,2],
                              sus.code = addDataIni[,3],
                              personLogits = personLogits,
                              stdErr = stdErr,
                              infitMSQ = infitMSQ,
                              outfitMSQ = outfitMSQ)
  
  filenameStatInfo <- paste("personStatTab", name, ".xlsx", sep = "")
  
  write.xlsx(dataForReturn, filenameStatInfo, sheetName="statisticTable")
  write.xlsx(totalStudentScore, filenameStatInfo, sheetName="totalStudentScore",append=TRUE)
  write.xlsx(meanPersonLogit, filenameStatInfo, sheetName="meanPersonLogit",append=TRUE)
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
} 

# get LR-Plot and Wald tables (for mean, cogAbTest and gender)
graphModelTest  <- function(itemsForAnalysis = items, rawData = rawData,
                                    spCrt = spCrt, conf= list(gamma = 0.95),  title = ""){
  library(eRm); library(xlsx)  
  conf = conf
  
  binaryDataForAnalysis       <- convertDataToBinary(rawData)[itemsForAnalysis]
  
  # calc mean by hand
  studScore <- (matrix(rowMeans(binaryDataForAnalysis))) # student ability
  meanStudScore <- mean(studScore)
  studScore[studScore >= meanStudScore] <- 2
  studScore[studScore < meanStudScore] <- 1
  
  rmDataOfAnalysis                                  <- RM(binaryDataForAnalysis)
  
  andersonLRTestMean                                <- LRtest(rmDataOfAnalysis, splitcr = studScore[,1])
  print(summary(andersonLRTestMean))
  names(andersonLRTestMean[8]$betalist$`1`)         <- paste("Q", itemsForAnalysis, sep = "")
  andersonLRTestcogAbMean                           <- LRtest(rmDataOfAnalysis, splitcr = spCrt$cgAbMean[,1])
  print(summary(andersonLRTestcogAbMean))
  names(andersonLRTestcogAbMean[8]$betalist$`1`)    <- paste("Q", itemsForAnalysis, sep = "") 
  andersonLRTestGender                              <- LRtest(rmDataOfAnalysis, splitcr = spCrt$spGender[,1])
  print(summary(andersonLRTestGender))
  names(andersonLRTestGender[8]$betalist$`1`)       <- paste("Q", itemsForAnalysis, sep = "")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("figGraphModelTest")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  
  titleMean     <- (paste("SplitCrit Mean of chosen items of ", title,  " with p-value: ", round(andersonLRTestMean$pvalue,3), sep=""))
  titleCogAb    <- (paste("SplitCrit CogAb-Test-Score of ", title,  " with p-value: ", round(andersonLRTestcogAbMean$pvalue,3), sep=""))
  titleGender   <- (paste("SplitCrit Gender of ", title,  " with p-value: ", round(andersonLRTestGender$pvalue,3), sep=""))
  
  #par(mfrow = c(1,3), cex = .5, oma=c(0,0,4,0))
  plotGOF(andersonLRTestMean, conf = conf, xlim = c(-3, 3), ylim = c(-3, 3), 
          xlab = "score of chosen items < Mean", ylab = "score of chosen items > Mean", main = titleMean)
  pdfName <- paste("graphModSplitMean", title, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  plotGOF(andersonLRTestcogAbMean, conf = conf, xlim = c(-3, 3), ylim = c(-3, 3), 
          xlab = "cog. Ab. Score < Mean", ylab = "cog. Ab. Score > Mean", main = titleCogAb)
  pdfName <- paste("graphModSplitCogAb", title, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  plotGOF(andersonLRTestGender, conf = conf, xlim = c(-3, 3), ylim = c(-3, 3), 
          xlab = "females", ylab = "males", main = titleGender)
  pdfName <- paste("graphModSplitGender", title, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)

  # wald tests
  waldTestMean        <- Waldtest(rmDataOfAnalysis, splitcr = studScore[,1])
  waldTestCogAbMean   <- Waldtest(rmDataOfAnalysis, splitcr = spCrt$cgAbMean[,1])
  waldTestGender      <- Waldtest(rmDataOfAnalysis, splitcr = spCrt$spGender[,1])

  waldTests <- list(mean      = waldTestMean[1],
                    cogabmean = waldTestCogAbMean[1],
                    gender    = waldTestGender[1])
  
  filenameWald <- paste("WaldTestOf", title, ".xlsx", sep = "")
  write.xlsx(waldTests, filenameWald)

  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}

graphModelTestTypes  <- function(itemsForAnalysis = items, rawData = rawData, grp = grp, name = name,
                                 title= "", type1 = "", type2 = "", conf= list(gamma = 0.95)){
  library(eRm); library(xlsx)  
  
  conf = conf
  
  binaryDataForAnalysis       <- convertDataToBinary(rawData)[itemsForAnalysis]
  
  rmDataOfAnalysis                                  <- RM(binaryDataForAnalysis)
  andersonLRTestMean                                <- LRtest(rmDataOfAnalysis, splitcr = grp)
  print(summary(andersonLRTestMean))
  names(andersonLRTestMean[8]$betalist$`1`)         <- paste("Q", itemsForAnalysis, sep = "")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("figGraphModelTestByTypes")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  
  title     <- (paste(name, " for ", type1, " & ", type2,  " with p-value: ", 
                      round(andersonLRTestMean$pvalue,3), sep=""))
  
  #par(mfrow = c(1,3), cex = .5, oma=c(0,0,4,0))
  plotGOF(andersonLRTestMean, conf = conf, xlim = c(-3, 3), ylim = c(-3, 3), 
          xlab = paste("score of chosen items for ", type1, sep = ""), 
          ylab = paste("score of chosen items for ", type2, sep = ""), 
          main = title)
  name <- name
  pdfName <- paste("figGraphModelTestByType", type1, type2, name, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}

graphModelTestCompPrePost  <- function(itemsForAnalysis = items, rawData = rawData,
                                 spCrt = spCrt, conf= list(gamma = 0.95),  title = ""){
  library(eRm); library(xlsx)  
  
  conf = conf
  
  binaryDataForAnalysis                   <- convertDataToBinary(rawData)[itemsForAnalysis]
  rmDataOfAnalysis                        <- RM(binaryDataForAnalysis)
  andersonLRTest                          <- LRtest(rmDataOfAnalysis, splitcr = spCrt)
  print(summary(andersonLRTest))
  names(andersonLRTest[8]$betalist$`1`)   <- paste("Q", itemsForAnalysis, sep = "") 
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("figGraphModelTestPrePost")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  
  titleGraph    <- (paste("SplitCrit Pre/Post ", title,  " with p-value: ", round(andersonLRTest$pvalue,3), sep=""))
  plotGOF(andersonLRTest, conf = conf, xlim = c(-3, 3), ylim = c(-3, 3), 
          xlab = "PRE test of chosen items", ylab = "POST test of chosen items", main = titleGraph)
  pdfName <- paste("figGraphModelTestCompPrePost", title, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  # wald tests
  waldTest   <- Waldtest(rmDataOfAnalysis, splitcr = spCrt[,1])
  waldTests <- list(prepost      = waldTest[1])
  filenameWald <- paste("WaldTestCompPrePost", title, ".xlsx", sep = "")
  write.xlsx(waldTests, filenameWald)
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}

myWrightmap <- function(rawdata, items, labOptions = c("text",0.8), title ="", name = "", dimn = "",
                                         labpos = ""){

    # pos:
  #Values of 1, 2, 3 and 4, respectively indicate positions below, 
  #to the left of, above and to the right of the specified coordinates.
  
  if (labOptions[1] == "num"){itemlogs <- data.frame(names = 1:49)}
  if (labOptions[1] == "text"){
    itemlogs <- data.frame(names = c("item 1 (C1)",  "item 2 (C5)",  "item 3 (C1)",  "item 4 (C3)", 
                                     "item 5 (C2)",  "item 6 (C5)",  "item 7",       "item 8 (C3)", 
                                     "item 9 (C6)",  "item 10 (C1)", "item 11 (C1)", "item 12 (C2)", 
                                     "item 13 (C3)", "item 14 (C5)", "item 15 (C5)", "item 16 (C1)", 
                                     "item 17 (C2)", "item 18 (C3)", "item 19",      "item 20 (C1)", 
                                     "item 21 (C2)", "item 22 (C7)", "item 23",      "item 24 (C6)", 
                                     "item 25 (C1)", "item 26 (C1)", "item 27 (C1)", "item 28 (C5)", 
                                     "item 29 (C5)", "item 30 (C4)", "item 31 (C2)", "item 32 (C5)", 
                                     "item 33 (C1)", "item 34 (C6)", "item 35 (C4)", "item 36 (C6)", 
                                     "item 37",      "item 38 (C5)", "item 39 (C5)", "item 40 (C7)", 
                                     "item 41 (C2)", "item 42 (C1)", "item 43 (C2)", "item 44 (C7)", 
                                     "item 45 (C3)", "item 46 (C7)", "item 47 (C5)", "item 48 (C6)", 
                                     "item 49 (C4)"))
  }
  
  itemlogs[,"con"] <- data.frame(con = c(1,5,1,3,2,5,0,3,6,1,
                                         1,2,3,5,5,1,2,3,0,1,
                                         2,7,0,6,1,1,1,5,5,4,
                                         2,5,1,6,4,6,0,5,5,7,
                                         2,1,2,7,3,7,5,6,4))
  
  itemlogs[,"pos"] <- data.frame(pos = c(1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1))
  
  itemlogs[,"numb"] <- 1:49
  itemlogs <- itemlogs[items,]
  
  binary            <- convertDataToBinary(rawdata$postIt)
  raschData         <- RM(binary[items], sum0 = TRUE) 
  personRaschData   <- person.parameter(raschData)   
  personLogits      <- personRaschData[11]   
  personLogits      <- personLogits$thetapar$NAgroup1
  itemLogits        <- -data.frame(raschData[11])
  itemlogs[,"itemlog"] <- itemLogits$betapar
  itemlogs          <- itemlogs[order(itemlogs$itemlog, decreasing=FALSE), ]
  itemlogs          <- itemlogs[order(itemlogs$con, decreasing=FALSE), ]
  if (labpos[1] != ""){itemlogs$pos <- labpos}
  #print(itemlogs)
  itSd <- sd(itemLogits$betapar)
  
  statDatItem           <- matrix(itSd,ncol=1)
  colnames(statDatItem) <- c("ItemSd")
  
  ########
  ######## artificially add extreme students
  ########
  # gives absolute number of correctly solved items of subsample
  
  correctPerStud    <- rowSums(binary[,items])
  corrPerItem       <- matrix(nrow = (length(items)+1),ncol = 1)
 
  for (i in 0:(length(items))){
    corrPerItem[i+1,1]    <- sum(correctPerStud==i)
  }
  
  zeroScore    <- corrPerItem[1];  fullScore   <- corrPerItem[length(corrPerItem)];

  addToHist   <- matrix(nrow = (zeroScore+fullScore),ncol = 1)
  zeroLogit   <- log(0.5/(length(items)-0.5))
  allLogit    <- -1*zeroLogit
  
  if ((corrPerItem[1]+corrPerItem[length(corrPerItem)]) != 0){
    addLogits  <- c(rep(zeroLogit,zeroScore),rep(allLogit,fullScore))
    for (i in 1:length(addLogits)){addToHist[i,1]   <- addLogits[i]}
  }

  print(paste("number of extreme students:", nrow(addToHist))) 
  if (nrow(addToHist) != 0) {print(paste("extreme logit:", addToHist[1,1]))}
  
  personLogits  <- matrix(personLogits, ncol = 1)
  personLogits  <- rbind(personLogits,addToHist)
  pMn           <- mean(personLogits)
  pSd           <- sd(personLogits)
  itSd          <- sd(itemLogits$betapar)
  
  statDatStud             <- matrix(c(pMn,pSd),nrow = 2)
  rownames(statDatStud)   <- c("mean","sd")
  
  wrightMap(personLogits, itemlogs$itemlog, 
            axis.persons = "Student Parameter Distribution",
            axis.cex = 1.5,
            # person.side
            person.side = personDens, 
            dim.names = dimn,
            person.points = c(pMn,pMn+pSd,pMn-pSd),
            p.point.col = c("black"),
            p.point.cex = 3,
            person.range = c(pMn-pSd,pMn+pSd),
            #item.side
            item.prop = 0.5, 
            item.side = itemModern, axis.items = "",
            cutpoints = c(-itSd,itSd),
            thr.lab.text = itemlogs$names, thr.lab.cex = as.numeric(labOptions[2]),
            thr.lab.pos = itemlogs$pos, #thr.lab.pos = 1, 
            min.l = -3, max.l = 3,
            label.items.ticks = FALSE, label.items = "",
            ##item.prop = 0.6,##,
            main.title = paste("                                              ", 
                               title),
            show.axis.logits = "R")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("fig1dWrightMaps")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  pdfName <- paste("1dWrightMap", name, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  filename <- paste("1dMeanAndSd", name, ".xlsx", sep = "")
  write.xlsx(statDatStud, filename)
  write.xlsx(statDatItem, filename, sheetName="statDatItem",append=TRUE)
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}

my2dWrightmap <- function(rawdata, items, labOptions = c("text",0.8), title = "", sub = "", name = "", dimn = TRUE,
                          labpos = ""){

    if (dimn == TRUE){
    dimn <- c(paste(sub," PRETEST (red)",sep = ""), paste(sub," POSTTEST (green)",sep = ""))  
  } else {dimn <- " "}
  # pos:
  #Values of 1, 2, 3 and 4, respectively indicate positions below, 
  #to the left of, above and to the right of the specified coordinates.
  
  if (labOptions[1] == "num"){itemlogs <- data.frame(names = 1:49)}
  if (labOptions[1] == "text"){
    itemlogs <- data.frame(names = c("item 1 (C1)",  "item 2 (C5)",  "item 3 (C1)",  "item 4 (C3)", 
                                   "item 5 (C2)",  "item 6 (C5)",  "item 7",       "item 8 (C3)", 
                                   "item 9 (C6)",  "item 10 (C1)", "item 11 (C1)", "item 12 (C2)", 
                                   "item 13 (C3)", "item 14 (C5)", "item 15 (C5)", "item 16 (C1)", 
                                   "item 17 (C2)", "item 18 (C3)", "item 19",      "item 20 (C1)", 
                                   "item 21 (C2)", "item 22 (C7)", "item 23",      "item 24 (C6)", 
                                   "item 25 (C1)", "item 26 (C1)", "item 27 (C1)", "item 28 (C5)", 
                                   "item 29 (C5)", "item 30 (C4)", "item 31 (C2)", "item 32 (C5)", 
                                   "item 33 (C1)", "item 34 (C6)", "item 35 (C4)", "item 36 (C6)", 
                                   "item 37",      "item 38 (C5)", "item 39 (C5)", "item 40 (C7)", 
                                   "item 41 (C2)", "item 42 (C1)", "item 43 (C2)", "item 44 (C7)", 
                                   "item 45 (C3)", "item 46 (C7)", "item 47 (C5)", "item 48 (C6)", 
                                   "item 49 (C4)"))
  }
  
  itemlogs[,"con"] <- data.frame(con = c(1,5,1,3,2,5,0,3,6,1,
                                         1,2,3,5,5,1,2,3,0,1,
                                         2,7,0,6,1,1,1,5,5,4,
                                         2,5,1,6,4,6,0,5,5,7,
                                         2,1,2,7,3,7,5,6,4))
  
  itemlogs[,"pos"] <- data.frame(pos = c(1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1))
  
  itemlogs[,"numb"] <- 1:49
  itemlogs <- itemlogs[items,]
  
  binary            <- convertDataToBinary(rawdata$postIt)
  raschData         <- RM(binary[items], sum0 = TRUE) 
  personRaschData   <- person.parameter(raschData)   
  personLogits      <- personRaschData[11]   
  personLogits      <- personLogits$thetapar$NAgroup1
  itemLogits        <- -data.frame(raschData[11])
  itemlogs[,"itemlog"] <- itemLogits$betapar
  itemlogs          <- itemlogs[order(itemlogs$itemlog, decreasing=FALSE), ]
  itemlogs          <- itemlogs[order(itemlogs$con, decreasing=FALSE), ]
  if (labpos[1] != ""){itemlogs$pos <- labpos}
  #print(itemlogs)
  itSd <- sd(itemLogits$betapar)
  
  statDatItem           <- matrix(itSd,ncol=1)
  colnames(statDatItem) <- c("ItemSd")
  
  personLogits2d <- matrix(nrow = length(personLogits),ncol = 2)
  
  if (sub == ""){
    PRE <- paste("PRE",1:604, sep=""); 
    POST <- paste("POST",1:604, sep=""); 
    for (i in 1:length(personLogits)){
      if (names(personLogits[i]) %in% PRE){
        personLogits2d[i,1] <- personLogits[i]}
      if (names(personLogits[i]) %in% POST){
        personLogits2d[i,2] <- personLogits[i] }
    }
  }
  
  if (sub == "trad"){
    tradPRE <- paste("PRE",1:215, sep=""); 
    tradPOST <- paste("POST",1:215, sep=""); 
    for (i in 1:length(personLogits)){
      if (names(personLogits[i]) %in% tradPRE){
        personLogits2d[i,1] <- personLogits[i]}
      if (names(personLogits[i]) %in% tradPOST){
        personLogits2d[i,2] <- personLogits[i] }
    }
  }
  
  if (sub == "frequ"){
    frequPRE <- paste("PRE",216:422,sep=""); 
    frequPOST <- paste("POST",216:422,sep=""); 
    for (i in 1:length(personLogits)){
      if (names(personLogits[i]) %in% frequPRE){
        personLogits2d[i,1] <- personLogits[i]}
      if (names(personLogits[i]) %in% frequPOST){
        personLogits2d[i,2] <- personLogits[i] }
    }
  }
  
  if (sub == "fa"){
    faPRE <- paste("PRE",423:604,sep=""); 
    faPOST <- paste("POST",423:604,sep="");
    for (i in 1:length(personLogits)){
      if (names(personLogits[i]) %in% faPRE){
        personLogits2d[i,1] <- personLogits[i]}
      if (names(personLogits[i]) %in% faPOST){
        personLogits2d[i,2] <- personLogits[i] }
    }
  }
  
  ########
  ######## artificially add extreme students
  ########
  # gives absolute number of correctly solved items of subsample
  
  if (sub == "") {
    numbOfCorrectPre <- rowSums(binary[1:583,items])
    numbOfCorrectPost <- rowSums(binary[(1+583):(583+583),items])
  }  
  if (sub == "trad") {
    numbOfCorrectPre <- rowSums(binary[1:207,items])
    numbOfCorrectPost <- rowSums(binary[(1+583):(207+583),items])
  }
  if (sub == "frequ") {
    numbOfCorrectPre <- rowSums(binary[208:407,items])
    numbOfCorrectPost <- rowSums(binary[(208+583):(407+583),items])
  }
  if (sub == "fa") {
    numbOfCorrectPre <- rowSums(binary[408:583,items])
    numbOfCorrectPost <- rowSums(binary[(408+583):(583+583),items])
  }
  
  corrPerItemPre  <- matrix(nrow = (length(items)+1),ncol = 1)
  corrPerItemPost <- matrix(nrow = (length(items)+1),ncol = 1)
  
  for (i in 0:(length(items))){
    corrPerItemPre[i+1,1]   <- sum(numbOfCorrectPre==i)
    corrPerItemPost[i+1,1]  <- sum(numbOfCorrectPost==i)
  }
  
  zeroPre     <- corrPerItemPre[1];   allPre  <- corrPerItemPre[length(corrPerItemPre)];
  zeroPost    <- corrPerItemPost[1];  allPost <- corrPerItemPost[length(corrPerItemPost)];
  
  addToHist   <- matrix(nrow = max((zeroPre+allPre),(zeroPost+allPost)),ncol = 2)
  zeroLogit   <- log(0.5/(length(items)-0.5))
  allLogit    <- -1*zeroLogit
  
  
  if ((corrPerItemPre[1]+corrPerItemPre[length(corrPerItemPre)]) != 0){
    preLogits   <- c(rep(zeroLogit,zeroPre),rep(allLogit,allPre))
    for (i in 1:length(preLogits)){addToHist[i,1]   <- preLogits[i]}
  }
  
  if ((corrPerItemPost[1]+corrPerItemPost[length(corrPerItemPost)]) != 0){
    postLogits  <- c(rep(zeroLogit,zeroPost),rep(allLogit,allPost))
    for (i in 1:length(postLogits)){addToHist[i,2]  <- postLogits[i]}
  }
  
  print(paste("number of extreme students:", nrow(addToHist))) 
  if (nrow(addToHist) != 0) {print(paste("extreme logit:", addToHist[1,1]))}
  
  personLogits2d <- rbind(personLogits2d,addToHist)
  
  PREPersLog   <- personLogits2d[,1]
  PREPersLog   <- PREPersLog[!is.na(PREPersLog)]
  POSTPersLog  <- personLogits2d[,2]
  POSTPersLog  <- POSTPersLog[!is.na(POSTPersLog)]

  pMnPRE       <- mean(PREPersLog)
  pMnPOST      <- mean(POSTPersLog)
  pSdPRE       <- sd(PREPersLog)
  pSdPOST      <- sd(POSTPersLog)

  statDatStud             <- matrix(c(pMnPRE,pSdPRE,pMnPOST,pSdPOST),nrow = 2)
  colnames(statDatStud)   <- c(paste(sub,"PRE",sep=""),paste(sub,"POST",sep=""))
  rownames(statDatStud)   <- c("mean","sd")

  wrightMap(personLogits2d, itemlogs$itemlog, 
            axis.persons = "Student Parameter Distribution",
            # person.side
            person.side = personDens, 
            dim.names = dimn,
            person.points = c(pMnPRE,pMnPOST),
            p.point.col = c("red","green"),
            p.point.cex = 3,
            #person.range = c(pMn-pSd,pMn+pSd),
            #item.side
            item.prop = 0.5, 
            item.side = itemModern, axis.items = "",
            cutpoints = c(pMnPRE,pMnPOST),
            thr.lab.text = itemlogs$names, thr.lab.cex = as.numeric(labOptions[2]),
            thr.lab.pos = itemlogs$pos, #thr.lab.pos = 1, 
            min.l = -3, max.l = 3,
            label.items.ticks = FALSE, label.items = "",
            ##item.prop = 0.6,##,
            main.title = paste("                                              ", 
                               title),
            show.axis.logits = "R")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("fig2dWrightMaps")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  pdfName <- paste("2dWrightMap", name, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  filename <- paste("2dMeanAndSd", name, ".xlsx", sep = "")
  write.xlsx(statDatStud, filename)
  write.xlsx(statDatItem, filename, sheetName="statDatItem",append=TRUE)
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}

my3dWrightmap <- function(rawdata, items, labOptions = c("text",0.8), title ="", name = "", dimn = "",
                          labpos = ""){
  
  
  if (dimn == "PRE") {dimn <- c("PRE(Trad., blue)",  "PRE(Frequ., red)",  "PRE(FA, green)") }
  else if (dimn == "POST") {dimn <- c("POST(Trad., blue)", "POST(Frequ., red)", "POST(FA, green)") }
  
  # pos:
  #Values of 1, 2, 3 and 4, respectively indicate positions below, 
  #to the left of, above and to the right of the specified coordinates.
  
  if (labOptions[1] == "num"){itemlogs <- data.frame(names = 1:49)}
  if (labOptions[1] == "text"){
    itemlogs <- data.frame(names = c("item 1 (C1)",  "item 2 (C5)",  "item 3 (C1)",  "item 4 (C3)", 
                                     "item 5 (C2)",  "item 6 (C5)",  "item 7",       "item 8 (C3)", 
                                     "item 9 (C6)",  "item 10 (C1)", "item 11 (C1)", "item 12 (C2)", 
                                     "item 13 (C3)", "item 14 (C5)", "item 15 (C5)", "item 16 (C1)", 
                                     "item 17 (C2)", "item 18 (C3)", "item 19",      "item 20 (C1)", 
                                     "item 21 (C2)", "item 22 (C7)", "item 23",      "item 24 (C6)", 
                                     "item 25 (C1)", "item 26 (C1)", "item 27 (C1)", "item 28 (C5)", 
                                     "item 29 (C5)", "item 30 (C4)", "item 31 (C2)", "item 32 (C5)", 
                                     "item 33 (C1)", "item 34 (C6)", "item 35 (C4)", "item 36 (C6)", 
                                     "item 37",      "item 38 (C5)", "item 39 (C5)", "item 40 (C7)", 
                                     "item 41 (C2)", "item 42 (C1)", "item 43 (C2)", "item 44 (C7)", 
                                     "item 45 (C3)", "item 46 (C7)", "item 47 (C5)", "item 48 (C6)", 
                                     "item 49 (C4)"))
  }
  
  itemlogs[,"con"] <- data.frame(con = c(1,5,1,3,2,5,0,3,6,1,
                                         1,2,3,5,5,1,2,3,0,1,
                                         2,7,0,6,1,1,1,5,5,4,
                                         2,5,1,6,4,6,0,5,5,7,
                                         2,1,2,7,3,7,5,6,4))
  
  itemlogs[,"pos"] <- data.frame(pos = c(1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1,1,
                                         1,1,1,1,1,1,1,1,1))
  
  itemlogs[,"numb"] <- 1:49
  itemlogs <- itemlogs[items,]
  
  binary            <- convertDataToBinary(rawdata$postIt)
  raschData         <- RM(binary[items], sum0 = TRUE) 
  personRaschData   <- person.parameter(raschData)   
  personLogits      <- personRaschData[11]   
  personLogits      <- personLogits$thetapar$NAgroup1
  itemLogits        <- -data.frame(raschData[11])
  itemlogs[,"itemlog"] <- itemLogits$betapar
  itemlogs          <- itemlogs[order(itemlogs$itemlog, decreasing=FALSE), ]
  itemlogs          <- itemlogs[order(itemlogs$con, decreasing=FALSE), ]
  if (labpos[1] != ""){itemlogs$pos <- labpos}
  #print(itemlogs)
  itSd <- sd(itemLogits$betapar)
  
  statDatItem           <- matrix(itSd,ncol=1)
  colnames(statDatItem) <- c("ItemSd")
  
  personLogits3d <- matrix(nrow = length(personLogits),ncol = 3)
  
  # PRE 
  trad  <- paste("PRE",1:215, sep=""); 
  frequ <- paste("PRE",216:422,sep=""); 
  fa    <- paste("PRE",423:604,sep=""); 
  for (i in 1:length(personLogits)){
    if (names(personLogits[i]) %in% trad){
      personLogits3d[i,1] <- personLogits[i]}
    if (names(personLogits[i]) %in% frequ){
      personLogits3d[i,2] <- personLogits[i] } 
    if (names(personLogits[i]) %in% fa){
      personLogits3d[i,3] <- personLogits[i] }
  }
  # POST
  trad  <- paste("POST",1:215, sep=""); 
  frequ <- paste("POST",216:422,sep=""); 
  fa    <- paste("POST",423:604,sep=""); 
  for (i in 1:length(personLogits)){
    if (names(personLogits[i]) %in% trad){
      personLogits3d[i,1] <- personLogits[i]}
    if (names(personLogits[i]) %in% frequ){
      personLogits3d[i,2] <- personLogits[i] } 
    if (names(personLogits[i]) %in% fa){
      personLogits3d[i,3] <- personLogits[i] }
  }
  
  ########
  ######## artificially add extreme students
  ########
  # gives absolute number of correctly solved items of subsample
  
  correctTrad   <- rowSums(binary[1:207,items])
  correctFrequ  <- rowSums(binary[208:407,items])
  correctFA     <- rowSums(binary[408:583,items])
  
  corrPerItemTrad   <- matrix(nrow = (length(items)+1),ncol = 1)
  corrPerItemFrequ  <- matrix(nrow = (length(items)+1),ncol = 1)
  corrPerItemFA     <- matrix(nrow = (length(items)+1),ncol = 1)
  
  for (i in 0:(length(items))){
    corrPerItemTrad[i+1,1]    <- sum(correctTrad==i)
    corrPerItemFrequ[i+1,1]   <- sum(correctFrequ==i)
    corrPerItemFA[i+1,1]      <- sum(correctFA==i)
  }
  
  zeroTrad    <- corrPerItemTrad[1];  allTrad   <- corrPerItemTrad[length(corrPerItemTrad)];
  zeroFrequ   <- corrPerItemFrequ[1]; allFrequ  <- corrPerItemFrequ[length(corrPerItemFrequ)];
  zeroFA      <- corrPerItemFA[1];    allFA     <- corrPerItemFA[length(corrPerItemFA)];
  
  addToHist   <- matrix(nrow = max((zeroTrad+allTrad),(zeroFrequ+allFrequ),(zeroFA+allFA)),ncol = 3)
  zeroLogit   <- log(0.5/(length(items)-0.5))
  allLogit    <- -1*zeroLogit
  
  if ((corrPerItemTrad[1]+corrPerItemTrad[length(corrPerItemTrad)]) != 0){
    tradLogits  <- c(rep(zeroLogit,zeroTrad),rep(allLogit,allTrad))
    for (i in 1:length(tradLogits)){addToHist[i,1]   <- tradLogits[i]}
  }
  if ((corrPerItemFrequ[1]+corrPerItemFrequ[length(corrPerItemFrequ)]) != 0){
    frequLogits <- c(rep(zeroLogit,zeroFrequ),rep(allLogit,allFrequ))
    for (i in 1:length(frequLogits)){addToHist[i,2]  <- frequLogits[i]}
  }
  if ((corrPerItemFA[1]+corrPerItemFA[length(corrPerItemFA)]) != 0){
    faLogits    <- c(rep(zeroLogit,zeroFA),rep(allLogit,allFA))
    for (i in 1:length(faLogits)){addToHist[i,3]  <- faLogits[i]}
  }
  
  print(paste("number of extreme students:", nrow(addToHist))) 
  if (nrow(addToHist) != 0) {print(paste("extreme logit:", addToHist[1,1]))}
  
  personLogits3d <- rbind(personLogits3d,addToHist)
  
  tradPersLog   <- personLogits3d[,1]
  tradPersLog   <- tradPersLog[!is.na(tradPersLog)]
  frequPersLog  <- personLogits3d[,2]
  frequPersLog  <- frequPersLog[!is.na(frequPersLog)]
  faPersLog     <- personLogits3d[,3]
  faPersLog     <- faPersLog[!is.na(faPersLog)]
  
  pMnTrad       <- mean(tradPersLog)
  pMnFrequ      <- mean(frequPersLog)
  pMnFA         <- mean(faPersLog)
  pSdTrad       <- sd(tradPersLog)
  pSdFrequ      <- sd(frequPersLog)
  pSdFA         <- sd(faPersLog)
  
  statDatStud             <- matrix(c(pMnTrad,pSdTrad,pMnFrequ,pSdFrequ,pMnFA,pSdFA),nrow = 2)
  colnames(statDatStud)   <- c("Trad","Frequ","FA")
  rownames(statDatStud)   <- c("mean","sd")
  
  wrightMap(personLogits3d, itemlogs$itemlog, 
            axis.persons = "Student Parameter Distribution",
            # person.side
            person.side = personDens, 
            dim.names = dimn,
            person.points = c(pMnTrad,pMnFrequ,pMnFA),
            p.point.col = c("blue","red","green"),
            p.point.cex = 3,
            #person.range = c(pMn-pSd,pMn+pSd),
            #item.side
            item.prop = 0.5, 
            item.side = itemModern, axis.items = "",
            cutpoints = c(pMnTrad,pMnFrequ,pMnFA),
            thr.lab.text = itemlogs$names, thr.lab.cex = as.numeric(labOptions[2]),
            thr.lab.pos = itemlogs$pos, #thr.lab.pos = 1, 
            min.l = -3, max.l = 3,
            label.items.ticks = FALSE, label.items = "",
            ##item.prop = 0.6,##,
            main.title = paste("                                              ", 
                               title),
            show.axis.logits = "R")
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDir1 <- paste("fig3dWrightMaps")
  if (file.exists(subDir1)){
    setwd(file.path(mainDir, subDir1))
  } else {
    dir.create(file.path(mainDir, subDir1))
    setwd(file.path(mainDir, subDir1))
  }
  pdfName <- paste("3dWrightMap", name, ".pdf", sep="")
  dev.copy2pdf(file = pdfName)
  
  filename <- paste("3dMeanAndSd", name, ".xlsx", sep = "")
  write.xlsx(statDatStud, filename)
  write.xlsx(statDatItem, filename, sheetName="statDatItem",append=TRUE)
  
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  return()
}
