  
  library(xlsx)
  library(riverplot)
  library(graphics)
  library(stringi)
  
  mainDirectory <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDirectory <- "riverClickerSourceFiles"
  setwd(file.path(mainDirectory, subDirectory))
  #source("riverPlotClickerData.R")
  source("riverPlotClickerAndreasData.R")
  source("myRiverPlotClickerDataFilter.R")
  #source("myRiverPlotClickerFct.R")
  source("myRiverPlotClickerFctDeutsch.R")
  setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  ######
  # Session 1
  ######
  dev.off()

  myClickerRiverPlot(item1session1$preForRiver, item1session1$postForRiver, session = 1, question = 1, sol = answerKeySession1[1])
  myClickerRiverPlot(item2session1$preForRiver, item2session1$postForRiver, session = 1, question = 2, sol = answerKeySession1[2])
  myClickerRiverPlot(item3session1$preForRiver, item3session1$postForRiver, session = 1, question = 3, sol = answerKeySession1[3])
  myClickerRiverPlot(item4session1$preForRiver, item4session1$postForRiver, session = 1, question = 4, sol = answerKeySession1[4])
  myClickerRiverPlot(item5session1$preForRiver, item5session1$postForRiver, session = 1, question = 5, sol = answerKeySession1[5])
  myClickerRiverPlot(item6session1$preForRiver, item6session1$postForRiver, session = 1, question = 6, sol = answerKeySession1[6])
  myClickerRiverPlot(item7session1$preForRiver, item7session1$postForRiver, session = 1, question = 7, sol = answerKeySession1[7])
  myClickerRiverPlot(item8session1$preForRiver, item8session1$postForRiver, session = 1, question = 8, sol = answerKeySession1[8])
  myClickerRiverPlot(item9session1$preForRiver, item9session1$postForRiver, session = 1, question = 9, sol = answerKeySession1[9])
  myClickerRiverPlot(item10session1$preForRiver, item10session1$postForRiver, session = 1, question = 10, sol = answerKeySession1[10])
  myClickerRiverPlot(item11session1$preForRiver, item11session1$postForRiver, session = 1, question = 11, sol = answerKeySession1[11])
  myClickerRiverPlot(item12session1$preForRiver, item12session1$postForRiver, session = 1, question = 12, sol = answerKeySession1[12])

  
  ######
  # Session 2
  ######
  
  myClickerRiverPlot(item1session2$preForRiver, item1session2$postForRiver, session = 2, question = 1, sol = answerKeySession2[1])
  myClickerRiverPlot(item2session2$preForRiver, item2session2$postForRiver, session = 2, question = 2, sol = answerKeySession2[2])
  myClickerRiverPlot(item3session2$preForRiver, item3session2$postForRiver, session = 2, question = 3, sol = answerKeySession2[3])
  myClickerRiverPlot(item4session2$preForRiver, item4session2$postForRiver, session = 2, question = 4, sol = answerKeySession2[4])
  myClickerRiverPlot(item5session2$preForRiver, item5session2$postForRiver, session = 2, question = 5, sol = answerKeySession2[5])
  myClickerRiverPlot(item6session2$preForRiver, item6session2$postForRiver, session = 2, question = 6, sol = answerKeySession2[6])
  myClickerRiverPlot(item7session2$preForRiver, item7session2$postForRiver, session = 2, question = 7, sol = answerKeySession2[7])
  myClickerRiverPlot(item8session2$preForRiver, item8session2$postForRiver, session = 2, question = 8, sol = answerKeySession2[8])
  myClickerRiverPlot(item9session2$preForRiver, item9session2$postForRiver, session = 2, question = 9, sol = answerKeySession2[9])
  myClickerRiverPlot(item10session2$preForRiver, item10session2$postForRiver, session = 2, question = 10, sol = answerKeySession2[10])
  myClickerRiverPlot(item11session2$preForRiver, item11session2$postForRiver, session = 2, question = 11, sol = answerKeySession2[11])
  myClickerRiverPlot(item12session2$preForRiver, item12session2$postForRiver, session = 2, question = 12, sol = answerKeySession2[12])
  
  
  
  
