######
# load data from xlsx file
######

# generates a data.frame consisting of the answers chosen by
# the students
# vector solutions consists of the correct answers
#   of the individual questions

library(xlsx) # to get xlsx file

sortOutForRiverPlot <- function(singleItemIndiv, singleItemGroup){
  
  item          <- list(PRE = singleItemIndiv, POST = singleItemGroup) 
  # get rid of "-" and NA
  item$PRE      <- item$PRE [!is.na(item$PRE)]
  item$PRE      <- item$PRE[item$PRE != "-"]
  item$PRE      <- item$PRE[item$PRE != -99999]
  item$POST     <- item$POST [!is.na(item$POST)]
  item$POST     <- item$POST[item$POST != "-"]
  item$POST     <- item$POST[item$POST != -99999]
  
  
  riverDataPoints <- vector(mode = "numeric", length = 0)
  
  for (i in names(item$PRE)){
    if(is.element(i, names(item$POST))){
      riverDataPoints <-  append(riverDataPoints,i)
    }
  }
  
  preForRiver   <- item$PRE[is.element(names(item$PRE),riverDataPoints)]
  postForRiver  <- item$POST[is.element(names(item$POST),riverDataPoints)]
  
  clickerRiverData <- list(preForRiver = preForRiver, postForRiver = postForRiver)
  
  return(clickerRiverData)
}


######
# Session 1
######

clickerExlDataSession1  <- read.xlsx("clickerForRiverPlotAndreas.xlsx", sheetName = "CS1")
clickerDataSession1     <- as.matrix(clickerExlDataSession1[1:182,5:28])
answerKeySession1 <- clickerExlDataSession1[183,5:28]
answerKeySession1 <-  as.matrix(answerKeySession1)
answerKeySession1 <- answerKeySession1[seq(1, length(answerKeySession1), 2)]

item1session1   <- sortOutForRiverPlot(clickerDataSession1[,1],clickerDataSession1[,2])
item2session1   <- sortOutForRiverPlot(clickerDataSession1[,3],clickerDataSession1[,4])
item3session1   <- sortOutForRiverPlot(clickerDataSession1[,5],clickerDataSession1[,6])
item4session1   <- sortOutForRiverPlot(clickerDataSession1[,7],clickerDataSession1[,8])
item5session1   <- sortOutForRiverPlot(clickerDataSession1[,9],clickerDataSession1[,10])
item6session1   <- sortOutForRiverPlot(clickerDataSession1[,11],clickerDataSession1[,12])
item7session1   <- sortOutForRiverPlot(clickerDataSession1[,13],clickerDataSession1[,14])
item8session1   <- sortOutForRiverPlot(clickerDataSession1[,15],clickerDataSession1[,16])
item9session1   <- sortOutForRiverPlot(clickerDataSession1[,17],clickerDataSession1[,18])
item10session1  <- sortOutForRiverPlot(clickerDataSession1[,19],clickerDataSession1[,20])
item11session1  <- sortOutForRiverPlot(clickerDataSession1[,21],clickerDataSession1[,22])
item12session1  <- sortOutForRiverPlot(clickerDataSession1[,23],clickerDataSession1[,24])

######
# Session 2
######

clickerExlDataSession2  <- read.xlsx("clickerForRiverPlotAndreas.xlsx", sheetName = "CS2")
clickerDataSession2     <- as.matrix(clickerExlDataSession2[1:182,5:28])
answerKeySession2       <- clickerExlDataSession2[183,5:28]
answerKeySession2       <-  as.matrix(answerKeySession2)
answerKeySession2       <- answerKeySession2[seq(1, length(answerKeySession2), 2)]

item1session2   <- sortOutForRiverPlot(clickerDataSession2[,1],clickerDataSession2[,2])
item2session2   <- sortOutForRiverPlot(clickerDataSession2[,3],clickerDataSession2[,4])
item3session2   <- sortOutForRiverPlot(clickerDataSession2[,5],clickerDataSession2[,6])
item4session2   <- sortOutForRiverPlot(clickerDataSession2[,7],clickerDataSession2[,8])
item5session2   <- sortOutForRiverPlot(clickerDataSession2[,9],clickerDataSession2[,10])
item6session2   <- sortOutForRiverPlot(clickerDataSession2[,11],clickerDataSession2[,12])
item7session2   <- sortOutForRiverPlot(clickerDataSession2[,13],clickerDataSession2[,14])
item8session2   <- sortOutForRiverPlot(clickerDataSession2[,15],clickerDataSession2[,16])
item9session2   <- sortOutForRiverPlot(clickerDataSession2[,17],clickerDataSession2[,18])
item10session2  <- sortOutForRiverPlot(clickerDataSession2[,19],clickerDataSession2[,20])
item11session2  <- sortOutForRiverPlot(clickerDataSession2[,21],clickerDataSession2[,22])
item12session2  <- sortOutForRiverPlot(clickerDataSession2[,23],clickerDataSession2[,24])
