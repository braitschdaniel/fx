# execution file to generate  "Riverplots"

library(Hmisc)
library(riverplot)
library(graphics)

mainDirectory <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
subDirectory <- "riverSourceFiles"
setwd(file.path(mainDirectory, subDirectory))
source("riverPlotData.R")
source("myRiverPlotDataFilter.R")
#source("riverQuantAssignment.R")
source("myRiverPlotFct.R")
source("typeComparison.R")
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))


dev.off()
items <- c(1:49)
items <- c(1)
multiRiverAllTypes(items, language = "ger")
multiRiverAllTypes(items, language = "eng")
multiRiverAllTypesAllItems(language = "ger")
multiRiverAllTypesAllItems(language = "eng")


dev.off()
for (i in items){
  myRiverPlot(preData[i], postData[i], quantMatrix = AllStudentsMatrix[i,], language = "ger", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "")
}

for (i in items){
  myRiverPlot(preForm1[i], postForm1[i], quantMatrix = TradMatrix[i,], language = "ger", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Traditionell")
}

for (i in items){
  myRiverPlot(preForm2[i], postForm2[i], quantMatrix = FrequMatrix[i,], language = "ger", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Frequent Testing")
}

for (i in items){
  myRiverPlot(preForm3[i], postForm3[i], quantMatrix = FAMatrix[i,], language = "ger", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Formative Assessment")
}

for (i in items){
  myRiverPlot(preData[i], postData[i], quantMatrix = AllStudentsMatrix[i,], language = "eng", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "")
}

for (i in items){
  myRiverPlot(preForm1[i], postForm1[i], quantMatrix = TradMatrix[i,], language = "eng", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Traditional")
}

for (i in items){
  myRiverPlot(preForm2[i], postForm2[i], quantMatrix = FrequMatrix[i,], language = "eng", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Frequent Testing")
}

for (i in items){
  myRiverPlot(preForm3[i], postForm3[i], quantMatrix = FAMatrix[i,], language = "eng", 
              question = i, numAnswers = numberOfChoices[i], sol = solutions[i],
              teachingType =  "Formative Assessment")
}
