# function myRiverPlot generates a "riverplot" and saves this plot as a pdf file
# input:
#   preData: data.frame (riverplotData.R)
#   postData: data.frame (riverplotData.R)
#   numAnswers: vector consisting of the number of possible answers (riverplotData.R)
#   solutions: vector consisting of the right solution (riverplotData.R)
#   quantMatrix: matrix consisting of the quantisation types 
#     (concentration analysis, file: riverQuantAssignment.R)
#   option: single question
#   option: teaching type
#   option: multiplot = TRUE: creates a figure consisting of riverplots of all three types


myClickerRiverPlot <- function(preData, postData, solutions, session = 0, question = ""){
  
  if (is.character(preData) && length(preData) == 0){
    plot(0,type='n',axes=FALSE,ann=FALSE)
    return()
  }
  
  # converting data into a dataMatrix
  dataMatrix <- myRiverPlotDataClickerFilter(preData, postData)
  
  # colors chosen for 4 answers
  col4 <- c( "mediumorchid2", "yellow", "deepskyblue2", "firebrick1",
             "mediumorchid2", "yellow", "deepskyblue2", "firebrick1")

  
  # the correct answer is presented in green
  if (solutions == "A") {solForCol <- 4}
  if (solutions == "B") {solForCol <- 3}
  if (solutions == "C") {solForCol <- 2}
  if (solutions == "D") {solForCol <- 1}
  
  col4[solForCol] <- "green2"
  col4[solForCol+4] <- "green2"
  
  # creating the 4-answer river plot
    edges <- list( A = list(E = dataMatrix[1,1], F = dataMatrix[1,2], 
                            G = dataMatrix[1,3], H = dataMatrix[1,4]),
                   B = list(E = dataMatrix[2,1], F = dataMatrix[2,2], 
                            G = dataMatrix[2,3], H = dataMatrix[2,4]),
                   C = list(E = dataMatrix[3,1], F = dataMatrix[3,2], 
                            G = dataMatrix[3,3], H = dataMatrix[3,4]),
                   D = list(E = dataMatrix[4,1], F = dataMatrix[4,2], 
                            G = dataMatrix[4,3], H = dataMatrix[4,4]))
    
    edges <- list( A = list(E = dataMatrix[4,4], F = dataMatrix[4,3], 
                            G = dataMatrix[4,2], H = dataMatrix[4,1]),
                   B = list(E = dataMatrix[3,4], F = dataMatrix[3,3], 
                            G = dataMatrix[3,2], H = dataMatrix[3,1]),
                   C = list(E = dataMatrix[2,4], F = dataMatrix[2,3], 
                            G = dataMatrix[2,2], H = dataMatrix[2,1]),
                   D = list(E = dataMatrix[1,4], F = dataMatrix[1,3], 
                            G = dataMatrix[1,2], H = dataMatrix[1,1]))

    nodes <- data.frame( ID= LETTERS[1:8],
                         x= c( 1, 1, 1, 1, 
                               2, 2, 2, 2),
                         col= col4,
                         labels= c( "D               ", "C               ", 
                                    "B               ", "A               ",
                                    "               D", "               C", 
                                    "               B", "               A"),
                         textcex = c(1.0,1.0,1.0,1.0,
                                     1.0,1.0,1.0,1.0)*0.8,
                         stringsAsFactors= FALSE)
  
    
  # use function makeRiver of the riverplot library
  r <- makeRiver(nodes, edges)
  plot(r, srt=0)
  
  #print(dataMatrix)
  
  # creating title and subtitle with additional information
  question <- question
  solutions <- solutions
  
  # correct answer (subtitle) 
  sol <- solutions

  # actual title of the figure
  titleContent <- paste("Q", question)

  # position of the title
  title(titleContent, line = -3, cex=0.9)

  # subtitle and saving of the figure
  #subtitleContent1 <- paste("Correct Answer: ", sol, ", Number of Students: ", length(preData), sep = "")
  #mtext(subtitleContent1, side=3, line=-6.3, cex=1)
  #pdfName <- paste("riverPlotClickerSession", session, "Question", question ,".pdf",sep="")
  #mainDir <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  #subDir1 <- paste("figuresClickerRiverPlotSession", session, sep = "")
  #if (file.exists(subDir1)){
  #  setwd(file.path(mainDir, subDir1))
  #} else {
  #  dir.create(file.path(mainDir, subDir1))
  #  setwd(file.path(mainDir, subDir1))
  #}
  #dev.copy2pdf(file = pdfName)
  #setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
}