  # packages that need to be installed
  library(Hmisc)                    # package used to get spss files
  library(xlsx)                     # to get xlsx files
  library(eRm)                      # package used to do Rasch analysis
  library(WrightMap)
  # loading source files 
  mainDirectory <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  subDirectory <- "zraschSourceFiles"
  setwd(file.path(mainDirectory, subDirectory))
  source("raschDataFile.R")
  source("raschFct.R")
  mainDirectory <- setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
  
  allitems      <- 1:49
  alldiag       <- c(10,11,16,20,26,33,42, 7,37, 12,17,21,31,43,30,35,
                     38,49, 2, 6,28,32,39,19,23,24,34,48,22,40,44,46)
  allpictab     <- c(1,25,5,41,4,8,13,18,45,14,29,9,36,3,27,15,47)
  fac1diag      <- c(10,11,26,33,42,12,17,21,31,43)
  fac2diag      <- c(2,35,38,40,30,24,48,22, 6,44,39,46)
  fac1pictab    <- c(1,25,3,27,14,29,15)
  fac2pictab    <- c(9,13,18,8,41,45,5,36)
  facAnaFac1    <- c(1,25,3,27,14,29,15,47)
  facAnaFac2    <- c(10,11,16,20,26,33,42,2,6,28,32,39)
  facAnaFac3    <- c(12,17,21,31,43,24,34,48)
  facAnaFac4    <- c(30,35,38,49,22,44,40,46)
  facAnaFac5    <- c(5,41,9,36,4,8,13,18,45)
  
  ###############
  # rasch item statistics
  ###############
  
  ######
  # all 49 items
  ######
  getRMItemStatTab(postAll,allitems,   name = "PostAllItemsAll")
  getRMItemStatTab(postForm1,allitems, name = "PostAllItemsTrad")
  getRMItemStatTab(postForm2,allitems, name = "PostAllItemsFrequ")
  getRMItemStatTab(postForm3,allitems, name = "PostAllItemsFA")
  
  ######
  # all 32 graphs
  ######
  getRMItemStatTab(postAll,alldiag,   name = "PostAllGraphsAll")
  getRMItemStatTab(postForm1,alldiag, name = "PostAllGraphsTrad")
  getRMItemStatTab(postForm2,alldiag, name = "PostAllGraphsFrequ")
  getRMItemStatTab(postForm3,alldiag, name = "PostAllGraphsFA")
  
  ######
  # all 17 pictabs
  ######
  getRMItemStatTab(postAll,allpictab,   name = "PostAllPictabsAll")
  getRMItemStatTab(postForm1,allpictab, name = "PostAllPictabsTrad")
  getRMItemStatTab(postForm2,allpictab, name = "PostAllPictabsFrequ")
  getRMItemStatTab(postForm3,allpictab, name = "PostAllPictabsFA")
  
  ######
  # 4 found factors
  ######
  # fac 1 diag (rasch)
  getRMItemStatTab(postAll,fac1diag,   name = "PostFac1diagAll")
  getRMItemStatTab(postForm1,fac1diag, name = "PostFac1diagTrad")
  getRMItemStatTab(postForm2,fac1diag, name = "PostFac1diagFrequ")
  getRMItemStatTab(postForm3,fac1diag, name = "PostFac1diagFA")
  
  # fac 2 diag (rasch)
  getRMItemStatTab(postAll,fac2diag,   name = "PostFac2diagAll")
  getRMItemStatTab(postForm1,fac2diag, name = "PostFac2diagTrad")
  getRMItemStatTab(postForm2,fac2diag, name = "PostFac2diagFrequ")
  getRMItemStatTab(postForm3,fac2diag, name = "PostFac2diagFA")
  
  # fac 1 pic tab (rasch)
  getRMItemStatTab(postAll,fac1pictab,   name = "PostFac1pictabAll")
  getRMItemStatTab(postForm1,fac1pictab, name = "PostFac1pictabTrad")
  getRMItemStatTab(postForm2,fac1pictab, name = "PostFac1pictabFrequ")
  getRMItemStatTab(postForm3,fac1pictab, name = "PostFac1pictabFA")
  
  # fac 2 pic tab (rasch)
  getRMItemStatTab(postAll,fac2pictab,   name = "PostFac2pictabAll")
  getRMItemStatTab(postForm1,fac2pictab, name = "PostFac2pictabTrad")
  getRMItemStatTab(postForm2,fac2pictab, name = "PostFac2pictabFrequ")
  getRMItemStatTab(postForm3,fac2pictab, name = "PostFac2pictabFA")
  
  ######
  # 5 factors found by EFA
  ######
  # fac 1 diag (rasch)
  getRMItemStatTab(postAll,facAnaFac1,   name = "PostFacAnaFac1gAll")
  getRMItemStatTab(postForm1,facAnaFac1, name = "PostFacAnaFac1Trad")
  getRMItemStatTab(postForm2,facAnaFac1, name = "PostFacAnaFac1Frequ")
  getRMItemStatTab(postForm3,facAnaFac1, name = "PostFacAnaFac1FA")
  
  # fac 2 diag (rasch)
  getRMItemStatTab(postAll,facAnaFac2,   name = "PostFacAnaFac2All")
  getRMItemStatTab(postForm1,facAnaFac2, name = "PostFacAnaFac2Trad")
  getRMItemStatTab(postForm2,facAnaFac2, name = "PostFacAnaFac2Frequ")
  getRMItemStatTab(postForm3,facAnaFac2, name = "PostFacAnaFac2FA")
  
  # fac 3 diag (rasch)
  getRMItemStatTab(postAll,facAnaFac3,   name = "PostFacAnaFac3All")
  getRMItemStatTab(postForm1,facAnaFac3, name = "PostFacAnaFac3Trad")
  getRMItemStatTab(postForm2,facAnaFac3, name = "PostFacAnaFac3Frequ")
  getRMItemStatTab(postForm3,facAnaFac3, name = "PostFacAnaFac3FA")
  
  # fac 4 diag (rasch)
  getRMItemStatTab(postAll,facAnaFac4,   name = "PostFacAnaFac4All")
  getRMItemStatTab(postForm1,facAnaFac4, name = "PostFacAnaFac4Trad")
  getRMItemStatTab(postForm2,facAnaFac4, name = "PostFacAnaFac4Frequ")
  getRMItemStatTab(postForm3,facAnaFac4, name = "PostFacAnaFac4FA")
  
  # fac 5 diag (rasch)
  getRMItemStatTab(postAll,facAnaFac5,   name = "PostFacAnaFac5All")
  getRMItemStatTab(postForm1,facAnaFac5, name = "PostFacAnaFac5Trad")
  getRMItemStatTab(postForm2,facAnaFac5, name = "PostFacAnaFac5Frequ")
  getRMItemStatTab(postForm3,facAnaFac5, name = "PostFacAnaFac5FA")
  

  ###############
  # rasch person statistics
  ###############
  
  ######
  # all 49 items
  ######
  getRMPersonStatTab(postAll,projectData,allitems,   name = "PostAllItemsAll")
  getRMPersonStatTab(postForm1,projectData,allitems, name = "PostAllItemsTrad")
  getRMPersonStatTab(postForm2,projectData,allitems, name = "PostAllItemsFrequ")
  getRMPersonStatTab(postForm3,projectData,allitems, name = "PostAllItemsFA")
  
  ######
  # all 32 graphs
  ######
  getRMPersonStatTab(postAll,projectData,alldiag,   name = "PostAllGraphsAll")
  getRMPersonStatTab(postForm1,projectData,alldiag, name = "PostAllGraphsTrad")
  getRMPersonStatTab(postForm2,projectData,alldiag, name = "PostAllGraphsFrequ")
  getRMPersonStatTab(postForm3,projectData,alldiag, name = "PostAllGraphsFA")
  
  ######
  # all 17 pictabs
  ######
  getRMPersonStatTab(postAll,projectData,allpictab,   name = "PostAllPictabsAll")
  getRMPersonStatTab(postForm1,projectData,allpictab, name = "PostAllPictabsTrad")
  getRMPersonStatTab(postForm2,projectData,allpictab, name = "PostAllPictabsFrequ")
  getRMPersonStatTab(postForm3,projectData,allpictab, name = "PostAllPictabsFA")
  
  
  