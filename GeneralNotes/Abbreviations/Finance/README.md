# Finance

| Abbr. | Topic: Finance                      |
| :---: |: ---------------------------------- | 
| MIC   | Market Identifier Code              |
| GTD   | Good Til Date                       |
| OMS   | Order Management System             |
| GTC   | Good Til Cancel                     |
| NAV   | Net Asset Value                     |
| TER   | Total Expense Ratio                 |
| FED   | Federal Reserve (US Central Bank)   |
| EONIA | Euro Overnight Index Average        |
| FAT   | Factory Acceptance Test             |
| UAT   | User Acceptance Test                |
| STIRT | Short Term Interest Rate Trading    |
| UTI   | Unique Trade Identifier             |
| TRNT  | Thompson Reuters Trade Notification |

