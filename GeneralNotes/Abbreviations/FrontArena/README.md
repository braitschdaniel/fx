# Front Arena 

| Abbr. | Topic: Front Arena                 |
| :---: |: --------------------------------- | 
| FSM   | Finite State Machine               |
| AIF   | Arena Integration Framework        |
| FAME  |  (.map) Front Arena Mapper Engine  |
| DMC   | Dynamic Market Capabilities        |
| FAST  | Front Arena Support Tool           |
| AMPH  | Arena Market Price Handler         |
| APH   | Arena Price Feed Handler           |
| AMAS  |  Arena Market Access Server        |
| AMS   | Arena Market Server                |
| TNP   | Transaction Network Protocol       |
| SGN   | Sungard Global Network             |
| TTT   | Table Transition Tool              |
| ADM   | Arena Data Model                   |
| FSM   | Finite State Machine               |
| DMA   | Direct Market Access               |