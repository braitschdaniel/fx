# IT

| Abbr. | Topic: IT                                      |
| :---: |: --------------------------------------------- | 
| EDV   | Elektronische Datenverarbeitung                |
| RTF   | Rich Text Format                               |
| PNG   | Portable Network Graphics                      |
| IDE   | Integrated Development Environment             |
| SSO   | Single Sign On                                 |
| UML   | Unified Modeling Language                      |
| XML   | Extensible Markup Language                     |
| API   | Application Programming Interface              |
| XSLT  | Extensible Stylesheet Language Transformations |
| URL   | Uniform Resource Locator                       |
| URI   | Uniform Resource Identifier                    |
| PEP   | Python Enhancement Proposal                    |
| MSDOS | Microsoft Disk Operating System                |
| JAR   | Java ARchive                                   |
| POM   | Project Object Model                           |
