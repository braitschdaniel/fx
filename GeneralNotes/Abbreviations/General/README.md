# General

| Abbr. | Topic: General Abbr.               |
| :---: |: --------------------------------- | 
| COO   | Chief Operation Officer            |
| CPI   | Consumer Price Index               |
| USP   | Unique Sales Proposition           |
| CRM   | Consumer Relationship Management   |

