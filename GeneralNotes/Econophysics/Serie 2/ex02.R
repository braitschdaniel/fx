
#-------------------------------------------------------------------------
# Task 2
#-------------------------------------------------------------------------
x <- read.csv("danishClaims.csv", sep = ";")
head(x)
x <- x[,2]
  head(x)
plot(x, type = "h", col = "steelblue",
     xlab = "days", ylab = "Claim Size", main = "Danish Claims")


#-------------------------------------------------------------------------
# Task 3
#-------------------------------------------------------------------------
# Threshold:
u <- 10 # Mio Danish Krones
  
  # Number of Losses:
  n <- length(x)  # from n days we know the fire damage
  n

# Number of Exceedances:
Nu <- sum(x > u)  # sums number of days which have damage > threshold
  Nu

# Excess samples:
y <- x[x>u]-u
  head(y)  # it should be possible to describe these y by the pareto distribution

# Visualization:
plot(x, type="h",
     col = "steelblue",
     xlab = "days", ylab = "Claim Size", main = "Danish Claims")
lines(ifelse(x<u, x,u), type="h", col="black") # colors the part below the threshold black
abline(h=u, col="red", lwd=1) # adds a horizontal line


#-------------------------------------------------------------------------
# Task 4
#-------------------------------------------------------------------------
# density function of the pareto distribution
beta <- 2; xi <- 0.5


ppareto <- function(x, beta, xi) {1 - (1+ x* xi/beta)^(-1/xi)}  # probability function
dpareto <- function(x, beta, xi) {(1+x*xi/beta)^(-1-1/xi) / beta} # densitiy function

# estimate parameters from data
nlogl <- function(par, x) {-mean(log(dpareto(x, par[1], par[2])))} # likelihood function
fit <- nlminb(c(1,1), nlogl, x=y, lower=c(0,0), upper=c(Inf,Inf), control=list(trace=TRUE)) # minimizer that comes with R

# retrieve fitted values
beta <- fit$par[1]
  xi <- fit$par[2]  # if xi < 0.5 we have an infinite variance!!!
  c(beta,xi)

# Plot figure 1:
# To simplify things we plot against y; and not the log of x
Ge <- seq(1/Nu, 1, len=Nu)
  plot(sort(y), Ge,  # plot empirical distribution
       xlab="y", ylab="G(y)", type="p", pch=20)
# Interpretation of this: the probability to see a loss which is less than 250M is equal to 1

v <- seq(min(y), max(y), length=100)
  lines(v, ppareto(v, beta, xi), col="red")

# This is actually what was done in the paper:
Ge <- seq(1/Nu, 1, len = Nu)
plot(sort(y+u), Ge, xlab="y", ylab="G(y)", type="p", pch=20, log="x")
v <- seq(min(y), max(y), length = 101)
lines(v+u, ppareto(v, beta, xi), col="red")


#-------------------------------------------------------------------------
# Task 5
#-------------------------------------------------------------------------
# Tail Estimator:
tailEstimator <- function(x, u, Nu, n, beta, xi) {
  1-Nu/n*(1+xi*(x-u)/beta)^(-1/xi)
}

# We don't know the real distribution -> take the historical distribution 
# use theoretical framework since we don't have enough data on the fat tails

# Plot figure 2:
# What was done in the paper is probably the following: 
#Fe <- seq((n-Nu)/n, 1-0.0002, len = Nu)
# since log(1-Fe) is infinite for Fe=1 (which breaks the plot).
# More correct would be to use Fe[length(Fe)] <- 1-2*1e-4; 
# otherwise all theoretical quantiles are slightly uncorrect.
Fe <- seq((n-Nu)/n, 1, len=Nu)  # probability that we see a loss that is equal or smaller than Nu
# Fe == empirical distribution  
Fe[length(Fe)] <- 1-2*1e-4
plot(sort(y+u), 1-Fe, log="xy", 
     xlab="log(x)", ylab="log(1-F(x))",
     type="p", pch=20)

# Compare to the theoretical solution
v <- seq(min(y+u), max(y+u), length=100)
  Fh <- tailEstimator(v, u, Nu, n, beta, xi)
  lines(v, 1-Fh , col="red")


#-------------------------------------------------------------------------
# Task 6
#-------------------------------------------------------------------------
# Value at Risk
VaR <- function(p, u, beta, xi) {u + beta/xi * ((n/Nu*(1-p))^(-xi)-1)}
VaR.evt <- VaR(0.99, u, beta, xi)
VaR.hst <- quantile(x,0.99)
  as.vector(c(VaR.evt, VaR.hst))

# Expected Shortfall
# Sometimes also called conditional value at risk (CVaR)
ES <- function(VaR, u, beta, xi) {VaR/(1-xi)+((beta-xi*u)/(1-xi))}
ES.evt <- ES(VaR.evt, u, beta, xi)
ES.hst <- mean(x[x > VaR.hst])  # brut force estimation of expected shortfall
  as.vector(c(ES.evt, ES.hst))
# let you go in an area where you don't have any data!!
