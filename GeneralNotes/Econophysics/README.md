# Econophysics:

Ein Aktienindex ist eine Kennzahl für die Entwicklung von ausgewählten Aktienkursen.

Stylized Fact: In social sciences, especially economics, a stylised fact is a simplified presentation of an empirical finding. It can often be a broad generalisation that summerises some complicated statistical calculation, which although essentially true may have inaccuracies in the detail.
"Education significantly raises lifetime income."

iid: independent identically distributed 

PDF: Probability density function
	
The third and higher-order cumulants of a normal distribution  are zero, and it is the only distribution with this property.

Volatility Clustering:
large changes tend to be followed by large changes, of either sign, and small changes tend to be followed by small changes.

Outlier: Sonderfall, high curtsies means a problem with outliers

Levy Verteilung: weder endliche Erwartungswerte, noch endliche Varianz

Compounding: Aufzinsung
