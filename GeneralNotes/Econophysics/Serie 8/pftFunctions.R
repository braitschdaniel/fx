riskRewardPlane <- function(returns, col="red", pch = 19,
                            names=colnames(returns), pos=4,
                            init = FALSE, add = FALSE,
                            xlimc = c(0.85,1.05),
                            ylimc = c(0.95,1.05)) {
  risks <- apply(as.matrix(returns), 2, sd)
  rewards <- apply(as.matrix(returns), 2, mean)
  
  dates <- rownames(returns)
  start <- dates[1]
  end <- rev(dates)[1]
  main <- paste("Daily Returns From", start, "to", end)
  
  if (init || (!init && !add)) {
    plot(NA, xlim = range(risks)*xlimc, ylim = range(rewards)*ylimc,
         xlab = "Risk", ylab = "Average Return  (%)", main=main, cex.main=0.8)
  }
  if (add || (!init && !add)) {
    points(risks, rewards, col = col, pch = pch, cex=1.1)
    text(risks, rewards, names, cex=0.8, pos=pos)
  }
}

pftMinRisk <- function(targetReturn, returns, obj = pftRisk, start = NULL,
                       solver = c("donlp2NLP","solnpNLP","nlminb2NLP"),
                       min = TRUE) {
  # Start Weights
  n <- ncol(returns)
  if (is.null(start)) {start <- rep(1/n, n)}
  
  # Objective Function and Target
  if (min) {
    objective <- function(w,...) {obj(w,...)}
  } else {
    objective <- function(w,...) {-obj(w,...)}
  }
  if (missing(targetReturn)) {
    targetReturn <- pftReward(start, returns)
  }
  
  # Box Constraints
  n <- ncol(returns)
  par.lower <- rep(0,n)
  par.upper <- rep(1,n)
  
  # Linear: Target Return and Full Investment Constraint
  eqA <- rbind(means=colMeans(returns), fullInvest=rep(1,n))
  eqA.bound <- c(targetReturn, 1)
  
  # Choose Solver
  solver <- match.arg(solver)
  funNLP <- match.fun(solver)
  
  # Optimize Portfolio
  control <- list(trace=FALSE)
  if (solver=="donlp2NLP") {
    control <- list(silent=TRUE, tau0=1e-8)
  }
  
  optPort <- funNLP(start = start, objective = objective,
                    par.lower = par.lower, par.upper = par.upper,
                    eqA = eqA, eqA.bound = eqA.bound,
                    control=control)
  
  weights <- optPort$solution
  obj <- objective(weights,ret=returns)
  if (!min) {obj <- -obj}
  
  pftReturns <- pftReturns(weights,ret=returns)
  pftReward <- pftReward(weights,ret=returns)
  pftRisk <- pftRisk(weights,ret=returns)
  
  portfolio <- list(objective = obj, weights = weights, 
                    pftReturns = pftReturns,
                    pftReward = pftReward, pftRisk = pftRisk)
  class(portfolio) <- c("myPft", "list")
  portfolio
}

print.myPft <- function(x) {
  print(as.numeric(x$objective))
  print(round(x$weights, 4))
}

feasibleSet <- function (returns, len=50, solver="donlp2NLP",
                         method=c("pairwise","optimize","both"), addPairLines=FALSE,
                         add = FALSE, ...) {
  ## Setup the risk-reward-plane
  if (!add) {riskRewardPlane(returns, ...)}
  riskRewardPlane(rowMeans(returns), names="EWP", pch=18, col="darkgreen", add=TRUE)
  
  ## Initializations
  method <- match.arg(method)
  noAssets <- ncol(returns)
  mu <- colMeans(returns)
  
  ## Create the target returns along the minimum and maximum variance locus
  mu <- colMeans(returns)
  targetReturnsMin <- seq(min(mu), max(mu), length.out = len)
  
  ## Calculate the risks for those returns
  getObjective <- function(x, ...) {pftMinRisk(x, ...)$objective}
  minimumVarianceLocus <- sapply(targetReturnsMin, getObjective, returns = returns,
                                 solver = solver, min = TRUE)
  
  if (method=="optimize" || method=="both") {
    targetReturnsOptim <- targetReturnsMin
    maximumVarianceLocusOptim <- sapply(targetReturnsOptim, getObjective, returns = returns,
                                        solver = solver, min = FALSE)
  }
  
  if (method=="pairwise" || method=="both") {
    ## Choose all possible asset pairs
    comb <- t(combn(1:noAssets,2))
    
    ## Include edge points (assets) to the target returns
    targetReturns <- unique(c(targetReturnsMin, mu))
    targetReturnsPair <- targetReturns[order(targetReturns)]
    
    ## Create a matrix that can hold the results for all the asset pair portfolios
    pairRiskMat <- matrix(nrow=length(targetReturnsPair),ncol=nrow(comb))
    
    ## Calculate the risk for all the asset pair portfolios
    for (i in 1:nrow(comb)) {
      ## Choose the pair
      pairIds <- comb[i, ]
      
      ## Chose the range of available target returns
      minReward <- min(mu[pairIds])
      maxReward <- max(mu[pairIds])
      posReward <- (targetReturnsPair >= minReward) & (targetReturnsPair <= maxReward)
      pairRewards <- targetReturnsPair[posReward]
      
      ## Calculate the maximized risks for the available rewards
      pairRisk <- function(targetReturn) {
        w1 <- (targetReturn-mu[pairIds[2]])/(mu[pairIds[1]]-mu[pairIds[2]])
        w <- c(w1,1-w1)
        sqrt(t(w) %*% cov(returns[,pairIds]) %*% w)
      }
      pairRisks <- sapply(pairRewards, pairRisk)
      
      ## Add the pair portfolio line
      if (addPairLines) {lines(pairRisks, pairRewards, col = "lightgrey")}
      
      # Store risks
      pairRiskMat[posReward,i] <- pairRisks
    }
    
    ## Calculate the maximum variance locus
    maximumVarianceLocusPair <- apply(pairRiskMat,1,max,na.rm=TRUE)
  }
  
  ## Add the feasible set
  lines(minimumVarianceLocus, targetReturnsMin, col = "steelblue")
  points(minimumVarianceLocus, targetReturnsMin, col = "steelblue", pch =19, cex=0.5)
  
  if (!(method=="both")) {
    if (method=="optimize") {
      maximumVarianceLocus <- maximumVarianceLocusOptim
      targetReturnsMax <- targetReturnsOptim
    }
    if (method=="pairwise") {
      maximumVarianceLocus <- maximumVarianceLocusPair
      targetReturnsMax <- targetReturnsPair
    }
    lines(maximumVarianceLocus, targetReturnsMax, col = "lightgreen")
    points(maximumVarianceLocus, targetReturnsMax, col = "lightgreen", pch =19, cex=0.5)
  } else {
    lines(maximumVarianceLocusPair, targetReturnsPair, col = "lightgreen")
    points(maximumVarianceLocusPair, targetReturnsPair, col = "lightgreen", pch =19, cex=0.5)
    lines(maximumVarianceLocusOptim, targetReturnsOptim, col = "darkblue")
    points(maximumVarianceLocusOptim, targetReturnsOptim, col = "darkblue", pch =19, cex=0.5)
    maximumVarianceLocus <- maximumVarianceLocusPair
  }
}
