# Bash Cheatsheet (Basics)
# TODO Revise the whole sheet

random notes:
| Key/Command | Description  |
| ----------- | ------------ |
| #           | comment      |
| clear       | clear screen |
| ctrl + c    | exit operating task |
| charm .     | open pycharm in current directory |
| open .      | open current directory |

## Bash Shortcuts

| Shortcut           | Topic: Bash Shortcuts                 |
| :----------------: |: ------------------------------------------ | 
| Control + C        | Exit a running task                         |



Parts of this cheatsheet is inspired by the following Tutorial(s): 

<a  href="http://www.youtube.com/watch?feature=player_embedded&v=BFMyUgF6I8Y" target="_blank"><img src="http://img.youtube.com/vi/BFMyUgF6I8Y/0.jpg" alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

 - [Bash Tutorial](https://www.youtube.com/watch?v=BFMyUgF6I8Y/ "Bash Tutorial")


# Shift-Alt-7 for backslash
# .md for .markdown
TODO save current working directory in a variable
TODO start a py file from a variable given by a relative path

set echo off

echo "sdasdfasdf$0"
cd "$(dirname "$0")"
cd "${0%/*}"
dirname_exe="${0%/*}"
echo "aaaa $dirname"
echo "fddddd $0"
echo "ddddd "$0""

# Define a Variable
my_var="SomeText"
echo "some text $my_var"


my_var="content_of_my_var"                      # define a variable
echo "Printing $my_var" & echo $my_var          # printing a variable
echo $0                                         # print "/bin/bash" confirming where we are right now
set echo off                                    # deactivate echo
set echo on                                     # activate echo
pwd                                             # print working directory
ls                                              # list files in cwd
ls -al                                          # list long format including hidden files & showing permissions
mkdir new_folder                                # create a directory
rmdir new_folder                                # remove a directory
touch new_file                                  # create a file (with and without extension)
rm new_file                                     # remove a file
touch exec_file & chmod +x exec_file            # change permissions of file to be executable
touch file1.md & cp file1.md file1-copy.md      # copy a file
touch file2.md & mv file2.md file3.md           # rename files

################################
# VI: Text editor within the terminal
# to exit VI: esc & :q!
# VI keyword for insert is "i"
# to write and exit: esc :wq
################################

cat file1.md                                    # show the content of a file
cat file1.md file3.md                           # show the content of multiple files
clear                                           # clear the screen
# sudo shutdown -r now                          # abbreviation for "super user do"
#                                               # allow programs to be executed as a super user






## English Version

_Letters are shown capitalized for readability only._  _Capslock should be off._

------------

### SHORTCUTS

| Key/Command | Description |
| ----------- | ----------- |
| Ctrl + A   | Go to the beginning of the line you are currently typing on.  This also works for most text input fields system wide.  Netbeans being one exception |
| Ctrl + E   | Go to the end of the line you are currently typing on.  This also works for most text input fields system wide.  Netbeans being one exception |
| Ctrl + L   | Clears the Screen |
| Cmd + K    | Clears the Screen |
| Ctrl + U   | Cut everything backwards to beginning of line |
| Ctrl + K   | Cut everything forward to end of line |
| Ctrl + W   | Cut one word backwards using white space as delimiter |
| Ctrl + Y   | Paste whatever was cut by the last cut command |
| Ctrl + H   | Same as backspace |
| Ctrl + C   | Kill whatever you are running.  Also clears everything on current line |
| Ctrl + D   | Exit the current shell when no process is running, or send EOF to a the running process |
| Ctrl + Z   | Puts whatever you are running into a suspended background process. fg restores it |
| Ctrl + _   | Undo the last command. (Underscore.  So it's actually Ctrl + Shift + minus) |
| Ctrl + T   | Swap the last two characters before the cursor |
| Ctrl + F   | Move cursor one character forward |
| Ctrl + B   | Move cursor one character backward |
| Option + →  | Move cursor one word forward |
| Option + ←  | Move cursor one word backward |
| Esc + T  | Swap the last two words before the cursor |
| Esc + Backspace | Cut one word backwards using none alphabetic characters as delimiters |
| Tab  | Auto-complete files and folder names |

### CORE COMMANDS

| Key/Command | Description |
| ----------- | ----------- |
| cd [folder] | Change directory e.g. `cd Documents` |
| cd |  Home directory |
| cd ~ |  Home directory |
| cd /  | Root of drive |
| cd -  | Previous directory |
| ls | Short listing |
| ls -l | Long listing |
| ls -a | Listing incl. hidden files |
| ls -lh| Long listing with Human readable file sizes |
| ls -R | Entire content of folder recursively |
| sudo [command] | Run command with the security privileges of the superuser (Super User DO) |
| open [file] | Opens a file ( as if you double clicked it ) |
| top | Displays active processes. Press q to quit |
| nano [file] | Opens the file using the nano editor |
| vim [file] | Opens the file using the vim editor |
| clear |  Clears the screen |
| reset |  Resets the terminal display |

### CHAINING COMMANDS

| Key/Command | Description |
| ----------- | ----------- |
| [command-a]; [command-b] | Run command A and then B, regardless of success of A |
| [command-a] && [command-b] | Run command B if A succeeded |
| [command-a] \|\| [command-b] | Run command B if A failed |
| [command-a] & | Run command A in background |


### PIPING COMMANDS

| Key/Command | Description |
| ----------- | ----------- |
| [command-a] \| [command-b] | Run command A and then pass the result to command B e.g ps auxwww \| grep google |


### COMMAND HISTORY

| Key/Command | Description |
| ----------- | ----------- |
| history n |  Shows the stuff typed – add a number to limit the last n items |
| Ctrl + r  | Interactively search through previously typed commands |
| ![value] |  Execute the last command typed that starts with ‘value’ |
| ![value]:p |  Print to the console the last command typed that starts with ‘value’ |
| !! |  Execute the last command typed |
| !!:p |  Print to the console the last command typed |

### FILE MANAGEMENT

| Key/Command | Description |
| ----------- | ----------- |
| touch [file] |   Create a new file |
| pwd | Full path to working directory |
| . |  Current folder, e.g. `ls .` |
| .. | Parent/enclosing directory, e.g. `ls ..` |
| ls -l .. | Long listing of parent directory |
| cd ../../ | Move 2 levels up |
| cat | Concatenate to screen |
| rm [file] |  Remove a file, e.g. `rm data.tmp` |
| rm -i [file] | Remove with confirmation |
| rm -r [dir] | Remove a directory and contents |
| rm -f [file] | Force removal without confirmation |
| cp [file] [newfile] | Copy file to file |
| cp [file] [dir] | Copy file to directory |
| mv [file] [new filename] |  Move/Rename, e.g. `mv file1.ad /tmp` |
| pbcopy < [file] | Copies file contents to clipboard |
| pbpaste | Paste clipboard contents |
| pbpaste > [file] | Paste clipboard contents into file, `pbpaste > paste-test.txt` |

### DIRECTORY MANAGEMENT

| Key/Command | Description |
| ----------- | ----------- |
| mkdir [dir] | Create new directory |
| mkdir -p [dir]/[dir] |  Create nested directories |
| rmdir [dir] | Remove directory ( only operates on empty directories ) |
| rm -R [dir] | Remove directory and contents |
| less [file]|  Output file content delivered in screensize chunks |
| [command] > [file] |  Push output to file, keep in mind it will get overwritten |
| [command] >> [file] | Append output to existing file |
| [command] < [file] |  Tell command to read content from a file |

### SEARCH

| Key/Command | Description |
| ----------- | ----------- |
| find [dir] -name [search_pattern] | Search for files, e.g. `find /Users -name "file.txt"` |
| grep [search_pattern] [file] | Search for all lines that contain the pattern, e.g. `grep "Tom" file.txt` |
| grep -r [search_pattern] [dir] | Recursively search in all files in specified directory for all lines that contain the pattern |
| grep -v [search_pattern] [file] | Search for all lines that do NOT contain the pattern |
| grep -i [search_pattern] [file] | Search for all lines that contain the case-insensitive pattern |
| mdfind [search_pattern] | Spotlight search for files (names, content, other metadata), e.g. `mdfind skateboard` |
| mdfind -onlyin [dir] -name [pattern] | Spotlight search for files named like pattern in the given directory |

### HELP

| Key/Command | Description |
| ----------- | ----------- |
| [command] -h |  Offers help |
| [command] --help | Offers help |
| info [command] | Offers help |
| man [command] |  Show the help manual for [command] |
| whatis [command] | Gives a one-line description of [command] |
| apropos [search-pattern] | Searches for command with keywords in description |

### GIT

For Git related commmands you can check my [git-basics-cheatsheet](https://github.com/0nn0/git-basics-cheatsheet)


