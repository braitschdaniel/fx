# Run a python script from bash

- create an .exe file with

```bat
#!/bin/bash
set echo off
python2.7 "./test.py"
```

run *./file_name.exe* using a simple test file *test.py*, eg.

```python
print 1
```