# Markdown Cheatsheet
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

<!-- HTML Comment -->

## Paragraphs
Paragraph 1

Paragraph 2  
Linebreak is done with two spaces

## Character styles    
*Italic characters* 
_Italic characters_
**bold characters**
__bold characters__
~~strikethrough text~~

## Horizontal rule

---

Hyphens

***

Asterisks

___

Underscores

## Unordered list    
*  Item 1
*  Item 2
*  Item 3
    *  Item 3a
    *  Item 3b
    *  Item 3c

## Ordered list    
1.  Step 1
2.  Step 2
3.  Step 3
    1.  Step 3.1
    2.  Step 3.2
    3.  Step 3.3
    
## List in list
1.  Step 1
2.  Step 2
3.  Step 3
    *  Item 3a
	*  Item 3b
	*  Item 3c

## Quotes or citations    
Introducing my quote:

> Neque porro quisquam est qui 
> dolorem ipsum quia dolor sit amet, 
> consectetur, adipisci velit...

## Inline code characters    
Use the backtick to refer to a `function()`.  
There is a literal ``backtick (`)`` here.

## Links to external websites
This is [an example](http://www.example.com/) inline link.  
[This link](http://example.com/ "Title") has a title attribute.  
Links are also auto-detected in text: http://example.com/

## Images
Inline-style:  
![Alt text](./bitbucket.jpg)
![Alt text](./bitbucket.jpg "Optional hover text")

Reference-style:  
![alt text][logo]

[logo]: ./bitbucket.jpg "Optional hover text2"

## Tables

| My table           | Topic: Markdown Cheatsheet  |
| :----------------: | : -------------------------- | 
| Table entry        | Some text                    |

## Backslash escapes
#### Certain characters can be escaped with a preceding backslash to preserve the literal display of a character instead of its special Markdown meaning. This applies to the following characters:
- \\  backslash 
- \`  backtick 
- \*  asterisk 
- \_  underscore 
- \{} curly braces 
- \[] square brackets 
- \() parentheses 
- \#  hash mark 
- \>  greater than 
- \+  plus sign 
- \-  minus sign (hyphen) 
- \.  dot 
- \!  exclamation mark


## YouTube Videos
#### They can't be added directly but you can add an image with a link to the video like this:
<a  href="http://www.youtube.com/watch?feature=player_embedded&v=1La4QzGeaaQ" target="_blank"><img src="http://img.youtube.com/vi/1La4QzGeaaQ/0.jpg" alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

## Task Lists

* [x] Task1
* [x] Task2
* [ ] Task3

## Code blocks
#### Indent every line of the block by at least 4 spaces.
  
```python
def my_func(a, b):
    return a + b
``` 

```bat
cd my_folder
ls
``` 
#### Here's a full list of supported languages:

- Cucumber ('*.feature')
- abap ('*.abap')
- ada ('*.adb', '*.ads', '*.ada')
- ahk ('*.ahk', '*.ahkl')
- apacheconf ('.htaccess', 'apache.conf', 'apache2.conf')
- applescript ('*.applescript')
- as ('*.as')
- as3 ('*.as')
- asy ('*.asy')
- bash ('*.sh', '*.ksh', '*.bash', '*.ebuild', '*.eclass')
- bat ('*.bat', '*.cmd')
- befunge ('*.befunge')
- blitzmax ('*.bmx')
- boo ('*.boo')
- brainfuck ('*.bf', '*.b')
- c ('*.c', '*.h')
- cfm ('*.cfm', '*.cfml', '*.cfc')
- cheetah ('*.tmpl', '*.spt')
- cl ('*.cl', '*.lisp', '*.el')
- clojure ('*.clj', '*.cljs')
- cmake ('*.cmake', 'CMakeLists.txt')
- coffeescript ('*.coffee')
- console ('*.sh-session')
- control ('control')
- cpp ('*.cpp', '*.hpp', '*.c++', '*.h++', '*.cc', '*.hh', '*.cxx', '*.hxx', '*.pde')
- csharp ('*.cs')
- css ('*.css')
- cython ('*.pyx', '*.pxd', '*.pxi')
- d ('*.d', '*.di')
- delphi ('*.pas')
- diff ('*.diff', '*.patch')
- dpatch ('*.dpatch', '*.darcspatch')
- duel ('*.duel', '*.jbst')
- dylan ('*.dylan', '*.dyl')
- erb ('*.erb')
- erl ('*.erl-sh')
- erlang ('*.erl', '*.hrl')
- evoque ('*.evoque')
- factor ('*.factor')
- felix ('*.flx', '*.flxh')
- fortran ('*.f', '*.f90')
- gas ('*.s', '*.S')
- genshi ('*.kid')
- glsl ('*.vert', '*.frag', '*.geo')
- gnuplot ('*.plot', '*.plt')
- go ('*.go')
- groff ('*.(1234567)', '*.man')
- haml ('*.haml')
- haskell ('*.hs')
- html ('*.html', '*.htm', '*.xhtml', '*.xslt')
- hx ('*.hx')
- hybris ('*.hy', '*.hyb')
- ini ('*.ini', '*.cfg')
- io ('*.io')
- ioke ('*.ik')
- irc ('*.weechatlog')
- jade ('*.jade')
- java ('*.java')
- js ('*.js')
- jsp ('*.jsp')
- lhs ('*.lhs')
- llvm ('*.ll')
- logtalk ('*.lgt')
- lua ('*.lua', '*.wlua')
- make ('*.mak', 'Makefile', 'makefile', 'Makefile.*', 'GNUmakefile')
- mako ('*.mao')
- maql ('*.maql')
- mason ('*.mhtml', '*.mc', '*.mi', 'autohandler', 'dhandler')
- markdown ('*.md')
- modelica ('*.mo')
- modula2 ('*.def', '*.mod')
- moocode ('*.moo')
- mupad ('*.mu')
- mxml ('*.mxml')
- myghty ('*.myt', 'autodelegate')
- nasm ('*.asm', '*.ASM')
- newspeak ('*.ns2')
- objdump ('*.objdump')
- objectivec ('*.m')
- objectivej ('*.j')
- ocaml ('*.ml', '*.mli', '*.mll', '*.mly')
- ooc ('*.ooc')
- perl ('*.pl', '*.pm')
- php ('*.php', '*.php(345)')
- postscript ('*.ps', '*.eps')
- pot ('*.pot', '*.po')
- pov ('*.pov', '*.inc')
- prolog ('*.prolog', '*.pro', '*.pl')
- properties ('*.properties')
- protobuf ('*.proto')
- py3tb ('*.py3tb')
- pytb ('*.pytb')
- python ('*.py', '*.pyw', '*.sc', 'SConstruct', 'SConscript', '*.tac')
- rb ('*.rb', '*.rbw', 'Rakefile', '*.rake', '*.gemspec', '*.rbx', '*.duby')
- rconsole ('*.Rout')
- rebol ('*.r', '*.r3')
- redcode ('*.cw')
- rhtml ('*.rhtml')
- rst ('*.rst', '*.rest')
- sass ('*.sass')
- scala ('*.scala')
- scaml ('*.scaml')
- scheme ('*.scm')
- scss ('*.scss')
- smalltalk ('*.st')
- smarty ('*.tpl')
- sourceslist ('sources.list')
- splus ('*.S', '*.R')
- sql ('*.sql')
- sqlite3 ('*.sqlite3-console')
- squidconf ('squid.conf')
- ssp ('*.ssp')
- tcl ('*.tcl')
- tcsh ('*.tcsh', '*.csh')
- tex ('*.tex', '*.aux', '*.toc')
- text ('*.txt')
- v ('*.v', '*.sv')
- vala ('*.vala', '*.vapi')
- vbnet ('*.vb', '*.bas')
- velocity ('*.vm', '*.fhtml')
- vim ('*.vim', '.vimrc')
- xml ('*.xml', '*.xsl', '*.rss', '*.xslt', '*.xsd', '*.wsdl')
- xquery ('*.xqy', '*.xquery')
- xslt ('*.xsl', '*.xslt')
- yaml ('*.yaml', '*.yml')