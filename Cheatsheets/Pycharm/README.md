# Shortcuts: Pycharm for Mac 

| Shortcut           | Topic: Shortcuts: Pycharm for Mac           |
| :----------------: | : ----------------------------------------- |
| ctrl + tab         |  Switch between different tabs              |                               
| cmd + 1            |  Project                                    |           
| cmd + 7            |  Structure                                  |                               
| ctrl + tab         |  Switch between different tabs              |                               
| ctrl + tab         |  Switch between different tabs              |                               
| ctrl + tab         |  Switch between different tabs              |

# HEADER

```python
"""______________________________________________
MODULE 
    ${FILE_NAME}

PROJECT
    ${PROJECT_NAME}

DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    ${DATE} Daniel Braitsch"""
```