## Excel Shortcuts for MS

f4 used for what in excel????

| Shortcut           | Topic: Excel Shortcuts                      |
| :----------------- |: ------------------------------------------ | 
| Ctrl + move a tab  | copy a tab                                  |                               
| with Ctrl & Shift  | select multiple tabs                        |
| with Ctrl & Shift  | select multiple columns                     |
| with Ctrl & Shift  | select multiple rows                        |      
| Ctrl + +           | add cells                                   |                         
| Ctrl + -           | remove cells                                |
| Ctrl + A           | select the whole content of the tab         |
| Ctrl + Home        | select cell A1                              |
| Ctrl + End         | select the last cell with content           |
| Ctrl + PgUp/PgDn   | switch between tabs                         |
| Esc                | undo the current input in the active cell   |
| F2                 | go to the end of a cell                     |
| F2 + Home          | go to the beginning of a cell               |
| Ctrl + Enter       | write into multiple selected lines          |
| Alt + Enter        | create a new line in a cell                 |
| Alt + H + E + F    | clear formats                               |
| Ctrl + 1           | format cells                                |
| Ctrl + 2           | bold                                        |
| Ctrl + 3           | italic                                      |
| Ctrl + 9           | hide the selected rows                      |
| Ctrl + 0           | hide the selected columns                   |
|   |                                   |
|   |                                   |
|   |                                   |