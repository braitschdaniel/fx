https://macreports.com/record-face-screen-mac/

How To Record Your Face and Screen On Your Mac

1-Launch QuickTime player

2-Select File > New Movie Recording

3-Select View > Float on top so that your camera window will be on top of any other application anywhere on your screen.

4-Now you can resize the camera window. You can move this camera view anywhere you want (left, right, top, bottom etc).

5-Now select File > New Screen Recording


You can change some settings by clicking the arrow next to the Record button.
You can record your full screen or part of your Mac screen.
You may want to arrange your screen (where will your camera be etc?) before you start your recording.

6-When you are ready to start recording, click the red record button.
The you have two options:
(a) if you want to record your entire screen, click anywhere on your screen to start the actual recording
(b) or you may drag to select an area.

7-Now you are recording your screen and your camera view (e.g. your face).
When you done recording you may press the Command-Control-Esc keys.
After you done, you may also edit your video using Quicktime.
For instance, you may go to QuickTime > Edit  > Trim to remove the unwanted parts of your video.





To capture a screenshot of the entire display:
Tap Shift + Command + 3.
Find the picture on your Mac’s desktop.

To capture just a part of the screen on your Mac:
Tap Shift + Command + 4.
Find the picture on your Mac’s desktop

To record a video on your Mac:
Open QuickTime
Tap “File” at the top of the display.
Choose “New Screen recording”
Hit record.
You can record the whole screen, or click and drag your mouse cursor to record just a specific part.
