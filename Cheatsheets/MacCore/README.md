## Mac Core Shortcuts

<!-- proxy server hola vpn chrome -->
<!-- 0102_forwarde: explanation of fwd aso -->

TO GOOGLE AGAIN:  
HOW TO DRAG THE BACKGROUND WINDOW  
WHAT IS FORCE TOUCHING?  
cmd + shift +.4 space ctrl +            Regoogle enter Screenshot to clipboard  
cmd + shift +.4 space                   Regoogle enter Screenshot with shadow  
cmd + shift +.4 space option +          Regoogle enter Screenshot without shadow



| Shortcut           | Topic: General MAC Shortcuts                |
| :----------------: |: ------------------------------------------ | 
| cmd + w            | Fermer un onglet                            |                               
| cmd + shift + .    | Hide/show invisible files                   | 
| cmd + spacebar     | Search                                      |             
| cmd + +/-          | Zoom in/out                                 | 
| cmd + opt +        | Delete Delete permanently                   | 
| fn + up/down       | Go up/down one page                         | 
| opt + delete       | Delete the entire word to the left          | 
| cmd + left/right   | Go to beginning/end of line                 |

# Safari Shortcuts

TODO Revise the whole section

| Shortcut           | Topic: Safari      Shortcuts                |
| :----------------: |: ------------------------------------------ |
| cmd + l            | Ouvrir une adresse                          |
| opt + cmd + .1-9   | Open favourites                             |
| cmd + d            | Ajouter un signet                           |
| shift + cmd + n    | Nouvelle fenêtre privée                     |
| cmd + w            | Fermer un onglet                            |
| cmd + y            | Afficher toute l'histoire                   |
| cmd + arroq        | Page précédente, suivante                   |
| cmd + shift + h    | Accueil                                     |
| cmd + shift + t    | Rouvrir la dernière fenêtre fermée          |