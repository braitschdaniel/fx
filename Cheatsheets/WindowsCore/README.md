# Windows Core Commands

### SHORTCUTS

| Key/Command             | Description                                                                                                             |
| ----------------------- | ---------------------------------------- |
| Alt + Print             | Copy a screenshot to the clipboard       |
| Ctrl + F4               | Close a tab                              |

### COMMAND WINDOW

| Key/Command             | Description                                                                                                             |
| ----------------------- | ---------------------------------------- |
| Ctrl + C                | Exit a running task                      |

### Modify User Account Control Settings temporarily

![UserAccountControlSettings](./UserAccountControlSettings.jpg)
