# Start incoming AMBA from command line and insert example message

```batch
"C:\Program Files\Front\Front Arena\AMBA\AMBA_FX_IN\amba.exe" -server frontarena:9000 -username ARENASYS -password intas -message_filename ExampleMessage.txt -log_messages 1 -log_db_writes 1 
REM -ael_module_name MessageAdaptations -ael_receiver_modify receiver_modify
Pause
```

# Start preconfigured AMBA from command line and insert example message

```batch
"C:\Program Files\Front\Front Arena\AMBA\AMBA_FX_IN\amba.exe" -server frontarena:9000 -username ARENASYS -password intas -instance_name AMBA_FX_IN -message_filename ExampleMessage.txt
Pause
```

# Regedit AMBA_FX_IN

| Name                  | Data                        |
| : ------------------- | : ------------------------- | 
| add_records           | {trade}                     |
| ael_module_name       | Message_Adaptations         |
| ael_receiver_modify   | receiver_modify             |
| amb_username          | mb                          |
| message_broker        | localhost:9100              |
| password              | intas                       |
| sender_mb_name        | -                           |
| sender_source         | -                           |
| fcs_suggest_name_file | path to suggestname.ttt     |
| receiver_off          | 0                           |
| sender_off            | 1                           |
| receiver_mb_name      | AMBA_FX_IN_RECEIVER         |
| receiver_sources      | {AFG_FX_IN_SENDER}          |