"""______________________________________________
MODULE 
    FAMBAMessageGenerator.py

PROJECT
    local_repo
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    21.02.2020 Daniel Braitsch
______________________________________________"""


import acm
import amb

trdnbr = 20034
trade = acm.FTrade[trdnbr]

gen = acm.FAMBAMessageGenerator()

# create AMBA message
gen.IncludeRefRecords("{{trade, insaddr}}")
gen.AddRefFields("{{leg, pay_calnbr, calendar, business_center}}")
gen.AddFields("{{leg, rolling_convention}, {leg, fixed_rate}, {leg, reset_day_offset}, {trade, updat_usrnbr}}")
# gen.ShowAllRef(1)
# gen.ShowAllFields(1)
message = gen.Generate(trade)
print message

# convert AMBA message to xml
message = str(message)
mbf_buffer = amb.mbf_create_buffer()
mbf_buffer.mbf_append_buffer(message, len(message))
mbf_object = mbf_buffer.mbf_read()
message_xml = mbf_object.mbf_object_to_string_xml()
# print message_xml
