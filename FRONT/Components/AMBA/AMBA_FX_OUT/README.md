# Regedit AMBA_FX_OUT

#### optional settings to disable the AMBA to write into AMB and ADS, resp. 
- db_writes_off
- mb_writes_off

| Name                | Data                        |
| : ----------------- | : ------------------------- | 
| add_fields          | {{trade, mirror_trdnbr}}    |
| add_records         | {trade}                     |
| ael_module_name     | Message_Adaptations         |
| ael_sender_modify   | sender_modify               |
| add_ref_fields 	  | See table below             |
| amb_username        | mb                          |
| include_ref_records | {{trade,insaddr}}           |
| message_broker      | localhost:9100              |
| password            | intas                       |
| remove_fields       | {{trade, base_cost_dirty}}  |
| sender_mb_name      | AMBA_FX_OUT_SENDER          |
| sender_source       | AMBA_FX_OUT_SENDER          |
| receiver_off        | 1                           |
| sender_off          | 0                           |
| receiver_mb_name    | -                           |
| receiver_sources    | -                           |

### add_ref_fields 

- {{trade, correction_trdnbr, trade, time},{trade, correction_trdnbr, trade, trdnbr},{trade, correction_trdnbr, trade, text1},{trade, mirror_trdnbr, trade, trdnbr},{trade, counterparty_ptynbr, party, type},{leg, pay_calnbr, calendar, business_center},{leg, pay2_calnbr, calendar, business_center},{leg, pay3_calnbr, calendar, business_center}}
}	
- {	
    - {trade, correction_trdnbr, trade, time}, 
    - {trade, correction_trdnbr, trade, trdnbr},
    - {trade, correction_trdnbr, trade, text1},
    - {trade, mirror_trdnbr, trade, trdnbr},
    - {trade, counterparty_ptynbr, party, type},
    - {leg, pay_calnbr, calendar, business_center}
    - {leg, pa2_calnbr, calendar, business_center}
    - {leg, pay3_calnbr, calendar, business_center}
    - }
    
    
    