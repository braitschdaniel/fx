# General notes: AMBA

#### File FAMBAMessageGenerator.py shows AMBA messages in acm

### Start incoming AMBA from command line and insert example message

```batch
"C:\Program Files\Front\Front Arena\AMBA\AMBA_FX_IN\amba.exe" -server frontarena:9000 -username ARENASYS -password intas -message_filename ExampleMessage.txt -log_messages 1 -log_db_writes 1 
REM -ael_module_name MessageAdaptations -ael_receiver_modify receiver_modify
Pause
```

### Start preconfigured AMBA from command line and insert example message

```batch
"C:\Program Files\Front\Front Arena\AMBA\AMBA_FX_IN\amba.exe" -server frontarena:9000 -username ARENASYS -password intas -instance_name AMBA_FX_IN -message_filename ExampleMessage.txt
Pause
```