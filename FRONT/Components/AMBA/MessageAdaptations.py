"""______________________________________________
MODULE
    MessageAdaptations.py

DESCRIPTION
    Template to create AMBA hooks

______________________________________________"""

import acm
import amb
import FLogger
import FBDPCommon

log = FLogger.FLogger(level=2, logOnce=False, logToConsole=True, logToPrime=False)


def wrap_with_transaction(msg):
    new_msg = amb.mbf_start_list("MESSAGE")
    transaction = amb.mbf_start_list("TRANSACTION")

    obj = msg.mbf_first_object()
    while obj:
        # Only sub-lists are adapted for the transaction
        # Top-level tags are kept as message attributes
        if obj.mbf_is_list():
            transaction.mbf_insert_object(obj)
        else:
            new_msg.mbf_insert_object(obj)
        obj = msg.mbf_next_object()

    new_msg.mbf_insert_object(transaction)
    return new_msg


def get_tag_value(mbf_object, tag):
    """Get value of tag in certain mbf_object"""
    if mbf_object:
        object = mbf_object.mbf_find_object(tag, 'MBFE_BEGINNING')
        if object:
            return object.mbf_get_value()


def set_tag_value(mbf_object, tag_name, tag_value):
    """Set value of tag in certain mbf_object"""
    clean_tag_value = str(tag_value)
    tag = mbf_object.mbf_find_object(tag_name, "MBF_BEGINNING")
    if tag:
        mbf_object.mbf_replace_string(tag_name, clean_tag_value)
    else:
        mbf_object.mbf_last_object()
        mbf_object.mbf_add_string(tag_name, clean_tag_value)


def set_premium_and_quantity(trd):
    """
    Fx trades are not rounded correctly when inflowing via AMBA.
    The AMBA message provided my the AFG contains either a flat quantity or a flat premium.
    The other one (premium or quantity) is rounded according to the rounding specification of the trade.
    """
    premium_curr = get_tag_value(trd, "CURR.INSID")
    quantity_curr = get_tag_value(trd, "INSADDR.INSID")
    if premium_curr and quantity_curr:
        premium_curr = acm.FCurrency[premium_curr]
        quantity_curr = acm.FCurrency[quantity_curr]
        if premium_curr and quantity_curr:
            roundingFunction = acm.GetFunction("round", 3)

            premium = get_tag_value(trd, "PREMIUM")
            price = get_tag_value(trd, "PRICE")
            quantity = get_tag_value(trd, "QUANTITY")
            if quantity:
                rounding = FBDPCommon.getPremiumRounding(premium_curr)
                if not rounding:
                    log.warn("Rounding not set for currency {premium_curr}, quantities are thus not rounded.".format(premium_curr=premium_curr.Name()))
                else:
                    premium = roundingFunction(-1 * float(quantity) * float(price), rounding.Decimals(), rounding.Type())
                    set_tag_value(trd, "PREMIUM", premium)
            elif premium:
                rounding = FBDPCommon.getPremiumRounding(quantity_curr)
                if not rounding:
                    log.warn("Rounding not set for currency {quantity_curr}, quantities are thus not rounded.".format(quantity_curr=quantity_curr.Name()))
                else:
                    quantity = roundingFunction(-1 * float(premium) / float(price), rounding.Decimals(), rounding.Type())
                    set_tag_value(trd, "QUANTITY", quantity)
                    set_tag_value(trd, "QUANTITY_IS_DERIVED", "Yes")


def receiver_modify(msg):
    msg_type = get_tag_value(msg, "TYPE")
    if msg_type and msg_type not in ("INSERT_INSTRUMENT", "INSERT_TRADE"):
        return msg
    # distinguish between top level tags instrument or trade
    list_ins = []
    list_trd = []
    # loop over top-level instrument tags and subsequent trade tags
    ins = msg.mbf_find_object("Instrument")
    while ins and ins.mbf_get_value() == "Instrument":
        list_ins.append(ins)
        trd = ins.mbf_find_object("trade")
        print trd.mbf_get_value()
        while trd and trd.mbf_get_value() == "trade":
            list_trd.append(trd)
            trd = ins.mbf_next_object()
        ins = msg.mbf_next_object()
    # loop over top-level trade tags
    trd = msg.mbf_find_object("Trade")
    while trd and trd.mbf_get_value() == "Trade":
        list_trd.append(trd)
        trd.mbf_find_object("Trade")
        trd = msg.mbf_next_object()
    for ins in list_ins:
        # only top-level instrument tags are treated
        pass
        """
        mapping example for ins
        otc_flag = get_tag_value(ins, "OTC")
        if otc_flag:
            set_tag_value(ins, "OTC", "No")
        """
    for trd in list_trd:
        # Set Premium and Quantity for FxSpot, FxFwd, FxSwap
        # TODO Set Premium and Quantity for NDF
        trade_process = get_tag_value(trd, "TRADE_PROCESS")
        if trade_process and int(trade_process) in (4096, 8192, 16384, 32768):
            set_premium_and_quantity(trd)
        # create mirror trade if counterparty has class "Internal Department"
        cp = get_tag_value(trd, "COUNTERPARTY_PTYNBR.PTYID")
        if cp and acm.FParty[cp].Class() == acm.FInternalDepartment:
            set_tag_value(trd, "COUNTERPARTY_PORTFOLIO.PRFID", "DEFAULT POOL")
        # default trader if not found in FA
        actual_trader = get_tag_value(trd, "TRADER_USRNBR.USERID")
        if actual_trader and not acm.FUser[actual_trader]:
            default_trader = "ARENASYS"
            set_tag_value(trd, "TRADER_USRNBR.USERID", default_trader)
            log.warn("Trader {actual_trader} not found, trader is defaulted to {default_trader}.".format(
                actual_trader=actual_trader, default_trader=default_trader))

    return wrap_with_transaction(msg)


def sender_modify(msg, subj):
    message_type = get_tag_value(msg, "TYPE")
    if not message_type or message_type == "DELETE_TRADE":
        return msg, subj
    # Customise this part in the AFG using the command local2utcTime
    local_time = get_tag_value(msg, "TIME")
    if local_time:
        utc_time = acm.Time.LocalToUtc(local_time)
        set_tag_value(msg, "TIME", utc_time)
    trd = msg.mbf_find_object("TRADE", 'MBFE_BEGINNING')
    if trd:
        mirror_trdnbr = get_tag_value(trd, "MIRROR_TRDNBR")
        if mirror_trdnbr:
            set_tag_value(trd, "MIRROR_TRDNBR.TRDNBR", mirror_trdnbr)
        # enrich AMBA message with addInfo field RoutingType
        # TODO test if callable in the AFG
        add_info_routing_type = trd.mbf_start_list("ADDITIONALINFO")
        trade = amb.mbf_start_list("TRADE")
        trade.mbf_insert_object(add_info_routing_type)
        routing_type = "FIX_OMS_ACCEPTOR"
        trdnbr = get_tag_value(trd, "TRDNBR")
        set_tag_value(add_info_routing_type, "ADDINF_SPECNBR.FIELD_NAME", "RoutingType")
        set_tag_value(add_info_routing_type, "VALUE", routing_type)
        log.info("Trade {trdnbr} is routed to the flow '{routing_type}'.".format(routing_type=routing_type, trdnbr=trdnbr))

    return msg, subj


message_string = """[MESSAGE]
  TYPE=INSERT_TRADE
  VERSION=1.0
  TIME=2020-03-29 17:16:27
  SOURCE=AFG_FX_IN_SENDER
  [Instrument]
    instype=INS_FUTURE
    curr.insid=USD
    mtm_from_feed=No
    contr_size=1
    und_insaddr.insid=EUR
    und_instype=INS_CURR
    settlement=SETTLEMENT_CASH
    paytype=PAY_FORWARD
    pay_day_offset=2
    pay_offset_method=DATEPERIOD_CALENDAR_DAYS
    quotation_seqnbr.name=Per Unit
    fixing_source_ptynbr.ptyid=New York 10am
    [trade]
      prfnbr.prfid=DEFAULT_POOL
      acquire_day=20200329
      acquirer_ptynbr.ptyid=DEFAULT_POOL_OWNER
      curr.insid=USD
      value_day=20200329
      time=20200329 17:16:27 UTC
      quantity=-1000000.000000
      price=1.181900
      status=TRADE_STATUS_EXCHANGE
      counterparty_ptynbr.ptyid=Reuters
      trader_usrnbr.userid=ARENASYS12
      optional_key=UniqueNDF21ExecID
      market_ptynbr.ptyid=FIX
      reference_price=1.1619
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=ClOrdID
        value=MyClOrdId
      [/!ADDITIONALINFO]
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=OrderID
        value=MyOrderId
      [/!ADDITIONALINFO]
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=ExecutingSystem
        value=FIX_OMS
      [/!ADDITIONALINFO]
    [/trade]
  [/Instrument]
    [Instrument]
    instype=INS_FUTURE
    curr.insid=USD
    mtm_from_feed=No
    contr_size=1
    und_insaddr.insid=EUR
    und_instype=INS_CURR
    settlement=SETTLEMENT_CASH
    paytype=PAY_FORWARD
    pay_day_offset=2
    pay_offset_method=DATEPERIOD_CALENDAR_DAYS
    quotation_seqnbr.name=Per Unit
    fixing_source_ptynbr.ptyid=New York 10am
    [trade]
      prfnbr.prfid=DEFAULT_POOL
      acquire_day=20200329
      acquirer_ptynbr.ptyid=DEFAULT_POOL_OWNER
      curr.insid=USD
      value_day=20200329
      time=20200329 17:16:27 UTC
      quantity=-1000000.000000
      price=1.181900
      status=TRADE_STATUS_EXCHANGE
      counterparty_ptynbr.ptyid=Reuters
      trader_usrnbr.userid=ARENASYS12
      optional_key=UniqueNDF21ExecID
      market_ptynbr.ptyid=FIX
      reference_price=1.1619
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=ClOrdID
        value=MyClOrdId
      [/!ADDITIONALINFO]
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=OrderID
        value=MyOrderId
      [/!ADDITIONALINFO]
      [!ADDITIONALINFO]
        addinf_specnbr.field_name=ExecutingSystem
        value=FIX_OMS
      [/!ADDITIONALINFO]
    [/trade]
  [/Instrument]
[/MESSAGE]"""


def main(message_string):
    """Parse string into MBF object"""
    amba_buffer = amb.mbf_create_buffer_from_data(message_string)
    msg = amba_buffer.mbf_read()
    msg = receiver_modify(msg)
    # msg, _ = sender_modify(msg, None)
    print msg.mbf_object_to_string()


main(message_string)
