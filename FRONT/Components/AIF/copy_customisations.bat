@ECHO OFF

REM SET AFG_FOLDER=\\imsas01078\e$\Front Arena\AFG
SET AFG_FOLDER=C:\Program Files\Front\Front Arena\AIF

REM XCOPY parameters
REM /e - copy subdirectories (even if empty)
REM /y - confirm all overwrites
REM /c - continue on error

XCOPY Customisations\AFG_FX_IN "%AFG_FOLDER%\AFG_FX_IN" /e /y /c
XCOPY Customisations\Shared "%AFG_FOLDER%\AFG_FX_IN" /e /y /c 

XCOPY Customisations\AFG_FX_OUT "%AFG_FOLDER%\AFG_FX_OUT" /e /y /c
XCOPY Customisations\Shared "%AFG_FOLDER%\AFG_FX_OUT" /e /y /c 