#define AFG_NOT_RELEVANT 0
#define AFG_FX_SPOT 1
#define AFG_FX_FWD 2
#define AFG_FX_SWAP 3
#define AFG_FX_NDF 4
#define AFG_FX_OPTION 5
#define AFG_MM_DEPOSIT 6
#define AFG_STOCK 7

/*  Trade process flags as defined in the trade process information prime help page */
#define TP_FX_SPLIT_CHILD 1 // 1<<0
#define TP_FX_SPLIT_PARENT 2 // 1<<1
#define TP_MM_SPLIT_CHILD 4 // 1<<2
#define TP_MM_SPLIT_PARENT 8 // 1<<3
#define TP_SPOT_COVER_CHILD 16 // 1<<4
#define TP_SPOT_COVER_PARENT 32 // 1<<5
#define TP_SALES_COVER_CHILD 64 // 1<<6
#define TP_SALES_COVER_PARENT 128 // 1<<7
#define TP_PROLONG_CHILD 254 // 1<<8
#define TP_PROLONG_PARENT 512 // 1<<9
/* 1<<10 unused */
#define TP_PREMIUM_PARENT 2048 // 1<<11
#define TP_FX_SPOT 4096 // 1<<12
#define TP_FX_FORWARD 8192 // 1<<13
#define TP_SWAP_NEAR_LEG 16384 // 1<<14
#define TP_SWAP_FAR_LEG 32768 // 1<<15
#define TP_REVERSAL 65536 // 1<<16
#define TP_SPOT_COVER_INS_CCY 131072 // 1<<17
#define TP_HISTORIC_PRICING 262144 // 1<<18
#define TP_SCM_BACK_2_BACK 524288  // 1<<19
#define TP_GROUP_PARENT 1048576 // 1<<20
#define TP_PREMIUM_CHILD 2097152 // 1<<21
/* 1<<22 to 1<<25 unused */
#define TP_INSIDE_CLS 67108864 // 1<<26
#define TP_OUTSIDE_CLS 134217728 // 1<<27
#define TP_DRAWDOWN_OFFSET 268435456 // 1<<28
#define TP_DRAWDOWN_CHILD 536870912 // 1<<29
#define TP_SWAP_TO_SPOT_CHILD 1073741824 // 1<<30


#define IsSecondaryMirrorTrade() \
    /* Filter for automatically created secondary mirror trades */ \
    (trade.trdnbr == trade.mirror_trdnbr.trdnbr) \


#define IsNotCorrected() \
    /* Filter used for voided trades that are not created a CANCEL/NEW */ \
    (trade.trdnbr != trade.correction_trdnbr.trdnbr) \


#define SetSide(trd) \
    /* Set Side of the trade */ \
    if (fieldDividedByFieldFloat(trd.quantity, abs(trd.quantity)) == "1") { \
        Side 1 \
    } else if (fieldDividedByFieldFloat(trd.quantity, abs(trd.quantity)) == "-1") { \
        Side 2 \
    } \


#define GetAddInfoByFieldName(field_name, field_val) \
    /* Get Additional Info by Field Name */ \
    field_val = "" \
    for (i=0; trade.ADDITIONALINFO[i].ADDINF_SPECNBR.FIELD_NAME != ""; i++) { \
        if (trade.ADDITIONALINFO[i].ADDINF_SPECNBR.FIELD_NAME == field_name) { \
            field_val = trade.ADDITIONALINFO[i].VALUE \
        } \
    } \


#define SetPartyOutgoing(index, party_role, party_id, party_id_source) \
    /* Add party info to message */ \
    PartyID[index] party_id \
    PartyIDSource[index] party_id_source \
    PartyRole[index] party_role \


#define SetBusinessCenterOutgoing(index, business_center) \
    /* Add business center to message */ \
    BusinessCenter[index] business_center \


#define SetExecTypeMappingOutgoing(trade_no, correct_trade_no) \
    /* Exec Type Mapping for outgoing messages */ \
    /* Cancel/Modify/New */ \
    if (trade.status == "TRADE_STATUS_VOID") { \
        ExecType "H" \
        ExecID append(trade_no, "_CANCEL") \
        ExecRefID trade_no \
        ClOrdID trade_no \
        OrderID trade_no \
    } else if (ifexist(correct_trade_no)) { \
        ExecType "G" \
        ExecID trade_no \
        ExecRefID correct_trade_no \
        ClOrdID trade_no \
        OrderID trade_no \
    } else { \
        ExecType "F" \
        ExecID trade_no \
        ClOrdID trade_no \
        OrderID trade_no \
    } \


#define SetCommonFixTradeFieldsOutgoing() \
    /* Fills FIX message fields common to most transactions for outgoing messages */ \
    GetAddInfoByFieldName("Account", trade_account) \
    if (ifexist(trade_account)) { \
        Account trade_account \
    } else { \
        Account "Default Account" \
    } \
    Price "" \
    Text trade.text1 \
    local_time = keepfirst(replace(replace(replace(trade.time, "-", ""), ":", ""), " ", ""), 16) \
    utc_time = tnp2fixtime(local_time, "Local2UTC") \
    trade_date = keepfirst(utc_time, 8) \
    TradeDate trade_date \
    TransactTime utc_time \
    SenderSubID trade.trader_usrnbr.userid \
    SendingTime utc_time \
    /* Fill in party information */ \
    NoPartyIDs 6 \
    SetPartyOutgoing(0, 1, trade.acquirer_ptynbr.ptyid, "D") \
    SetPartyOutgoing(1, 3, trade.counterparty_ptynbr.ptyid, "D") \
    SetPartyOutgoing(2, 11, trade.trader_usrnbr.userid, "D") \
    SetPartyOutgoing(3, 16, "FA", "D") \
    SetPartyOutgoing(4, 38, trade.prfnbr.prfid, "D") \
    SetPartyOutgoing(5, 75, "GVA", "D") \


#define GetPartyIdByPartyRoleIncoming(party_role, party_id) \
    /* GetPartyIdByPartyRole */ \
    party_id = "" \
    for (i=0; i < NoPartyIDs; i++) { \
        if (PartyRole[i] == party_role) { \
        party_id = PartyID[i] \
        } \
    } \


/*
 * Remove fields which are filled with dummy values (eg. "MISSING") by the AFG
 * and can thus never be correct. Will by applied to all messages.
 */
#define RemoveTradeDefaults() \
    /* Remove fields filled with dummy values by AFG standard */ \
    trade.account "" \
    trade.companyid "" \
    trade.free "" \
    trade.market_ptynbr.ptyid "" \
    trade.marketaccount "" \
    trade.net_premium "" \
    trade.reference "" \


/*
 * By default generates an AMBA TRADE message. For instrument bases transactions
 * (eg. FX Options, Deposits, NDFs, ...) we want to create an AMBA INSTRUMENT message
 * with a nested trade object. For this reason, we delete all top-level TRADE fields
 * which are normally set by the AFG default implementation.
 */

#define RemoveAllDefaultFieldsFromTrade(trd) \
    /* Remove all fields filled by AFG standard */ \
    trd.account "" \
    trd.baseid "" \
    trd.companyid "" \
    trd.counterparty_ptynbr.ptyid "" \
    trd.curr.insid "" \
    trd.free "" \
    trd.insaddr.insid "" \
    trd.optional_key "" \
    trd.marketaccount "" \
    trd.net_premium "" \
    trd.price "" \
    trd.quantity "" \
    trd.reference "" \
    trd.status "" \
    trd.text2 "" \
    trd.time "" \


#define RemoveExecutionReportDefaults() \
    /* Remove all Execution Report default fields */ \
    AvgPx "" \
    CumQty "" \
    Currency "" \
    ExecType "" \
    LastPx "" \
    LastQty "" \
    LeavesQty "" \
    OrderQty "" \
    OrdStatus "" \
    Price "" \
    SecurityId "" \
    SecurityIDSource "" \
    Side "" \
    Symbol "" \


#define RemoveBusinessMessageDefaults() \
    /* Remove all Business Message default fields */ \
    BusinessRejectReason "" \
    RefMsgType "" \


#define SetQuantityIncoming(trd, trx_id, trd_quantity) \
    /* Set quantity of incoming trades depending on the Side */ \
     if ((trx_id == 1) && (IsFxSwap() || IsNDS())) { \
        if (Side == 1) { \
            trd_quantity =  mul(-1, LastQty) \
        } else if (Side == 2) { \
            trd_quantity = LastQty \
        } \
    } else { \
        if (Side == 1) { \
        trd_quantity = LastQty \
        } else if (Side == 2) { \
            trd_quantity = mul(-1, LastQty) \
        } \
    } \


#define SetExecTypeMappingIncoming(trd, trx_id, nbr_trd, exec_id, exec_ref_id) \
    /* Exec Type mapping for incoming trades*/ \
    /* TODO Life Cycling in ADS in Transaction History for those trades */ \
    if (ExecType == "F") { \
        /* Insert trade */ \
        Type "INSERT_TRADE" \
        if (trx_id == 0) { \
            trd[trx_id].optional_key exec_id \
        } else if (trx_id == 1) { \
            trd[trx_id].connected_trdnbr.optional_key exec_id \
            trd[trx_id].optional_key append(exec_id, "F") \
        } \
        trd.status "TRADE_STATUS_EXCHANGE" \
    } else if (ExecType == "H") { \
        /* Cancel trade */ \
        Type "UPDATE_TRADE" \
        if (trx_id == 0) { \
            trd[trx_id].optional_key exec_ref_id \
        } else if (trx_id == 1) { \
            trd[trx_id].connected_trdnbr.optional_key exec_ref_id \
            trd[trx_id].optional_key append(exec_ref_id, "F") \
        } \
        trd[trx_id].status "TRADE_STATUS_VOID" \
    } else if (ExecType == "G") { \
        /* Modify trade */ \
        Type "INSERT_TRADE" \
        if (trx_id == 0) { \
            trd[trx_id].correction_trdnbr.optional_key exec_ref_id \
            trd[trx_id].optional_key exec_id \
        } else if (trx_id == 1) { \
            trd[trx_id].connected_trdnbr.optional_key exec_id \
            trd[trx_id].correction_trdnbr.optional_key append(exec_ref_id, "F") \
            trd[trx_id].optional_key append(exec_id, "F") \
        } \
        trd.status "TRADE_STATUS_EXCHANGE" \
        trd[trx_id].text1 exec_ref_id \
        mod_trx_id = add(trx_id, nbr_trd) \
        if (trx_id == 0) { \
            trd[mod_trx_id].optional_key exec_ref_id \
        } else if (trx_id == 1) { \
        trd[mod_trx_id].optional_key append(exec_ref_id, "F") \
        } \
        trd[mod_trx_id].status "TRADE_STATUS_VOID" \
    } \


#define SetCommonFixTradeFieldsIncoming(trd) \
    /* Fills AMBA message fields common to most transactions */ \
    /* Party information: static or from repeating group */ \
    /* TODO Get FIX Message with Repeating Group information */ \
    /* GetPartyIdByPartyRoleIncoming(38, position_account) */ \
    trd.prfnbr.prfid Account /*replace by position_account */ \
    /* GetPartyIdByPartyRoleIncoming(1, acquirer) */ \
    trd.acquirer_ptynbr.ptyid "DEFAULT_POOL_OWNER" /*replace by acquirer */ \
    /*GetPartyIdByPartyRoleIncoming(3, cp_id) */ \
    trd.counterparty_ptynbr.ptyid "Reuters" /* cp_id */ \
    trd.market_ptynbr.ptyid "FIX" \
    /* GetPartyIdByPartyRoleIncoming(11, trader_id) */ \
    trd.trader_usrnbr.userid "ARENASYS12" /* toUpper(trader_id) */ \
    trd.text1 Text \
    /* time_amba_format = append2(keepfirst(replace(ifexist(TransactTime, SendingTime), "-", " "), 17), " UTC") */ \
    time_amba_format = append2(keepfirst(replace(ifexist(SendingTime, TransactTime), "-", " "), 17), " UTC") \
    trd.time time_amba_format \
    /* Save FIX ID fields in AddInfo to identify information of the order/fill */ \
    trd.UPDATEADDITIONALINFO[0].ADDINF_SPECNBR.FIELD_NAME "ClOrdID" \
    trd.UPDATEADDITIONALINFO[0].VALUE ClOrdID \
    trd.UPDATEADDITIONALINFO[1].ADDINF_SPECNBR.FIELD_NAME "OrderID" \
    trd.UPDATEADDITIONALINFO[1].VALUE OrderID \
    /* GetPartyIdByPartyRoleIncoming(16, executing_system) */ \
    trd.UPDATEADDITIONALINFO[2].ADDINF_SPECNBR.FIELD_NAME "ExecutingSystem" \
    trd.UPDATEADDITIONALINFO[2].VALUE "FIX_OMS" /* executing_system */ \

