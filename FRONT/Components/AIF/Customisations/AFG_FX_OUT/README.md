# Regedit AFG_FX_OUT

### Controllers\IController\Parameters

| Name          | Type      | Data     |
| : ----------- | : ------- | : -------| 
| UnlockedWait  | REG_DWORD | 1        |


### Database\UserFdbFiles2

| Name                | Type      | Data                                                              |
| : ----------------- | : ------- | : --------------------------------------------------------------- | 
| DumpFolder          | REG_SZ    | LogFiles                                                          |
| Folder              | REG_SZ    | FdbFiles                                                          |
| RegPath             | REG_SZ    | SOFTWARE\Front\Front Arena\AIF\AFG_FX_OUT\Databases\UserFdbFiles2 |
| Separator           | REG_SZ    | ;                                                                 |

#### NEW KEY: Databases\UserFdbFiles2\TradeDb

| Name                          | Type      | Data                      |
| : --------------------------  | : ------- | : ----------------------- | 
| DeleteFDBIfNotProperlyClosed  | REG_DWORD | 0                         |
| DumpDbToFile                  | REG_DWORD | 1                         |
| Enabled                       | REG_DWORD | 1                         |
| FileMapKeepAliveViewCount     | REG_DWORD | 0xffffffff                |
| FileMapViewSize               | REG_DWORD | 1000000000                |
| Filename                      | REG_SZ    | FdbFiles\TradeDb.fdb      |
| InitialBaseRecordCount        | REG_DWORD | 1000                      |
| Name                          | REG_SZ    | TradeDb                   |
| Recovery                      | REG_DWORD | 0                         |
| ReleaseKeepAliveViewsOnDumpDb | REG_DWORD | 0                         |
| WaitTime                      | REG_DWORD | 0xffffffff                |
| XmlFile                       | REG_SZ    | ..\customised\TradeDb.xml |

### Database\XmlFiles

#### NEW KEY: Databases\XmlFiles\CustomXmlDb

| Name         | Type      | Data                          |
| : ---------- | : ------- | : --------------------------- |
| Enabled      | REG_DWORD | 1                             |
| Name         | REG_SZ    | CustomXmlDb                   |
| XmlFile      | REG_SZ    | ..\customised\CustomXmlDb.xml |

### Providers\AMBProvider\Parameters

| Name             | Data                          |
| : -------------- | : --------------------------- |
| Host             | localhost                     |
| MsgFileName      | AFG_AMB_Messages.log          |
| ReceiverItems    | Trade                         |
| ReceiverName     | AFG_FX_OUT_RECEIVER           |
| ReceiverSubjects | AMBA_FX_OUT_SENDER            |
| SenderName       | -                             |
| SenderSource     | -                             |
| SenderSubject    | -                             |
| User             | mb                            |

## Providers\FixProvider\Parameters

| Name              | Data                          |
| : --------------- | : --------------------------- |
| FIXVersion        | FIX.4.4                       |
| HeartbeatInterval | 30                            |
| Host              | localhost                     |
| IsServer          | 1                             |
| Port              | depending on cp               |
| SenderCompId      | AFG_OUT                       |
| TargetCompId      | depending on cp               |

## Runtime\AFG_AMB\AMBTrades

| Name                             | Data    |
| : ------------------------------ | : ----- |
| FIXMapExecutionReportBack        | 0       |
| FIXReceiveExecutionReport        | 0       |
| FIXReceiveTradeCaptureReport     | 0       |
| FIXSendExecutionReport           | 1       |
| FIXSendTradeCaptureReport        | 0       |
| MessageSendSimulatedTrade        | 1       |
| SubscribeToTradeCaptureReports   | 0       |
| UseAELQueryForIsinIdentification | 0       |
