# General notes: AFG

TODO
- learn more about FIX Engine
- use JetBrains IDE for C# with QuickFIX/n is v1.9.0
- https://github.com/quickfix-j/quickfixj/tree/master/quickfixj-examples
- AFG Logging **log2statusMgr**

### TNPFIND in real time

```batch
tnpfind /source=AFG_FX_OUT /full /real
```
### TNPFIND: Look for text in given time

```batch
tnpfind /source=AFG_FX_OUT /from=123000 /to=140000 /text="150=F"
```

### TNPFIND from eventlog file

```batch
tnpfind /source=AFG_FX_OUT /inputfile="AFG_FX_OUT-xxxxxx-xxxxxx.EVT" /from="20190826 230500"
```