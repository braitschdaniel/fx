# Remote Connection to frontarena server

### Access Data

- **Computer**: frontarena

- **User**: DF-FA006\Administrator

- **Password**: !!Prime99

### Remote access to server

- Enable access to server for windows user 
```bat
\\frontarena\c$
``` 
- Type in the credentials of the admin user

- Connect to services and regedit of the server via 

![Alt text](./servicesRemoteAccess.jpg)

![Alt text](./regeditRemoteAccess.jpg)