@echo off
title %~n0: RunScriptCMD (C) Copyright 2011 by Sungard FRONT ARENA
SETLOCAL
for %%i in ("C:\Program Files (x86)\Front\Front Arena";"C:\Program Files\Front\Front Arena") do if exist %%~i\PRIME set FRONTBASE=%%~i\PRIME& set PYTHONPATH=%%~i\CommonLib\PythonLib26;%%~i\CommonLib\PythonExtensionLib26
for /f "" %%i in ('dir /B "%FRONTBASE%\201?.?"') do set aelexe=%FRONTBASE%\%%i\ael.exe&set PRIME_VERSION=%%i
echo Using Prime version %PRIME_VERSION%

for %%i in ("%ProgramData%\Front\RunScriptCMD.ini";"%LOCALAPPDATA%\Front\RunScriptCMD.ini";".\RunScriptCMD.ini") do if exist %%i set runscriptcmdini=%%i

if "%aelexe%"=="" echo Can not find ael.exe & pause & goto :EOF

for %%i in ("C:\Program Files (x86)\Front\Front Arena\extras\RunScriptCMD\RunScriptCMD.py";"C:\Program Files\Front\Front Arena\extras\RunScriptCMD\RunScriptCMD.py";RunScriptCMD.py) do if exist %%i set runscriptcmd=%%~i
if "%runscriptcmd%"=="" echo Can not find RunScriptCMD.py & pause & goto :EOF 

set xmlcommand=%1

if "%xmlcommand%"=="" set xmlcommand=%~n0.xml

if not exist %xmlcommand% echo Can not find %xmlcommand%  & pause & goto :EOF

for /f "skip=1 tokens=2 delims='" %%a in ('find "<!--'" %xmlcommand%') do echo %%a
echo.

if exist %runscriptcmdini% for /f "eol=# skip=2 tokens=2 delims==" %%a in ('find "server=" %runscriptcmdini%') do set server=%%a
if exist %runscriptcmdini% for /f "eol=# skip=2 tokens=2 delims==" %%a in ('find "username=" %runscriptcmdini%') do set username=%%a

if "%server%"=="" set/p server="Please enter ADS (adress:port):"
if "%username%"=="" set/p username="Please enter user name:"

echo Run XML command file %xmlcommand% on ADS %server% as %username%
echo.
"%aelexe%" "%runscriptcmd%" --server=%server% --username=%username% --pwdPrompt --xmlcommand=%xmlcommand%
if errorlevel 1 echo * * * * unknown error * * * *

pause
