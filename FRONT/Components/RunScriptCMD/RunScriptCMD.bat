@echo off
title %~n0: RunScriptCMD %R% (C) Copyright 2019 by FIS (FRONT ARENA)
SETLOCAL
for %%i in ("C:\Program Files (x86)\Front\Front Arena";"C:\Program Files\Front\Front Arena") do if exist %%~i\PRIME set FRONTBASE=%%~i\PRIME& set PYTHONPATH=%%~i\CommonLib\PythonLib37;%%~i\CommonLib\PythonExtensionLib37
for /f "" %%i in ('dir /B "%FRONTBASE%\201?.?"') do set arenapython=%FRONTBASE%\%%i\arena_python.exe&set PRIME_VERSION=%%i
echo Using Prime version %PRIME_VERSION%

for %%i in ("%ProgramData%\Front\RunScriptCMD.ini";"%LOCALAPPDATA%\Front\RunScriptCMD.ini";".\RunScriptCMD.ini") do if exist %%i set runscriptcmdini=%%i

if "%arenapython%"=="" echo Can not find arena_python.exe & pause & goto :EOF

for %%i in ("C:\Program Files (x86)\Front\Front Arena\extras\RunScriptCMD\RunScriptCMD.py";"C:\Program Files\Front\Front Arena\extras\RunScriptCMD\RunScriptCMD.py";RunScriptCMD.py) do if exist %%i set runscriptcmd=%%~i
if "%runscriptcmd%"=="" echo Can not find RunScriptCMD.py & pause & goto :EOF 

"%arenapython%" "%runscriptcmd%" %*