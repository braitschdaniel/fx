# AMB Settings for FX flow

### Monitor AMB users 

Run in command line:
- amb_monitor -all 9100

### Settings in db 
TODO understand how **mb** user is added correctly
- Add client user **mb** to the AMB database with user type: SQL user with login

### Add names of AMB users - senders and receivers

```sql
insert into [AMB].[dbo].[system] (name)
values ('AFG_FX_IN_SENDER'),('AFG_FX_IN_RECEIVER'),('AFG_FX_OUT_RECEIVER'),('AMBA_FX_IN_RECEIVER'),('AMBA_FX_OUT_SENDER')
``` 

### Query to show db entries of FX flow

```sql
select * from [AMB].[dbo].[system] where (name) like '%FX%'
``` 

### Query to delete db entries containing 'XMBA'

```sql
delete from [AMB].[dbo].[system] where (name) like '%XMBA%'
``` 