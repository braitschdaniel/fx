# ATS Settings

## Group Mode ATS

| Name                | Data                        |
| : ----------------- | : ------------------------- | 
| ats_restart         | 1                           |
| group               | name of the group           |

## Module Mode ATS

| Name                | Data                        |
| : ----------------- | : ------------------------- | 
| module_name         | JsonModuleModeATS           |
