from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Hello world!')

    def do_POST(self):
        """Reads post request body."""
        self.send_response(200)
        content_len = int(self.headers.getheader('content_length', 0))
        post_body = self.rfile.read(content_len)
        print(str(post_body))
        self.wfile.write("received post request:<br>{}".format(post_body))


httpd = HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()
