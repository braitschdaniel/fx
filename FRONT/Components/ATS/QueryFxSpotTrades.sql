select
	trd.trdnbr
	, display_id(trd, 'insaddr') 'Insaddr'
	, trd.quantity
	, display_id(trd, 'curr') 'Curr'
	, trd.premium
	, trd.price
	, display_id(trd, 'prfnbr') 'Prf'
	, display_id(trd, 'counterparty_ptynbr') 'Cp'
	, display_id(trd, 'acquirer_ptynbr') 'Acquirer'
	, display_id(trd, 'trader_usrnbr') 'Trader'
	, trd.status
	, trd.updat_time
from 
	trade trd	
where 1=1
	and trd.trade_process = 4096
	and trd.status in ('Exchange', 'Void', 'FO Confirmed')
	