"""______________________________________________
MODULE 
    ATSJsonToWebservice.py

PROJECT
    fx
    
DESCRIPTION
    -

HISTORY
    30.01.2020 Daniel Braitsch
______________________________________________"""

import json
import urllib2
import pickle

import acm
import ael

import FLogger

log = FLogger.FLogger(level=2, logOnce=False, logToConsole=True, logToPrime=False)

params = {
    'bauth_token': r'C:\Program Files\Front\Front Arena\ATS\ATS_JSON\bauth_token.token',
    'query_name': 'MyTradeFilter',
    'status_file': r'C:\Program Files\Front\Front Arena\ATS\ATS_JSON\trade_status.pickle',
    'url': 'http://localhost:8000',
}

bauth_token = None
trade_filter = None

STATUS = {
    'Exchange': 'Confirmed',
    'FO Confirmed': 'Confirmed',
    'Void': 'Confirmed',
    'Simulated': 'Not Relevant',
    'Reserved': 'Not Relevant',
}


class TradeFilter:
    """Checks whether a trade satisfied all specified criteria"""

    def __init__(self, filter_query=None, status_file=None):
        self.filter_query = filter_query
        self.status_file = status_file

        self._trade_status = {}
        if status_file:
            self._load_trade_status()

        self.transitions = {
            ('NEW', 'Exchange'),
            ('NEW', 'FO Confirmed'),
            ('Simulated', 'FO Confirmed'),
            ('Reserved', 'FO Confirmed'),
            ('Exchange', 'Void'),
            ('FO Confirmed', 'Void'),
        }

    def _get_last_trade_status(self, trade):
        trdnbr = trade.Oid()
        if trdnbr in self._trade_status:
            return self._trade_status[trdnbr]
        else:
            return "NEW"

    def _load_trade_status(self):
        persisted_status = None

        try:
            with open(self.status_file, 'rb') as fp:
                persisted_status = pickle.load(fp)
                print persisted_status
        except Exception as E:
            log.warn('Could not open trade status file {}. Will create a new file and start fresh.'.format(self.status_file))
            pickle.dump({0: "Simulated"}, open(self.status_file, "wb" ))
            with open(self.status_file, 'rb') as fp:
                persisted_status = pickle.load(fp)
                print persisted_status


        if persisted_status:
            self._trade_status = persisted_status
            log.info('Loaded trade status mapping from file {}.'.format(self.status_file))

    def _set_last_trade_status(self, trade):
        trdnbr = trade.Oid()
        status = trade.Status()
        self._trade_status[trdnbr] = status

    def _trade_matches_filter(self, trade):
        if not self.filter_query:
            return True
        try:
            trade_matches_filter = self.filter_query.IsSatisfiedByUseAttributes(trade)
        except RuntimeError as E:
            # if eg. portfolio is part of the filter, but the trade does not have a portfolio specified
            if str(E) == "run-time assertion failed:(anObject) at FAttribute::AssertAccess":
                trade_matches_filter = False
            else:
                raise
        return  trade_matches_filter

    def is_relevant(self, trd):
        if not self._trade_matches_filter(trd):
            log.info('Trade {trdnbr} did not match filter query'.format(trdnbr=trd.Oid()))
            return False

        old_status = self._get_last_trade_status(trd)
        new_status = trd.Status()
        relevancy = (old_status, new_status) in self.transitions
        log.info("Trade moved from status {old} to {new}: {rel}".format(
            old=old_status, new=new_status, rel="Relevant" if relevancy else "Not relevant"
        ))
        return relevancy

    def persist_trade_status(self):
        if self.status_file:
            log.info('Persisting trade status map to file {}.'.format(self.status_file))
            print 'self._trade_status', self._trade_status
            with open(self.status_file, 'wb') as fp:
                pickle.dump(self._trade_status, fp)
        else:
            log.info('Skipping trade status persistence as no file is specified.')

    def processed(self, trade):
        self._set_last_trade_status(trade)


def banking_account_info(trd):
    return '{portfolio}'.format(portfolio=trd.Portfolio().Name())


def json_payload(trd):
    """Returns the json payload for the trade feed."""
    dict_json_payload = {
        'banking_account': banking_account_info(trd),
        'sourceName': 'FrontArena',
        'status': STATUS[trd.Status()],
    }
    return dict_json_payload


def post_json_and_log_response(url, payload):
    req = urllib2.Request(url, payload, {'Content-Type': 'text_plain'})
    req.add_header('Authorisation', 'Basic %s' % bauth_token)

    f = urllib2.urlopen(req)
    print 'Status response code', f.getcode()
    print f.read()
    f.close()


def process_trade(trd):
    log.info('Processing trade {trd_nbr}.'.format(trd_nbr=trd.Oid()))
    try:
        if trade_filter.is_relevant(trd):
            log.info('Sending trade {trd_nbr}'.format(trd_nbr=trd.Oid()))
            json_dump = json.dumps(json_payload(trd), indent=2)
            log.info(json_dump)
            post_json_and_log_response(params["url"], json_dump)
            trade_filter.processed(trd)

    except Exception as E:
        log.error("Failed to process trade {trdnbr}".format(
            trdnbr=trd.Oid() if trd else "unknown", ), exc_info=1)


def trade_callback(obj, ael_trade, arg, event):
    """
    Callback for the subscription on the trade table.
    Creates a JSON payload for the url feed.
    """
    try:
        trd = acm.Ael.AelToFObject(ael_trade)
        process_trade(trd)
        trade_filter.persist_trade_status()
    except Exception as E:
        log.error("Could not convert trade {trdnbr} to acm object".format(
            trdnbr=ael.trade.trdnbr if ael_trade else "unknown",
        ))


def start():
    """ATS Entry Point"""
    global trade_filter , bauth_token

    # Read user/password for basic auth
    path_to_token = params['bauth_token']
    with open(path_to_token, 'r') as fp:
        bauth_token = fp.read()
    log.info('Loaded basic auth token from file {}.'.format(path_to_token))

    if not path_to_token:
        raise RuntimeError('Missing basic auth token for LRC connection.')

    status_file = params['status_file']
    # build trade_filter for processing
    query_name = params['query_name']
    stored_query = acm.FStoredASQLQuery[query_name]
    query = stored_query.Query()

    trade_filter = TradeFilter(filter_query=query, status_file=status_file,)
    
    #Re-process existing trades from today
    trades_today = acm.FTrade.Select('updateTime >= {0}'.format(acm.Time.DateToday()))
    for trd in list(trades_today):
        process_trade(trd)
        trade_filter.persist_trade_status()

    ael.Trade.subscribe(trade_callback)


def stop():
    """ATS exit point"""
    ael.Trade.unsubscribe(trade_callback)
    trade_filter.persist_trade_status()
