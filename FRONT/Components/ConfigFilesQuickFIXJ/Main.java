package dfine.oliX;

import dfine.oliX.broker.core.FIXimulator;
import dfine.oliX.broker.core.FIXimulatorApplication;
import dfine.oliX.broker.core.LogMessageSet;
import dfine.oliX.broker.ui.MessageTableModel;
import dfine.oliX.client.*;
import quickfix.*;
import java.util.concurrent.TimeUnit;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        FIXimulator broker = new FIXimulator();
        broker.start();


        Client client = new Client();

        client.logon();

        Initiator clientInitiator = client.getInitiator();
        ClientApplication clientApplication = client.getApplication();

        SessionID clientSessionID = null;
        for(SessionID sessionID: clientInitiator.getSessions()){
            clientSessionID = sessionID;
        }

        Order orderIBM = new Order();
        orderIBM.setSessionID(clientSessionID);
        orderIBM.setQuantity(1000);
        orderIBM.setSide(OrderSide.BUY);
        orderIBM.setSymbol("IBM");
        orderIBM.setType(OrderType.MARKET);

        String message_str = "8=FIX.4.2\u00019=137\u000135=D\u000134=2\u000149=BANZAI\u000152=20190418-11:23:26.818\u000156=FIXIMULATOR\u000111=1555586599176\u000121=1\u000138=1000\u000140=1\u000154=1\u000155=IBM\u000159=0\u000160=20190418-13:23:26.816\u000110=163\u0001";
        //String message_str = "8=FIX.4.4\u00019=130\u000135=D\u000134=2\u000149=CLIENT1\u000152=20200320-08:47:17.524\u000156=SIMPLE\u000111=1234\u000121=1\u000138=100000\u000140=1\u000154=1\u000155=CHF/USD\u000159=1\u000160=20200320-09:47:06.843\u000110=197\u0001";
        Message message = null;
        try {
            message = new Message(message_str, true);
        } catch (InvalidMessage e){
            e.printStackTrace();
            System.exit(0);
        }

        /**
        System.out.println("Do you want to buy 1000 shares of IBM at market price?");
        Scanner input = new Scanner(System.in);
        String user_response = input.nextLine();
        **/

        //clientApplication.send(message, clientSessionID);
        delayBySeconds(2);
        //if(user_response.equals("Y")){
        clientApplication.send(orderIBM);
        //}
        delayBySeconds(2);
        client.logout();

        broker.stop();

    }
    public static void delayBySeconds(long period){
        long startTime = System.currentTimeMillis();
        while(System.currentTimeMillis() - startTime < period*1000) {
        }
    }

}
