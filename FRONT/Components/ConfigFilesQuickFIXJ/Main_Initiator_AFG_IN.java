package dfine.oliX;

import dfine.oliX.client.*;
import quickfix.Initiator;
import quickfix.InvalidMessage;
import quickfix.Message;
import quickfix.SessionID;
import java.util.Scanner;

public class Main_Initiator_AFG_IN {

    public static void main(String[] args){
        /**
        FIXimulator broker = new FIXimulator();
        broker.start();
        **/
        Client client = new Client();
        client.logon();


        Initiator clientInitiator = client.getInitiator();
        ClientApplication clientApplication = client.getApplication();

        SessionID clientSessionID = null;
        for(SessionID sessionID: clientInitiator.getSessions()){
            clientSessionID = sessionID;
        }


        Order orderIBM = new Order();
        orderIBM.setSessionID(clientSessionID);
        orderIBM.setQuantity(1000);
        orderIBM.setSide(OrderSide.BUY);
        orderIBM.setSymbol("IBM");
        orderIBM.setType(OrderType.MARKET);

        // stock insert
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=Unique123456ExecID\u000131=1234\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=BMWG.DE\u000160=20190418-13:23:26.816\u000164=20200331\u0001150=F\u0001167=CS\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // stock cancel
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueExecIDCancel\u000119=UniqueExecID\u000131=1234\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=BMWG.DE\u000160=20190418-13:23:26.816\u000164=20200331\u0001150=H\u0001167=CS\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // stock modify
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueExecIDModify\u000119=UniqueExecID\u000131=1234\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=BMWG.DE\u000160=20190418-13:23:26.816\u000164=20200331\u0001150=G\u0001167=CS\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // fx spot insert
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueSpot12341156ExecID\u000132=10\u000137=MyOrderId\u000154=1\u000155=EUR/USD\u000160=20190418-13:23:26.816\u000164=20200331\u0001120=USD\u0001150=F\u0001167=FXSPOT\u0001194=1.1619\u0001453=1\u0001452=ARENAYSYS1\u0001448=D\u0001447=11\u000110=233a\u0001";
        // fx forward insert
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueForward1234ExecID\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=EUR/USD\u000160=20190418-13:23:26.816\u000164=20200331\u0001120=USD\u0001150=F\u0001167=FXFWD\u0001194=1.1619\u0001195=0.02\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // ndf insert
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueNDF1121ExecID\u000132=1000000\u000137=MyOrderId\u000154=2\u000155=EUR/USD\u000160=20190418-13:23:26.816\u000164=20200331\u0001120=USD\u0001150=F\u0001167=NDF\u0001194=1.1619\u0001195=0.02\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // nds insert
        String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueNDS111ExecID\u000132=1000000\u000137=MyOrderId\u000154=2\u000155=EUR/USD\u000160=20190418-13:23:26.816\u000164=20200331\u0001120=USD\u0001150=F\u0001167=NDS\u0001194=1.1619\u0001195=0.03\u0001541=20200901\u0001641=0.04\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // fx swap insert
        // String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueSwap0123ExecID\u000131=1.1619\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=EUR/USD\u000160=20190418-13:23:26.816\u000164=20200331\u0001120=USD\u0001150=F\u0001167=FXSWAP\u0001194=1.1619\u0001195=0.03\u0001641=0.05\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        // invalid security type
        //String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueFailTradeID1\u000131=1234\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=BMWG.DE\u000160=20190418-13:23:26.816\u000164=20200331\u0001150=F\u0001167=CS1\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        //String message_str = "8=FIX.4.4\u00019=137\u000135=8\u000134=2\u000149=AFG_FX_IN\u000152=20190418-11:23:26.818\u000156=FIX_CLIENT\u00011=DEFAULT_POOL\u000111=MyClOrdId\u000115=EUR\u000117=UniqueFailTradeID1\u000131=1234\u000132=1000000\u000137=MyOrderId\u000154=1\u000155=BMWG.DE\u000160=20190418-13:23:26.816\u000164=20200331\u0001150=F\u0001167=FXSPOT\u0001453=1\u0001452=ARENAYSYS\u0001448=D\u0001447=11\u000110=233a\u0001";
        Message message = null;
        try {
            message = new Message(message_str, true);
        } catch (InvalidMessage e){
            e.printStackTrace();
            System.exit(0);
        }



        System.out.println("Do you want to buy 1000 shares of IBM at market price?");
        Scanner input = new Scanner(System.in);
        String user_response = input.nextLine();


        clientApplication.send(message, clientSessionID);
        delayBySeconds(2);
        //if(user_response.equals("Y")){
        clientApplication.send(orderIBM);
        //}
        delayBySeconds(2);
        /**
        client.logout();

        broker.stop();

        **/
    }
    public static void delayBySeconds(long period){
        long startTime = System.currentTimeMillis();
        while(System.currentTimeMillis() - startTime < period*1000) {
        }
    }

}
