# FA Automatic Deployment

This repository contains relevant configuration and utilities for an automatic deployment procedure in FA.

## Repository Structure

### Deployment Packages

Packages in this directory will be automatically deployed. 
Which packages are deployed is controlled by specific package lists, which are managed in the Git Repo for different scopes:
`PackagesToUpload.txt` and `PackagesToUpload2.txt`. 
The order of deployment is determined by the order in the input file, the numbering is just to give a visual indication of the desired order.

#### Common Input

This is not an actually deployed package but a place to collect Python libs and input files that are available to all deployment packages.

- **Lib**: Python files placed here will be available for import in all Pre- and Posthook scripts of the deployment packages.
For this, the path to this folder is added to the `PYTHONPATH` environment variable, so the modules can be imported without further adaptations.

- **InputFiles**: Place of choice for non-python input files used in multiple packages.
The path of this folder is available through the `fx_conf` package: `import fx_conf`; `print fx_conf.input_dir`

#### Remaining deployment packages (10_FirstPackage, 20_SecondPackage, ...)

| Deployment Type  | Directory                      | Comment                          |
| ---------------- | ------------------------------ | -------------------------------- |
| PreHook          | .\PreHook                      | Python scripts to be executed once at the beginning of the package deployment without persistent import |
| ExtensionModule  | .\Transporter\Extension_Module | Extension Modules to be imported |
| Python Scripts   | .\Transporter\Python           | Python modules to be imported    |
| Tasks            | .\Transporter\Task             | Tasks to be imported             |
| Workbooks        | .\Transporter\Workbook         | Workbooks to be imported         |
| PostHook         | .\PostHook | Python scripts to be executed once at the end of the package deployment without persistent import |

### Utils 

Utilities for the automatic deployment process.