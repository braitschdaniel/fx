# Manual to connect Pycharm to Front Arena

1. Add Front Arena to System Path:   
   Control Panel\System\Advanced System Settings\Environment Variables\Path
    - C:\Program Files\Front\Front Arena\PRIME\2019.2; 
    - C:\Program Files\Front\Front Arena\CommonLib\PythonExtensionLib27; 
    - C:\Program Files\Front\Front Arena\CommonLib\PythonLib27

    ![SystemPathFA1](./SystemPathFA1.jpg)

    ![SystemPathFA2](./SystemPathFA2.jpg)

2. Set the Project Interpreter to Python 2.7 in Pycharm 		

3. Add new Project Interpreter Paths to Python 2.7
    - C:\Program Files\Front\Front Arena\PRIME\2019.2; 
    - C:\Program Files\Front\Front Arena\CommonLib\PythonExtensionLib27; 
    - C:\Program Files\Front\Front Arena\CommonLib\PythonLib27

    ![AddFAPathToPycharm1](./AddFAPathToPycharm1.jpg)

    ![AddFAPathToPycharm2](./AddFAPathToPycharm2.jpg)

    ![AddFAPathToPycharm3](./AddFAPathToPycharm3.jpg)

4. Try to import acm and connect to FA

```python
import acm
acm.Connect("frontarena:9000", "arenasys", "intas", "", "test_app", 0 ,0 ,0, None, 0, [])
``` 