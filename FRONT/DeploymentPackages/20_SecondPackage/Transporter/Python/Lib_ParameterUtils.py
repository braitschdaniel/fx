import acm
import ast
import collections

def get_parameter(name):
	dict = collections.defaultdict(lambda: None)
	param = acm.GetDefaultContext().GetExtension("FParameters", "FExtension", name)
	if param and param.Value():
		param = param.Value()
		dict = {}
		for k in param.Keys():
			k = str(k)
			try:
				dict[k] = ast.literal_eval(str(param.At(k)))
			except:
				dict[k] = str(param.At(k))
	return dict
	
print get_parameter("EnvironmentInfo")
