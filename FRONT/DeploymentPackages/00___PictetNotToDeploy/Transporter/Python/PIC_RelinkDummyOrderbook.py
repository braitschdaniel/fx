'''---------------------------------------------------------------------------------
 MODULE
    PIC_RelinkDummyOrderbook

 DESCRIPTION
    This script is moving orderbooks and price links for a certain instrument to a 
    new object. If the dummy instrument can't be deleted in the end of the 
    procedure it is very likely due to references on the dummy instrument which 
    should actually not exist (for example trades should always be done on the 
    placeholder instrument if the correct instrument has not been downloaded yet. 

 HISTORY
    Christoph Schenke & Marc-Stephan Maenner    2016-10-11      Created
    
    Oliver Behm                                 2017-02-21      Updated
        Added functionality to call the task not only with a single ISIN, 
        but also with '~*' - which will search the ADS for all ISINs for 
        which there are instruments with both <ISIN> and ~<ISIN> to try
        and activate the relink.

 ENDDESCRIPTION
---------------------------------------------------------------------------------'''

import ael
import acm

import textwrap
from collections import namedtuple

import FLogger
import PIC_ListenerPriceDefinitions


logger = FLogger.FLogger(__name__, level=2)


def get_table_references(table_name):
    """
    Get all references to the given table and return these as a list
    of TableRef objects.
    """
    TableRef = namedtuple('TableRef', ['table_name', 'id_col', 'ref_col'])
    qry_table_references = textwrap.dedent("""\
        select
          col_ins_ref.table_name 'table_name'
        , col_id.column_name 'id_col'
        , col_ins_ref.column_name 'ref_col'
        from ds_tables col_ins_ref
        left outer join ds_tables col_id
          on col_ins_ref.table_name = col_id.table_name
          and col_id.column_type = 'id'
        where 1=1
        and (col_ins_ref.column_type = 'ref({table_name})'
                 or col_ins_ref.column_type = 'parent({table_name})')
        """.format(table_name=table_name))
    return [TableRef(*row) for row in ael.dbsql(qry_table_references)[0]]

def get_object_references(acm_object, table_name="instrument"):
    """
    Get all references to the row in the given table with a primary key
    value of `acm_object.Oid()`.
    
    Note: This function does not validate that the type of acm_objects
    fits the given table name. The call should fit the underlying table, e.g.
      typeof(acm_object) == FBond
      table_name == instrument
    or
      typeof(acm_object) == FVolatilityStructure
      table_name == volatility
    """
    table_refs = get_table_references(table_name)
    object_id =  acm_object.Oid()

    results = []
    for ref in table_refs:
        query = textwrap.dedent("""\
            select 
              '{table:31}'
            , '{id_col:31}'
            , {table}.{id_col} 
            from {table} 
            where {table}.{ref_col} = {object_id}
            """.format(
                table=ref.table_name,
                id_col=ref.id_col,
                ref_col=ref.ref_col,
                object_id=object_id))
        results.extend(ael.dbsql(query)[0])
    return results
  




def RelinkObjectsAndDeleteDummy(DummyIns,BBIns):
    try:
            
        whitelist = [acm.FPriceLinkDefinition, acm.FOrderBook]
        acm.BeginTransaction()
        for classType in whitelist:
            for object in classType.Select('instrument=%d' % DummyIns.Oid()).AsList():
                logger.info("Moved {type}({ob_id}) from {dummy_name} to {bb_name} ".format(
                    type=classType, 
                    ob_id = object.Oid(),
                    dummy_name=DummyIns.Name(),
                    bb_name =BBIns.Name()  )         
                ) 
                object.Instrument(BBIns)
                object.Commit()
        DummyIns.Delete()
        logger.info("Removing Dummy Instrument {name}".format(name = DummyIns.Name()))
        acm.CommitTransaction()
        #activatePrimaryMarketPriceLink(BBIns)
        PIC_ListenerPriceDefinitions.activatePriceDefinition(BBIns, True)
    except Exception,e:
        acm.AbortTransaction()
        logger.error("Exception occured in RelinkObjectsAndDeleteDummy: {0} ".format(e)) 
        logger.error("Probably some references are missing for instrument %s " % DummyIns.Name()) 
        for (table, column, id) in get_object_references(DummyIns):
            logger.error("%s: %s=%s" %(table, column, id))


def relink_isin(isin):
    instrument = acm.FInstrument['~%s' % isin]
    newInstrument = acm.FInstrument[isin]
    if not (instrument and newInstrument):
        logger.error("No Instrument or dummy instrument in ADS found for the corresponding isin")
        return -1
        
    RelinkObjectsAndDeleteDummy(instrument,newInstrument)


def get_duplicate_isins():
    qry = """\
        select
          ins.isin
        from instrument ins
        inner join instrument dummy
          on '~'+ins.isin = dummy.insid
        order by 1 desc
        """

    result = ael.dbsql(qry)
    
    return [inner[0] for inner in result[0]]


ael_variables = [
    ['ISIN', 'ISIN', "string", None,None, 0, 0, "Enter '~*' to relink all ISINs which exist both normally and as dummy."],
]

def ael_main(dict):
    print dict["ISIN"]
    if dict["ISIN"] == "~*":
        isins = get_duplicate_isins()
        print isins
        for isin in isins:
            try:
                relink_isin(isin)
            except:
                logger.error("Could not relink {0}".format(isin))
    else:
        relink_isin(dict["ISIN"])
   
