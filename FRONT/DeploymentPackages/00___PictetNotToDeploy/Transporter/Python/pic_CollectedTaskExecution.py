import acm

#-*- coding: iso-8859-1 -*-
"""
-----------------------------------------------

 Collects a predefined number of tasks and executed them sequentially
 Requirement by RISK to execute the number of EoY Script sequentially
  
 
 @since: 2015-11-13
 @author: Christoph Kirchsteiger

-----------------------------------------------
"""



"""
############################################################
#
#                     ATS Task Runscript
#
############################################################
"""
defaultTasks = None#",".join(['Task1','Task2'])



ael_variables = [
['Tasks', 'Tasks (executed sequentially)', acm.FAelTask, None, defaultTasks, 1, 1, 'Select the tasks in the sequence they shall be executed'],
['Send email', 'Send email', "bool", [True,False], False, 1, '']]


def ael_main(params):
    
    sendEmail = params['Send email']   
    
    print "Executing selected Tasks..."
    tasks = params['Tasks']
    
    for tsk in tasks:
        print "Executing Task '%s'..." % tsk.Name()
        #print "Parameters", task.Parameters()
        tsk.Execute()
        print "Finished execution of Task '%s'" % tsk.Name()
    
    
    print "Finished execution of all given tasks"
    
    
    
