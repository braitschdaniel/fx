"""
For all bond-like instruments
    - Bonds
    - FreeDefCashFlows
    - FRNs
    - Zeros
whose IDP code ends on
    - Corp
    - Govt
find the (active or inactive) price link "BBG-Realtime" and create three new price links:
    - BGN
    - CBBT
    - BVAL
by cloning it. In each of these new price links, adjust the IDP code such that
they get the prices from the respective market (BGN, CBBT, or BVAL). After that, delete
    - BBG-Realtime,
    - BBG-Delayed.
"""

import sys
import os
import csv
import re
import logging
from datetime import datetime

import acm

# Constants
INSTRUMENT_TYPES = ["Bond", "FRN", "Zero", "FreeDefCF"]

NEW_MARKET_NAMES = ['BGN', 'CBBT', 'BVAL']
OLD_MARKET_NAMES = ['BBG-Realtime', 'BBG-Delayed']
PRICEFINDING_MARKET_NAMES = ["EUREX_ETS", "SWX", "CBBT", "BGN", "BVAL", "BBG-Realtime", "BBG-Delayed"]

INACTIVE_TRADERS = {"ADEVAUD_INACTIVE", "JMENOUD_INACTIVE", "FMAINTENANCE", "NWARMUTH"}

# Logger
log = logging.getLogger()
log.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s - %(message)s')
ch.setFormatter(formatter)
if not log.handlers:
    log.addHandler(ch)


###########
# FUNCTIONS
###########

DATETIME = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")


def write_to_file(output_dirpath, fn, data):

    filename = os.path.join(output_dirpath, fn + "_" + DATETIME + ".csv")
    log.info("Writing control output to {fn}".format(fn=filename))
    try:
        with open(filename, "wb") as f:
            if len(data) > 0:
                writer = csv.writer(f, delimiter=";")
                writer.writerows(data)
    except Exception as E:
        log.critical(str(E))

########################
# CONNECT TO FRONT ARENA
########################

try:
    import my_ads
    my_ads.connect("DFINE", "DEV4")
except:
    pass

###############
# RUNSCRIPT GUI
###############

# Find output directory relative to server from which the script runs
import PIC_Context
try:
    server_name = PIC_Context.AMB_CONNECTION_URL.split(":")[0]
except Exception as E:
    log.critical(str(E))
    log.critical("Could not determine server name, exiting to system ...")
    sys.exit(1)

import FRunScriptGUI
directorySelection = FRunScriptGUI.DirectorySelection()
# directorySelection.SelectedDirectory(r"M:\Common\awingerter\PriceCleanup")
directorySelection.SelectedDirectory(r"\\" + server_name + r"\E$\Rapports\Risque\BPIPE review")

ael_variables = [
    ['output_dirpath', 'Output Directory', directorySelection, None, directorySelection, 0, 1,
     'The path to the directory for DEBUG data', None, 1],
    ['TEST', 'Test Run', "int", [0, 1], 1, 1, 0,
     'Test run or not', None, 1],
    ['CREATE_NEW_PRICELINKS', 'Create new price links for BGN, CBBT, BVAL', "int", [0, 1], 1, 1, 0,
     'Create price links for markets BGN, CBBT, BVAL', None, 1],
    ['DELETE_PRICELINKS', 'Delete price links for BBG-Realtime and BBG-Delayed (if BGN, CBBT, or BVAL exist)', "int",
     [0, 1], 1, 1, 0, 'Delete price links for BBG-Realtime and BBG-Delayed', None, 1],
    ['DELETE_PRICES', 'Delete prices for BBG-Realtime and BBG-Delayed', "int", [0, 1], 1, 1, 0,
     'Deletes prices for BBG-Realtime and BBG-Delayed in price entry table', None, 1],
]


def ael_main(params):

    output_dirpath = str(params["output_dirpath"])
    test_mode = bool(params["TEST"])

    create_new_pricelinks = bool(params["CREATE_NEW_PRICELINKS"])
    delete_pricelinks = bool(params["DELETE_PRICELINKS"])
    delete_prices = bool(params["DELETE_PRICES"])

    # If none of the check boxes in Runscript GUI are ticked, there is nothing to be done
    if not create_new_pricelinks and not delete_pricelinks and not delete_prices:
        return

    """
    GET INSTRUMENTS

    We obtain all bond-like instruments that are not dummies, i.e.
        - instrument name does not start with '~'
        - instrument type is either Bond, FRN, FreeDefCF, Zero,
        - at least 1 Bloomberg-like price link exists (BBG-Realtime, BBG-Delayed, BGN, CBBT, or BVAL),
        - IDP code of price link ends on 'Govt' or 'Corp'
    """

    query = acm.CreateFASQLQuery(acm.FInstrument, 'AND')
    query.AddAttrNodeString('InsType', INSTRUMENT_TYPES, 'EQUAL')
    query.AddAttrNodeString('Name', "~*", 'RE_LIKE_NOCASE').Not(True)

    instruments = query.Select()

    log.info("{num} bond-like instruments in total (Bond, FRN, FreeDefCF, Zero)".format(num=len(instruments)))

    bonds = list()
    data = list()
    data.append(["Instrument", "Type", "Markets", "Traders", "Description"])
    for ins in instruments:

        # Get trader names
        traders = set([x.Trader().Name() for x in ins.Trades() if x.Trader()])
        traders = traders - INACTIVE_TRADERS
        traders = list(traders)

        # Get all price links
        pricelinks = acm.FPriceLinkDefinition.Select("instrument='{ins_name}'".format(ins_name=ins.Name()))
        existing_markets = sorted([x.Market().Name() for x in pricelinks])

        # BBG-Realtime, BBG-Delayed, BGN, CBBT, BVAL
        bloomberg_pricelinks = [x for x in pricelinks if
                                x and x.Market() and x.Market().Name() in OLD_MARKET_NAMES + NEW_MARKET_NAMES]

        if len(bloomberg_pricelinks) > 0:
            some_pricelink = bloomberg_pricelinks[0]
        else:
            data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders), "No price links"])
            continue

        # Bond is not Corp or Govt
        idp_code = some_pricelink.IdpCode().strip()
        idp_code = re.sub(r'@\w+', '', idp_code)
        t = idp_code.split()
        if t[-1] not in ["Corp", "Govt"]:
            data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders), "No 'Corp' or 'Govt' in IDP code"])
            continue

        data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders), "In scope"])

        bonds.append(ins)

    # Delete instruments so that we do not use it by accident
    del instruments

    log.info("{num} bond-like instruments with sector 'Govt' or 'Corp'".format(num=len(bonds)))

    # Write control data to file
    write_to_file(output_dirpath, "PriceEntry_BondUniverse", data)

    """
    CREATE PRICE LINKS

    If BGN, CBBT, or BVAL already exist, nothing will be done, since these are exactly the markets that are
    to be created. Note that we only check for the existence of at least one, not all of these markets.

    If these markets do not exist, but there is also no BBG-Realtime market, nothing will be done, since we
    need the BBG-Realtime market as a template to be cloned and renamed into BGN, etc. This case may occur, if
    there is an SWX price link. If there is also no SWX price link, this is probably an error. Also, it is not
    clear to me whether it is consistent to have SWX and Bloomberg markets side-by-side (probably not).

    In all other cases, price links for the markets BGN, CBBT, and BVAL will be created.
    """

    if create_new_pricelinks:

        log.info("Creating new BGN, CBBT, BVAL price links")

        data = list()
        data.append(["Instrument", "Type", "Markets", "Traders", "Description"])
        for ins in bonds:

            # Get trader names
            traders = set([x.Trader().Name() for x in ins.Trades() if x.Trader()])
            traders = traders - INACTIVE_TRADERS
            traders = list(traders)

            # Get all price links
            pricelinks = acm.FPriceLinkDefinition.Select("instrument='{ins_name}'".format(ins_name=ins.Name()))

            existing_markets = sorted([x.Market().Name() for x in pricelinks])

            # BGN, CBBT, or BVAL already exist
            if set(NEW_MARKET_NAMES) & set(existing_markets):
                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Price links already exist, nothing to be done"])
                continue

            bbg_realtime_pricelink = next((x for x in pricelinks if x.Market().Name() == 'BBG-Realtime'), None)
            if bbg_realtime_pricelink is None:
                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Critical: No BBG-Realtime price link"])
                continue

            # Create new idp_code
            old_idp_code = bbg_realtime_pricelink.IdpCode().strip()
            if not old_idp_code:
                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Critical: No idp_code"])
                continue

            old_idp_code = re.sub(r'\s+', ' ', old_idp_code)
            old_idp_code = re.sub(r'@\w+', '', old_idp_code)
            t = old_idp_code.split()
            if len(t) < 2:
                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Critical: idp_code in wrong format"])
                continue

            for new_market_name in NEW_MARKET_NAMES:

                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Creating {is_active} price link for market {market}".format(
                                 is_active="inactive" if bbg_realtime_pricelink.NotActive() else "active",
                                 market=new_market_name)])

                new_market = acm.FMarketPlace[new_market_name]
                new_idp_code = " ".join(t[:-1]) + "@" + new_market_name + " " + t[-1]
                new_pricelink = bbg_realtime_pricelink.Clone()
                new_pricelink.Market(new_market)
                new_pricelink.IdpCode(new_idp_code)
                try:
                    if not test_mode:
                        new_pricelink.Commit()
                except Exception as E:
                    log.critical(str(E))
                    data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                                 "Critical: {exception}".format(exception=str(E))])

        # Write control data to file
        write_to_file(output_dirpath, "PriceEntry_NewPricelinks", data)


    """
    DELETE PRICE LINKS

    If at least one of the new price links BGN, CBBT, or BVAL exists, delete the old price links
    BBG-Realtime and BBG-Delayed.
    """

    if delete_pricelinks:

        log.info("Deleting BBG-Realtime and BBG-Delayed price links")

        data = list()
        data.append(["Instrument", "Type", "Markets", "Traders", "Description"])
        for ins in bonds:

            # Get trader names
            traders = set([x.Trader().Name() for x in ins.Trades() if x.Trader()])
            traders = traders - INACTIVE_TRADERS
            traders = list(traders)

            # Get all price links
            pricelinks = acm.FPriceLinkDefinition.Select("instrument='{ins_name}'".format(ins_name=ins.Name()))

            existing_markets = sorted([x.Market().Name() for x in pricelinks])

            bbg_realtime_pricelink = next((x for x in pricelinks if x.Market().Name() == 'BBG-Realtime'), None)
            bbg_delayed_pricelink = next((x for x in pricelinks if x.Market().Name() == 'BBG-Delayed'), None)
            bbg_pricelinks = filter(lambda x: x is not None, [bbg_realtime_pricelink, bbg_delayed_pricelink])

            if set(NEW_MARKET_NAMES) & set(existing_markets):
                for bbg_pricelink in bbg_pricelinks:
                    data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                                 "Deleting price link {market_name} for {ins_name}".format(
                                     market_name=bbg_pricelink.Market().Name(), ins_name=ins.Name())])

                    try:
                        if not test_mode:
                            bbg_pricelink.Delete()
                    except Exception as E:
                        log.critical(str(E))
                        data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                                     "Critical: Could not delete price link {market_name} for {ins_name}".format(
                                         market_name=bbg_pricelink.Market().Name(), ins_name=ins.Name())])

            else:
                data.append([ins.Name(), ins.InsType(), ",".join(existing_markets), ",".join(traders),
                             "Will not delete old market; new markets do not exist"])

        # Write control data to file
        write_to_file(output_dirpath, "PriceEntry_DeletePricelinks", data)

    """
    DELETE PRICES

    If a price is in any of the markets
        - "EUREX_ETS", "SWX", "CBBT", "BGN", "BVAL", "BBG-Realtime", "BBG-Delayed",
    but there exists no price link for that market (active or inactive), this price
    is deleted (in the price entry table only, not in the historical one).
    """

    if delete_prices:

        log.info("Deleting prices for non-existent price links")

        data = list()
        data.append(["Instrument", "Type", "Market", "Update Time", "Delay in Days", "Traders"])

        for ins in bonds:

            # Traders
            traders = set([x.Trader().Name() for x in ins.Trades() if x.Trader()])
            traders = traders - INACTIVE_TRADERS
            traders = list(traders)

            # Pricelinks
            pricelinks = acm.FPriceLinkDefinition.Select("instrument='{ins_name}'".format(ins_name=ins.Name()))

            # Markets
            existing_markets = sorted([x.Market().Name() for x in pricelinks])

            for price in list(ins.Prices()):

                if price.Market() and price.Market().Name() in set(PRICEFINDING_MARKET_NAMES) - set(existing_markets):

                    unix_time = price.UpdateTime()
                    update_time_str = datetime.fromtimestamp(int(unix_time)).strftime('%Y-%m-%d %H:%M:%S')
                    my_time = datetime.strptime(update_time_str, '%Y-%m-%d %H:%M:%S')
                    time_difference = datetime.now() - my_time
                    delay_in_days = time_difference.days

                    data.append([ins.Name(),
                                 ins.InsType(),
                                 price.Market().Name(),
                                 update_time_str,
                                 delay_in_days,
                                 ",".join(traders)])

                    try:
                        if not test_mode:
                            price.Delete()
                    except Exception as E:
                        log.critical(str(E))
                        data.append([ins.Name(),
                                     ins.InsType(),
                                     price.Market().Name(),
                                     update_time_str,
                                     delay_in_days,
                                     ",".join(traders)])

        # Write control data to file
        write_to_file(output_dirpath, "PriceEntry_DeletePrices", data)


if __name__ == "__main__":
    params = {
        "output_dirpath": r"M:\Common\awingerter\PriceCleanup",
        "TEST": 1,
        "CREATE_NEW_PRICELINKS": 1,
        "DELETE_PRICELINKS": 1,
        "DELETE_PRICES": 1,
    }

    ael_main(params)
