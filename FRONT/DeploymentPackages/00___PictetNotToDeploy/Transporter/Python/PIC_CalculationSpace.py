import acm

"""
Singleton Calculation Space class. 
Main purpose to make sure the same one instance of FCalculationSpaceCollection is reused for calcilations in trading manager columns.

@since: 2014-05-20
@author: Yauhen Mikulich
"""

class PIC_CalculationSpace(object):
    """
    Singleton Calculation Space class. 
    Main purpose to make sure the same one instance of FCalculationSpaceCollection
    is reused for calculations in trading manager columns
    """
    _Standard_Instance = None
    _ACI_Instance = None
    
    def __new__(cls, isStandardCalcSpace = False, debugMode = False):
        if isStandardCalcSpace is True:        
            if PIC_CalculationSpace._Standard_Instance is None:
                PIC_CalculationSpace._Standard_Instance = acm.Calculations().CreateStandardCalculationsSpaceCollection()            
            return PIC_CalculationSpace._Standard_Instance
            
        else:
            if PIC_CalculationSpace._ACI_Instance is None:
                PIC_CalculationSpace._ACI_Instance = acm.FCalculationSpaceCollection()            
            return PIC_CalculationSpace._ACI_Instance 
            
def test():        
    s1 = PIC_CalculationSpace()    
    s2 = PIC_CalculationSpace()    
    
    if(id(s1)==id(s2)):
        print "Same"
    else:
        print "Different"
        
    print "id1 = ", id(s1)
    print "id2 = ", id(s2)

    calcSpace1 = s1.GetSpace('FPortfolioSheet', 'Standard')
    calcSpace2 = s2.GetSpace('FPortfolioSheet', 'Standard')
    
    print "calcSpace1 = ", calcSpace1
    print "calcSpace2 = ", calcSpace2

                
if __name__ == "__main__":
    test()
        

