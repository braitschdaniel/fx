"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_DateTime

DESCRIPTION
        A gathering of date and time functions. 

CREATED BY
Bangin Mollazadegan, Favner Nordic AB 
2013-12-06  

CHANGE HISTORY

USER                             DATE             COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bangin Mollazadegan              2013-12-06       Created
Bangin Mollazadegan              2014-02-03       Added AddBankingDays() and GetPreviousBankingDay()
Bangin Mollazadegan              2014-02-10       Added ConvertDateTimeFormat()
Tobias Hagstrom                 2014-06-23       Added ACM method, swiss to FA conv 
Tobias Hagstrom                 2014-07-10       Conversions between FA str and Python DateTime and ISO 
Thomas Werth                     2015-09-22       calculateRelativeDate(), isValidStartEndDate
Yauhen Mikulich                  2015-10-27       Added date param to GetBankingDayNDaysAgo function
Yang Zhang			 2017-01-11	  FA Upgrade: Changed calendar name "Zurich" to "CHF - SZ"
----------------------------------------------------------------------------"""

import acm
import ael
import time
from datetime import datetime, timedelta


def GetCurrentDate_ISO():
    return time.strftime('%Y%m%d',time.gmtime())

def GetCurrentDateTime_ISO():
    return time.strftime('%Y%m%d_%H%M%S',time.gmtime())
    
def AddBankingDays(date, numberOfDays, calendar = 'CHF - SZ'):
    cal= ael.Calendar[calendar]
    d = ael.date(date)
    return d.add_banking_day(cal, numberOfDays)
  
def GetPreviousBankingDay():
    return AddBankingDays(ael.date_today(), -1)
    
def GetBankingDayNDaysAgo(date, N):
    return AddBankingDays(date, -N)

def ConvertDateTimeFormat(theDateTime, oldFormat, newFormat):
    datetime = time.strptime(theDateTime, oldFormat)
    datetimeReformated = time.strftime(newFormat,datetime)
    return datetimeReformated

    
# date and time modules modules added by Tobias Hagstrom

# swiss date conversions anc checks
def isSwissDateFormat(_date):
    _swissDate = None
    if _date[2:3]=="." and _date[5:6]==".":                
        _swissDate = 1
    return _swissDate
    
def ConvertFAtoSwissDate(_faDate):
    return ConvertDateTimeFormat(_faDate,"%Y-%m-%d","%d.%m.%Y")

def ConvertSwisstoFADate(_faDate):
    if isSwissDateFormat(_faDate):    
        _faDate = ConvertDateTimeFormat(_faDate,"%d.%m.%Y","%Y-%m-%d")         
    return _faDate
        
# offset banking days
def GetNthFABankingDay(_faDate,N):
    cal = acm.FCalendar['Zurich']
    return cal.AdjustBankingDays(_faDate, N)

#
#   CONVERT USING PYTHON LIBRARIES TIME AND DATETIME
# 
   
# creates a python DATETIME object from a FA date string (without the time)
# does not support time, sent [:10] if it is ok to truncate or modify
def GetPyDateTimefromFAStr(_FAdate):
    datetime_obj = datetime.strptime(_FAdate, '%Y-%m-%d')
    return datetime_obj

# creates the UNIX FLOAT since EPOC from the FA date string (without the time)    
def EPOCtimefromFAStr(_fa_date_str):
    _pyTimeStructObj = time.strptime(_fa_date_str,"%Y-%m-%d") 
    _pyISOTimeObj = time.mktime(_pyTimeStructObj)
    return _pyISOTimeObj 

def calculateRelativeDate(relativeDate, dateParseFormat='%Y-%m-%d'): 
    try: #ie. string +1y => datetime 2016-09-21 11:15:01
        fixDate = None
        try:
            fixDate = datetime.strptime(relativeDate, dateParseFormat)
            return fixDate
        except Exception as ex:
            pass
        assert len(relativeDate) > 0
        dayDelta = 0
        number = int(relativeDate[:-1])
        period = relativeDate[-1:].upper()
        assert period in ('Y', 'M', 'D')
        if period == 'Y':
            dayDelta = number * 365
        elif period == 'M':
            dayDelta = int(number * (365.0/12.0))
        elif period == 'D':
            dayDelta = number
        return datetime.now() + timedelta(days=dayDelta)
    except Exception as ex:
        print "could not calculate relative date from '%s': %s" % (relativeDate, ex)

def isValidStartEndDate(startDate, endDate):
    if type(startDate) != type(datetime.now()):
        print "start date '%s' is no datetime object" % startDate
        return False
    if type(endDate) != type(datetime.now()):
        print "end date '%s' is no datetime object" % endDate
        return False
    if endDate < startDate:
        print "endDate '%s' is before startDate '%s'" % (endDate, startDate)
        return False
    return True

