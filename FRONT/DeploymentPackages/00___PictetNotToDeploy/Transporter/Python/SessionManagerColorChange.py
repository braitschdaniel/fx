
"""
    Change the session color
    
    This script gets called on Session Manager creation.
    It attempts to set the session colour depending on the used ADS
    Typically, use red for production, use green for test, etc.


    If the session coloring should be done please insert the following 2 lines of code
    as a FUIEventHandler
    [RISK]FSessionManagerFrame:ChangeSessionColor =
        OnCreate=SessionManagerColorChange.OnCreate





    Color Index
    
    0	Standard
    1	Light Red
    2	Dark Red
    3	Light Green
    4	Dark Green
    5	Light Blue
    6	Dark Blue
    7	Purple
    8	Olive
    9	Gold

    
    
    HISTORY
    2018-05-01 Marc-Stephan Maenner (d-fine)
    CREATED

ENDDESCRIPTION
---------------------------------------------------------------------------------
""" 


import acm
import ael
import collections



def OnCreate(eii):
    try:
        print "SessionManagerColorChange"
        p = eii.ExtensionObject().SessionPreferences()
        print "--SessionPreferences Handle", p.Handle()
        
        
        env_color =  {
                        'ADSDEV2' : 3, 
                        'ADSDEV3' : 9,
                        'INTG' :    4,                  
                        'CTLQ' :    7,         
                        'ADSRISK1': 0						
        }
        environment_name = ael.ServerData.select()[0].customer_name
        print "--Environment", environment_name
        if environment_name in env_color:
            print "--Color:", env_color[environment_name]
            p.Color(p.Colors()[env_color[environment_name]])
    except Exception,e:
        print e

