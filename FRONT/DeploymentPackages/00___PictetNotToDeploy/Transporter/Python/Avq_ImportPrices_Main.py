'''---------------------------------------------------------------------------------
MODULE
    Avq_ImportPrices

DESCRIPTION
    Imports Avaloq EoD prices as last prices in the "Avaloq" market, if they are
    both 
        1. In position and contained in the Avaloq price extract
    and 
        2. Contained in the whitelist given as an query folder to the task.

HISTORY
    2018-03-19 Daniel Braitsch (d-fine)

ENDDESCRIPTION
---------------------------------------------------------------------------------'''

import acm
import os
import csv
import unicodedata
import pprint
import calendar
import cgi
import collections
from StringIO import StringIO
from datetime import datetime
import FBDPGui

import FRunScriptGUI
import FLogger

# import PIC_Mail

log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True)
default_mail = "email_PTS_FA_DEV@pictet.com"

ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    ['test_mode', 'Test Mode', 'bool', [True, False], True, 1],
    ['avaloq_dir', 'Avaloq Directory', 'string', None, r"\\<REMOVED>\Transfert_PROD\Negoce\FrontArena\Avaloq_Reco"],
    ['result_dir', 'Result Directory', 'string', None, r"C:\Temp"],
    ['result_file', 'Result Filename', 'string', None, "avaloq_to_mtm_market.html"],
    ['mail_list', 'EMail recipients (comma sep.)', 'string', None, default_mail, 0, 0, 'List of recipient emails.', None, 1],
    ['latest_market', 'Latest Price Market (not saved if empty)', 'string', acm.FMarketPlace.Instances()],
    ['mtm_market', 'MtM Market (not saved if empty)', 'string', acm.FMTMMarket.Instances()],
    ['instruments','Instruments', acm.FInstrument, None, FBDPGui.insertInstruments(), 0, 1, "Restrict to this Instrument Set"]   
]


# function to get rid of accents importing the .txt file
def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')

def _get_avaloq_files(avaloq_dir):
    """Fetch relevant Avaloq files for todays date"""
    date_today = datetime.now()
    y_m_d = '%Y_%m_%d'
    date_today = date_today.strftime(y_m_d)
    file_paths = []
    for file_name in os.listdir(avaloq_dir):
        if not file_name.startswith(date_today) or (not file_name.endswith(".txt")) or ("IMMO" in file_name):
            continue
        path = os.path.join(avaloq_dir, file_name)
        file_paths.append(path)
    return file_paths

# function to import data from avaloq files
def import_data(avaloq_dir):
    avaloq_data_dictionary = {}
    
    # pick avaloq txt files and save provided data
    file_paths = _get_avaloq_files(avaloq_dir)
    if not file_paths:
        raise ValueError("Could not find any Avaloq input files")
    
    for path in file_paths:
        with open(path, "rb") as f:
            data = f.read().decode('utf-8-sig')
            # remove accents
            data = strip_accents(data)
            reader = csv.DictReader(StringIO(data), delimiter='\t')

            for row in reader:

                # distinguish between AVALOQ txt files which use FR/EN name for prix/price
                if 'Prix' in row.keys():
                    row["Price"] = row['Prix']
                try:
                    row['Price'] = float(row['Price'].replace("'", ''))
                    avaloq_data_dictionary[row['AS: ISIN']] = {
                        'ISIN': row['AS: ISIN'],
                        'Currency': row['Monnaie'],
                        'Ava_Price': row['Price']
                    }
                except ValueError as E:
                    pass
                            
    return avaloq_data_dictionary


def write_yesterdays_mtm_price(instrument, market, data, test_mode, mtm_log):
    log.LOG("Writing MtM price: {} {} {}".format(instrument.Name(),market,str(data)))
    if not test_mode:
        last_banking_day = acm.FCalendar["PictechCal"].AdjustBankingDays(acm.Time.DateToday(), -1)
        existing_price = acm.FPrice.Select(
            "instrument=%d and market='%s' and day='%s'" % (
                instrument.Oid(), market, last_banking_day))

        mtm_log[instrument.Name()]["new_price"] = data['Ava_Price']
        mtm_log[instrument.Name()]["type"] = instrument.InsType()
          
        if existing_price:
            price = existing_price[0] 
            mtm_log[instrument.Name()]["old_price"] = price.Settle()
            if price.Settle() == data['Ava_Price']:
                # do not proceed if the price is equal to begin with
                return
        else:
            price =acm.FPrice(instrument=instrument, market=market)
            mtm_log[instrument.Name()]["old_price"] = "-"
        
        
        price.Currency(data['Currency'])
        price.Bid(data['Ava_Price'])
        price.Ask(data['Ava_Price'])
        price.Settle(data['Ava_Price'])
        last_banking_day = acm.FCalendar["PictechCal"].AdjustBankingDays(acm.Time.DateToday(), -1)
        price.Day(last_banking_day)
        price.Commit()

    
def write_latest_price(instrument, market, data, test_mode):
    log.LOG("Writing latest price: {} {} {}".format(instrument.Name(),market,str(data)))
    if not test_mode:
        existing_price = [p for p in instrument.Prices() if p.Market().Name() == market]
        price = existing_price[0] if existing_price else acm.FPrice(instrument=instrument, market=market)
        price.Currency(data['Currency'])
        price.Last(data['Ava_Price'])
        price.Settle(data['Ava_Price'])
        price.Day(acm.Time.DateToday())
        price.Commit()


def _ins_has_avaloq_price_finding(instrument):
    return instrument.PriceFindingChlItem() and instrument.PriceFindingChlItem().Name() == "Avaloq"


def ael_main(params):
    latest_market = params["latest_market"]
    mtm_market = params["mtm_market"]
    instruments = params['instruments']
    test_mode = params['test_mode']
    
    emails = None
    if params['mail_list']:
        emails = [mail for mail in params['mail_list'].split(",")]
    
    output_path = params['result_dir']
    output_filename = params['result_file']
    
    avaloq_data = import_data(params["avaloq_dir"])
    
    if mtm_market:
        log.LOG("Writing MtM prices for all instruments present in both the filter and Avaloq data")
    else:
        log.LOG("No MtM Market supplied - skipping MtM price insertion")
        
    if latest_market:
        log.LOG("Writing latest prices for all instruments present in both the filter and Avaloq data which have price fining group Avaloq")
    else:
        log.LOG("No marketplace supplied - skipping latest price insertion")
        
    mtm_log = collections.defaultdict(lambda: {}) # store overrides for later report
    for instrument in instruments:
        if instrument.Isin() in avaloq_data:
            avaloq_data_for_ins = avaloq_data[instrument.Isin()]
            if mtm_market:
                write_yesterdays_mtm_price(instrument, mtm_market, avaloq_data_for_ins, test_mode, mtm_log)
            if latest_market and _ins_has_avaloq_price_finding(instrument):
                write_latest_price(instrument, latest_market, avaloq_data_for_ins, test_mode)
        else:
            log.LOG("%s: not found in Avaloq File" % instrument.Name())
    
    
    report = _html_report(mtm_log)
        
    if output_path and output_filename:
        try:
            output_file = os.path.join(output_path, output_filename.format(date=acm.DateToday()))
            with open(output_file, "w") as of:
                of.write(report)
        except:
            log.ELOG("Prices written but could not write report!")
            raise
                
    if emails:
        subject = "Front Arena price import from Avaloq ({})".format(acm.DateToday())
        sendingMail = PIC_Mail.SendEmail(None, emails, subject, report, ishtml=True)
        sendingMail.send_EMail()


def _html_report(mtm_log):
    head = [
        "<html>",
        "  <head>",
        "    <title>Avaloq Price Import</title>",
        "    <style>",
        "      body { font-family:Arial,sans-serif;font-size:10pt; padding-left:25px; padding-top:25px; }",
        "      table, th, td { font-size:10pt; border-collapse: collapse; border: 1px solid #ddd; padding: 3px; }",
        "      .diff { background: #ffd; }"
        "    </style>",
        "  <head>",
        "  <body>",
    ]
    
    table = [
        "  <h1>Avaloq Price Import ({})</h1>".format(acm.Time.TimeNow()),
        "  The following instruments MtM prices were overwritten as part of the Avaloq price import. Actual changes ",
        "  (e.g. were the imported price differed from the existing price) are highlighted.<br/><br/>",
        "    <table>",
        "      <tr style=\"background: #f0f0f0\"><th><b>InsType</b></th><th><b>Instrument</b></th><th><b>Avaloq</b></th><th><b>Front Arena</b></th></tr>"
    ]
    table.extend([
        "<tr><td>{type}</td><td>{insid}</td><td class=\"{clss}\">{new}</td><td class=\"{clss}\">{old}</td></tr>".format(
            insid=cgi.escape(insid), 
            type=val["type"], 
            new=val["new_price"], 
            old=val["old_price"],
            clss="diff" if val["new_price"]!=val["old_price"] else "")
        for insid, val in sorted(mtm_log.items(), key=lambda (i, v): v["type"]+i)
        ])
    
    table.extend(["    </table>"])

    footer = [
        "  <br/><br/></body>",
        "</html>"
    ]
    return "".join(head+table+footer)
