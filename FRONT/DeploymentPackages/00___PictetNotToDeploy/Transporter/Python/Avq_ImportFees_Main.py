# encoding: utf-8
"""---------------------------------------------------------------------------------
MODULE
    Avq_ImportFees_Main


DESCRIPTION
    Import of fees and costs from Avaloq:
        - Brokerage fees paid on deals done
        - Interest charges on funding cash accounts
        - Lending commissions


ADDITIONAL INFORMATION ON PARAMETERS
    There is one input file (_SX_) for the import of brokerage fees and another
    input file (_BK_) for the import of interest and lending charges.

HISTORY
    2015-10-09 Tobias Hagstrom
        Created

    2017-04-05 Noureddine Meziane
        rise Exception when file not found.

    2018-03-29 Oliver Behm (d-fine)
    2018-04-11 Daniel Braitsch (d-fine)
        - Fixed issue with the detection of already imported fees
        - Consolidated SX and BK import scripts
        - Small refactoring (e.g. use csv instead of manually parsing)

ENDDESCRIPTION
---------------------------------------------------------------------------------"""

import acm
import FLogger

import os
import csv
from collections import namedtuple

log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=True)

Fee = namedtuple("Fee", "isin curr type amount portfolio trade_date report_date")


def switch_import_remove(index, field_values):
    """Callback for the task interface, to disable/enable the flag fields.

    If removeImportedFeesBool (index 1) or importFeesBool (index 2) was set change
    the other accordingly so only one is ever checked.
    """
    if index == 1:
        field_values[2] = "false" if field_values[1] == "true" else "true"
    if index == 2:
        field_values[1] = "false" if field_values[2] == "true" else "true"
    return field_values


default_variables = {
    "sx_default_path": "\\\<REMOVED>\Transfert_PROD\Negoce\FrontArena\PCO_Nostro\\NEG_CD - Analyse NOS - fcohen - FA - V20130621_SX_",
    "bk_default_path": "\\\<REMOVED>\Transfert_PROD\Negoce\FrontArena\PCO_Nostro\\NEG_CD - Analyse NOS - fcohen - FA - V20130621_BK_",
    "report_date_default": acm.Time.DateToday(),
    "portfolio_list_default": acm.FPhysicalPortfolio['']
}

ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    ['testModeBool', 'Test Mode', 'bool', [True, False], True, 1],
    ['removeImportedFeesBool', 'Revert Imported Fees', 'bool', [True, False], False, 1, 0, "", switch_import_remove],
    ['importFeesBool', 'Import Fees', 'bool', [True, False], True, 1, 0, "", switch_import_remove],
    ['dateSel', 'Report Date (Today/YYYY-mm-dd)', 'string', None, default_variables["report_date_default"], 0, 0,
     "Leave blank or write Today for using todays date, otherwise select a real historical date."],
    ['portSel', 'Select Portfolio', acm.FPhysicalPortfolio, None, default_variables["portfolio_list_default"], 1, 1,
     "Select one or multiple portfolios."],
    ['fileStrSX', 'SX File (Broker Fees)', 'string', None, default_variables["sx_default_path"], 0, 0,
     "The base name of the file including the path. Date will be added at runtime"],
    ['fileStrBK', 'BK File (Interest)', 'string', None, default_variables["bk_default_path"], 0, 0,
     "The base name of the file including the path. Date will be added at runtime"]
]


def convert_date_for_file_path(date_str):
    """Return a reformatted date."""
    return date_str.replace("-", "")


def avaloq_no_to_flt(ava_str):
    """Convert avaloq price to float and return it."""
    return float(ava_str.replace("'", ""))


def create_cash_fee(fee_key, fee):
    """Commit cash fee and save its key in OptionalKey."""
    fee_quantity = fee.amount
    if fee_quantity == 0.0:
        return

    t = acm.FTrade()
    t.Quantity = 0.0
    t.Price = 0.0
    t.Portfolio = fee.portfolio
    t.TradeTime = fee.trade_date
    t.ValueDay = fee.trade_date
    t.AcquireDay = fee.trade_date
    t.Instrument = fee.curr
    t.Currency = fee.curr
    t.Status = "FO Confirmed"
    t.Acquirer = "PICTET"
    t.Trader = "FMAINTENANCE"
    t.Counterparty = "FEES"
    t.OptionalKey = fee_key
    t.Commit()
    return t


def create_cash_payment(trade, amount, currency, booking_date, payment_type):
    """create cash_fee payment"""
    payment = acm.FPayment()
    payment.Type(payment_type)
    payment.Currency(currency)
    payment.Amount(amount)
    payment.ValidFrom(booking_date)
    payment.PayDay(booking_date)
    payment.Party('FEES')
    payment.Trade(trade)	
    payment.Commit()


def create_ins_fee(fee_key, fee):
    """Commit instrument fee and save its key in OptionalKey."""
    fee_quantity = fee.amount
    if fee_quantity == 0.0:
        return

    t = acm.FTrade()
    t.Instrument = fee.isin
    t.Quantity = 0
    t.Price = 0.0
    t.Fee = fee_quantity
    t.Portfolio = fee.portfolio
    t.TradeTime = fee.trade_date
    t.ValueDay = t.Instrument().GetSpotDay(fee.trade_date, None)
    t.AcquireDay = t.Instrument().GetSpotDay(fee.trade_date, None)
    t.Currency = fee.curr
    t.Status = "FO Confirmed"
    t.Acquirer = "PICTET"
    t.Trader = "FMAINTENANCE"
    t.Counterparty = "FEES"
    t.OptionalKey = fee_key
    t.Commit()


def fee_from_row(row, report_date):
    """Extract and return fee information.

    Based on the input file type the fee information is extracted by row.
    The data is stored in named tuple Fee.
    """
    # Column names in the SX file
    sx_col_portfolio = "v_sx_nid_cn"
    sx_col_trade_date = "v_sx_date_trade"
    sx_col_isin = "v_sx_aid_as"
    sx_col_ccy = "v_sx_key_ccy"
    sx_col_cost = "v_sx_num_ccy_cost"

    # Column names in the BK file
    bk_col_portfolio = "v_bk_nid_cn"
    bk_col_trade_date = "v_bk_date_bill"
    bk_col_ccy = "v_bk_key_ccy"
    bk_col_cost = "v_bk_num_ccy_pos"

    if sx_col_portfolio in row:
        return Fee(
            isin=row[sx_col_isin],
            curr=row[sx_col_ccy],
            type="SX",
            amount=-1.0 * avaloq_no_to_flt(row[sx_col_cost]),
            portfolio=row[sx_col_portfolio],
            trade_date=row[sx_col_trade_date],
            report_date=report_date
        )

    elif bk_col_portfolio in row:
        return Fee(
            isin=None,
            curr=row[bk_col_ccy],
            type="BK",
            amount=avaloq_no_to_flt(row[bk_col_cost]),
            portfolio=row[bk_col_portfolio],
            trade_date=row[bk_col_trade_date],
            report_date=report_date
        )

    else:
        raise ValueError("Unknown format of row {0}".format(row))


def key_from_fee(fee):
    """Netting key for fee objects."""
    return "{type}{report_date}{isin}{ccy}{portfolio}".format(
        type=fee.type,
        isin=fee.isin or "",
        report_date=convert_date_for_file_path(fee.report_date),
        ccy=fee.curr,
        portfolio=fee.portfolio[-6:]
    )


def add_and_net_fee(fees, fee, portfolios):
    """Extract and return fee information.

    Based on the input file type the fee information is extracted by row.
    The data is stored in named tuple Fee.
    """
    if fee.portfolio not in [portfolio.Name() for portfolio in portfolios]:
        return

    key = key_from_fee(fee)
    if key not in fees:
        fees[key] = fee
    else:
        fees[key] = Fee(
            isin=fees[key].isin,
            curr=fees[key].curr,
            type=fees[key].type,
            amount=fees[key].amount + fee.amount,
            portfolio=fees[key].portfolio,
            trade_date=fees[key].trade_date,
            report_date=fees[key].report_date
        )


def open_utf_8_sig_as_dict_reader(fee_file_path, delimiter="\t"):
    """Open utf-8-sig file as csv.DictReader object.

    Reason: Input file starts with a utf-8 BOM
    """
    with open(fee_file_path, "r") as infile_unicode:
        infile_ascii = infile_unicode.read().decode("utf-8-sig").encode("utf-8")
        reader = csv.DictReader(infile_ascii.splitlines(), delimiter=delimiter)
        return reader


def assemble_file_path_for_report_date(sx_file_path_base, bk_file_path_base, report_date):
    """Assemble and return file paths for report date if both exist"""
    date_for_file_path = convert_date_for_file_path(report_date)
    sx_file_path = None
    bk_file_path = None
    if sx_file_path_base:
        sx_file_path = sx_file_path_base + date_for_file_path + ".txt"
    if bk_file_path_base:
        bk_file_path = bk_file_path_base + date_for_file_path + ".txt"

    # Check whether both input files exist
    if not os.path.isfile(sx_file_path):
        log.error('Cannot find SX/broker fees file path {sx_file_path}'.format(sx_file_path=sx_file_path))
        raise Exception('Path to SX/broker fees file not valid - aborting.')
    if not os.path.isfile(bk_file_path):
        log.error('Cannot find BK/interest charge file path {bk_file_path}'.format(bk_file_path=bk_file_path))
        raise Exception('Path to BK/interest charge file not valid - aborting.')

    return sx_file_path, bk_file_path


def import_fees_from_files(sx_file_path, bk_file_path, report_date, portfolios, test_mode):
    """Import fees from Avaloq files and book them into Front Arena.

    For report date and portfolios to be chosen fees of  resp. sx- and bk_file are imported.
    For disables test_mode fee trades are booked if not already committed.
    For this purpose a key is saved as external ID.
    """

    fees = {}

    for fpath in [sx_file_path, bk_file_path]:
        with open(fpath, "r") as infile:
            reader = open_utf_8_sig_as_dict_reader(fpath)
            for row in reader:
                fee = fee_from_row(row, report_date)
                add_and_net_fee(fees, fee, portfolios)

    if fees:
        log.info("")
        log.info("Loading SX/broker fees file with path {sx_file_path}.".format(sx_file_path=sx_file_path))
        log.info("Loading BK/interest charge file with path {bk_file_path}.".format(bk_file_path=bk_file_path)) 
        log.info("Importing fees for report_date {report_date}.".format(report_date=report_date))
        log.info("")

    for fee_key, fee in fees.items():

        # continue if fee amount is zero
        if abs(fee.amount) < 1e-5:
            continue

        # continue if fee trade already exist
        search_string = "counterparty = 'FEES' and optionalKey = {fee_key}"
        if acm.FTrade.Select(search_string.format(fee_key=fee_key)):
            log.info("Fee trade with External ID: {fee_key} already loaded.".format(fee_key=fee_key,
            portfolio=fee.portfolio, isin=fee.isin, curr=fee.curr))
            continue

        log.info("Booking fee with External ID: {fee_key} Portfolio: {portfolio} ISIN: {isin} Currency: {curr}".format(fee_key=fee_key,
            portfolio=fee.portfolio, isin=fee.isin, curr=fee.curr))

        if not test_mode:
            try:
                acm.BeginTransaction()

                if not fee.isin:
                    cash_trade = create_cash_fee(fee_key, fee)
                    create_cash_payment(cash_trade, fee.amount, fee.curr, fee.trade_date, payment_type='Internal Fee')

                elif not acm.FInstrument[fee.isin]:
                    log.warn("No instrument found for ISIN {isin}, booking fee on cash {curr} for portfolio {portfolio}.".format(isin=fee.isin, 
                        curr=fee.curr, portfolio=fee.portfolio))
                    cash_trade = create_cash_fee(fee_key, fee)
                    create_cash_payment(cash_trade, fee.amount, fee.curr, fee.trade_date, payment_type='Broker Fee')

                else:
                    create_ins_fee(fee_key, fee)
                acm.CommitTransaction()

            except Exception as E:
                acm.AbortTransaction()
                log.error('Exception: {E}, Fee transaction {fee_key} did not succeed.'.format(E=E, fee_key=fee_key))


def delete_fees_for_report_date(report_date, portfolios, test_mode):
    """Delete all fees for a report date and portfolios to be chosen."""
    for portfolio in portfolios:
        search_string = "counterparty = 'FEES' and trader = 'FMAINTENANCE' and optionalKey like '*{report_date}*' and portfolio = {portfolio}"
        trades = acm.FTrade.Select(
            search_string.format(report_date=convert_date_for_file_path(report_date), portfolio=portfolio.Name()))
            
        if trades:
            log.info("")
            log.info("Deletion of fees for report date {report_date} of portfolio {portfolio}.".format(report_date=report_date, 
                portfolio=portfolio.Name()))              
            
        else:
            log.info("")
            log.info("No fees to be deleted for report date {report_date} and portfolio {portfolio}.".format(report_date=report_date, 
                portfolio=portfolio.Name()))

        for trade in list(trades):
            log.info("")
            log.info("Deleting trade " + str(trade.Oid()))
            if not test_mode:
                trade.Delete()


def ael_main(params):
    """Either import or delete fees for report date and portfolios to be chosen."""
    test_mode = params['testModeBool']
    import_fees = params['importFeesBool']
    revert_import = params['removeImportedFeesBool']

    report_date = str(params['dateSel'])
    portfolios = params['portSel']

    sx_file_path_base = str(params['fileStrSX'])
    bk_file_path_base = str(params['fileStrBK'])

    date_today = acm.Time.DateToday()
    if report_date == "" or report_date.upper() == "TODAY":
        report_date = date_today
        
    log.info("Starting Avq_ImportFeesMain script for parameters:")
    log.info("")

    log.info("Report date: {report_date}.".format(report_date=report_date))
    log.info("")
    for portfolio in portfolios:
        log.info("Portfolio {portfolio}.".format(portfolio=portfolio.Name()))

    sx_file_path = assemble_file_path_for_report_date(sx_file_path_base, bk_file_path_base, report_date)[0]
    bk_file_path = assemble_file_path_for_report_date(sx_file_path_base, bk_file_path_base, report_date)[1]

    if import_fees:
        import_fees_from_files(sx_file_path, bk_file_path, report_date, portfolios, test_mode)

    if revert_import:
        delete_fees_for_report_date(report_date, portfolios, test_mode)
