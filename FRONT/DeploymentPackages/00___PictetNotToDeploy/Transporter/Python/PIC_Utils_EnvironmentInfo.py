"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_EnvironmentInfo

DESCRIPTION
        From this utility module we one can determain which environment
        we are in. 

        This is depending on that the correct value is set by the database 
        restore script, when the database is restored from PROD. 
        

CREATED BY
Bangin Mollazadegan, Favner Nordic AB 
2014-02-14  

CHANGE HISTORY

USER                                    DATE                    COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bangin Mollazadegan                    2014-02-14               Created
Thomas Werth                           2015-09-30               getRelevantContextName, _getContent, getPrimeUserName
--------------------------------------------------------------------------------------------------------------------"""

import acm
import ael


def GetEnvironment():
    environmentValue = ael.ServerData.select()[0].customer_name
    return str(environmentValue).upper()
'''
to use:
if PIC_Utils_EnvironmentInfo.GetEnvironment() in ['CTLQ', 'QA']:
    print 'qa'

'''

def _getContent(pars, paramType='Context Par'):
    pars = list(pars)
    result = []
    for pm in pars:
        for pmi in [pmi for pmi in pm.ParMappingInstances() if pmi.ParameterType() == paramType]:
            result.append(pmi)
    return result

def getRelevantContextName(userName=acm.User().Name()):
    user = acm.FUser[userName]
    workspace = _getContent(acm.FParameterMapping.Select('overrideLevel="Workspace" and user="%s"' % user.Name()))
    if len(workspace) > 0:
        return workspace[0].Context().Name()
    usr = _getContent(acm.FParameterMapping.Select('overrideLevel="User" and user="%s"' % user.Name()))
    if len(usr) > 0:
        return usr[0].Context().Name()
    group = _getContent(acm.FParameterMapping.Select('overrideLevel="Group" and userGroup="%s"' % user.UserGroup().Name()))
    if len(group) > 0:
        return group[0].Context().Name()
    organization = _getContent(acm.FParameterMapping.Select('overrideLevel="Organisation" and organisation="%s"' % user.UserGroup().Organisation().Name()))
    if len(organization) > 0:
        return organization[0].Context().Name()
    glob = _getContent(acm.FParameterMapping.Select('overrideLevel="Global"'))
    if len(glob) > 0:
        return glob[0].Context().Name()

def getPrimeUserName():
    return acm.User().Name()

