import acm
import re

import FDLApi
import FLogger

from itertools import combinations
from collections import defaultdict

logger = FLogger.FLogger.GetLogger('DL')


def execute_DL_request(stocks, load):
    # Adjust to "-14d", if we decide to exactly reproduce the old script
    from_date = acm.Time.DateAdjustPeriod(acm.Time.DateToday(), "-14d")
    to_date = "Today"

    FDLApi.DataLoader_request(input=stocks,
                              identifier_type='ISIN',
                              mode='SecurityID',
                              load=load,
                              provider='Bloomberg',
                              user=acm.User().Name(),
                              notification_media='PRIME_LOG',
                              notify_level='TRACK',
                              log_file_level='INFO',
                              from_date=from_date,
                              to_date=to_date
                              )


def return_stocks_relevant_for_dividends(stored_query, include_expired_instruments=False, include_zero_positions=False,
                                         excluded_portfolios=None):
    # Create calculation space and insert results
    context = acm.GetDefaultContext()
    sheet_type = 'FPortfolioSheet'
    calc_space = acm.Calculations().CreateCalculationSpace(context, sheet_type)

    # Get Oids of excluded portfolios
    if excluded_portfolios:
        excluded_portfolios_oid = [p.Oid() for p in excluded_portfolios]
    else:
        excluded_portfolios_oid = []

    # Insert query into calculation space
    tree_proxy = calc_space.InsertItem(stored_query)
    calc_space.Refresh()

    # Group by underlying
    grouper_1 = acm.Risk().GetGrouperFromName("Underlying")
    grouper_2 = acm.Risk().GetGrouperFromName("Trade Portfolio")
    grouper = acm.CreateWithParameter('FChainedGrouper', [grouper_1, grouper_2])
    tree_proxy.ApplyGrouper(grouper)
    calc_space.Refresh()

    # Print all children to the top level
    stocks = set()
    child = tree_proxy.Iterator().FirstChild()
    while child:

        multi_instrument = child.Tree().Item()

        underlying_name = multi_instrument.StringKey()
        underlying = acm.FInstrument[underlying_name]

        if not underlying:
            logger.ELOG("Could not find instrument with name {underlying_name}".format(underlying_name=underlying_name))
            continue

        for combi in multi_instrument.PortfolioAndInstruments():
            portfolio = combi.Portfolio()
            instrument = combi.Instrument()

            if instrument.IsExpired() and not include_expired_instruments:
                continue

            if portfolio.Oid() in excluded_portfolios_oid:
                continue

            trades = acm.FTrade.Select(
                "portfolio='{portfolio}' and instrument='{insid}'".format(portfolio=portfolio.Name(),
                                                                          insid=instrument.Name()))
            position = sum([t.Quantity() for t in trades])
            is_zero_position = abs(position) < 1e-8
            if is_zero_position and not include_zero_positions:
                continue

            if underlying.InsType() in ["Stock"]:
                stocks.add(underlying.Isin())

        child = child.NextSibling()

    return stocks


def create_query_from_portfolios(portfolios):
    query = acm.CreateFASQLQuery(acm.FTrade, 'AND')

    some_node = query.AddOpNode('OR')
    for pf in portfolios:
        some_node.AddAttrNode('Portfolio.name', 'EQUAL', pf.Name())

    return query


# Used in ael_variables below
allQueries = acm.FStoredASQLQuery.Select('user=0 and subType="FTrade"')

choice_of_portfolios = acm.FPhysicalPortfolio.Select('')
choice_of_stocks = [t for t in acm.FStock.Select("") if not re.match(r'~.*', t.Name())]

ael_variables = [
    ['Query folder_Trading', 'INCLUDE (query folder)_Trading', 'FStoredASQLQuery', allQueries,
     'DividendsTradingFlagSet', 1, 0,
     'None'],
    ['Excluded_portfolios_Trading', 'EXCLUDE (portfolios)_Trading', 'FPhysicalPortfolio', choice_of_portfolios,
     'NEGPEC,PEC BuySide', 1, 1,
     'None'],
    ['Current Dividend_Trading', 'Current Dividend_Trading', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Dividend Estimates_Trading', 'Dividend Estimates_Trading', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Historical Dividends_Trading', 'Historical Dividends_Trading', 'bool', [True, False], False, 1, 0,
     'None'],
    ['Include expired instruments_Trading', 'Include expired instruments_Trading', 'bool', [True, False], False, 1, 0,
     'None'],
    ['Include zero positions_Trading', 'Include zero positions_Trading', 'bool', [True, False], False, 1, 0,
     'None'],
    ['Portfolios_PEC', 'Portfolios_PEC', 'FPhysicalPortfolio', choice_of_portfolios, 'NEGPEC,PEC BuySide', 1, 1,
     'None'],
    ['Current Dividend_PEC', 'Current Dividend_PEC', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Dividend Estimates_PEC', 'Dividend Estimates_PEC', 'bool', [True, False], False, 1, 0,
     'None'],
    ['Historical Dividends_PEC', 'Historical Dividends_PEC', 'bool', [True, False], False, 1, 0,
     'None'],
    ['Include expired instruments_PEC', 'Include expired instruments_PEC', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Include zero positions_PEC', 'Include zero positions_PEC', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Stocks_Instruments', 'Stocks_Instruments', 'FStock', choice_of_stocks, 'SLHN VX,LHN VX,KNIN VX,MDU UN', 1, 1,
     'None'],
    ['Current Dividend_Instruments', 'Current Dividend_Instruments', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Dividend Estimates_Instruments', 'Dividend Estimates_Instruments', 'bool', [True, False], True, 1, 0,
     'None'],
    ['Historical Dividends_Instruments', 'Historical Dividends_Instruments', 'bool', [True, False], False, 1, 0,
     'None'],
]


def ael_main(params):

    d = defaultdict(set)

    suffixes = ["_Trading", "_PEC", "_Instruments"]
    for suffix in suffixes:

        # Depending on tab in GUI, the input must first be preprocessed
        # 1) If input is query folder, there is nothing to be done
        # 2) If input is a list of portfolios, they are converted to query folders
        # 3) If input is a list of stocks, the input is already the result ("underlyings")
        if suffix == "_Trading":
            stored_query = params['Query folder' + suffix]
        elif suffix == "_PEC":
            portfolios = params['Portfolios_PEC']
            stored_query = create_query_from_portfolios(portfolios)
        elif suffix == "_Instruments":
            stored_query = None

        # Set the arguments for the function call below
        include_expired_instruments = params.get('Include expired instruments' + suffix, True)
        include_zero_positions = params.get('Include zero positions' + suffix, True)
        excluded_portfolios = params.get('Excluded_portfolios' + suffix, None)

        # Get "underlyings" (stocks only)
        # If we are in the case "Trading" or "PEC", we use an FASQL query
        # If we are in the case "Instruments", the input values are already the underlyings
        if suffix in ["_Trading", "_PEC"]:
            stocks = return_stocks_relevant_for_dividends(stored_query,
                                                          include_expired_instruments=include_expired_instruments,
                                                          include_zero_positions=include_zero_positions,
                                                          excluded_portfolios=excluded_portfolios)
        elif suffix == "_Instruments":
            stocks = set([ins.Isin() for ins in params['Stocks' + suffix]])

        # Choose which dividend data is to be downloaded
        if params['Current Dividend' + suffix]:
            d[('Dividend',)] = d[('Dividend',)] | stocks
        if params['Dividend Estimates' + suffix]:
            d[('Dividend Estimates',)] = d[('Dividend Estimates',)] | stocks
        if params['Historical Dividends' + suffix]:
            d[('Historical Dividends',)] = d[('Historical Dividends',)] | stocks

    # Intersection of all sets
    d_keys = d.keys()
    d[tuple([x[0] for x in d_keys])] = reduce(set.intersection, d.values())

    # Intersection of 2 sets, respectively
    for s1, s2 in combinations(tuple(d_keys), 2):
        d[s1 + s2] = set.intersection(d[s1], d[s2])

    # Remove stocks from one set, if they are already downloaded in another
    for s1, s2 in combinations(d.keys(), 2):
        if set(s1) < set(s2):
            d[s1] = d[s1] - d[s2]
        elif set(s1) > set(s2):
            d[s2] = d[s2] - d[s1]

    for load, stocks in d.iteritems():
        if 0 < len(stocks) <= 1000:
            logger.LOG("Launching Data Loader to download {load} for {num} stocks".format(load=" and ".join(load),
                                                                                          num=len(stocks)))
            execute_DL_request(list(stocks), list(load))
        elif len(stocks) > 1000:
            logger.ELOG("Too many stocks ({num}): DataLoader request will not be executed".format(num=len(stocks)))

if __name__ == "__main__":
    pass
