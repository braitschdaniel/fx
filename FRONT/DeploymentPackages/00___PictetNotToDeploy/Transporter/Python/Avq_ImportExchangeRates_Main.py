import acm
import os
import ael
import datetime
import sys

YES = 'Yes'
NO = 'No'
BASE_CURRENCY_AVALOQ = 'CHF'

def importForeignExchangeRate(aPath, aPrefix, anExtension, anExpectedDay, test_mode, anInternalMarketName="internal"):
    if not anExpectedDay:
        day = datetime.date.today()
        anExpectedDay = day.strftime("%Y%m%d")
        print "Using current business day: %s" % anExpectedDay
        
    inputFile = os.path.join(aPath, '%s%s%s' % (aPrefix, anExpectedDay, anExtension))
    if not os.path.exists(inputFile):
        print "Error, input file %s does not exist" % inputFile
        sys.exit(0)
    elif not os.path.isfile(inputFile):
        print "Error, input file %s is not a regular file" % inputFile
        sys.exit(0)
    else:
        print "Input file is", inputFile
        file = open(inputFile, "r")
        lines = file.readlines()
    
        internalMarket = acm.FParty[anInternalMarketName]
        base_curr_Avaloq = acm.FCurrency[BASE_CURRENCY_AVALOQ]
        if internalMarket:    
            currencyPairCHF = {}
            
            for line in lines:
                currencyName, exchangeRateAsk, exchangeRateBid, date  = line.split(',')
                formattedDate = date[4:8] + date[2:4] + date[:2]
                if formattedDate == anExpectedDay:
                    currencyPairCHF[currencyName] = exchangeRateAsk
                else:
                    print "Out of scope %s" % formattedDate, line
            
            print "Found %s exchange rates" % len(currencyPairCHF)
            
            currencies = acm.FCurrency.Select("")
            # Mezn @ 18.07.2016 upgrade PLD-2-PLS
            for currency in currencies:
                priceLinkDefinitions = acm.FPriceLinkDefinition.Select("instrument='"+currency.Name()+"'")
                for priceLinkDef in priceLinkDefinitions:
                    currencyIn = currency
                    currencyOut = priceLinkDef.Currency()
                    print ">>>>>>>>>>>>>>>>>>>>>>", currencyIn.Name(), currencyOut.Name()
                    
                    rateIn = currencyPairCHF.get(currencyIn.Name())
                    if rateIn:
                        rateOut = currencyPairCHF.get(currencyOut.Name())
                        if rateOut:
                            if rateOut > 0:
                                rate = float(rateIn) / float(rateOut)
                                print currencyIn.Name()+currencyOut.Name(), rate, anExpectedDay
                                
                                price = None
                                query = "instrument = '%s' and currency = '%s' and market = '%s' and day = '%s'"%(
                                        currencyIn.Name(), currencyOut.Name(), anInternalMarketName, anExpectedDay)
                                prices = acm.FPrice.Select(query)
                                if len (prices) == 0:                                
                                    price = acm.FPrice()
                                    price.Instrument(currencyIn)
                                    price.Currency(currencyOut)
                                    price.Market(internalMarket)
                                    price.Day(anExpectedDay)
                                elif len (prices) == 1:
                                    print "Update Mode"
                                    price = prices[0]
                                else:
                                    print "Error, found several prices for the same definition"
                                price.Settle(rate)
                                if not test_mode:
                                    price.Commit()
                               
                        else:
                            print "Error, exchange rate %s not available in the file" % currencyOut.Name()
                    else:
                        print "Error, exchange rate %s not available in the file" % currencyIn.Name()
                    
                    print "<<<<<<"
                    
                    # ###############################################################################################    
                    # Insert price entry for the currency against CHF
                    if currency.Name() != BASE_CURRENCY_AVALOQ:                    
                        curr_basecurr = currencyPairCHF.get(currency.Name())
                        if curr_basecurr:
                            query = "instrument = '%s' and currency = '%s' and market = '%s' and day = '%s'"%(currency.Name(), BASE_CURRENCY_AVALOQ, anInternalMarketName, anExpectedDay)
                            thePrices = acm.FPrice.Select(query)
                            if not thePrices:
                                thePrice = acm.FPrice()
                                thePrice.Instrument(currency)
                                thePrice.Currency(base_curr_Avaloq)
                                thePrice.Market(internalMarket)
                                thePrice.Day(anExpectedDay)
                                thePrice.Settle(curr_basecurr)
                                print "Price settled: ", currency.Name(), BASE_CURRENCY_AVALOQ, curr_basecurr, anExpectedDay, anInternalMarketName
                                if not test_mode:
                                    thePrice.Commit() 
                                                       
                            elif len(thePrices) > 1:
                                print "There is more than one price returned by the query: ", query
                            else:
                                thePrice = thePrices[0] 
                                clone = thePrice.Clone()
                                clone.Settle(curr_basecurr)
                                thePrice.Apply(clone)
                                print "Price updated: ", currency.Name(), BASE_CURRENCY_AVALOQ, curr_basecurr, anExpectedDay, anInternalMarketName
                                if not test_mode:
                                    thePrice.Commit()
                                     
                        else:
                            print "Price for %s %s not available in the file" % (currency.Name(), BASE_CURRENCY_AVALOQ)
                        
                
ael_variables = [['test_mode', 'Test Mode?', 'string', [YES, NO], YES, 1, 0],
                ['FilePath', 'Input File', 'string', None, "X:\INTRO\Bla", 1, 0],
                ['FilePrefix', 'File Prefix', 'string', None, 'AvaloqSpotRates_', None, None, ''],
                ['FileExtension', 'File Extension', 'string', None, '.txt', None, None, ''],
                ['ExpectedDate', 'Day', 'string', None, datetime.date.today().strftime("%Y%m%d"), None, None, '']]


def ael_main(dict):
    path = dict['FilePath']
    prefix = dict['FilePrefix']
    extension = dict['FileExtension']
    day = dict['ExpectedDate']
    test_mode = dict["test_mode"] == YES

    
    print "Launch import with the following params: path=%s, prefix=%s, extension=%s, day=%s" % (path, prefix, extension, day)
    
    importForeignExchangeRate(path, prefix, extension, day, test_mode)

