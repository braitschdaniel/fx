"""---------------------------------------------------------------------------------
MODULE
    EoY_DeleteTransactionHistory

DESCRIPTION
    The module is part of the technical EoY process
    1. It deletes instruments in the transaction history
    that do not exist any more.
    2. It deletes trades in the transaction history
    that do not exist any more.

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    2018-02-20 Daniel Braitsch (d-fine)

ENDDESCRIPTION
---------------------------------------------------------------------------------"""

import acm
import ael
import sys
import time

import FRunScriptGUI
import FLogger
log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=True)

ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    ['test_mode','Test Mode', 'bool', [True, False], True, 1]
]

def ael_main(params):

    # query of trans_hst of instruments that do not exist any more
    # trans_record_type = 4: 'instrument'
    instruments_control_query = """select
    count(*)
from trans_hst
where trans_record_type = 4 and record_id not in (select insaddr from instrument)
"""

    instruments_delete_query = """delete
from trans_hst
where trans_record_type = 4 and record_id not in (select insaddr from instrument)
"""

    # query of trans_hst of trades that do not exist any more
    # trans_record_type = 19: 'trade'
    trades_control_query = """select
    count(*)
from trans_hst
where trans_record_type = 19 and record_id not in (select trdnbr from trade)
"""

    trades_delete_query = """delete
from trans_hst
where trans_record_type = 19 and record_id not in (select trdnbr from trade)
"""


    if params['test_mode']:

        result = ael.dbsql(instruments_control_query)[0][0][0]
        log.info('Transaction hist entries with record type instrument to be deleted: {num_inst_2_delete}'.format(num_inst_2_delete=result))

        result = ael.dbsql(trades_control_query)[0][0][0]
        log.info('Transaction hist entries with record type trade to be deleted: {num_trades_2_delete}'.format(num_trades_2_delete=result))

    if not params['test_mode']:
        # actually delete instruments from transaction history
        num_instruments_2_delete_pre = ael.dbsql(instruments_control_query)[0][0][0]
        log.info('Number of instruments to be deleted: {num_instruments_2_delete_pre}'.format(
            num_instruments_2_delete_pre=num_instruments_2_delete_pre))
        ael.dbsql(instruments_delete_query)
        log.info('Instruments deleted from transaction history')
        num_instruments_2_delete_post = ael.dbsql(instruments_control_query)[0][0][0]
        log.info('Number of instruments that could not be deleted: {num_instruments_2_delete_post}'.format(
            num_instruments_2_delete_post=num_instruments_2_delete_post))

        # actually delete trades from transaction history
        num_trades_2_delete_pre = ael.dbsql(trades_control_query)[0][0][0]
        log.info('Number of trades to be deleted: {num_trades_2_delete_pre}'.format(
            num_trades_2_delete_pre=num_trades_2_delete_pre))
        ael.dbsql(trades_delete_query)
        log.info('Trades deleted from transaction history')
        num_trades_2_delete_post = ael.dbsql(trades_control_query)[0][0][0]
        log.info('Number of trades that could not be deleted: {num_trades_2_delete_post}'.format(
            num_trades_2_delete_post=num_trades_2_delete_post))
