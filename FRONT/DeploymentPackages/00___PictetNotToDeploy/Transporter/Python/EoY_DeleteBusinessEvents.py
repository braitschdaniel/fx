"""---------------------------------------------------------------------------------
MODULE
    EoY_DeleteBusinessEvents

DESCRIPTION
    The module is part of the technical EoY process and
    it deletes business events and their links for entries
	with the creation time up to the end of last year.
 
HISTORY
    2018-02-15 Mahan Tahvildari (d-fine)
    2018-02-14 Dr Dominik Bauernfeind (d-fine)
    
ENDDESCRIPTION
---------------------------------------------------------------------------------"""

import acm
import FBDPGui
import FBDPCurrentContext
from FBDPCurrentContext import Summary
from FBDPCurrentContext import Logme
from datetime import datetime

ael_variables = FBDPGui.LogVariables()

def ael_main(params):

    FBDPCurrentContext.CreateLog(__file__,
                             1,
                             params['LogToConsole'],
                             params['LogToFile'],
                             params['Logfile'],
                             0,
                             "",
                             "")
    start = datetime.now()
    Logme()("Start time: {0}".format(start))
    
    # Counter for object deletion
    DelCorporateAction = 0
    DelBusinessEventPaymentLink = 0
    DelBusinessEventTradeLink = 0
    DelBusinessEventInstrumentLink = 0
    DelBusinessEvent = 0
    
    # Deletion of coporate actions
    for i in list(acm.FCorporateAction.Select('')):
        if int(datetime.fromtimestamp(i.CreateTime()).strftime('%Y')) < int(datetime.now().strftime('%Y')):
            try:
                Logme()("Delete corporate action {0}".format(i.Oid()))
                i.Delete()
                DelCorporateAction += 1
            except Exception as E:
                Logme()("Corporate action {0} could not be deleted".format(i.Oid(), E))
    
    # Deletion of business event payment links
    for i in list(acm.FBusinessEventPaymentLink.Select('')):
        if int(datetime.fromtimestamp(i.CreateTime()).strftime('%Y')) < int(datetime.now().strftime('%Y')):
            try:
                Logme()("Delete business event payment link {0}".format(i.Oid()))
                i.Delete()
                DelBusinessEventPaymentLink += 1
            except Exception as E:
                Logme()("Business event payment link {0} could not be deleted".format(i.Oid(), E))
    
    # Deletion of business event trade links
    for i in list(acm.FBusinessEventTradeLink.Select('')):
        if int(datetime.fromtimestamp(i.CreateTime()).strftime('%Y')) < int(datetime.now().strftime('%Y')):
            try:
                Logme()("Delete business event trade link {0}".format(i.Oid()))
                i.Delete()
                DelBusinessEventTradeLink += 1
            except Exception as E:
                Logme()("business event trade link {0} could not be deleted".format(i.Oid(), E))
    
    # Deletion of business event instrument links
    for i in list(acm.FBusinessEventInstrumentLink.Select('')):
        if int(datetime.fromtimestamp(i.CreateTime()).strftime('%Y')) < int(datetime.now().strftime('%Y')):
            try:
                Logme()("Delete business event instrument link {0}".format(i.Oid()))
                i.Delete()
                DelBusinessEventInstrumentLink += 1
            except Exception as E:
                Logme()("Business event instrument link {0} could not be deleted".format(i.Oid(), E))
            
    # Deletion of business events
    for i in list(acm.FBusinessEvent.Select('')):
        if int(datetime.fromtimestamp(i.CreateTime()).strftime('%Y')) < int(datetime.now().strftime('%Y')):
            try:
                Logme()("Delete business event {0}".format(i.Oid()))
                i.Delete()
                DelBusinessEvent += 1
            except Exception as E:
                Logme()("Business event {0} could not be deleted".format(i.Oid(), E))
    
    Logme()("SUMMARY:\n Corporate Action deleted: {0}\n Business Event Payment Links deleted: {1}\n Business Event Trade Links deleted: {2}\n Business Event Instrument Links deleted: {3}\n Business Events deleted: {4}".format(DelCorporateAction, DelBusinessEventPaymentLink, DelBusinessEventTradeLink, DelBusinessEventInstrumentLink, DelBusinessEvent))
    
    end = datetime.now()
    diff = end - start
    Logme()("End time: {0}, Script execution time: {1}".format(end, diff))
