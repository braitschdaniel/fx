'''
-----------------------------------------------------------------------------------------
DESCRIPTION
Library of Methods To Manipulate Price Data

CREATED BY
Tobias Hagstrom
2014-06-18

CHANGE HISTORY
USER                               DATE                    COMMENT
-----------------------------------------------------------------------------------------
Tobias Hagstrom                    2014-06-18              Created
Tobias Hagstrom                    2014-07-16              Minor Changes in The Delete Last Price methods
-----------------------------------------------------------------------------------------
'''

import acm
import PIC_Utils_DateTime as dt


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#
#   FIND PRICES
#

# Finds Historical Price for a particular Date
def findHstMarketPrice(_ins,_date,_market):   
    hp = _ins.HistoricalPrices()     
    _price = None 
    for p in hp: 
        #print p.Market().Name(), _market, p.Day(), _date  
        if p.Market().Name() == _market and dt.ConvertSwisstoFADate(p.Day()) == dt.ConvertSwisstoFADate(_date):
            _price = p            
    return _price
    
# Finds All Historical Prices for a market
# Returns a list of historical prices
def histPricesForMarket(_ins,_market):   
    hp = _ins.HistoricalPrices()         
    _marketHistPrices = []
    for p in hp:         
        if p.Market().Name() == _market:            
            _marketHistPrices.append(p)
    return _marketHistPrices
    
# Looks for prices in the LAST attached to instrument
def findLastPrice(_ins,_date,_market):   
    prices = _ins.Prices()
    _lastPrice = None
    for price in prices:
        if price.Market().Name()==_market:            
            _lastPrice = price                    
    return _lastPrice

# Looks for prices in the LAST attached to instrument
def findLastPriceDateForMarket(_ins,_market):   
    prices = _ins.Prices()
    _lastPriceDate = None
    for price in prices:
        if price.Market().Name()==_market:            
            _lastPriceDate = price.Day()                    
    return _lastPriceDate

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#
#   UPDATE & SET PRICES
#

# type FInstrument, str, str, float, str 
# updates a price either last or historical 
def updateMarketPrice(_ins,_date,_market,_price,_curr):   
    _marketPrice = findHstMarketPrice(_ins,_date,_market)
    if not _marketPrice:
        _marketPrice = findLastPrice(_ins,_date,_market)       
    if _marketPrice:
        updatePrice(_marketPrice,_ins,_date,_market,_price,_curr)
    return _marketPrice
    
# writes the actual price update of a FPrice object 
def updatePrice(_fprice,_ins,_date,_market,_price,_curr):    
    _fprice.Last(_price)
    _fprice.Settle(_price)        
    _fprice.Currency(_curr)
    _fprice.Day(_date)
    _fprice.Commit()
    return _fprice
    
def updateOrSetMarketPrice(_ins,_date,_market,_price,_curr):   
    _marketPrice = findHstMarketPrice(_ins,_date,_market)
    if not _marketPrice:
        _marketPrice = findLastPrice(_ins,_date,_market)       
    if _marketPrice:
        _price = updateMarketPrice(_ins,_date,_market,_price,_curr)
    else:
        _price = setNewMarketPrice(_ins,_date,_market,_price,_curr)    
    return _price

# type FInstrument, str, str, float, str    
def setNewMarketPrice(_ins,_date,_market,_price,_curr):   
    existPrice = findHstMarketPrice(_ins,_date,_market)
    newPrice = ""
    if not existPrice:        
        newPrice = acm.FPrice()
        newPrice.Last(_price)
        newPrice.Settle(_price)        
        newPrice.Day(_date)
        newPrice.Market(_market)
        newPrice.Instrument(_ins)
        newPrice.Currency(_curr)        
        newPrice.Commit()
        print "new price set for",_ins.Name(),_market,"to",_price
    return newPrice
    
# writes an historical price to the Latest table
# only todays date is printed in the last table so
# the price is first written with TODAY and then modified
def writeLastPriceForHistDate(_ins,_date,_market,_price,_curr):   
    today = acm.Time.DateToday()
    _marketPrice = findLastPrice(_ins,today,_market)    
    if _marketPrice:
        _fprice = updateMarketPrice(_ins,_date,_market,_price,_curr)        
    else:
        _fprice = setNewMarketPrice(_ins,today,_market,_price,_curr)    
    # updatePrice date of last price to actual acc historical date    
    updatePrice(_fprice,_ins,_date,_market,_price,_curr)    
    delHistPrice(_ins,_date,_market)
    return _price


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#
#   DELETE PRICES
#  
 
def delHistPrice(_ins,_date,_market):       
    # look for price in histories
    _marketPrice = findHstMarketPrice(_ins,_date,_market)     
    removed = None
    # look among last prices
    if _marketPrice:
        _marketPrice.Delete()
        print "price for",_ins.Name(),"at market",_market,", was deleted from HIST"
        removed = 1            
    if not removed:
        i = 1
        #print "price to del for",_ins.Name(),"at market",_market,",",_date,",does not exist"    
    return removed
    
def delLastPrice(_ins,_date,_market):  
    prices = _ins.Prices()
    removed = None
    for _price in prices:
        if _price.Market().Name()==_market and _date==_price.Day():            
            _price.Delete()
            print "last price deleted for",_ins.Name(),_market,_date
            removed = 1
    return removed
    
def delLastPriceFromMarket(_ins,_market):  
    prices = _ins.Prices()
    removed = None
    for _price in prices:
        if _price.Market().Name()==_market:                      
            _date=_price.Day()
            _price.Delete()
            print "last price deleted for",_ins.Name(),_market,_date
            removed = 1
    return removed

# Deletes either Hist or Last if exist
def delMarketPrice(_ins,_date,_market):       
    # look for price in histories and last
    _marketPrice = delHistPrice(_ins,_date,_market)
    print _marketPrice
    if not _marketPrice:
        _marketPrice = findLastPrice(_ins,_date,_market)
    removed = None
    # look among last prices
    removed = delLastPrice(_ins,_date,_market)    
    if _marketPrice:
        _marketPrice.Delete()
        print "price for",_ins.Name(),"at market",_market,", was deleted from HIST"
        removed = 1            
    if not removed:
        print "price to del for",_ins.Name(),"at market",_market,",",_date,",does not exist"    
    return removed
 
# removes last prices for the date specified for
# ALL Markets
def removeLastPriceForDate(_ins,_date):       
    prices = _ins.Prices()    
    for price in prices:
        if price.Day()==_date:       
            price.Delete()
            return 1
    return None
    
# removes last prices for the date specified for
# ALL Markets
def removeLastPriceForMarket(_ins,_market):       
    prices = _ins.Prices()    
    for price in prices:
        if price.Market().Name()==_market:       
            price.Delete()            
            return 1
    return None


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
#
# Examples
#

'''
# Format for imputs

ins     = acm.FInstrument['PICTET-EUROPE EQ SEL-I EUR']
date    = "2014-06-24"
price   = float(111.55)
curr    = "CHF"
market  = "IM"
date    = "23.06.2014"
NAVprice= 169.33
curr    = "CHF"
market  = "IM"
'''

# Example Method Calls
#print "check if price exists\n",findMarketPrice(ins,date,"internal") 
#print "updat\n",updateMarketPrice(ins,date,"internal",price,curr) 
#print "set new marketprice\n",setNewMarketPrice(ins,date,market,price,curr) 
#print "set new marketprice or update existing\n",updateOrSetMarketPrice(ins,date,market,price,curr) 
#print "remove the Last Price of a selected market if it exist\n",removeLastPriceForMarket(ins,"Bloomberg")
#print "sets Last price for an historical date:",writeLastPriceForHistDate(ins,date,market,price,curr),"for date,",date,"and inst,",ins.Name()
#print "latest price date:",findLastPriceDateForMarket(ins,market)
