"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_Instrument

DESCRIPTION
        Utilities module to work with instruments

USAGE
        import PIC_Utils_AddInfo
        

CHANGE HISTORY

USER                             DATE              COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Thomas Werth                 2015-10-16            Created
Noureddine Meziane           2016-07-18            Upgrade PLD-2-PLS
Noureddine MEZIANE           2017-01-30    BPIPE Project : add Realtime and Delayed Markets
"""

import traceback
import acm
import PIC_Utils_AddInfo


#BBG Distributor
APH_DISTRIBUTOR ='APH-BPIPE1'

def setInstrumentAlias(instrument, inputAliasType, inputAlias):
    try:
        aliases = acm.FInstrumentAlias.Select("instrument = '%s'" % instrument.Name())
        alias_exists = False
        for alias in aliases:
            if alias.Type().Name() == inputAliasType:
                alias_exists = True
                if alias.Alias() != inputAlias:
                    alias.Alias(inputAlias)
                    alias.Commit()
                    print "updated instrument alias %s = '%s'" % (inputAliasType, inputAlias)
                else:
                    print 'alias already exists, nothing to update'
        if not alias_exists:
            alias = acm.FInstrumentAlias()
            alias.Instrument(instrument)
            aliasType = acm.FInstrAliasType.Select("name='%s'" % inputAliasType)[0]
            if aliasType:
                alias.Type(aliasType)
                alias.Alias(inputAlias)
                alias.Commit()        
            print "created instrument alias %s = '%s'" % (inputAliasType, inputAlias)
    except Exception as ex:
        print ex
        print traceback.print_exc()
        print 'error on setting instrument %s alias for instrument' % instrument.Name()

#MEZN 24.11.2016 BPIPE Project : manage Realtime and Delayed markets
def setPriceDefinition(instrument, bbgTicker, overwrite):
    try:
        setBbgPriceDefinition(instrument, bbgTicker, "BBG-Realtime", overwrite)
        setBbgPriceDefinition(instrument, '/'+bbgTicker, "BBG-Delayed", overwrite)
    except Exception as ex:
        print ex
        print traceback.print_exc()
        print 'error on setting price definition for instrument %s' % instrument.Name()


#MEZN 24.11.2016 BPIPE Project : manage Realtime and Delayed markets
def setBbgPriceDefinition(instrument, bbgTicker, bbgMarket, overwrite):
    try:
        priceDef = acm.FPriceLinkDefinition.Select("priceDistributor = '%s'  and instrument='%s' and currency='%s'" \
                                               % (APH_DISTRIBUTOR, instrument.Name(), instrument.Currency().Name()))
        newPriceDef = bbgTicker
        if len(priceDef) == 1 and overwrite:
            priceDef = priceDef[0]
            if newPriceDef != priceDef.IdpCode():
                oldPriceDef = priceDef.IdpCode()
                priceDef.IdpCode(newPriceDef)
                priceDef.Commit()
				print "*** Changed existing price link definition from [%s] to [%s] for distributor [%s], Market '%s' and instrument '%s'" \
                      % (oldPriceDef, newPriceDef, APH_DISTRIBUTOR,  bbgMarket, instrument.Name())                
            else:
				print "**** Correct price link definition [%s] for distributor [%s], Market '%s' and instrument '%s' already exists, nothing changed" \
                      % (priceDef.IdpCode(), APH_DISTRIBUTOR,  bbgMarket, instrument.Name())
        elif len(priceDef) == 1 and not overwrite:
            priceDef = priceDef[0]
			print "**** Price link definition [%s] for distributor [%s], Market '%s' and instrument '%s' already exists, nothing changed" \
                  % ( priceDef.IdpCode(), APH_DISTRIBUTOR, bbgMarket, instrument.Name())
        else:
            priceDef = acm.FPriceLinkDefinition()
            priceDef.Currency(instrument.Currency())
            priceDef.IdpCode(newPriceDef)
            priceDef.Instrument(instrument)
            priceDef.RecordType('PriceLinkDefinition')
            priceDef.RequestedSubscription(False)
            priceDef.KeepIntradayPrices(False)
            priceDef.FourEyeOn(False)
            priceDef.Market(bbgMarket)
            priceDef.PriceDistributor(APH_DISTRIBUTOR)
            priceDef.Protection(3072)
            priceDef.Commit()
			print "Created price link definition [%s] for distributor [%s], Market '%s'  and instrument '%s'" % (newPriceDef,APH_DISTRIBUTOR, bbgMarket, instrument.Name())
    except Exception as ex:
        print ex
        print traceback.print_exc()
        print 'error on setting price link definition for instrument %s' % instrument.Name()


def setInstrumentName(instrument, bbgTicker, symbol):
    try:
        newInsName = ''
        if symbol not in ('', None):
            newInsName = str(symbol)
            print "start changing instrument name from '%s' to '%s' using Chronos symbol" % (instrument.Name(), newInsName)
        elif bbgTicker not in ('', None):
            tmp = str(bbgTicker).split(' ')
            if len(tmp) > 0:
                newInsName = tmp[0]
                print "start changing instrument name from '%s' to '%s' using Chronos bloombergCode" \
                      % (instrument.Name(), newInsName)
        if newInsName == '':
            print "both symbol and bbgTicker are empty, instrument name '%s' untouched" % instrument.Name()
            return
        if instrument.Name() == newInsName:
            print 'instrument name is already correct, nothing to do'
            return
        instrument.Name(newInsName)
        instrument.Commit()
        print 'instrument name changed'
    except Exception as ex:
        print ex
        print traceback.print_exc()
        print "error while trying to change instrument name '%s'" % instrument.Name()

