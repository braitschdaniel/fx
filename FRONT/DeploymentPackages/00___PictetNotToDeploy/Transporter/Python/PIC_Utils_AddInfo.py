"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_AddInfo

DESCRIPTION
        Utilities module to work with additional infos

USAGE
        import PIC_Utils_AddInfo
        

CREATED BY
Yauhen Mikulich
2014-31-10 

CHANGE HISTORY

USER                             DATE              COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Yauhen Mikulich              2014-31-10        Created
Thomas Werth                 2015-10-16        GetAdditionalInfo
Thomas Werth                 2017-01-17        DelAdditionalInfo
"""
import traceback
import acm


def SetAdditionalInfo(acmObject, additionalInfoName, value, commit=False):    
    value = str(value)
    additionalInfoSpec = acm.FAdditionalInfoSpec[additionalInfoName]
    if not additionalInfoSpec:
        raise ValueError('AdditionalInfo %s does not exist.' % (additionalInfoName))
    if type(value) != type('string'):
        raise ValueError('Argument "value" must be a string.')
    try:
        getattr(acmObject, 'AdditionalInfo')
    except:
        raise AttributeError('ACM object %s has no AdditionalInformation method.' % (acmObject.StringKey()))      
    # If AddInfo exists, modify it else create a new one on the objecy.
    additionalInfo = acm.FAdditionalInfo.Select01("addInf = %i and recaddr = %i" \
        % (additionalInfoSpec.Oid(), acmObject.Oid()),'Duplicate AddInfos found.')       

    if not additionalInfo:
        # Create new AddInfo on the trade.
        additionalInfo = acm.FAdditionalInfo()  
    else:
        print "Addition info %s on instrument %s was already set - updating" % (additionalInfoName, acmObject.Name())
    additionalInfo.Recaddr(acmObject.Oid())
    additionalInfo.AddInf(additionalInfoSpec.Oid())
    additionalInfo.FieldValue(value)    
    if commit:
        try:
            additionalInfo.Commit()           
            print "Sucessfully commited add info for %s with oid %s" % ( acmObject.Name(), str(acmObject.Oid()) )
        except Exception, e:
            print "Failed to commit add info for %s with oid %s : %s " % (acmObject.Name(), str(acmObject.Oid()), str(e))
    else:
        print "No Commit: commiting add info for %s with oid %s" % ( acmObject.Name(), str(acmObject.Oid()) )
    return None


def GetAdditionalInfo(acmObject, additionalInfoName):  
    try:  
        additionalInfoSpec = acm.FAdditionalInfoSpec[additionalInfoName]
        if not additionalInfoSpec:
            raise ValueError('AdditionalInfo %s does not exist.' % (additionalInfoName))
        try:
            getattr(acmObject, 'AdditionalInfo')
        except:
            raise AttributeError('ACM object %s has no AdditionalInformation method.' % (acmObject.StringKey()))      
        # If AddInfo exists, modify it else create a new one on the objecy.
        additionalInfo = acm.FAdditionalInfo.Select01("addInf = %i and recaddr = %i" \
            % (additionalInfoSpec.Oid(), acmObject.Oid()),'Duplicate AddInfos found.')
        if additionalInfo:
            return additionalInfo.FieldValue()
    except Exception:
        print traceback.print_exc()


def DelAdditionalInfo(acmObject, additionalInfoName):
    try:  
        additionalInfoSpec = acm.FAdditionalInfoSpec[additionalInfoName]
        if not additionalInfoSpec:
            raise ValueError('AdditionalInfo %s does not exist.' % (additionalInfoName))
        try:
            getattr(acmObject, 'AdditionalInfo')
        except:
            raise AttributeError('ACM object %s has no AdditionalInformation method.' % (acmObject.StringKey()))      
        # If AddInfo exists, modify it else create a new one on the objecy.
        additionalInfo = acm.FAdditionalInfo.Select01("addInf = %i and recaddr = %i" \
            % (additionalInfoSpec.Oid(), acmObject.Oid()),'Duplicate AddInfos found.')
        if additionalInfo:
            additionalInfo.Delete()
            print 'deleted value of add info field %s' % additionalInfoName
    except Exception:
        print traceback.print_exc()














