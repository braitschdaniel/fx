'''


hook::suggest_id

Module: FInstrumentSuggestId

The instrument provided as in argument is self contained, meaning that one 

can use access methods for the instrument to get legs, underlying or other 

references associated with the instrument. The classic TTT rules have a 

limited set of information available to build the name as opposed the ael 

hook which have the ability to get additional information if needed.

@param Instrument An ael instrument object

@return str The suggested name as a string according to the rules implemented 

in the function. If the function returns an empty string or None then the 

"classic" suggestname is called which is utilized by TTT rules.




Added by MaHa 2005-06-08
050719 FCS/MiHa Added Repo/Reverse rules

'''

def suggest_id(ins, *rest):
    if ins.instype == 'Repo/Reverse':
        try: rr = str(ins.legs()[0].fixed_rate)[:4] + '-'
        except: rr = ''
        try: undid = ins.und_insaddr.insid[:25] + '-'
        except: undid = ''
        name='REPO-' + undid + rr + ins.curr.insid
        return name

    if ins.instype == 'CreditDefaultSwap':              # Added by ChrisR 2006-11-03

        for l in ins.legs():
            name = ins.curr.insid + "/CDS/"
            if l.type=="Credit Default":
                name = name + str(l.credit_ref.insid)[6:(len(l.credit_ref.insid)-10)] + "/"
                name = name + str(l.start_day)[8:10] + str(l.start_day)[3:5] + str(l.start_day)[:2] + "-"
                name = name + str(l.end_day)[8:10] + str(l.end_day)[3:5] + str(l.end_day)[:2]
                return name
                
                
    if ins.instype == 'Warrant':
        # PICTET Customisation for Warrants
        
        call_put = 'P'
        if ins.call_option == 1:
            call_put = 'C'
        
        name = "%s/%s/%s/%s/%s/%s" %( "WAR"                                     # Indicate Warrant
                                    , ins.curr.insid                            # Currency
                                    , call_put                                  # Call/Put Indicator
                                    , ins.und_insaddr.insid[0:10]               # Underlying
                                    , str(ins.exp_day)[8:10] + str(ins.exp_day)[3:5] + str(ins.exp_day)[:2]     # Expiry date
                                    , "%.2f" % ins.strike_price)                # Strike
                                    
        return name
                                    
        
        
                
    return None     # Let classic TTT files handle the other cases!!
