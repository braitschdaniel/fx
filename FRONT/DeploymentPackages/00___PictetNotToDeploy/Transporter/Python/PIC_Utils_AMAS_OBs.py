import acm
import pyodbc

'''
    KEYS to DICTIONARYS and EXPLANAITION
    
      'cMarketInstrumentId'     =>      ISIN CODE
      'cOrderBookId'            =>      External ID
      'cOrderBookName'          =>      AMAS orderbook Name
      'cOrderBookNameShort'     =>      AMAS orderbook Name
      'iInstrumentType'         =>      AMAS instrument type Inteteger Key
      'cTradingCurrency'        =>      Orderbook Currency
      'dtLastUpdateDateTime'    =>      Timestamp of Latest Update
      'cHomeMarketServerId'     =>      Market Name
'''

def connectToAMAS(_user,_database,_sqlServer,*pwd):
    #database  = 'AMSDB_SWX'
    #sqlUser   = 'THAGSTROM'
    #sqlServer = 'MSHQ01GC30\MSIQ01GC30'
    print "USER:",_user,"\nDB:",_database,"\nSERVER:",_sqlServer
    if pwd == (): # use Windows authentication
        _conn = pyodbc.connect(driver = '{SQL Server}', server = _sqlServer, database = _database, uid = _user, Trusted_Connection='yes')
        print "using Windows authentication"
    else: # use sql authentication
        _conn = pyodbc.connect(driver = '{SQL Server}', server = _sqlServer, database = _database, uid = _user, password = pwd[0])
        print "using SQL authentication"
    print 10 * "\n"
    print "connection to:\n\n -",_conn,"\n - server  ",_sqlServer,"\n - database",_database,"\n"
    return _conn
#end def


def _fetchDataFromAMAS(_AMAS_DB,_conn):
    
    SQL2 = '''SELECT 
           [cOrderBookName]
          ,[cOrderBookNameShort]
          ,[iInstrumentType]
          ,[cHomeMarketServerId]
          ,[cOrderBookId]
          ,[cMarketInstrumentId]
          ,[cTradingCurrency]
          ,[dtLastUpdateDateTime]
         FROM ['''+_AMAS_DB+'''].[dbo].[TNT_OB]'''
    
    cur = _conn.cursor()
    sqlExec = cur.execute(SQL2)
    sqlRows = cur.fetchall()
    
    return sqlRows
#end def


def buildOBdict(_AMAS_DB,_conn):

    sqlRows = _fetchDataFromAMAS(_AMAS_DB,_conn)
    
    orderBookDic = {}

    for row in sqlRows:
        tempDic = {}
        tempIsinDic = {}
        oID = row.cOrderBookId.strip()
        isin = row.cMarketInstrumentId.strip()
        tempDic['cMarketInstrumentId'] = isin
        tempDic['cOrderBookId'] = oID
        tempDic['cOrderBookName'] = row.cOrderBookName.strip()
        tempDic['cOrderBookNameShort'] = row.cOrderBookNameShort.strip()
        tempDic['iInstrumentType'] = row.iInstrumentType
        tempDic['cTradingCurrency'] = row.cTradingCurrency.strip()
        tempDic['dtLastUpdateDateTime']= row.dtLastUpdateDateTime
        tempDic['cHomeMarketServerId']= row.cHomeMarketServerId.strip()
        tempDic['foundonADS'] = 0
        orderBookDic[oID]=tempDic
    #cur.close()
    return orderBookDic
#end def


def buildOBIsinCCYDict(_AMAS_DB,_conn, delimiter="|"):
    
    sqlRows = _fetchDataFromAMAS(_AMAS_DB,_conn)
    
    obISINCCYDic = {}
    
    for row in sqlRows:
        tempDic = {}
        tempIsinDic = {}
        oID = row.cOrderBookId.strip()
        isin = row.cMarketInstrumentId.strip()
        tempDic['cMarketInstrumentId'] = isin
        tempDic['cOrderBookId'] = oID
        tempDic['cOrderBookName'] = row.cOrderBookName.strip()
        tempDic['cOrderBookNameShort'] = row.cOrderBookNameShort.strip()
        tempDic['iInstrumentType'] = row.iInstrumentType
        tempDic['cTradingCurrency'] = row.cTradingCurrency.strip()
        tempDic['dtLastUpdateDateTime']= row.dtLastUpdateDateTime
        tempDic['cHomeMarketServerId']= row.cHomeMarketServerId.strip()
        obISINCCYDic[isin+delimiter+row.cTradingCurrency.strip()]=tempDic
        #cur.close()
    return obISINCCYDic
#end def


def buildOBisinDict(_AMAS_DB,_conn):
    
    sqlRows = _fetchDataFromAMAS(_AMAS_DB,_conn)
    
    obISINDic = {}
    
    for row in sqlRows:
        tempDic = {}
        tempIsinDic = {}
        oID = row.cOrderBookId.strip()
        isin = row.cMarketInstrumentId.strip()
        tempDic['cMarketInstrumentId'] = isin
        tempDic['cOrderBookId'] = oID
        tempDic['cOrderBookName'] = row.cOrderBookName.strip()
        tempDic['cOrderBookNameShort'] = row.cOrderBookNameShort.strip()
        tempDic['iInstrumentType'] = row.iInstrumentType
        tempDic['cTradingCurrency'] = row.cTradingCurrency.strip()
        tempDic['dtLastUpdateDateTime']= row.dtLastUpdateDateTime
        tempDic['cHomeMarketServerId']= row.cHomeMarketServerId.strip()
        obISINDic[isin]=tempDic
        #cur.close()
    return obISINDic
#end def


def info():
    print "\n\n\nMSQL connect lib, methods:\n"

    #database  = 'AMSDB_SWX'
    #sqlUser   = 'THAGSTROM'
    #sqlServer = 'MSHQ01GC30\MSIQ01GC30'

    print " - connectToAMAS(user,database,sqlServer) -> SQL connection to AMAS"
    print "   QserverName: 'MSHQ01GC30\MSIQ01GC30'\n"
    print " - buildOBdict(AMAS_DB,conn)               -> OrderBook Dict <ExternalID-key>"
    print " - buildOBisinDict(AMAS_DB,conn)           -> OrderBook Dict <ISIN-key>"
    print "\n"
    #conn = connectToAMAS(sqlUser,database,sqlServer)
    #orderBookDic = buildOBdict(database,conn)
    print '''
    KEYS to DICTIONARYS and EXPLANAITION
    
      'cMarketInstrumentId'     =>      ISIN CODE
      'cOrderBookId'            =>      External ID
      'cOrderBookName'          =>      AMAS orderbook Name
      'cOrderBookNameShort'     =>      AMAS orderbook Name
      'iInstrumentType'         =>      AMAS instrument type Inteteger Key
      'cTradingCurrency'        =>      Orderbook Currency
      'dtLastUpdateDateTime'    =>      Timestamp of Latest Update
      'cHomeMarketServerId'     =>      Market Name
'''

def help():
    info()

#help()

#print "use method: help() or info() to get a detailed desc of keys and methods"
