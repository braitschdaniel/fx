"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_Registry

DESCRIPTION
        This module enables access to the registry of a service, e.g. ATS and AMBA. 
        The registry values will be returned in a dictionary

USAGE
        import PIC_Utils_Registry

        def start():
            print PIC_Utils_Registry.GetServiceRegistry('ATS')
            stop()
            
        def stop():
            print 'will now exit'

CREATED BY
Bangin Mollazadegan, Favner Nordic AB 
2014-04-10 

CHANGE HISTORY

USER                             DATE              COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bangin Mollazadegan              2014-04-10        Created
"""
import os, sys, time, datetime,platform
import _winreg

REGISTRY_PATH_LIST = ['SOFTWARE\\Wow6432Node\\Front\\FRONT ARENA','SOFTWARE\\Front\\Front Arena']
EXCLUDE_LIST = ['Setup','ComputerMRU','ADM','AMS Management','AMSManagement','RuleEditor']


DEBUG = 0

def GetCurrentServer():
    return str(platform.uname()[1]).upper()

def GetServiceName(sPath):
    sServiceName = sPath[sPath.rfind('\\')+1:]
    if DEBUG > 1:
        print 'DEBUG_GetServiceName.sServiceName:',sServiceName
    return sServiceName
    
def getValuesList(regKey) :
    lst = []
    i   = 0
    while True:
        try:
            tup = _winreg.EnumValue(regKey,i)
            lst.append(tup)
            i += 1
        except:
            break
    return lst

def getValuesDic(regKey):
	dic = {}
	lst = getValuesList(regKey)
	for l in lst:
	    dic[l[0]] = l[1]
	return dic    

def GetKeysNValues(handle,dic, regPath, childDic = {}, maxLevel = 30): 
    try:
        reg = _winreg.OpenKey(handle, regPath, 0, _winreg.KEY_READ)
    except:
        pass
    i   = 0
    while True and maxLevel > 0:
        try:
            subkey = _winreg.EnumKey(reg,i)
            childPath = r'%s\%s' %(regPath,subkey)
            regKey = _winreg.OpenKey(reg, subkey, 0, _winreg.KEY_READ)
            dic.append([childPath,getValuesDic(regKey)])
            GetKeysNValues(handle,dic, childPath,dic, maxLevel - 1)
            i += 1
        except:
            break
    return dic

def GetProcessDicTree(server,registryPathList,level = 30):
    HKEY = _winreg.ConnectRegistry(server, _winreg.HKEY_LOCAL_MACHINE)
    reglist = []
    for paths in registryPathList:
        if DEBUG:
            print 'GetProcessDicTree.paths',paths
        reglist.append(GetKeysNValues(HKEY,reglist,paths, childDic = {}, maxLevel = level))
        HKEY.Close()
    return reglist[-1]

def ProcessDictionaryList(processList, name):
    ''' Processeing a multidimentional dictionary'''
    for obj in processList:
        if type(obj[0])== type(' '):
            serviceName = GetServiceName(obj[0])
            if serviceName and serviceName !=-1:
                if serviceName == name and len(obj[1])>1:
                    return obj

def GetRegistry(server = GetCurrentServer()):
    serviceList = [] 
    for path in REGISTRY_PATH_LIST:
        registryNodes = GetProcessDicTree(server,[path],2)
        if registryNodes:
            for obj in registryNodes:
                if type(obj[0])== type(' '):
                    serviceName = GetServiceName(obj[0])
                    if serviceName and serviceName !=-1 and len(obj[1])>1 and serviceName not in EXCLUDE_LIST:
                        serviceList.append([serviceName,obj[1]])
    return serviceList

def GetServiceRegistry(serviceName, server = GetCurrentServer()):
    if serviceName:
        alarmServiceList = []    
        for path in REGISTRY_PATH_LIST:
            registryNodes = ProcessDictionaryList(GetProcessDicTree(server,[path]), serviceName )
            if registryNodes:
                for nodes in registryNodes:
                    alarmServiceList.append(nodes)
        return alarmServiceList
    else:
        raise Exception('No service name provided')
