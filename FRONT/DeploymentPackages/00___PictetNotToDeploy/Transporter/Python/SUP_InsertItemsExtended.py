
import acm

ael_variables = [

    ['Class', 'Class', 'string', acm.FCommonObject.Subclasses(),

            None, 1, None, 'TestMode for Activation Deactivation Script']

]

def ael_main(param):

    class_str =  param['Class']

    query = acm.CreateFASQLQuery(getattr(acm,class_str), 'AND')

    query.AddAttrNodeString('Oid', '', 'EQUAL')

    acm.StartApplication('',query)
