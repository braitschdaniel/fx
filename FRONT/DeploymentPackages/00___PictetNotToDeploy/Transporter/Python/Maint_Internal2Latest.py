'''---------------------------------------------------------------------------------
MODULE
    Maint_Internal2Latest

DESCRIPTION
    Add yesterday's mtm price to Latest price entry table for a market to be chosen

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    2018-02-22 Mahan Tahvildari (d-fine)
    2018-02-27 Daniel Braitsch (d-fine)

ENDDESCRIPTION
---------------------------------------------------------------------------------'''

import acm
import ael
import sys
import time

import FRunScriptGUI
import FLogger
log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=True)


ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    ['test_mode', 'Test Mode', 'bool', [True, False], True, 1],
    ['my_market', 'Market to add mtm', 'string', acm.FMarketPlace.Instances()], 
    ['calendar', 'Caledar', 'FCalendar', 0,
     None, 0, 0, 'Calendar used to get yesterdays mtm', None],
    ['instruments_add_mtm', 'Instruments', 'FInstrument', 1,
     None, 0, 1, 'Instruments to add mtm', None]
]


def ael_main(params):
        
    calendar = params['calendar']
    today = acm.Time.DateToday()
    yesterday = calendar.AdjustBankingDays(acm.Time.DateToday(), -1)

    calc_space = acm.Calculations().CreateStandardCalculationsSpaceCollection()
           
    for instruments in params['instruments_add_mtm']:
        yesterday_mtm_bid    = instruments.Calculation().MarkToMarketPrice(calc_space, yesterday, None)    #Settle
        yesterday_mtm_ask    = instruments.Calculation().MarkToMarketPrice(calc_space, yesterday, None)    #Settle
        yesterday_mtm_last   = instruments.Calculation().MarkToMarketPrice(calc_space, yesterday, None)    #Settle
        yesterday_mtm_settle = instruments.Calculation().MarkToMarketPrice(calc_space, yesterday, None)    #Settle
        
        new_price_my_market = None
        all_latest_prices = [x for x in instruments.Prices() if x.Market().Name() == params['my_market']]
        if all_latest_prices:
            new_price_my_market = all_latest_prices[0]
        else:
            new_price_my_market = acm.FPrice()
            new_price_my_market.Instrument(instruments.Name())
            new_price_my_market.Market(params['my_market'])
        
        new_price_my_market.Day(today)
        new_price_my_market.Bid(yesterday_mtm_bid.Value().Number())
        new_price_my_market.Ask(yesterday_mtm_ask.Value().Number())
        new_price_my_market.Last(yesterday_mtm_last.Value().Number())
        new_price_my_market.Settle(yesterday_mtm_settle.Value().Number())
        new_price_my_market.Currency(yesterday_mtm_settle.Unit())
              
        if not params['test_mode']:
            try:
                new_price_my_market.Commit()
                log.info('Settlement price {mtm_settle} {currency} of {ins_name} committed for {today}, market being {my_market} and calendar being {calendar}.'.format(
                    ins_name=instruments.Name(), today=today, my_market=params['my_market'], 
                    mtm_settle=yesterday_mtm_settle.Value().Number(), currency=yesterday_mtm_settle.Value().Unit(),
                    calendar=params['calendar'].Name()))
            except Exception as E:
                log.error('Settlement price can not be committed for {ins_name}.'.format(
                    ins_name=instruments.Name()))
                log.error(str(E))
        else:
            new_price_my_market.Unsimulate()
            log.info('Test mode: Settlement price {mtm_settle} {currency} of {ins_name} for {today}, market being {my_market} and calendar being {calendar}.'.format(
                ins_name=instruments.Name(), today=today, my_market=params['my_market'], 
                mtm_settle=yesterday_mtm_settle.Value().Number(), currency=yesterday_mtm_settle.Value().Unit(),
                calendar=params['calendar'].Name()))

