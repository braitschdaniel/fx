"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_ServerClientLog

DESCRIPTION
        This module enables usage of the ServerClientLog
USAGE:
        CreateNewServerClientLog('PIC_MonitorLogsAndServices: Crash of xxx','Fatal')
        list = FindTodaysServerClientLog('PIC_MonitorLogsAndServices','Fatal')
        for o in list:
            print o.Message()

CREATED BY
Bangin Mollazadegan, Favner Nordic AB 
2014-05-05

CHANGE HISTORY

USER                             DATE              COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bangin Mollazadegan              2014-05-05        Created
Bangin Mollazadegan              2014-05-13        Added GetLatestMessageToday()
"""

import acm
import PIC_Utils_DateTime

def CreateNewServerClientLog(message, type):
    newLog = acm.FServerClientLog()
    newLog.Message(message)
    newLog.Severity(type)
    newLog.Commit()

def FindTodaysServerClientLog(message, type):
    logs = acm.FServerClientLog.Select("severity='%s' and updateTime>'%s'" %(type,acm.Time.DateNow()))
    resultList = []
    for log in logs:
        theMessage = log.Message()
        if theMessage.find(message) !=-1:
            resultList.append(log)
    return resultList

def GetLatestMessageToday(message, type):
    listToday = FindTodaysServerClientLog(message, type)
    maxdate = 0
    latestMessage = None
    for obj in listToday:
        if obj.UpdateTime() > maxdate:
            latestMessage = obj
    return latestMessage

def DeleteOldServerClientLog(numberOfDays, commitSize = 400, logList = None):
    dateNow = acm.Time.DateNow()
    dateBack = PIC_Utils_DateTime.AddBankingDays(dateNow, -numberOfDays)
    if not logList:
        logList = acm.FServerClientLog.Select("updateTime<'%s'" %(dateBack))
    try:
        acm.BeginTransaction()
        # log[:] forces the list to be sliced to actually delete all object in the list
        for index,log in enumerate(logList[:]):
            if index < (commitSize -1):
                print "Deleting server client log oid %s with index %d" % (logList.Oid(), index)
                log.Delete()
            else:
                break
        acm.CommitTransaction()
        if len(logList) > 0:
            DeleteOldServerClientLog(numberOfDays, commitSize, logList)
    except:
        acm.AbortTransaction()
