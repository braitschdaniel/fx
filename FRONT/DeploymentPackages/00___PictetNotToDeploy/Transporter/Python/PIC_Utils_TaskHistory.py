"""----------------------------------------------------------------------------
MODULE
        PIC_Utils_TaskHistory

DESCRIPTION
        This module enables access to the TaskHistory. 

CREATED BY
Bangin Mollazadegan, Favner Nordic AB 
2014-05-08 

CHANGE HISTORY

USER                             DATE              COMMENT
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Bangin Mollazadegan              2014-05-08        Created
Bangin Mollazadegan              2014-05-19        Added GetLastSuccesfulTaskTimeToday() and GetLastFailedTaskToday()
Bangin Mollazadegan              2014-05-22        Added GetTaskHistoryOlderThan() and SetTaskHistoryLength
Yauhen Mikulich                  2015-09-23        Added DeleteOldTaskHistory() to delete old task history entries 
"""
import acm
import datetime
import PIC_Utils_DateTime

def GetTaskHistory(taskName):
    taskHistoryList = acm.FAelTask[taskName].History()
    return taskHistoryList
    
def GetTaskHistoryPerDay(taskName,date):
    taskHistoryList = GetTaskHistory(taskName)
    historyList = []
    for history in taskHistoryList:
        if history.StartTime():
            startTime = acm.Time.DateFromTime(history.StartTime())
            dateDiff = acm.Time.DateDifference(startTime,date)
            if dateDiff == 0:
                historyList.append(history)
    return historyList

def GetStatusTaskHistoryPerDay(taskName,dateString, status):
    date = acm.Time.FromDate(dateString)
    taskHistoryList = GetTaskHistory(taskName)
    historyList = []
    for history in taskHistoryList:
        if history.StartTime():
            startTime = acm.Time.DateFromTime(history.StartTime())
            dateDiff = acm.Time.DateDifference(startTime,date)
            if dateDiff == 0 and history.Status() == status:
                historyList.append(history)
    return historyList

def GetFailedActiveTasksToday():
    failedTaskDict = {}
    taskList = acm.FAelTask.Select('')
    for task in taskList:
        failedHistoryList = GetStatusTaskHistoryPerDay(task.Name(),acm.Time.DateToday(), 'Failed')
        if len(failedHistoryList) > 0:
            failedTaskDict[task.Name()] = failedHistoryList
    return failedTaskDict

def GetLastSuccesfulTaskTimeToday(taskName):
    succeededTaskList = GetStatusTaskHistoryPerDay(taskName,acm.Time.DateToday(), 'Succeeded')
    latest = 0
    for task in succeededTaskList:
        if latest < hist.StopTime():
            latest = hist.StopTime()
    return latest        

def GetLastFailedTaskToday():
    failedTaskDict = {}
    failedDict = GetFailedActiveTasksToday()
    for task in failedDict.keys():
        latest = 0
        for hist in failedDict[task]:
            if latest < hist.StopTime():
                latest = hist.StopTime()
        failedTaskDict[task] = latest
        
    return failedTaskDict        

def GetTaskHistoryOlderThan(acmTask, timeBack):
    resultList = []
    historyList = acmTask.History()
    for history in historyList:
        if (int(history.UpdateTime()) - timeBack) < 0:
            resultList.append(history)
    return resultList
    
    
def DeleteOldTaskHistory(numberOfDays, commitSize = 400, taskHistList = None):
    """ This function will delete entries in the task history
        table older than "numberOfDays" back in time.
    """
    dateNow = acm.Time.DateNow()    
    dateBack = PIC_Utils_DateTime.AddBankingDays(dateNow, -numberOfDays)    
    if not taskHistList:
        taskHistList = acm.FAelTaskHistory.Select("updateTime<'%s'" %(dateBack))        
    try:
        acm.BeginTransaction()
        # log[:] forces the list to be sliced to actually delete all object in the list
        for index,taskHist in enumerate(taskHistList[:]):
            if index < (commitSize -1):
                taskHistTimeStamp = taskHist.UpdateTime()                
                taskHistDate =  datetime.datetime.fromtimestamp(int(taskHistTimeStamp))                
                print "Deleting task history for task %s, update time %s" % (taskHist.Task().Name(), str(taskHistDate))
                taskHist.Delete()
            else:
                break
        acm.CommitTransaction()
        if len(taskHistList) > 0:
            DeleteOldTaskHistory(numberOfDays, commitSize, taskHistList)
    except:
        acm.AbortTransaction()

def SetTaskHistoryLength(numberOfDays):
    """ This function will activate the build-in
        function of deleting task history older than
        "numberOfDays" back in time.
    """
    try:
        acm.BeginTransaction()
        for task in acm.FAelTask.Select(''):
            if str(task.HistoryLength_count()) != str(numberOfDays):
                tc = task.Clone()
                tc.HistoryLength_count(numberOfDays)
                tc.HistoryLength_unit('Days')
                task.Apply(tc)
                task.Commit()
        acm.CommitTransaction()
        print 'The given history length have been set for all tasks'
    except:
        print "Could not set Task History Length"
        acm.AbortTransaction()
    
