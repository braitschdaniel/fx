select
    pty.ptyid
    , pty.type
from
    Party pty
where 1=1
    and pty.type = 'MtM Market'
    and pty.ptyid ~= 'FMAINTENANCE'
    and pty.ptyid like '%Market%'