# Notes: Extension Modules

### Custom Ribbons for Trading Manager Sheets and Application GUI

- Load the Module *ButtonAndRibbon.txt* into Prime
- It provides the following features:
    - Custom Ribbons that immediately pop up
    - Buttons for application windows (FX Spot, FX NDF, FX Option)
    - Creation of Custom Buttons 
 - Custom Buttons and Ribbons have to be added in the Ribbon Designer:
    - In the Extension Editor, move the module of choice to the bottom of the module view to save the changes 
    - Enable the context category for your tab of choice ![Alt text](./addContextCategory.jpg)


### Environment Module: Can be called with the following function:

```python
import acm
import ast
import collections

def get_parameter(name):
	dict = collections.defaultdict(lambda: None)
	param = acm.GetDefaultContext().GetExtension("FParameters", "FExtension", name)
	if param and param.Value():
		param = param.Value()
		dict = {}
		for k in param.Keys():
			k = str(k)
			try:
				dict[k] = ast.literal_eval(str(param.At(k)))
			except:
				dict[k] = str(param.At(k))
	return dict
	
print get_parameter("EnvironmentInfo")
```

### ButtonAndRibbon: TODO

![RibbonUtility](./RibbonUtility.jpg)
