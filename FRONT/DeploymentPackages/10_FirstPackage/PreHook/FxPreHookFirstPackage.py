"""______________________________________________
MODULE 
    FxPreHookFirstPackage

PROJECT
    fx

DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

INTERACT
    import code, acm
    code.interact(local=dict(globals(),**locals()))
    acm.Connect("frontarena:9000", "arenasys", "intas", "", "test_app", 0, 0, 0, None, 0, [])

HISTORY
    29.01.2020 Daniel Braitsch
______________________________________________"""

import FLogger

from ProtectionUpload import ael_main as ael_main_protection_upload
from CurveUpload import ael_main as ael_main_curve_upload

import fx_conf

if __name__ == '__main__':
    params = {
        'input_dir': fx_conf.input_dir,
        'is_first_package': True,
        'logger': FLogger.FLogger(level=2, logOnce=False, logToConsole=True, logToPrime=False),
        'test_mode': False,
    }

    log = params['logger']

    log.info('')
    log.info('Start upload scripts for the PREHOOK of the {scope}.'.format(
        scope='first package' if params['is_first_package'] else 'second package')
    )
    log.info('Test Mode: {test_mode}.'.format(test_mode=params['test_mode']))

    ael_main_protection_upload(params)
    ael_main_curve_upload(params)

    log.info('All upload scripts for the PREHOOK of the {scope} successfully completed.'.format(
        scope='first package' if params['is_first_package'] else 'second package')
    )
