select
    pty.ptyid
from
    Party pty
where 1=1
    and pty.type = 'Intern Dept'
    and pty.ptyid ~= 'FMAINTENANCE' /* unequal */
    and pty.ptyid like '%DEFAULT%' /* wildcards */