select
	pm.override_level
	, (pm.override_level = 'workspace')?
		to_string(usr.userid, ': ', pm.workspace_name)
		: to_string(usr.userid, grp.grpid) 'name'
	, pmi.parameter_type
	, to_string(ctx.name, vp.name, rfsh.rfspecid) 'override'
from
	ParameterMapping pm
	, ParMappingInstance pmi
	, User usr
	, arena.Group grp
	, Context ctx
	, ValuationParameters vp
	, RiskFactorSpecHeader rfsh
where 1=1
	and pmi.mapping_seqnbr = pm.seqnbr
	and pm.usrnbr *= usr.usrnbr
	and pm.grpnbr *= grp.grpnbr
	and pmi.context_seqnbr *= ctx.seqnbr
	and pmi.val_seqnbr *= vp.seqnbr
	and pmi.rfspechdrnbr *= rfsh.rfspechdrnbr
order by 1 desc, 2 desc, 3 desc, 4 desc