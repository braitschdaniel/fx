/* TODO update_method=0 */
/* TODO ouuter joins, *= and =* */
/* TODO Unions and temporary tables */
select
    p.ptyid
    , avg(t.quantity)
    , max(t.quantity)
    , count(t)
from
    Trade t
    , Party p
where 1=1
    and t.counterparty_ptynbr = p.ptynbr
group by
    p.ptyid
order by
    4, 3 desc