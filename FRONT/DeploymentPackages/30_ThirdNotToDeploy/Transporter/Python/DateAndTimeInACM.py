import time
import datetime

import acm

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass

today = acm.Time.DateToday()
last_banking_day = acm.FCalendar["New York"].AdjustBankingDays(today, -1)
print today
print last_banking_day

ins = acm.FStock["StockDefault"]
print "Update time", ins.UpdateTime()

print datetime.datetime.fromtimestamp(ins.UpdateTime()).strftime("%Y-%m-%d")

# get current time and convert format
now = datetime.datetime.now()
y_m_d = "%Y-%m-%d"
date_today = now.strftime(y_m_d)
print date_today


# reformat date given as integer to readable form
def submit_time(int_time):
    return datetime.datetime.fromtimestamp(int_time).strftime("%Y-%m-%dT%H:%M:%S.000")

int_time = time.time()
print submit_time(int_time)

trades = acm.FTrade.Select("")
if trades:
    print trades[0].TradeTime()
    print acm.Time.LocalToUtc(trades[0].TradeTime())
