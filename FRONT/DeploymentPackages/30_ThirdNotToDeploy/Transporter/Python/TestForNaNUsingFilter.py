"""______________________________________________
MODULE
    TestForNaNUsingFilter

PROJECT
    fx

DESCRIPTION
    Look for empty/invalid price entries
    Can be either nan, None, empty or infinity

ADDITIONAL INFORMATION ON PARAMETERS
    -

INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    01.03.2020 Daniel Braitsch
______________________________________________"""

def is_nan(x):
    if x != x:
        return True
    elif not str(x).strip():
        return True
    elif x is None:
        return True
    else:
        return False


# choose prices without day
prices = acm.FPrice.Select('day=""')

# choose prices without market
prices = acm.FPrice.Select("")
prices = filter(lambda x: not x.Market(), prices)

# choose prices without instrument
prices = acm.FPrice.Select("")
prices = filter(lambda x: not x.Instrument(), prices)

# choose prices with no bid/ask/last/settle
prices = acm.FPrice.Select("")
prices = filter(lambda x: is_nan(x.Ask()) and is_nan(x.Bid()) and is_nan(x.Settle()) and is_nan(x.Last()), prices)
