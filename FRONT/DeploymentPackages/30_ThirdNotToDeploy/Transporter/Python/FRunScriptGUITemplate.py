#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""---------------------------------------------------------------------------------
MODULE
    FRunScriptGUI Template

DESCRIPTION
    Template for FRunScriptsGUIs

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
HISTORY
    2020-03-01 Daniel Braitsch
        Created

INTERACT
    code.interact(local=dict(globals(),**locals()))

ENDDESCRIPTION
---------------------------------------------------------------------------------"""

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass

import acm
import FRunScriptGUI

def switch_import_remove(index, field_values):
    """Callback for the task interface, to disable/enable the flag fields.

    If flag_for_index1 (index 1) or flag_for_index2 (index 2) was set,
    change the other accordingly so only one is ever checked.
    """
    if index == 1:
        field_values[2] = "false" if field_values[1] == "true" else "true"
    if index == 2:
        field_values[1] = "false" if field_values[2] == "true" else "true"
    return field_values


ael_gui_parameters = {
    "windowCaption": "MY GUI Title",
    "runButtonLabel": "&&MY BUTTON LABLE",
    "hideExtraControls": False,  # to verify where flag is used,
}

instypes = [str(instype) for instype in acm.FEnumeration["enum(InsType)"].Values()]

my_query = acm.CreateFASQLQuery(acm.FStock, "AND")
my_query.AddAttrNodeString("Name", "StockDefault", "EQUAL")
my_query.AddAttrNodeNumerical("CreateTime", "-100d", "0d")
# print my_query.Select()

default_variables = {
    "default_path": r"C:\Users\D60080\Desktop",
    "report_date": acm.Time.DateToday(),
    "report_file": "report_file_" + acm.Time.DateToday() + ".csv",
    "portfolio_list_default": acm.FPhysicalPortfolio[""],
}

developer = ["dbraitsch@dfine.com", "fbraitsch@dfine.com"]

# instances sorts alphabetically
market = acm.FMarketPlace["Reuters"]
begin_message = "My Message Begin"

directorySelection = FRunScriptGUI.DirectorySelection()
directorySelection.SelectedDirectory(default_variables["default_path"])

ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    # test mode
    ["test_mode", "Test Mode", "bool", [True, False], True, 1, 0, "Description"],
    # switch flag
    ["switch1", "Switch1", "bool", [True, False], False, 1, 0, "", switch_import_remove],
    ["switch2", "Switch2", "bool", [True, False], True, 1, 0, "", switch_import_remove],
    ["dateSel", "Report Date (Today/YYYY-mm-dd)", "string", None, default_variables["report_date"], 0, 0],
    # directory selection
    ["my_dir", "Directory", directorySelection, None, directorySelection, 0, 1, "Description"],
    # file selection
    ["my_file", "File", acm.FFileSelection, None, default_variables["report_file"], 0, 1, "Description"],
    # different choices to select an qcm element
    ["dropdown_cal", "Dropdown cal", acm.FCalendar, acm.FCalendar.Select(""), "New York", 1, 0,
     "Dorpdown Menu Calendar"],
    ["select_mult", "Select multiple cal ", "string", [i for i in acm.FCalendar.Select("")], "New York,Tokyo", 0, 1,
     "Select multiple Calendar"],
    ["calendar", "Calendar through Insert Items", "FCalendar", 0, None, 0, 0, "Insert Items Calendar", None],
    # choose from a list of strings
    ["developer", "Developer", "string", developer, None, 0, 1, ""],
    # pick a sublist of all instypes
    ["instype", "InsTypes", "string", instypes, "EquityIndex,Future/Forward", 0, 1, "Select multiple instypes"],
    # Prefilled Insert Items Query
    ["orderbooks", "Preselected Stocks", acm.FStock, None, my_query, 0, 1, "Prefilled Insert Items Query"],
    # select ASQL and Insert Item queries
    ["asql_query_list", "ASQL Query", acm.FSQL, None, None, 0, 1, "Select ASQL Query(s)"],
    ["query_folder_list", "Query Folder", acm.FStoredASQLQuery, None, None, 0, 1, "Select Query Folder(s)"],
    [market1, "Market1", "FMarketPlace", acm.FMarketPlace, market, 1],
    [market2, "Market2", "FMarketPlace", acm.FMarketPlace.Instances(), market, 1, 1],
    [market3, "Market3", "FMarketPlace", acm.FMarketPlace.Instances(), market, 1, 0],
    [begin_message, "Message Begin", "string", None, begin_message, 1]
]


# instances sorts alphabetically
market = acm.FMarketPlace["EUREX_ETS"]
begin_message = "My Message Begin"



def ael_main(params):
    print 1
    return
