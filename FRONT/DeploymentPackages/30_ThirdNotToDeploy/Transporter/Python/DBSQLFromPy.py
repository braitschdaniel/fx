#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SQL Query started from py
import pprint
import ael

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass


my_query = """
select
    ins.insid "HEADER1"
    , ins.instype "HEADER2"
from
    instrument ins
where 1=1
    and ins.insid like '%DE%'
    and creat_usrnbr between 1 and 1000
"""

count_my_query = """
select
    count(*)
from
    instrument ins
where 1=1
    and ins.insid like '%DE%'
    and creat_usrnbr between 1 and 1000
"""

result = ael.dbsql(my_query)
count = ael.dbsql(count_my_query)[0][0][0]
print result
print count

# The standard module pprint is useful for displaying nested data structures such as the
# result sets returned by asql( ) in a readable form
result = ael.asql("""
select
    insid§
    , insaddr
    , instype
from
    instrument
where
    insid like 'BMW%'
""")
pprint.pprint(result)
