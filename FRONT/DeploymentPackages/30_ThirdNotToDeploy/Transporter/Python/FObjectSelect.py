#!/usr/bin/env python
# -*- coding: utf-8 -*-
import code

import acm

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass


opt_key = ""
search_string = "counterparty = 'Counterparty Default' and optionalKey = {opt_key}"
trds = acm.FTrade.Select(search_string.format(opt_key=opt_key))
print trds

qty = 1000
trds = acm.FTrade.Select("trader in ('ARENASYS', 'JOB') and quantity > {qty}".format(qty=qty))
print trds

# return as array/list
print acm.FTrade.Select("archiveStatus = False").AsArray()
print acm.FTrade.Select("archiveStatus = False").AsList()

# wildcards in acm.FSelect()
q_stock = acm.FInstrument.Select("name like 'Stock*'")
print q_stock 


# prices without instrument
# select all prices and apply 'filter'
prices = acm.FPrice.Select("")
prices = filter(lambda x: not x.Instrument(), prices)
print prices

code.interact(local=dict(globals(), **locals()))
