try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass


import FLogger

log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=False)

multi_line = ("""
Summary:

total num {num1:4} \t {num1}
total num {num2:4} \t {num2}
""".format(num1=12, num2=1)
)

log.info(multi_line)

default_variables = {
    "default_path": r"C:\Users\D60080\Desktop",
    "report_file": "my_file_name.csv",
    "test_mode": True,
}

# Log with linebreak
log.warn(""" Hallo
    ddd
    dddd
    d """
)

default_path = default_variables["default_path"]
report_file = default_variables["report_file"]
test_mode = default_variables["test_mode"]

log.debug("DEBUG not logged for log level 1")
log.info("AEL MAIN, with default_path {default_path}.".format(default_path=default_path))
log.warn("AEL MAIN with report_file, {report_file}".format(report_file=report_file))
log.error("AEL MAIN with test mode set to {test_mode}".format(test_mode=test_mode))
log.critical("AEL MAIN")
