# TODO The following files include examples how to include the calculation space to get trading manager data
"""
Virtual trading manager for python: get access to calculated values from the trading manager
it creates a calculated space
"""
"""
_AWG_options_02
SUP_SetBBTickerForInsInPos
Maint_SetBBTicker_Main
Maint_Internal2Latest
EOY script for calc space
Div_Import_Main
"""

import acm
context = acm.FContext("Global")
calc_space = acm.Calculations().CreateCalculationSpace(context, "FPortfolioSheet")

context = acm.GetDefaultContext()
calc_space = acm.Calculations().CreateCalculationSpace(context, "FPortfolioSheet")


def iterate(node):
    child = node.FirstChild()
	if not child:
		yield[]
	else:
		while child:
				for branch in iterate(child.Tree().Iterator()):
					yield [child.Tree().Item()] + branch
				child = child.NextSibling()

for portfolio in portfolios:
	calc_space = acm.Calculations().CreateCalculationSpace(acm.GetStandardContext(), "FPortfolioSheet")
	top_node = calc_space.InsertItems(portfolio)
	grp = acm.Risk.GetGrouperFromName('Default')
	top_node.ApplyGrouper(grp)
	calc_space.Refresh()

	for portfolio_row in iterate(top_node.Iterator()):
		try:
			instrument = portfolio_row[0].Instruments()[0]
		except Exception as E:
			log.warn("empty portfolio")
			continue


# Extract values for column from Trading Manager
def get_values_portfolio(date,pf,column):
    grouper_name = 'Currency Split'
    context = acm.GetDefaultContext()
    sheet_type= 'FPortfolioSheet'
    column_id = column
    portfolio = acm.FPhysicalPortfolio[pf]
    calc_space = acm.Calculations().CreateCalculationSpace(context, sheet_type)
    #print type(calc_space)
    cal = acm.FCalendar['CHF - SZ']
    #lastbankingday = cal.AdjustBankingDays(acm.DateToday(), -1)
    lastbankingday = cal.AdjustBankingDays(acm.Time.DateToday(), -1)
    #print lastbankingday
    calc_space.SimulateGlobalValue( 'Portfolio Profit Loss Start Date' , 'First of Year' )
    calc_space.SimulateGlobalValue( 'Portfolio Profit Loss End Date' , 3)
    #calc_space.SimulateGlobalValue( 'Portfolio Profit Loss End Date Custom' , acm.FDomain.ParseValue(lastbankingday))
    calc_space.SimulateGlobalValue( 'Portfolio Profit Loss End Date Custom' , date ) #lastbankingday
    # Yesterday ist Problematisch - Last Banking Day brauchen wir eigentlich
    top_node = calc_space.InsertItem( portfolio )
    groupers = [ acm.Risk().GetGrouperFromName( grouper_name ) ]
    chained_grouper = acm.FChainedGrouper( groupers )
    top_node.ApplyGrouper( chained_grouper )
    calc_space.Refresh()
    child = top_node.Iterator().FirstChild()
    output = {}
    while child:
        output[str((calc_space.CalculateValue(child.Tree(), column_id)).Unit())]=str((calc_space.CalculateValue(child.Tree(), column_id)).Number())
        child = child.NextSibling()
    return output
