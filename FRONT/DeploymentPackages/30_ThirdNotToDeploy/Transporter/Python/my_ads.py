import acm
import sys


def connect(user="ARENASYS", ads="Training", archive=0):
    if acm.IsConnected():
        return
    if ads == "Training":
        ads_address = "frontarena:9000"
    elif ads == "Pictet":
        ads_address = "frontarena:7000"
    else:
        print "ADS not known."
        sys.exit(1)

    if user == "ARENASYS":
        password = "intas"
    elif user == "JOB":
        password = "welcome"
    else:
        print "User not known."
        sys.exit(1)

    acm.Connect(ads_address, user, password, "")

    # acm.Connect(ads_address, user, password, "", "test_app", archive, 0, 0, None, 0, [])
