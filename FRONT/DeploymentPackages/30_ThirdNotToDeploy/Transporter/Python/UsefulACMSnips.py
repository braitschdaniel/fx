import code

import acm

try:
    import my_ads

    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass

# start applications
acm.StartApplication("FX Cash", None)
acm.StartApplication("FX Option", None)
acm.StartApplication("NDF", None)

# acm class model 1
my_trd = acm.FTrade[20034]
print "Trade is FxSpot", my_trd.IsFxSpot()
print "Trade is FxForward", my_trd.IsFxForward()
print "trade_process of trade fulfils the bit cond 1 << 12", my_trd.TradeProcess() & 1 << 12
print "trade_process of trade fulfils the bit cond 1 << 13", my_trd.TradeProcess() & 1 << 13
my_trd.Inspect()
# acm class model 2
my_ins = acm.FInstrument["BMWG.DE"]
print "inherits from class FInstrument", my_ins.IsKindOf(acm.FInstrument)  # True
print "class is FStock", my_ins.Class() == acm.FStock  # True
my_ins.Inspect()  # super useful, opens Property GUI

# sort and filter
my_ins = acm.FInstrument['BMWG.DE']
latest_prices = sorted(my_ins.Prices(), key=lambda x: x.Day())
print "update time of sorted attribute", [x.UpdateTime() for x in latest_prices]
# filter for prices without market
filtered_latest_prices = filter(lambda x: x.Market().Name() in ["BBG-Realtime"], my_ins.Prices())
print "number of filtered latest_prices", len(filtered_latest_prices)

# dir returns the list of names in the current local scope
for _ in dir(my_ins):
    print _

# use eval
my_list = ["Instrument", "Stock"]
for obj in my_list:
    acm_obj = eval("acm.F" + obj + "['BMWG.DE'].Name()")
    print acm_obj
# alternative to eval
obj_type = "Instrument"
bmw_select = "name like 'BMW*'"
obj_bmw = getattr(acm, "F" + obj_type).Select(bmw_select)

# check if price already filled
import acm

list_prices = acm.FPrice.Select("instrument='BMWG.DE'")
if list_prices:
    p = list_prices[0]
    if str(p.Last()) not in ["nan", "0", "NaN", "0.0"]:
        print "Last price already set:", p.Last()

# simulate/unsimulate
usd = acm.FCurrency["USD"]
usd.SpotBankingDaysOffset(3)
usd.Simulate()
usd.Unsimulate()
