#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""______________________________________________
MODULE 
    ArenaPythonScriptTemplate.py

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    01.03.20 Daniel Braitsch
______________________________________________"""

import code
import acm


try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass

import FLogger
log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=True)

default_variables = {
    "default_path": r"C:\Users\D60080\Desktop",
    "report_file": "my_file_name.csv",
    "test_mode": True,
}

import FRunScriptGUI
directorySelection = FRunScriptGUI.DirectorySelection()
directorySelection.SelectedDirectory(default_variables["default_path"])

ael_variables = [
    # [VariableName,
    #       DisplayName,
    #       Type, CandidateValues, Default,
    #       Mandatory, Multiple, Description, InputHook, Enabled]
    ["test_mode", "Test Mode", "bool", [True, False], default_variables["test_mode"], 1, 0, "Description"],
    # directory selection
    ["my_dir", "Directory", directorySelection, None, directorySelection, 0, 1, "Description"],
    # file selection
    ["my_file", "File", acm.FFileSelection, None, default_variables["report_file"], 0, 1, "Description"],
]


def ael_main(params):
    """to extend"""
    default_path = params["default_path"]
    report_file = params["report_file"]
    test_mode = params["test_mode"]

    log.debug("DEBUG not logged for log level 1")
    log.info("AEL MAIN, with default_path {default_path}.".format(default_path=default_path))
    log.warn("AEL MAIN with report_file, {report_file}".format(report_file=report_file))
    log.error("AEL MAIN with test mode set to {test_mode}".format(test_mode=test_mode))
    log.critical("AEL MAIN")

    code.interact(local=dict(globals(), **locals()))


if __name__ == "__main__":
    params = {
        "default_path": default_variables["default_path"],
        "report_file": default_variables["report_file"],
        "test_mode": default_variables["test_mode"],
    }

    ael_main(params)

