import code

import acm

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass


# select all ins with instype FXSWAP with name *DKK*
query = acm.CreateFASQLQuery(acm.FInstrument, "AND")
query.AddAttrNodeString("InsType", "FxSwap", "EQUAL")
query.AddAttrNodeString("Name", "*DKK*", "RE_LIKE_NOCASE")
query.AddAttrNodeString("Generic", False, "EQUAL").Not(True)
my_q = query.Select()
print query
print "Number of results: ", len(my_q), "\t results:", my_q
print query.AsqlNodes()

# select all trades with specific features
my_trd = acm.FTrade[20034]
my_cp = my_trd.Counterparty().Name()
my_curr = my_trd.Currency().Name()
query = acm.CreateFASQLQuery(acm.FTrade, "AND")
query.AddAttrNodeNumerical("CreateTime", "-1d", "0d")
query.AddAttrNodeNumerical("Quantity", 0, 1000000)
query.AddAttrNodeUTCTime("ValueDay", my_trd.ValueDay(), "15.05.2020")
query.AddAttrNodeString("Status", ["Simulated", "FO Confirmed"], "EQUAL")
query.AddAttrNodeString("Acquirer.Name", "*ARENAYSY*", "RE_LIKE_NOCASE")
query.AddAttrNodeString("Counterparty.Name", my_cp, "EQUAL")
query.AddAttrNodeString("Currency.Name", my_curr, "EQUAL")
# query.AddAttrNodeString("AdditionalInfo.OrderID", "*123*", "RE_LIKE_NOCASE").Not(True)
query.AddAttrNodeString("Quantity", 0, "GREATER")
query.AddAttrNodeString("Price", 100, "LESS_EQUAL")
my_q = query.Select()
print query
print "Number of results: ", len(my_q), "\t results:", my_q
print query.AsqlNodes()

code.interact(local=dict(globals(), **locals()))
