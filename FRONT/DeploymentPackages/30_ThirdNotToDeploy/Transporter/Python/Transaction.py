"""______________________________________________
MODULE 
    ArenaPythonScriptTemplate.py

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    01.03.20 Daniel Braitsch
______________________________________________"""

import acm

try:
    import my_ads
    my_ads.connect("ARENASYS", "Training")
except Exception as E:
    print E
    pass


import FLogger

log = FLogger.FLogger(level=1, logOnce=False, logToConsole=True, logToPrime=False)

test_mode = False

trd_list = [
    acm.FTrade[20034],
    # acm.FTrade[20035],
]
if not test_mode:
    try:
        acm.BeginTransaction()
        for trd in trd_list:
            trd.Commit()
        acm.CommitTransaction()
        log.info("Transaction successfully committed.")
    except Exception as E:
        acm.AbortTransaction()
        log.error("Exception: {E}, transaction did not succeed.".format(E=E))
else:
    for trd in trd_list:
        trd.Unsimulate()
        log.warn("Unsimulate since we are in test mode.")
