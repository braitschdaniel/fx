import os.path as path

# Storage path for input files relative to \Lib folder
_INPUT_FILE_DIR = '..\InputFiles'
_lib_path = path.dirname(__file__)

# Exposed variable to the directory for common input files
# exposed by fx_conf
input_dir = path.join(_lib_path, _INPUT_FILE_DIR)
