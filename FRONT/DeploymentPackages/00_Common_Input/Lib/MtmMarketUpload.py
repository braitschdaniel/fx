"""______________________________________________
MODULE 
    MtmMarketUpload

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    29.01.2020 Daniel Braitsch
______________________________________________"""

import os
import csv

from collections import defaultdict

import acm

default_variables = {
    'file_mtm_markets': 'MTMMarkets.csv'
}


def get_data_party(input_dir, input_file):
    """Returns relevant acm fields for mtm-markets"""
    file_name = os.path.join(input_dir, input_file)
    nested_dict = lambda: defaultdict(nested_dict)
    dict = nested_dict()

    with open(file_name, 'rb') as file_name:
        dict_reader = csv.DictReader(file_name, delimiter=';')
        for row in dict_reader:
            if row['Name']:
                pty = row['Name']
                dict[pty]['Name'] = pty
                dict[pty]['City'] = row['City']
                dict[pty]['ExternalCutOff'] = row['ExternalCutOff']
                dict[pty]['TimeZone'] = row['TimeZone']
                dict[pty]['is_first_package'] = row['ScopeIsFirstPackage']
    return dict


def update_party(dict_party, log, test_mode):
    """Update/Create new MtmMarkets."""
    if not dict_party:
        return
    for party in dict_party:
        new_party = acm.FParty[party]
        if new_party:
            log.info('Updating MtmMarket {mtm_market}.'.format(mtm_market=party))
        else:
            new_party = acm.FMTMMarket()
            log.info('Creating new MtmMarket {mtm_market}.'.format(mtm_market=party))

        del dict_party[party]['is_first_package']
        for attribute in dict_party[party]:
            getattr(new_party, attribute)(dict_party[party][attribute])

        if not test_mode:
            try:
                new_party.Commit()
            except Exception as E:
                log.error('Exception {E}.'.format(E=E))

    return


def ael_main(params):
    input_dir = str(params['input_dir'])
    is_first_package = params['is_first_package']
    log = params['logger']
    test_mode = params['test_mode']

    log.info('')
    log.info('Starting MtmMarketUpload.')
    log.info('')
    log.info('Test mode: {test_mode}'.format(test_mode=test_mode))
    log.info('Scope: First Scope.') if is_first_package else log.info('Scope: Second Scope.')
    log.info('Input directory: {input_dir}.'.format(input_dir=input_dir))
    log.info('Input file: {input_file}.'.format(input_file=default_variables['file_mtm_markets']))

    dict_party = get_data_party(input_dir, default_variables['file_mtm_markets'])

    if is_first_package:
        dict_party = dict((i, dict_party[i]) for i in dict_party if dict_party[i]['is_first_package'] == 'TRUE')
    else:
        dict_party = dict((i, dict_party[i]) for i in dict_party if dict_party[i]['is_first_package'] == 'FALSE')

    log.info('Number of MtmMarkets in scope: {num}'.format(num=len(dict_party)))
    log.info('')
    update_party(dict_party, log, test_mode)

    log.info('')
    log.info('Python Script MtmMarketUpload successfully completed.')
    log.info('')
    log.info('')
    log.info('#' * 80)
    log.info('#' * 80)
    log.info('')
