"""______________________________________________
MODULE 
    CurrencyUpload

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    06.02.2020 Daniel Braitsch
______________________________________________"""

import os
import csv

from collections import defaultdict

import acm

default_variables = {
    'file_ccy': 'Currencies.csv',
}

ROUNDING_MAPPING = {
    '0': 'curr_0',
    '2': 'curr_2',
    '3': 'curr_3',
}


def get_data_currency(input_dir, input_file):
    """Returns relevant acm fields for currencies"""
    file_name = os.path.join(input_dir, input_file)
    nested_dict = lambda: defaultdict(nested_dict)
    dict = nested_dict()

    with open(file_name, 'rb') as file_name:
        dict_reader = csv.DictReader(file_name, delimiter=';')
        for row in dict_reader:
            if row['Name']:
                ccy = row['Name']
                dict[ccy]['DayCountMethod'] = row['DayCount']
                dict[ccy]['RoundingSpecification'] = row['Rounding']
                dict[ccy]['SpotBankingDaysOffset'] = row['SpotDays']
                dict[ccy]['is_first_package'] = row['ScopeIsFirstPackage']
    return dict


def update_currency(dict_currency, log, test_mode):
    """Update/Create new Currencies."""
    if not dict_currency:
        return

    for ccy in dict_currency:
        if not acm.FCurrency[ccy]:
            log.info('Create new currency {ccy}.'.format(ccy=ccy))
            curr = acm.FCurrency['USD'].StorageNew()
            curr.Name(ccy)
        else:
            log.info('Update of currency {ccy}.'.format(ccy=ccy))
            curr = acm.FCurrency[ccy]

        # update day count method and spot days
        try:
            curr.MainLeg().DayCountMethod(dict_currency[ccy]['DayCountMethod'])
        except Exception as E:
            log.warn('Day count method {dcm} for currency {ccy} (the one given in BBG) is not available in FA.').format(
                dcm=dict_currency[ccy]['DayCountMethod'], ccy=ccy, )
            log.warn('Day count method {dcm} used instead for currency {ccy}.').format(dcm="30/360", ccy=ccy, )
            log.warn('')
            curr.MainLeg().DayCountMethod("30/360")

        rounding = ROUNDING_MAPPING[dict_currency[ccy]['RoundingSpecification']]
        curr.RoundingSpecification(rounding)
        curr.SpotBankingDaysOffset(dict_currency[ccy]['SpotBankingDaysOffset'])

        if not test_mode:
            try:
                acm.BeginTransaction()
                curr.Commit()
                curr.MainLeg().Commit()
                acm.CommitTransaction()
            except Exception as E:
                log.error('Exception {E}, transaction did not succeed.'.format(E=E))
                acm.AbortTransaction()


def ael_main(params):
    input_dir = str(params['input_dir'])
    is_first_package = params['is_first_package']
    log = params['logger']
    test_mode = params['test_mode']

    log.info('')
    log.info('Starting CurrencyUpload.')
    log.info('')
    log.info('Test mode: {test_mode}.'.format(test_mode=test_mode))
    log.info('Scope: First Package.') if is_first_package else log.info('Scope: Second Package.')
    log.info('Input directory: {input_dir}.'.format(input_dir=input_dir))
    log.info('Input file: {input_file}.'.format(input_file=default_variables['file_ccy']))

    dict_currency = get_data_currency(input_dir, default_variables['file_ccy'])

    if is_first_package:
        dict_currency = dict(
            (i, dict_currency[i]) for i in dict_currency if dict_currency[i]['is_first_package'] == 'TRUE')
    else:
        dict_currency = dict(
            (i, dict_currency[i]) for i in dict_currency if dict_currency[i]['is_first_package'] == 'FALSE')

    log.info('Number of currencies in scope: {num}.'.format(num=len(dict_currency)))
    log.info('')
    update_currency(dict_currency, log, test_mode)

    log.info('')
    log.info('Python Script CurrencyUpload successfully completed.')
    log.info('')
    log.info('')
    log.info('#' * 80)
    log.info('#' * 80)
    log.info('')
