"""______________________________________________
MODULE 
    CurveUpload

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    06.03.2020 Daniel Braitsch
______________________________________________"""

import os
import csv

from collections import defaultdict

import acm

default_variables = {
    'file_curves': 'CurveUpload.csv'
}


def get_data_yield_curve(input_dir, input_file):
    """Returns relevant data for yield curce creation"""
    file_name = os.path.join(input_dir, input_file)
    nested_dict = lambda: defaultdict(nested_dict)
    dict = nested_dict()

    with open(file_name, 'rb') as file_name:
        dict_reader = csv.DictReader(file_name, delimiter=';')
        for row in dict_reader:
            if row['Name']:
                curve_name = row['Name']

                dict[curve_name]['is_first_package'] = row['ScopeIsFirstPackage']

                dict[curve_name]['yield_curve']['CalculationFormat'] = 'Use Storage Format'
            dict[curve_name]['yield_curve']['Currency'] = row['Currency']
            dict[curve_name]['yield_curve']['EstimationType'] = 'Boot Strap'
            dict[curve_name]['yield_curve']['ExtrapolationTypeLongEnd'] = 'Flat'
            dict[curve_name]['yield_curve']['ExtrapolationTypeShortEnd'] = 'Flat'
            dict[curve_name]['yield_curve']['ForwardPeriod'] = '0d'
            dict[curve_name]['yield_curve']['InterpolationType'] = 'Linear'
            dict[curve_name]['yield_curve']['IpolRateType'] = 'Spot Rate'
            dict[curve_name]['yield_curve']['IrFormat'] = 'Zero Coupon Curve'
            dict[curve_name]['yield_curve']['Owner'] = 'ARENASYS'
            dict[curve_name]['yield_curve']['RecordType'] = 'YieldCurve'
            dict[curve_name]['yield_curve']['RealTimeUpdated'] = 1
            dict[curve_name]['yield_curve']['RiskType'] = 'Interest Rate'
            dict[curve_name]['yield_curve']['StorageCalcType'] = 'Spot Rate'
            dict[curve_name]['yield_curve']['StorageCompoundingType'] = 'Annual Comp'
            dict[curve_name]['yield_curve']['StorageDayCount'] = 'Act/360'
            dict[curve_name]['yield_curve']['Type'] = 'Benchmark'
            dict[curve_name]['yield_curve']['UseBenchmarkDates'] = 1

            tenor_structure = row['Tenor Structure'].split(',')
            dict[curve_name]['tenor_structure'] = map(str.strip, tenor_structure)

    return dict


def create_yield_curves(curve_name, dict_curve_name, update_existing, log, test_mode):
    """Update/Create Yield Curves."""

    if acm.FYieldCurve[curve_name]:
        log.debug('Yield curve {curve_name} already exists.'.format(curve_name=curve_name))
        if update_existing:
            log.debug('Update of yield curve {curve_name}, its benchmark instruments and yield points.'.format(
                curve_name=curve_name))
            new_yield_curve = acm.FYieldCurve[curve_name]
        else:
            return
    else:
        new_yield_curve = acm.FYieldCurve('')
        new_yield_curve.Name(curve_name)
        log.info('Creating new yield curve {curve_name}.'.format(curve_name=curve_name))

    if dict_curve_name:
        for attribute in dict_curve_name['yield_curve']:
            getattr(new_yield_curve, attribute)(dict_curve_name['yield_curve'][attribute])

    if not test_mode:
        try:
            new_yield_curve.Commit()
        except Exception as E:
            log.error(
                'Could not commit yield curve {yield_curve} with Exception {E}.'.format(yield_curve=yield_curve, E=E))


def add_yield_points_to_curve(curve_name, list_tenor, update_existing, log, test_mode):
    """Add benchmark instruments and yield points to the curve."""
    for tenor in list_tenor:

        if not acm.FInstrument[tenor]:
            log.warn('Warning: Benchmark {tenor} cannot be found.'.format(tenor=tenor))
            log.warn(
                'Therefore no benchmark named {tenor} can be added to yield curve {curve_name}.'.format(tenor=tenor,
                                                                                                        curve_name=curve_name))
            continue

        benchmark_to_be_updated = acm.FBenchmark.Select(
            "instrument='{tenor}' and curve='{curve_name}'".format(tenor=tenor, curve_name=curve_name))
        yieldpoint_to_be_updated = acm.FYieldPoint.Select(
            "instrument='{tenor}' and curve='{curve_name}'".format(tenor=tenor, curve_name=curve_name))
        exist_benchmark_and_yieldpoint = bool(benchmark_to_be_updated) & bool(yieldpoint_to_be_updated)

        delete_existing = update_existing and exist_benchmark_and_yieldpoint

        if delete_existing and not test_mode:
            benchmark_to_be_updated[0].Delete()
            yieldpoint_to_be_updated[0].Delete()
        elif exist_benchmark_and_yieldpoint:
            continue

        new_benchmark = acm.FBenchmark()
        new_benchmark.Curve(acm.FYieldCurve[curve_name])
        new_benchmark.Instrument(tenor)

        new_yield_point = acm.FYieldPoint()
        new_yield_point.Curve(acm.FYieldCurve[curve_name])
        new_yield_point.Instrument(tenor)

        log_text = " of benchmark and yield point for instrument {tenor} and curve {curve_name}".format(tenor=tenor,
                                                                                                        curve_name=curve_name)

        if delete_existing:
            log.debug("Update" + log_text + ".")
        else:
            log.info("Creation" + log_text + '.')

        if not test_mode:
            try:
                new_benchmark.Commit()
                new_yield_point.Commit()

            except Exception as E:
                log.error('Creation' + log_text + 'failed.')
                log.error('Exception: {E}.'.format(E=E))
	log.info('')


def ael_main(params):
    input_dir = str(params['input_dir'])
    is_first_package = params['is_first_package']
    log = params['logger']
    test_mode = params['test_mode']

    log.info('')
    log.info('Starting CurveUpload.')
    log.info('')
    log.info('Test mode: {test_mode}'.format(test_mode=test_mode))
    log.info('Scope: First Scope.') if is_first_package else log.info('Scope: Second Scope.')
    log.info('Input directory: {input_dir}.'.format(input_dir=input_dir))
    log.info('Input file: {input_file}.'.format(input_file=default_variables['file_curves']))

    dict_curves = get_data_yield_curve(input_dir, default_variables['file_curves'])

    if is_first_package:
        dict_curves = dict((i, dict_curves[i]) for i in dict_curves if dict_curves[i]['is_first_package'] == 'TRUE')
    else:
        dict_curves = dict((i, dict_curves[i]) for i in dict_curves if dict_curves[i]['is_first_package'] == 'FALSE')

    log.info('Number of CurveUpload in scope: {num}'.format(num=len(dict_curves)))
    log.info('')
    for curve_name in dict_curves:
        create_yield_curves(curve_name, dict_curves[curve_name], True, log, test_mode)
        log.info('')
        add_yield_points_to_curve(curve_name, dict_curves[curve_name]['tenor_structure'], True, log, test_mode)
        log.info('')

    log.info('')
    log.info('Python Script CurveUpload successfully completed.')
    log.info('')
    log.info('')
    log.info('#' * 80)
    log.info('#' * 80)
    log.info('')
