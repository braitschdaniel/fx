"""______________________________________________
MODULE 
    ProtectionUpload

PROJECT
    fx
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

HISTORY
    05.03.2020 Daniel Braitsch
______________________________________________"""

import os
import csv

from collections import defaultdict

import acm

default_variables = {
    'file_protection_upload': 'ProtectionUpload.csv'
}

PROTECTION_MAPPING = {
    # This mapping can be found in the Prime Help: Protection Database field.
    "W:RWD": 0,
    "W:RW": 2 ** 11,
    "W:R": (2 ** 10 + 2 ** 11),
    "W:None": (2 ** 9 + 2 ** 10 + 2 ** 11),
    "O:RWD": 0,
    "O:RW": 2 ** 8,
    "O:R": (2 ** 7 + 2 ** 8),
    "O:None": (2 ** 6 + 2 ** 7 + 2 ** 8),
    "G:RWD": 0,
    "G:RW": 2 ** 5,
    "G:R": (2 ** 4 + 2 ** 5),
    "G:None": (2 ** 3 + 2 ** 4 + 2 ** 5),
    "U:RWD": 0,
    "U:RW": 2 ** 2,
    "U:R": (2 ** 1 + 2 ** 2),
    "U:None": (2 ** 0 + 2 ** 1 + 2 ** 2),
}


def get_data_protections(input_dir, input_file):
    """Returns relevant data for object protection"""
    file_name = os.path.join(input_dir, input_file)
    nested_dict = lambda: defaultdict(nested_dict)
    dict = nested_dict()

    with open(file_name, 'rb') as file_name:
        dict_reader = csv.DictReader(file_name, delimiter=';')
        for row in dict_reader:
            if row['acm Object Type']:
                acm_object_type = row['acm Object Type']
                dict[acm_object_type][row['Name']]['BMW'] = row['BMW']
                dict[acm_object_type][row['Name']]['is_first_package'] = row['ScopeIsFirstPackage']
                dict[acm_object_type][row['Name']]['Owner'] = row['Owner']
                dict[acm_object_type][row['Name']]['Protection'] = row['Protection'].split(",")
    return dict


def update_protections(dict_protections, log, test_mode):
    """Update object protections of acm objects."""
    if not dict_protections:
        return
    for obj_type in dict_protections:
        names_unspec_protection = []
        names_spec_protection = [name for name in dict_protections[obj_type] if name != "N/A"]

        obj_spec_rights = []
        for name in names_spec_protection:
            eval_in_acm = "acm.F" + obj_type + "['" + name + "']"
            obj_spec_protection = eval(eval_in_acm)
            if obj_spec_protection is None:
                log.error("Object {eval_in_acm} not found.".format(eval_in_acm=eval_in_acm))
                log.error("")
                continue
            obj_spec_rights.append(obj_spec_protection)

        if "N/A" in dict_protections[obj_type] and dict_protections[obj_type]["N/A"]["BMW"] == "TRUE":
            bmw_select = "name like 'BMWG*'"
            obj_bmw = getattr(acm, "F" + obj_type).Select(bmw_select)
            names_unspec_protection = [x for x in obj_bmw if x.Name() not in names_spec_protection]
        elif "N/A" in dict_protections[obj_type]:
            all_obj_type = eval("acm.F" + obj_type + ".Select('')")
            names_unspec_protection = [x for x in all_obj_type if x.Name() not in names_spec_protection]

        # change object protection of acm object WITHOUT specified object names
        # special case: objects called 'BMWG*'
        if names_unspec_protection:
            if dict_protections[obj_type]["N/A"]["Owner"]:
                owner = dict_protections[obj_type]["N/A"]["Owner"]
                if dict_protections[obj_type]["N/A"]["BMW"] == "TRUE":
                    log.info(
                        "Set Owner {owner} for objects type {obj_type} having name 'BMWG*'.".format(
                            obj_type=obj_type, owner=owner)
                    )
                else:
                    log.info(
                        "Set Owner {owner} for object type {obj_type} without specified object names.".format(
                            obj_type=obj_type, owner=owner)
                    )
            else:
                owner = None
            if dict_protections[obj_type]["N/A"]["Protection"]:
                list_protection = dict_protections[obj_type]["N/A"]["Protection"]
                int_protection = sum([PROTECTION_MAPPING[i] for i in list_protection])
                if dict_protections[obj_type]["N/A"]["BMW"] == "TRUE":
                    log.info(
                        "Set Protection {prot} for object type {obj_type} having name 'BMWG*'.".format(
                            obj_type=obj_type, prot=str(list_protection).replace("'", ""))
                    )
                else:
                    log.info(
                        "Set Protection {prot} for object type {obj_type} without specified object names.".format(
                            obj_type=obj_type, prot=str(list_protection).replace("'", ""))
                    )
            else:
                int_protection = None
            log.info("")

            for obj in names_unspec_protection:
                obj.Owner() if owner is None else obj.Owner(owner)
                obj.Protection() if int_protection is None else obj.Protection(int_protection)
                if not test_mode:
                    try:
                        obj.Commit()
                    except Exception as E:
                        log.error(
                            "Could not commit {name} of object type {obj_type} Exception {E}.".format(name=obj.Name(),
                                                                                                      obj_type=obj_type,
                                                                                                      E=E))

        # change object protection of acm object WITH specified object names
        for obj in obj_spec_rights:
            if dict_protections[obj_type][obj.Name()]["Owner"]:
                owner = dict_protections[obj_type][obj.Name()]["Owner"]
                obj.Owner(owner)
                log.info(
                    "Set Owner {owner} for {obj_type} {name}.".format(obj_type=obj_type, name=obj.Name(), owner=owner))

            if dict_protections[obj_type][obj.Name()]["Protection"]:
                list_protection = dict_protections[obj_type][obj.Name()]["Protection"]
                int_protection = sum([PROTECTION_MAPPING[i] for i in list_protection])
                obj.Protection(int_protection)
                log.info(
                    "Set Protection {prot} for {obj_type} {name}.".format(
                        obj_type=obj_type, name=obj.Name(), prot=str(list_protection).replace("'", ""))
                )

            log.info("")
            if not test_mode:
                try:
                    obj.Commit()
                except Exception as E:
                    log.error("Could not commit {name} of object type {obj_type} Exception {E}.".format(name=obj.Name(),
                                                                                                        obj_type=obj_type,
                                                                                                        E=E))


def ael_main(params):
    input_dir = str(params['input_dir'])
    is_first_package = params['is_first_package']
    log = params['logger']
    test_mode = params['test_mode']

    log.info('')
    log.info('Starting ProtectionUpload.')
    log.info('')
    log.info('Test mode: {test_mode}'.format(test_mode=test_mode))
    log.info('Scope: First Scope.') if is_first_package else log.info('Scope: Second Scope.')
    log.info('Input directory: {input_dir}.'.format(input_dir=input_dir))
    log.info('Input file: {input_file}.'.format(input_file=default_variables['file_protection_upload']))

    dict_protections = get_data_protections(input_dir, default_variables['file_protection_upload'])

    if is_first_package:
        nested_dict = lambda: defaultdict(nested_dict)
        dict_protections_in_package = nested_dict()
        for obj_type in dict_protections:
            for obj in dict_protections[obj_type]:
                if dict_protections[obj_type][obj]['is_first_package'] == 'TRUE':
                    dict_protections_in_package[obj_type][obj] = dict_protections[obj_type][obj]
        dict_protections = dict_protections_in_package
    else:
        nested_dict = lambda: defaultdict(nested_dict)
        dict_protections_in_package = nested_dict()
        for obj_type in dict_protections:
            for obj in dict_protections[obj_type]:
                if dict_protections[obj_type][obj]['is_first_package'] == 'FALSE':
                    dict_protections_in_package[obj_type][obj] = dict_protections[obj_type][obj]
        dict_protections = dict_protections_in_package

    update_protections(dict_protections, log, test_mode)

    log.info('')
    log.info('Python Script ProtectionUpload successfully completed.')
    log.info('')
    log.info('')
    log.info('#' * 80)
    log.info('#' * 80)
    log.info('')
