"""______________________________________________
MODULE
    createReportDeploymentLogs.py

PROJECT
    local_repo

DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    02.03.2020 Daniel Braitsch
______________________________________________"""

import os
import csv

filename_deployment_summary = "aaSummaryDeploymentLogging.log"

list_lines_to_exclude = [
    "The system cannot find the path specified: 'AMBALoader/*.*'",
    "Column valuation_parameters.fx_shift_curve_recalibration not known in server",
    "All members of choicelist 'Party Choice Lists' should be assigned a unique sort order value",
    "All members of choicelist 'Trade Keys' should be assigned a unique sort order value",
    "FExtensionContext Warning: Module FX Option Pricer Ux can't be found",
    "W inivar     Unknown parameter value",
]


def exclude_line(line, list_lines_to_exclude):
    """Check if line should be excluded from summary."""
    for line_to_exclude in list_lines_to_exclude:
        if line_to_exclude in line:
            return True
    return


def write_title_of_deployment_summary():
    """Create a title for the deployment summary."""
    with open(filename_deployment_summary, "wb") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(["Summary of Deployment Logging"])
        csv_writer.writerow("")


def write_summary_amba_uploads():
    path_deployment = os.path.join(os.getcwd(), "fx\FRONT\DeploymentPackages")
    paths_packages = [x[0] for x in os.walk(path_deployment) if x[0][-10:] == "AMBALoader"]
    with open(filename_deployment_summary, "ab") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow("")
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow("")
        csv_writer.writerow(["Summary AMBA Uploads"])
        csv_writer.writerow("")
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow("")

        for package in paths_packages:

            csv_writer.writerow("")
            package_name = package.split("\\")[-2]
            log_file = os.path.join(os.getcwd(), "Import_" + package_name + ".log")

            try:
                with open(log_file, "rb") as filename:
                    csv_reader = csv.reader(filename, delimiter=";")

                    logging_date = 0
                    for row in csv_reader:
                        try:
                            date_row = row[0][0:6]
                            logging_date = date_row if int(date_row) > int(logging_date) else logging_date
                        except IndexError:
                            continue

            except:
                continue

            index_uploaded_files = 0

            for filename_amba in os.listdir(package):

                old_index = index_uploaded_files

                amba_line1 = "Transporter Loading AMBA messages from file: '" + filename_amba + "'"
                amba_line2 = "Transporter Found"
                amba_line3 = "Transporter Processing AMBA message 1"
                # TODO Extend this part to handle files with more than 1 message
                amba_flags = [False, False]

                with open(log_file, "rb") as filename:
                    csv_reader = csv.reader(filename, delimiter=";")

                    for row in csv_reader:
                        # check third line of AMBA message upload for chosen AMBA message
                        if amba_flags[1] and logging_date in row[0] and amba_line3 in row[0]:
                            index_uploaded_files += 1
                        # check second line of AMBA message upload for chosen AMBA message
                        if amba_flags[0] and logging_date in row[0] and amba_line2 in row[0]:
                            amba_flags[1] = True
                        else:
                            amba_flags[1] = False
                        # check first line of AMBA message upload for chosen AMBA message
                        if logging_date in row[0] and amba_line1 in row[0]:
                            amba_flags[0] = True
                        else:
                            amba_flags[0] = False

                if old_index == index_uploaded_files:
                    csv_writer.writerow(["To investigate: AMBA messsage " + str(filename_amba)])

            if len(os.listdir(package)) == index_uploaded_files:
                log_success_amba_upload = "For package " + package_name + " and date " + logging_date + \
                                          " all AMBA messages were successfully uploaded."
                csv_writer.writerow([log_success_amba_upload])
            else:
                log_amba_upload_failed = "For package " + package_name + " a problem occured in the AMBA message uploads."
                csv_writer.writerow([log_amba_upload_failed])

        csv_writer.writerow("")
        csv_writer.writerow("")


def write_summary_error_messages():
    with open(filename_deployment_summary, "ab") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow("")
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow("")
        csv_writer.writerow(["Summary Logging Files"])
        csv_writer.writerow("")
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow(["*" * 60])
        csv_writer.writerow("")

        for filename in os.listdir(os.getcwd()):

            if filename == filename_deployment_summary or not filename.endswith(".log"):
                continue

            if filename.endswith(".log"):

                csv_writer.writerow([filename])
                csv_writer.writerow("")
                with open(filename, "rb") as filename:
                    csv_reader = csv.reader(filename, delimiter=";")

                    flag_traceback = False

                    # loop over rows of log file
                    for row in csv_reader:
                        try:
                            row[0][14]
                        # exclude nearly empty lines from summary
                        except IndexError:
                            continue

                        # write lines following a traceback error
                        if flag_traceback and row[0][0:2] == "  ":
                            csv_writer.writerow([row[0]])
                        else:
                            flag_traceback = False

                        # exclude lines listed in the exclusion list
                        if exclude_line(row[0], list_lines_to_exclude):
                            continue

                        # write lines indicating Error or Warning
                        elif row[0][0:2] in "20" and row[0][14] in ("E", "W", "U"):
                            csv_writer.writerow([row[0]])

                        # write traceback errors
                        elif "Traceback" in row[0]:
                            csv_writer.writerow([row[0]])
                            flag_traceback = True

            csv_writer.writerow("")
            csv_writer.writerow(["#" * 60])
            csv_writer.writerow("")


write_title_of_deployment_summary()
write_summary_amba_uploads()
write_summary_error_messages()
