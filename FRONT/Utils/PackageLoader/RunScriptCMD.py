"""
    MODULE 
        RunScriptCMD

        (c) Copyright 2009-2013 by SunGard FRONT ARENA. All rights reserved.

    PURPOSE
        Run any RunScript GUI from the command line extracting options from the python module including help texts and default values.
        Optionally the callback hooks in the GUI can be used. When using the option --saveinifile it creates an inifile with a section
        named like the python module.

        example:
            python RunScriptCMD.py --user username --password pass --server localhost:9040 --inifile myini.ini modulename --option1=option
            python RunScriptCMD.py --user username --password pass --server localhost:9040 --xmlcommand myxml.xml

        myini.ini:
            [modulename]
            option1 = option

        myxml.xml:
            <?xml version="1.0" encoding="UTF-8"?>
            <RunScriptCMD>
                <SetEnvironment>
                    <name>LocalFile</name>
                    <value parseEnv="True">${PROCESSOR_IDENTIFIER}/function_type1.txt</value>
                </SetEnvironment>

                <InstallExtension filename="${ENVPATH}/Transporter.txt" add_before="%user" /> <!-- bootstrap Transporter if not installed-->
                <InstallExtension extension="mymodule" add_before="%user" context="mycontext" /> <!-- add extension to context -->

                <Command module="modulename" exitOnError="True" >
                    <option1 parseEnv="True">${option}</option1>
                </Command>

                <Command module="modulename2" exitOnError="False" callback="True" task="TaskName" hook="Module.Function(Parameters, ${paramw})" >
                    <option2>option</option2>
                </Command>
                
                <ChangeDir>c:\tmp</ChangeDir>

                <UninstallExtension extension="oldmodule" context="mycontext" /> <!-- delete and remove extension from context -->      
                <UninstallExtension extension="Transporter" /> <!-- uninstall Transporter -->
            </RunScriptCMD>

    VERSION
        4.0.0
    MAJOR REVISIONS
        2009-01-16  RL  Initial implementation
        2009-01-20  RL  Change the parameter parsing, add support for inifile load and save
        2009-02-04  RL  Added task to the parameters. The task name has to be entered as well as the module name.
        2009-03-12  RL  Added support for callback hook's in the run script gui. Has to be turned on with an option.
        2009-03-17  RL  Added support for date_today and historical date.
        2010-03-04  RL  New feature to read many commands from a XML file added. Also possible to write this kind of file. 
                        !!! Minor changes in parameter names: "()" are now replaced !!!
        2010-06-07  RL  Added callback, task option also when run as XML file
                        Added testmode option
        2010-12-17  RL  Added parameter hook tag
        2011-01-14  RL  Added InstallExtension Transporter bootstrap command
        2011-02-22  RL  Support for using arena_python as interpreter
        2011-02-22  RL  Added pwdfile support
        2011-03-01  RL  Redirect stdout to acm.Log or to file
        2011-03-07  RL  Accept env variables on install extension
        2011-08-01  RL  Change check for arena_python
        2012-12-13  RL  Saint Lucia Edition, Adding UninstallExtension, pretty print XML output
        2013-01-22  RL  Output traceback for called module on exception, Log XML file processed
        2013-06-24  RL  Minor updates for PackageInstaller
        2014-02-14  RL  Calling python command line interface within the process
"""

#from __future__ import with_statement # This isn't required in Python 2.6

import os
import time
import sys
import traceback
import re
import imp
from optparse import Option, OptionParser, OptionGroup, Values, SUPPRESS_HELP, OptionError
from string import Template
import string
import platform
import xml.etree.ElementTree as ET

try: # prepare for python 3.x
    import ConfigParser
except ImportError:
    import configparser as ConfigParser
import subprocess
import shlex
invalidchars = None
try:
    invalidchars = str.maketrans(r''' ():?!"#$%&'*+,./@;<>=[]\^{}~''','''_LRkqeshdpnsspkpsasLReLRbcLRt''')
except:
    invalidchars = string.maketrans(r''' ():?!"#$%&'*+,./@;<>=[]\^{}~''','''_LRkqeshdpnsspkpsasLReLRbcLRt''')   
__version__ = "4.0.0"
stdout = sys.stdout # save stdout
stderr = sys.stderr # save stderr

class FrontOption(Option):
    def __init__(self, *opts, **attrs):
        if getattr(sys, 'frozen', False) and opts[0].startswith('--') and len(opts[0]) > 3:
            opts = tuple(list((opts[0][1:],)) + list(opts[1:],) )
        self._short_opts = []
        self._long_opts = []
        opts = self._check_opt_strings(opts)
        self._set_opt_strings(opts)

        # Set all other attrs (action, type, etc.) from 'attrs' dict
        self._set_attrs(attrs)

        # Check all the attributes we just set.  There are lots of
        # complicated interdependencies, but luckily they can be farmed
        # out to the _check_*() methods listed in CHECK_METHODS -- which
        # could be handy for subclasses!  The one thing these all share
        # is that they raise OptionError if they discover a problem.
        for checker in self.CHECK_METHODS:
            checker(self)


    def _set_opt_strings(self, opts):
        for opt in opts:
            if len(opt) < 2:
                raise OptionError(
                    "invalid option string %r: "
                    "must be at least two characters long" % opt, self)
            elif len(opt) == 2:
                self._short_opts.append(opt)
            else:
                self._long_opts.append(opt)

class FrontOptionParser(OptionParser):
    """
    An unknown option pass-through implementation of OptionParser.

    When unknown arguments are encountered, bundle with largs and try again,
    until rargs is depleted.  

    sys.exit(status) will still be called if a known argument is passed
    incorrectly (e.g. missing arguments or bad argument types, etc.)        
    def __init__(self, skipInvalidOptions=False, **kwargs):
        self.skipInvalidOptions = skipInvalidOptions
        OptionParser.__init__(self,**kwargs)
    """

    def _process_args(self, largs, rargs, values):
        """_process_args(largs : [string],
                         rargs : [string],
                         values : Values)

        Process command-line arguments and populate 'values', consuming
        options and arguments from 'rargs'.  If 'allow_interspersed_args' is
        false, stop at the first non-option argument.  If true, accumulate any
        interspersed non-option arguments in 'largs'.
        """
        while rargs:
            arg = rargs[0]
            # We handle bare "--" explicitly, and bare "-" is handled by the
            # standard arg handler since the short arg case ensures that the
            # len of the opt string is greater than 1.
            if getattr(values,'module', False):
                rargs.insert(0, values.module)
                return
            if arg == "--":
                del rargs[0]
                return
            elif arg[0:2] == "--":
                # process a single long option (possibly with value(s))
                self._process_long_opt(rargs, values)
            elif arg[:1] == "-" and len(arg) > 1:
                # process a cluster of short options (possibly with
                # value(s) for the last one only)
                if len(arg) > 2:
                    self._process_long_opt(rargs, values)
                else:
                    self._process_short_opts(rargs, values)
            elif self.allow_interspersed_args:
                largs.append(arg)
                del rargs[0]
            else:
                return                  # stop now, leave this arg in rargs


def SetupConnect(connected=False):
    """ setup the internal RunScriptCMD parameters """
    global progname
    config = ConfigParser.SafeConfigParser()

    # Get path to module/frozen exe
    if hasattr(sys, "frozen"):
        try:
            (progdir,progname) = os.path.split(str(sys.executable))
        except:
            (progdir,progname) = os.path.split(unicode(sys.executable, sys.getfilesystemencoding( )))
    else:
        try:
            (progdir,progname) = os.path.split(str(__file__))
        except NameError:
            try:
                (progdir,progname) = os.path.split(str(sys.argv[0])) # __file__ missing in ael and ael.exe
            except:
                try:
                    (progdir,progname) = os.path.unicode(str(__file__))
                except:
                    (progdir,progname) = os.path.split(unicode(sys.argv[0], sys.getfilesystemencoding( ))) # __file__ missing in ael and ael.exe
                    
    # Build commandline interface to login to ADS and enter standard options like inifile and so on.
    (progroot, progext) = os.path.splitext(progname)
    if isArenaPython:
        usage = """usage: %prog [-help] -server SERVER:PORT -user USER -password PASSWORD [options] [--help] module [--help] [module options]
RunScriptCMD can start ADS python modules either by entering a taskname or a module name. If module options are needed, the module name is mandatory
When using arena_python to start RunScriptCMD, arena_python is taking care of all connection handling. The available connection parameters can be queried with -help"""
    else:
        usage = """usage: %prog [options] --server=SERVER:PORT --user=USER --password=PASSWORD [--help] module [--help] [module options]
RunScriptCMD can start ADS python modules either by entering a taskname or a module name. If module options are needed, the module name is mandatory"""

    parser = FrontOptionParser(usage = usage,prog = progname, version = "%prog "+__version__)
    parser.disable_interspersed_args() # Important to make the parser quit parameter scan after the module name
    if isArenaPython:
        # When run with arena_python '-h' is an invalid option
        parser.remove_option("--help")
        parser.add_option(FrontOption("--help", dest = "help", action = "help", help = "show this help message and exit"))
    else:
        parser.add_option(FrontOption("--server", dest = "server", action = "store", help = "ARENA Data Server (ADS) address"))
        parser.add_option(FrontOption("--username", dest = "username", action = "store", help = "ARENA user name"))

        parser.add_option(FrontOption("--password", dest = "password", action = "store", help = "ARENA user password"))
        parser.add_option(FrontOption("--pwdfile", dest = "pwdfile", action = "store", help = "File to read password from"))
        parser.add_option(FrontOption("--pwdPrompt", dest = "pwdPrompt", action = "store_true", default = False, help = "Prompt for ARENA user password"))
        parser.add_option(FrontOption("--amaspwd", dest = "amaspwd", action = "store", help = "ARENA Market Access Server (AMAS) password"))

        parser.add_option(FrontOption("--date_today", dest = "date_today", action = "store", help = "Set date for calculations"))
        parser.add_option(FrontOption("--historicaldate", dest = "historicaldate", action = "store", help = "Set historical date for calculations"))

    parser.add_option(FrontOption("--pythonpath", dest = "pythonpath", action = "store", help = "inserts path to the pythonpath"))

    parser.add_option(FrontOption("--inifile", dest = "inifile", action = "store", help = "Parameter values read from a initialisation file (%s.ini)"%progroot))
    parser.add_option(FrontOption("--saveinifile", dest = "saveinifile", action = "store", help = "Module options are saved to an initialisation file"))

    parser.add_option(FrontOption("--task", dest = "taskname", action = "store", help = "Parameter values read from a task in ADS"))
    parser.add_option(FrontOption("--callback", dest = "callback", action = "store_true", default = False, help = "Turn on GUI callbacks"))

    parser.add_option(FrontOption("--xmlcommand", dest = "xmlcommand", action = "store", help = "Run xml command file(s)"))
    parser.add_option(FrontOption("--savexmlfile", dest = "savexmlfile", action = "store", help = "Module options are added to an xml command file"))
    parser.add_option(FrontOption("--testmode", dest = "testmode", action = "store_true", default = False, help = "Turn on testmode, no modules are run. Modules with Run Script GUI are still loaded."))

    parser.add_option(FrontOption("--logtoacm", dest = "logtoacm", action = "store_true", help = "Use acm.Log as output instead of stdout"))
    parser.add_option(FrontOption("--logfile", dest = "logfile", action = "store", help = "Use a file as output instead of stdout"))
    parser.add_option(FrontOption("--debug", dest = "debug", action = "store_true", help = SUPPRESS_HELP))
    parser.add_option(FrontOption("--callhook", dest = "callhook", action = "store", help = SUPPRESS_HELP))
    parser.add_option(FrontOption("--codec", dest = "codec", action = "store", default="ISO-8859-1", help = SUPPRESS_HELP))
    parser.add_option(FrontOption("--module", dest = "module", action = "store", help = "the module to run"))
    #parser.add_option(FrontOption("--logenv", dest = "logenv", action = "store_true", help = "log environment"))
    (options, args) = parser.parse_args()

    # use RunScriptCMD.ini (_64bit/_32bit) in dir of py/exe
    for ininame in ( os.path.join(progdir,progroot+ '_' + platform.architecture()[0] + '.ini'), 
                     os.path.join(progdir,progroot + '.ini') ):
        if os.path.exists(ininame):
            if options.inifile:
                options.inifile = '%s%s%s'%(ininame,os.pathsep,options.inifile)
            else:
                setattr(options,'inifile',ininame)
            break

    # Read inifile(s) and add to the parameters
    if options.inifile:
        config.read(options.inifile.split(os.pathsep)) # Will read many files delimited by ":" or ";" depending on OS
        if config.has_section('ARENA'):
            for option in ('server','username','password','amaspwd','pythonpath','codec'):
                if not getattr(options,option,''):
                    if config.has_option( 'ARENA',option):
                        setattr(options,option,config.get('ARENA',option))
            config.remove_section('ARENA') # We do not want to save an inifile with login information, this has to be added manually.

    if (not options.xmlcommand) and (not options.taskname) and (not options.module) and len(args) == 0:
        parser.error( "Need a module name, task name or option xmlcommand set" )

    if not isArenaPython:
        if getattr(options,'pwdfile','') and not getattr(options,'password',''):
            with open(getattr(options,'pwdfile',''),'r') as fh:
                options.password = fh.readline(40).strip('\n\r')

        if getattr(options,'pwdPrompt','') and not getattr(options,'password',''):
            import getpass
            options.password = getpass.getpass()

        if not (options.server and options.username):
            parser.error( "Can not login without server, username & password")

        if not options.amaspwd:
            options.amaspwd = options.password

    if options.xmlcommand:
        if options.taskname:
            parser.error( "Options xmlcommand and task can not be mixed")
        elif options.savexmlfile:
            parser.error( "Options xmlcommand and savexmlfile can not be mixed")
        elif options.saveinifile:
            parser.error( "Options xmlcommand and saveinifile can not be mixed")

    if options.logtoacm and options.logfile:
        parser.error( "Options logtoacm and logfile can not be mixed")

    # Prepend pythonpath with the parameter
    if options.pythonpath:
        for path in reversed(options.pythonpath.split(os.pathsep)): # add more path's delimited by ":" or ";" depending on OS
            if path not in sys.path:
                sys.path.insert(0,path)
                
    if options.debug:
        print("args", args)
        print ("options", options)
        print ("config", dict([(section, config.options(section)) for section in config.sections()]))

    return args, options, config


def Connect(options):
    """ Connect to ADS and set date according to parameters """
    if options.date_today:
        acm.GetFunction('setDateToday', 1)(options.date_today)

    if options.historicaldate:
        acm.GetFunction('setHistoricalDate', 1)(options.historicaldate)

    #acm.Connect(options.server, options.username, options.password, options.amaspwd)
    acm.Connect(options.server, options.username, options.password, options.amaspwd,'RSC',0,None,None)
    #Connect(server, user, adsPassword, amasPassword, application, archive, session, principal[, configHandler])

    del options.username, options.password, options.amaspwd


def IsAelScript(module):
    pyt = acm.GetDefaultContext().GetExtension('FPythonCode', 'FObject', module)
    if not pyt:
        pyt = acm.FAel[module]
    if not pyt:
        raise ImportError("%s not found"%module)
    has_ael_variables = True if 'ael_variables' in str(pyt.Value()) and 'ael_main' in str(pyt.Value()) else False
    return has_ael_variables


def LocalImport( module ):
    try:
        importedModule = __import__(module)
    except Exception as e:
        (type, value, tb) = sys.exc_info()
        trace = traceback.format_exception(type, value, tb)
        trace.pop(1)
        print( "".join(trace))
        raise
    return importedModule


def SetupModule(args, options, config):
    """ Build commandline interface from the pythonmodule containing ael_variables & ael_main """
    module = None
    if len(args) > 0:
        module = args.pop(0)

    callback = options.callback
    saveinifile = options.saveinifile
    savexmlfile = options.savexmlfile

    # Add named task to the options
    taskprm = {}
    if options.taskname:
        task = acm.FAelTask[options.taskname]
        if task:
            if module in (None,'taskparams'): # allow 'taskparams' as magic word if modulename is not known
                module = task.ModuleName()
            if task.ModuleName() != module:
                raise Exception("Task '%s' does not belong to module %s"%(options.taskname,module))
            prm = task.Parameters()
            taskprm = dict([(str(a),str(b)) for (a,b) in zip(prm.Keys(),prm.Values())])
        else:
            raise Exception("task %s not found"%(options.taskname))
    elif not IsAelScript(module): # If no ael_variables or ael_main, we are done.
        if not options.testmode:
            logger.LOG("Starting %s"%(module))
            LocalImport( module )
        if saveinifile:
            SaveIniFile(module,saveinifile,config,options)
        if savexmlfile:
            SaveXMLFile(module, savexmlfile, guiOptions=Values(), options=options, scriptParameters=())
        return (None,None)

    logger.LOG("Starting %s"%(module))
    importedModule = LocalImport( module )

    if importedModule is None:
        raise ImportError("%s not found"%module)

    aelMain = getattr(importedModule,'ael_main', None)
    aelVariables = getattr(importedModule,'ael_variables', None)
    aelGuiParameters = getattr(importedModule,'ael_gui_parameters', None)

    if not (aelMain and aelVariables != None):
        return (None,None)

    usage = "usage: %%prog [--help] %s [--help] [module options]"%module
    version = None

    if aelGuiParameters:
        if aelGuiParameters.get('scriptDescription',None):
            usage += "\n\nDescription from the %s module:\n%s"%(module, aelGuiParameters.get('scriptDescription', None))
        if aelGuiParameters.get('version',None):
            version = "%s %s"%(module,aelGuiParameters.get('version',None))
    parser = FrontOptionParser(usage = usage, prog = progname, version = version)
    
    if isArenaPython:
        # When run with arena_python, '-h' is an invalid option. The standard help is altered
        parser.remove_option("--help")
        parser.add_option(FrontOption("--help", dest = "help", action = "help", help = "show this help message and exit"))

    grouphelp = "These options are parsed from the %s module. Value in parentheses are default values. (*) indicates mandatory option"%module
    group = OptionGroup(parser, "module options", grouphelp)
    parser.add_option_group(group)

    scriptParameters = acm.ScriptParameters(module, acm.GetDefaultContext().Name())
    groupname = ""
    for tab in scriptParameters.Tabs():
        if str(tab) != groupname: # Each tab in GUI get's it's own group
            groupname = str(tab)
            group = OptionGroup(parser, "%s options"%groupname)
            parser.add_option_group(group)

        for prm in scriptParameters.Parameters(tab):
            id = str(prm.Id()).translate(invalidchars) # We can not use names with blanks or '()'
            if prm.Description():
                description = str(prm.Description())
            else:
                description = str(prm.Name())

            if prm.IsMandatory() and not prm.HasBooleanChoices():
                mandatory = " (*)"
            else:
                mandatory = ""

            if prm.DefaultValue():
                help = "%s (%s)%s"%(description,str(prm.DefaultValue()), mandatory)
            else:
                help = "%s%s"%(description, mandatory)

            if help == " ":    help = ""
            group.add_option(FrontOption("--%s"%id, dest = id, action = "store", default = None, help = help))

    (guiOptions, args) = parser.parse_args(args)

    # read options in command line, option in file, task and default value order
    aelValues = [''] * scriptParameters.Size()
    for i in range(scriptParameters.Size()):
        prm = scriptParameters.At(i)
        try:
            name = str(prm.Id()).translate(invalidchars)

            if getattr(guiOptions, name, None) != None:
                value = getattr(guiOptions, name, '')
            elif config.has_option( module, name):
                value = config.get(module, name)
            elif taskprm.get(str(prm.Id()), None) != None:
                value = taskprm.get(str(prm.Id()), '')
            elif prm.DefaultValue() != None:
                value = str(prm.DefaultValue())
            else:
                value = ''
            setattr(guiOptions, name, value)
            aelValues[i] = value
            if callback and prm.InputHook():
                tmp_values = prm.InputHook()(i, aelValues)
                if tmp_values:
                    aelValues = tmp_values
            #print aelValues,prm.InputHook()
        except Exception as msg:
            parser.error("option %s:%s"%(name, msg))

    # parse options and convert to ael_vars dictionary
    aelParams = {}

    for i in range(scriptParameters.Size()):
        prm = scriptParameters.At(i)

        if str(prm.Type()) == 'date':
            if aelValues[i]:
                aelParams[str(prm.Id())] = ael.date(aelValues[i])
            else:
                aelParams[str(prm.Id())] = None
        elif prm.HasBooleanChoices():
            if aelValues[i] and str(aelValues[i]).upper() in ("1","TRUE","YES"):
                aelParams[str(prm.Id())] = prm.ParseInput(prm.BooleanTrueChoice())
            else:
                aelParams[str(prm.Id())] = prm.ParseInput(prm.BooleanFalseChoice())
        else:
            try:
                aelParams[str(prm.Id())] = prm.ParseInput(aelValues[i])
            except RuntimeError as msg:
                raise Exception('Could not parse field %s: %s'%(prm.Id(),msg))
    if saveinifile:
        SaveIniFile(module, saveinifile, config, guiOptions, scriptParameters)
    if savexmlfile:
        SaveXMLFile(module, savexmlfile, guiOptions, options, scriptParameters, callback = callback)
    if len(args) > 0:
        parser.error("invalid arguments: %s"%(" ".join(args)))

    return (aelMain, aelParams)


def SaveIniFile(module, saveinifile, config, guiOptions, scriptParameters = ()):
    """ Save the ini file using the commandline parameters """
    if not config.has_section(module):
        config.add_section(module)
    for prm in scriptParameters:
        name = str(prm.Id()).translate(invalidchars)
        config.set( module, name, getattr(guiOptions, name, '') )
    fileobject = open(saveinifile, 'w')
    config.write(fileobject)
    fileobject.close()
    acm.Log("ini file %s saved"%saveinifile)


def SaveXMLFile(module, savexmlfile, guiOptions, options, scriptParameters = (),callback = None):
    """ Append to the xml file using the commandline parameters """
    def indent(elem, level=0):
        i = "\n" + level*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    xmlfile = ET.ElementTree()
    try:
        commands = xmlfile.parse(savexmlfile)
    except IOError:
        commands = ET.Element("RunScriptCMD")
        xmlfile._setroot(commands)
    command = ET.Element("Command")
    command.set('module',module)
    command.set('exitOnError','False')
    if callback != None:
        command.set('callback',str(callback))
    commands.append(command)
    for prm in scriptParameters:
        name = str(prm.Id()).translate(invalidchars) 
        if getattr(guiOptions,name,'') != '':
            parameter = ET.Element(name)
            parameter.text = getattr(guiOptions,name,'')
            if '${' in parameter.text:
                parameter.set('parseEnv','True')
            command.append(parameter)
    indent(commands)
    xmlfile.write(savexmlfile,options.codec)
    acm.Log("xml command file %s saved"%savexmlfile)


def BuildCommandLine(module, cmd, options):
    """ Build command line parameters from XML """
    args = [module,] # Module is always the first argument
    if getattr(sys, 'frozen', False):
        preminus = '-'
    else:
        preminus = '--'
    for param in cmd.getchildren(): # Build a commandline from the xml tags
        for attr in list(param.keys()):
            if attr not in ('parseEnv','query','joinLines'):
                raise SyntaxError('Tag %s unknown attribute "%s"'%(cmd.tag, attr))

        if param.text:
            param.text = getString(param.text.encode(options.codec))
            if param.get('parseEnv','FALSE').upper() in ('TRUE','1','YES'):
                class TemplateEnv(Template):
                    idpattern="[_A-Za-z][\._A-Za-z0-9]*"

                param.text = TemplateEnv(param.text).safe_substitute(os.environ)
            if param.get('joinLines','FALSE').upper() in ('TRUE','1','YES'):
                args.append('%s%s="%s"'%(preminus, param.tag, param.text.strip().replace('\n','\\n').replace('\r','') ))
            else:
                args.append('%s%s=%s'%(preminus, param.tag, param.text.strip().replace('\n','').replace('\r','')))
                #",".join([ part for part in param.text.strip().replace('\n','').replace('\r','').split(',') ])))
        elif param.get('query',None) != None:
            query = param.get('query',None)
            value = input(query)
            args.append('%s%s="%s"'%(preminus, param.tag, value) )
        else:
            args.append('%s%s=""'%(preminus, param.tag) )
    return args

def getString(attributeVal):
    try:
        if isinstance(attributeVal, bytes):           
            attributeVal = attributeVal.decode('utf-8')
    except:
        pass
    try:
        if isinstance(attributeVal, unicode):
            attributeVal = attributeVal.encode('ascii', 'replace')
    except:
        pass
    return attributeVal

def ProcessHook(module, cmd, options):
    """ Process parameter hook, filling the parameters """
    hook = cmd.get('hook')   
    config = ConfigParser.RawConfigParser()
    logger.LOG("Calling parameter hook '%s'"%(hook))

    try:        
        m = re.match(r"^(?P<module>\w+)\..*$", hook)        
        mod = m.groups()[0]        
    except Exception:
        raise SyntaxError("RunScriptCMD: parameter hook has to be of format Module.Function(Parameters)")
    
    params = {}
    for param in cmd.getchildren(): # Build the params from the xml tags
        if param.text:
            param.text = getString(param.text.encode(options.codec))
            
            if param.get('parseEnv','FALSE').upper() in ('TRUE','1','YES'):
                param.text = Template(param.text).safe_substitute(os.environ)               
            params[param.tag] = ",".join([ part.strip() for part in param.text.strip().replace('\n','').replace('\r','').split(',') ])

    hookmod = LocalImport(mod)
    hook = Template(hook).safe_substitute(os.environ)
    params = eval(hook, {"__builtins__": __builtins__, mod:hookmod, 'params':params})   
    if type(params) != type({}):
        raise SyntaxError("parameter hook return result has to be a dictionary")
    config.add_section(module)
    for k,v in params.items():
        config.set( module, k, v )
    return config


def ProcessCallHook(hook, aelMain, params):
    """ Let hook take care of calling module """
    logger.LOG("Calling call hook '%s'"%(hook))

    try:
        m = re.match(r"^(?P<module>\w+)\.(?P<function>\w+)\((?P<param>.*)\)$", hook)
        mod,function,paramstring = m.groups()
    except Exception:
        raise SyntaxError("RunScriptCMD: call hook has to be of format Module.Function(ael_main, params, ...)")

    hook = Template(hook).safe_substitute(os.environ)

    hookmod = LocalImport(mod)
    try:
        returncode = eval(hook, {"__builtins__": __builtins__, 'module':mod, mod:hookmod, 'ael_main':aelMain, 'params':params })
    except Exception as e:
        (type, value, tb) = sys.exc_info()
        trace = traceback.format_exception(type, value, tb)
        trace.pop(1)
        print("".join(trace))
        raise

    return returncode


def ProcessAELmain( aelMain, aelParams):
    try:
        returnValue = aelMain(aelParams)
    except Exception as e:
        (type, value, tb) = sys.exc_info()
        trace = traceback.format_exception(type, value, tb)
        trace.pop(1)
        print("".join(trace))
        raise

    return returnValue


def InstallExtension( filename, add_before='%user', add_after='', modulename=None, contextname=None ):
    """ Bootstrap extension module e.g Transporter, if not in system """
    
    if filename:
        filename = Template(filename).safe_substitute(os.environ)

        logger.LOG("InstallExtension %s %s"%(filename, "add_after "+add_after if add_after else "add_before "+add_before))
        
        with open(filename,'rU') as fh:
            input = fh.read()

        newem = acm.ImportExtensionModule(input)
        real_name = newem.Name()
        em = acm.FExtensionModule[real_name]
        if em:
            logger.LOG('Update %s'%real_name)
        else:
            logger.LOG('Adding %s'%real_name)
            em = acm.FExtensionModule()

        em.Apply(newem)
        em.Commit()
    elif modulename:
        logger.LOG("InstallExtension %s %s"%(modulename, "add_after "+add_after if add_after else "add_before "+add_before))
        real_name = modulename
    else:
        raise Exception("InstallExtension: either filename or extension has to be entered")

    if contextname:
        context = acm.FExtensionContext[contextname]
        if not context:
            raise Exception("InstallExtension: Context %s does not exist"%contextname)
    else:
        context = acm.GetDefaultContext()

    sourceModules = [ext for ext in context.ModuleNames() if ext != real_name]

    conclone = context.Clone()
    for module in conclone.ModuleNames(): # All modules removed, and added afterwards
        conclone.RemoveModule(module)

    if add_after:
        if add_after in sourceModules:
            sourceModules.insert(sourceModules.index(add_after)+1,real_name)
        else:
            raise Exception( "InstallExtension: can not add extension to context '%s'(add_after='%s')"%(context.Name(), add_after) )
    elif add_before and add_before in sourceModules:
        sourceModules.insert(sourceModules.index(add_before),real_name)
    else:
        raise Exception( "InstallExtension: can not add extension to context '%s'(add_before='%s'"%(context.Name(), add_before) )

    for module in sourceModules:
        conclone.AddModule(module)
    context.Apply(conclone)
    context.Commit()
    logger.LOG("Extension %s added to Context '%s'"%(real_name, context.Name()))


def UninstallExtension( extension, contextname=None ):
    """ Remove Extension from system incl. default context  """

    extension = Template(extension).safe_substitute(os.environ)
    logger.LOG("UninstallExtension %s"%(extension))
    em = acm.FExtensionModule[extension]
    if em == None:
        logger.WLOG('Missing %s'%extension)
        raise Exception("Extension %s not found"%extension)

    if not contextname:
        contextname = acm.GetDefaultContext().Name()

    for contextname in contextname.split(','):
        contextname = contextname.strip()
        
        context = acm.FExtensionContext[contextname]
        if not context:
            raise Exception("UninstallExtension: Context %s does not exist"%contextname)

        sourceModules = [ext for ext in context.ModuleNames() if ext != extension]

        if context.ModuleNames().Size() > len(sourceModules):
            conclone = context.Clone()
            for module in conclone.ModuleNames(): # All modules removed, and added afterwards
                conclone.RemoveModule(module)

            for module in sourceModules:
                conclone.AddModule(module)
            context.Apply(conclone)
            context.Commit()
            logger.LOG("%s removed from Context '%s'"%(extension, context.Name()))
        else:
            logger.WLOG("%s not found in Context '%s'"%(extension, context.Name()))

    em.Delete()
    logger.LOG('Extension %s deleted'%extension)


def CheckCmdKeys(cmd, attrList):
    for attr in list(cmd.keys()):
        if attr not in attrList:
            raise SyntaxError('Tag %s unknown attribute "%s". Valid are(%s)'%(cmd.tag, attr, ",".join(attrList)))


def SetEnvironment(cmd, options):
    """ Set an environment variable from the Command XML """ 
    envname = cmd.findtext('name','')
    valueObj = cmd.find('value')

    if envname == '' or valueObj == None:
        raise SyntaxError('SetEnvironment needs "name" and "value" tags')
    envvalue = valueObj.text.encode(options.codec)
    if valueObj.get('parseEnv','FALSE').upper() in ('TRUE','1','YES'):
        class TemplateEnv(Template):
            idpattern="[_A-Za-z][\._A-Za-z0-9]*"

        envvalue = TemplateEnv(envvalue).safe_substitute(os.environ)

    os.environ[envname] = envvalue


def ChangeDir(directory, lastDir):
    currentDir = os.getcwd()
    
    if directory:
        os.chdir(directory)
    elif lastDir:
        os.chdir(lastDir)
    
    return currentDir


def RunXMLCommands(xmlfile,options,configin):
    """ run many python modules described in the xml:
    <?xml version="1.0" encoding="UTF-8"?>
    <RunScriptCMD>
        <InstallExtension filename="${ENVPATH}/Transporter.txt" add_before="%user"/> <!-- bootstrap Transporter if not installed-->
        <UninstallExtension extension="Transporter" /> <!-- cleanly uninstall Transporter -->
        
        <Command exitOnError="False" module="ModuleName" task="TaskName" callback="True" callhook="Module.Function(ael_main, params, ...)" hook="Module.Function(params, Parameters)" >
            <basepath parseEnv="True" >${RV_INTERFACES_HOME}/acustom/current/src/sheets</basepath>
            <password query="Password?" />
            <ambamessage joinLines="True">[multiline AMBAMESSAGE]</ambamessage>
        </Command>

        <Python exitOnError="False" module="ModuleName" callback="True" hook="Module.Function(params, Parameters)" >
            <param1 parseEnv="True" >${RV_INTERFACES_HOME}/acustom/current/src/sheets</param1>
            <param2 query="Password?" />
            <param3 joinLines="True">Test</param3>
        </Python>

        <QueryParameters>
        </QueryParameters>

        <SetEnvironment>
            <name>LocalFile</name>
            <value parseEnv="True">${PROCESSOR_IDENTIFIER}/function_type1.txt</value>
        </SetEnvironment>
    </RunScriptCMD>

    In the callhook, handling of reruns on certain conditions can be covered.
    """

    returnCode = 0
    try:
        cmds = ET.parse(xmlfile).getroot()
    except SyntaxError as msg:
        raise SyntaxError("Error parsing file %s (%s)"%(xmlfile,msg))
    
    if cmds.tag != 'RunScriptCMD':
        raise SyntaxError("Error parsing file %s, wrong file format? (RunScriptCMD tag missing)"%(xmlfile))
    
    for cmd in cmds.getchildren(): # check all commands
        if cmd.tag not in ('Command','PythonCommand','Python','InstallExtension','QueryParameters','UninstallExtension','SetEnvironment','Shell','ChangeDir','cd',):
            raise SyntaxError("Error parsing file %s (unknown tag %s)"%(xmlfile,cmd.tag))

    os.environ['RSCxmlcommand']=xmlfile
    lastDir=None
    #callback = options.callback
    #taskname = options.taskname

    for cmd in cmds.getchildren(): # run all commands

        # Create a local copy of the cmd line options 
        localoptions = Values()
        for (key, value) in vars(options).items():
            setattr(localoptions, key, value)

        # Set the timestamp for the command
        os.environ['RSCtimestamp']=time.strftime('%Y%m%d %H%M%S', time.localtime())
        
        if cmd.tag == 'Command':
            module = cmd.get('module','')

            CheckCmdKeys(cmd, ('module','exitOnError', 'callback', 'task', 'hook', 'callhook', ))

            exitOnError = cmd.get('exitOnError','False').upper() in ('TRUE','1','YES')
            localoptions.callback = options.callback or cmd.get('callback','False').upper() in ('TRUE','1','YES') # callback option controlable per script call
            localoptions.taskname = options.taskname or cmd.get('task',None) # task as template per script call
            
            if localoptions.taskname:
                task = acm.FAelTask[localoptions.taskname]
                if task and not module:
                    module = task.ModuleName()

            if not module:
                raise SyntaxError("missing 'module' tag in %s"%xmlfile)

            if cmd.get('hook',None):
                config = ProcessHook(module, cmd, localoptions)
            else:
                config = configin

            args = BuildCommandLine(module, cmd, localoptions)

            returnValue = 0
            try:
                (aelMain,aelParams) = SetupModule(args, localoptions, config)
                logger.DLOG("Parameters: %s"%(aelParams))

                if not localoptions.testmode:
                    if cmd.get('callhook',None):
                        returnValue = ProcessCallHook(cmd.get('callhook'), aelMain, aelParams)
                    elif aelMain:
                        returnValue = ProcessAELmain(aelMain, aelParams)
            except ImportError:
                raise
            except Exception as msg:
                logger.ELOG("%s %s(%s)"%(module,Exception,msg))
                if exitOnError:
                    raise

            if returnValue:
                logger.ELOG("%s returncode %s"%(module,str(returnValue)))
                returnCode = returnValue or returnCode
                if exitOnError:
                    logger.WLOG("exiting")
                    sys.exit(returnValue) # Force exit
            else:
                logger.DLOG("%s returncode %s"%(module,str(returnValue)))
            # END Command
        elif cmd.tag == 'PythonCommand':
            CheckCmdKeys(cmd, ('module','exitOnError', 'hook', 'callhook', ))

            exitOnError = cmd.get('exitOnError','False').upper() in ('TRUE','1','YES')
            
            modulename = cmd.get('module','')
            
            if not module:
                raise SyntaxError("missing 'module' tag in %s"%xmlfile)
            
            fp, pathname, description = imp.find_module(modulename)
            
            if not fp:
                raise ImportError("'%s' not found"%modulename)

            if cmd.get('hook',None):
                config = ProcessHook(module,cmd, localoptions)
            else:
                config = configin

            args = BuildCommandLine(module, cmd, localoptions)

            import contextlib
            @contextlib.contextmanager
            def redirect_argv(argv):
                sys._argv = sys.argv[:]
                sys.argv=argv
                yield
                sys.argv = sys._argv

            returnValue = 0
            logger.DLOG("Parameters: %s"%(args))

            with redirect_argv(args):
                try:
                    imp.load_module("__main__", fp, pathname, description)
                except ImportError:
                    raise
                except Exception as msg:
                    logger.ELOG("%s %s(%s)"%(module,Exception,msg))
                    if exitOnError:
                        raise
                finally:
                    if fp:
                        fp.close()

            if returnValue:
                logger.ELOG("%s returncode %s"%(module,str(returnValue)))
                returnCode = returnValue or returnCode
                if exitOnError:
                    logger.WLOG("exiting")
                    sys.exit(returnValue) # Force exit
            else:
                logger.DLOG("%s returncode %s"%(module,str(returnValue)))
            # END PythonCommand
        elif cmd.tag == 'Python':
            CheckCmdKeys(cmd, ('module','exitOnError', 'hook', 'callhook', ))

            exitOnError = cmd.get('exitOnError','False').upper() in ('TRUE','1','YES')
            
            module = cmd.get('module','')

            if not module:
                raise SyntaxError("missing 'module' tag in %s"%xmlfile)

            if cmd.get('hook',None):
                config = ProcessHook(module,cmd, localoptions)
            else:
                config = configin

            args = BuildCommandLine(module, cmd, localoptions)

            returnValue = 0
            try:
                (aelMain,aelParams) = SetupModule(args, localoptions, config)
                logger.DLOG("Parameters: %s"%(aelParams))
                if not localoptions.testmode:
                    if cmd.get('callhook',None):
                        returnValue = ProcessCallHook(cmd.get('callhook'), aelMain, aelParams)
                    elif aelMain:
                        returnValue = ProcessAELmain( aelMain, aelParams)
            except ImportError:
                raise
            except Exception as msg:
                logger.ELOG("%s %s(%s)"%(module,Exception,msg))
                if exitOnError:
                    raise

            if returnValue:
                logger.ELOG("%s returncode %s"%(module,str(returnValue)))
                returnCode = returnValue or returnCode
                if exitOnError:
                    logger.WLOG("exiting")
                    sys.exit(returnValue) # Force exit
            else:
                logger.DLOG("%s returncode %s"%(module,str(returnValue)))
            # END Python
        elif cmd.tag == 'Shell':
            """    <Shell exitOnError="True" basepath="c:\tmp" cmd="ls -l ${dir}" hook="TransporterHooks.FileNames(tag='dir', name='*.txt', separator=' ')">
                   <dir>examples</dir>
                </Shell>
            """
            """cmd='svn co ${files}'"""
            # cmdtemplate%{'files': "file.txt"}

            for attr in list(cmd.keys()):
                if attr not in ('cmd','exitOnError', 'hook', 'callhook', 'basepath', ):
                    raise SyntaxError('Tag %s unknown attribute "%s"'%(cmd.tag, attr))

            exitOnError = cmd.get('exitOnError','False').upper() in ('TRUE','1','YES')
            
            cmdtemplate = cmd.get('cmd','')
            basepath = cmd.get('basepath','').strip()
            shell = cmd.get('shell','FALSE').strip().upper() == 'TRUE'
            if basepath and basebath != '.':
                lastDir = ChangeDir(basepath, lastDir)

            if not cmdtemplate:
                raise SyntaxError("missing Shell 'cmd' tag in %s"%xmlfile)

            """
            if cmd.get('hook',None):
                params = ProcessHookDict(cmd)
            else:
                params = BuildParamDict(cmd)
            """
            cmdline = cmdtemplate #Template(cmdtemplate).safe_substitute(params)

            logger.LOG("Starting %s"%(cmdline))
            returnValue = 0
            try:
                cmd_args = shlex.split(cmdline)
                #(ael_main,ael_params) = SetupModule(args, options, config)
                #logger.DLOG("Parameters: %s"%(ael_params))
                if not options.testmode:
                    if cmd.get('callhook',None):
                        pass
                        #returnValue = ProcessCallHook(cmd.get('callhook'), ael_main, ael_params)
                    else:
                        if sys.platform.startswith("win"):
                            CREATE_NO_WINDOW = 0x8000000
                            proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=shell, creationflags=CREATE_NO_WINDOW) # env=sys.env
                        else:
                            proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=shell) # env=sys.env
                        while 1:
                            line = proc.stdout.readline()
                            returnValue = proc.poll()
                            if (not line) and (returnValue is not None):
                                break
                            print(line[:-1])
                        #returnValue = CallCmdLine()
            except Exception as msg:
                logger.ELOG("%s %s(%s)"%(cmdline,Exception,msg))
                if exitOnError:
                    raise

            if returnValue:
                logger.ELOG("%s returncode %s"%(cmdline,str(returnValue)))
                returnCode = returnValue or returnCode
                if exitOnError:
                    logger.WLOG("exiting")
                    sys.exit(returnValue) # Force exit
            else:
                logger.DLOG("%s returncode %s"%(cmdline,str(returnValue)))
            # END Shell
        elif cmd.tag == 'InstallExtension':
            CheckCmdKeys(cmd, ('filename', 'add_before', 'add_after', 'extension', 'context'))

            InstallExtension( cmd.get('filename',None), cmd.get('add_before','%user'), cmd.get('add_after',''), cmd.get('extension',None), cmd.get('context',None) )
            # END InstallExtension
        elif cmd.tag == 'UninstallExtension':
            CheckCmdKeys(cmd, ('extension','context'))

            UninstallExtension( cmd.get('extension', None), cmd.get('context',None) )
            # END UninstallExtension
        elif cmd.tag == 'SetEnvironment':
            SetEnvironment(cmd, localoptions)
            # END SetEnvironment
        elif cmd.tag == 'QueryParameters':
            """
            CheckCmdKeys(cmd, ('extension',))
            """
            pass
            # END QueryParameters
        elif cmd.tag in ('ChangeDir','cd'):
            lastDir = ChangeDir(cmd.text, lastDir)
            # END ChangeDir

        if not acm.IsConnected():
            logger.ELOG("not connected")
            returnCode = 1
            break

    return returnCode


def ael_main(params):
    options = Values()
    options.callback = options.saveinifile = options.savexmlfile = options.taskname = options.testmode = None
    options.codec = "ISO-8859-1"
    config = ConfigParser.SafeConfigParser()
    
    if params['xmlfile']:
        xmlfile = str(params['xmlfile'])
        try:
            RunXMLCommands(xmlfile,options,config)           
        except Exception:
            acm.LogAll(traceback.format_exc().splitlines()[-1])


class LogObject:   
    """ Class which either writes to a file or to the acm log. stdout is redirected to this class """
    def __init__(self, logtoacm=False, fileh=None, name=__name__, debug=False):
        self.logtoacm = logtoacm
        self.fileh = fileh
        self.name = name
        self.debug = debug
        self.pid = os.getpid()       
    def LOG(self, inputString):
        self.writelog(inputString, levelname='INFO')
    def WLOG(self, inputString):
        self.writelog(inputString, levelname='WARNING')
    def ELOG(self, inputString):
        self.writelog(inputString, levelname='ERROR')
    def DLOG(self, inputString):
        if self.debug:
            self.writelog(inputString, levelname='DEBUG')
    def CLOG(self, inputString):
        self.writelog(inputString, levelname='CRITICAL')
    def writelog(self, inputString, levelname):
        dict = {'asctime':time.strftime('%y%m%d %H%M%S', time.localtime()), 'levelname':levelname,'process':self.pid, 'name':self.name, 'message':inputString}
        self.writeln('%(asctime)s %(levelname).1s %(process)04X %(name)s %(message)s'%dict, levelname)
    def writeln(self, inputString, levelname='INFO'):
        self.write("%s\n"%inputString, levelname)
    def write(self, inputString, levelname='INFO'):
        if self.fileh and not self.fileh.closed:
            self.fileh.write(inputString)
        if self.logtoacm:
            acm.LogAll(inputString)

        if not (self.logtoacm or self.fileh):
            if levelname == 'INFO':
                stdout.write(inputString)
            else:
                stderr.write(inputString)
 

def SetupInit():
    global acm, ael, logger, isArenaPython

    isArenaPython = 'built-in' in str(sys.modules.get('acm',None)) or (sys.modules.get('acm',None) and sys.modules.get('acm',None).IsConnected())

    (args, options, config) = SetupConnect()
    import acm, ael

    if not isArenaPython and not acm.IsConnected():
        Connect(options)

    os.environ['RSCuser']=acm.User().Name()
    if options.logfile:
        fileh=open(options.logfile,"a")
    else:
        fileh=None

    logger = LogObject(name='RunScriptCMD', logtoacm=options.logtoacm, fileh=fileh, debug=options.debug)

    if options.logfile or options.logtoacm:
        sys.stdout = logger # stdout redirection
        sys.stderr = logger # stderr redirection

    try:
        returnCode = 0
        if options.xmlcommand:
            xmlcommand = str(options.xmlcommand)
            for xmlfile in xmlcommand.split(','):
                logger.LOG("Processing XML: %s"%xmlfile)
                returnSingle = RunXMLCommands(xmlfile,options,config)
                returnCode = returnCode or returnSingle
        else:
            #while len(args) > 0: ## Would allow many Module --Parameters on the same command line
            (aelMain, aelParams) = SetupModule(args, options, config)
            logger.DLOG("Parameters: %s"%(aelParams))
            if not options.testmode:
                if options.callhook:
                    returnCode = ProcessCallHook(options.callhook, aelMain, aelParams)
                elif aelMain:
                    del args,config
                    returnCode = ProcessAELmain( aelMain, aelParams)
        if returnCode:
            logger.ELOG("There were errors: %s"%str(returnCode))
        sys.exit(returnCode)
    except Exception:
        logger.ELOG(traceback.format_exc().splitlines()[-1])
        if options.debug:
            print(traceback.format_exc())
        sys.exit(1)
    finally:
        if fileh: fileh.close()
        sys.stdout = stdout
        sys.stderr = stderr
        if acm.IsConnected():
            acm.Disconnect()

if __name__ == '__main__':
    SetupInit()   
else:
    isArenaPython = 'built-in' in str(sys.modules.get('acm',None)) or (sys.modules.get('acm',None) and sys.modules.get('acm',None).IsConnected())
    if isArenaPython:
        import acm, ael
        import FRunScriptGUI

        os.environ['RSCuser']=acm.User().Name()

        isArenaPython = False
        progname = __name__
        logger = LogObject(name='RunScriptCMD', logtoacm=False, fileh=None )
        inSelection = FRunScriptGUI.InputFileSelection()
        locvars = [['xmlfile','XML Command file',inSelection,None,inSelection,1,1,'The path and filename',None,True]]
        ael_variables = FRunScriptGUI.AelVariablesHandler(locvars)
