@ECHO OFF

SETLOCAL

SET IP_CURR_DIR=%cd%

SET PACKAGE_DIR=%~1
SET PACKAGE_NAME=%~n1

REM PYTHONPATH for common libraries shared across packages
SET COMMON_LIB_PATH=%PACKAGE_DIR%\..\00_Common_Input\Lib
SET PYTHONPATH=%COMMON_LIB_PATH%;%PYTHONPATH%

ECHO ### ImportPackage.bat
ECHO Importing %PACKAGE_NAME%

CD /D %PACKAGE_DIR%

REM Execution of single use scripts
IF EXIST PreHook (
	CD /D PreHook
	FOR /F "tokens=*" %%f in ('dir /b *.py^| sort') DO (
		ECHO Running python script %%f & %py27% -exit_on_disconnect 1 -server %ADS_SERVER% -user %USER% -password %PWD% %%f
	)
	CD /D %PACKAGE_DIR%
)

REM General import task: Transporter and AMBA Loader as configured in centralized ImportPackage.xml
%py27% "%~dp0\RunScriptCMD.py" -server %ADS_SERVER% -user %USER% -password %PWD% --xmlcommand="%~dp0\ImportPackage.xml" --logfile="%LOG_DIR%\Import_%PACKAGE_NAME%%.log"

REM Run the packages Custom RunScriptCMD.xml if config exists
if exist RunScriptCMD.xml (
    %py27% "%~dp0\RunScriptCMD.py" -server %ADS_SERVER% -user %USER% -password %PWD% --xmlcommand="RunScriptCMD.xml" --logfile="%LOG_DIR%\Custom_%PACKAGE_NAME%%.log"
)
REM Execution of single use scripts
IF EXIST PostHook (
	CD /D PostHook
	FOR /F "tokens=*" %%f in ('dir /b *.py^| sort') DO (
		ECHO Running python script %%f & %py27% -exit_on_disconnect 1 -server %ADS_SERVER% -user %USER% -password %PWD% %%f
	)
	CD /D %PACKAGE_DIR%
)

CD /D %IP_CURR_DIR%