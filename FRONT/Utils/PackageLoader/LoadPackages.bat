@ECHO OFF

SET LOG_DIR=%cd%

SET PACKAGES_FILE=%1
SET PACKAGE_FILENAME=%~nx1
SET PACKAGES_DIR=%~dp1

ECHO ### Loading packages defined in %PACKAGE_FILENAME%
ECHO ### From directory %PACKAGES_DIR%

REM Call import on different packages
FOR /F "tokens=*" %%p in (%PACKAGES_FILE%) DO @(
	IF EXIST "%PACKAGES_DIR%%%p" (
		ECHO INFO: Loading package %%p
		REM ECHO %PACKAGES_DIR%%%p
		"%~dp0\ImportPackage.bat" "%PACKAGES_DIR%%%p"
	) ELSE (
		ECHO ERROR: Cannot find package %%p
	)
)
