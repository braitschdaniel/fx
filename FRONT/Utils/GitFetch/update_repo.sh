# Name of the branch in the REMOTE repository to be updated and checked out
export TARGET_BRANCH=master

# Overwrite the plain ssh command to use a local key for repository access

export GIT_SSH_COMMAND='ssh -i ./../fx_key/id_rsa -o StrictHostKeychecking=no'

# Git update/checkout while overwriting all local changes
git fetch --all
git reset --hard origin/$TARGET_BRANCH
git clean -fd