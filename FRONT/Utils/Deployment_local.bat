@ECHO OFF

SET FALog="C:\Users\D60080\PycharmProjects\local_repo\Deployment.log"

REM Local path to Arena Python executable
SET py27="C:\Program Files\Front\Front Arena\PRIME\2019.2\arena_python.exe"
SET pyForReport="C:\Users\D60080\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 2.7.17\python.exe"

REM Credentials
SET ADS_SERVER=frontarena:9000
SET USER=ARENASYS
SET PWD=intas
SET FCS_SUGGEST_NAME_FILE="C:\Program Files\Front\Front Arena\PRIME\2019.2\suggestname.ttt"

SET PROJECT_DIR=%cd%                                                                                > %FALog%
REM start /wait cmd /c update_repo.bat                                                              >> %FALog%
REM Package Loader for all packages listed in the given .txt file                                   >> %FALog%
REM .\fx\FRONT\Utils\PackageLoader\LoadPackages.bat fx\FRONT\DeploymentPackages\PackagesToUpload.txt >> %FALog% 2>&1 & %pyForReport% .\fx\FRONT\Utils\CreateReportDeploymentLogs.py
