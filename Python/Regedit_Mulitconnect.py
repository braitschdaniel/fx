import win32com.client
import time
import os

# Server names
# Note: Connect to each of them once before (via Explorer or another script)
# and save credentials
HOSTNAMES = ["FRONTARENA"]

CONNECT_SLEEP = 3.0  # time needed to start regedit and connect to servers
WINDOW_SLEEP = 2.0  # time needed to open windows in regedit
#SERVER_SLEEP = 16.0  # time needed to connect to servers


shell = win32com.client.Dispatch("WScript.Shell")


os.startfile("excel.exe")
time.sleep(CONNECT_SLEEP)

shell.SendKeys("%FO")  # send ALT, F and C
time.sleep(WINDOW_SLEEP)
shell.SendKeys("1")  # send ALT, F and C
time.sleep(3.0)
shell.SendKeys("new random text")  # send ALT, F and C
shell.SendKeys("~")  # send ENTER
shell.SendKeys("%FC")  # send ALT, F and C
time.sleep(2.0)
shell.SendKeys("S")  # send ALT, F and C
time.sleep(1.0)
shell.SendKeys("~Y")  # send ENTER

"""
os.startfile("regedit.exe")
time.sleep(CONNECT_SLEEP)

for hostname in HOSTNAMES:
    print 1
    shell.SendKeys("%FC")  # send ALT, F and C
    time.sleep(WINDOW_SLEEP)
    shell.SendKeys(hostname) # send host name
    shell.SendKeys("~") # send ENTER
    time.sleep(SERVER_SLEEP)
"""

