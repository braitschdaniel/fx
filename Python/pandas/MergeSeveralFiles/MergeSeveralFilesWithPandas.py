import os
import pandas as pd
import functools

# Set pandas options to widen screen output
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)

#####################
# Read data from disk
#####################

dir_name = os.getcwd()
file_names = ["csv_merge_name1.csv", "csv_merge_name2.csv", "csv_merge_name3.csv"]

columns_to_be_kept = ["Instrument", "Type",]

dfs = []
for file_name in file_names:
    suffix = file_name.split("/")[-1][10:15]

    file_name = os.path.join(dir_name, file_name)

    df = pd.read_csv(file_name, skiprows=1, error_bad_lines=True, delimiter=";")
    df = df[columns_to_be_kept]
    df.rename(columns={"Price": "Price" + "_" + suffix}, inplace=True)
    df = df.fillna("")
    dfs.append(df)

df_final = functools.reduce(lambda left, right: pd.merge(left, right, on="Instrument"), dfs)
df_final = df_final.drop_duplicates(keep="first")

#################
# Save everything
#################

df_final.to_csv(os.path.join(dir_name, "SeveralFileRecon.csv"), sep=";", index=False)

import code
code.interact(local=dict(globals(), **locals()))
