import os
import pandas as pd

# Set pandas options to widen screen output
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)

#####################
# Read data from disk
#####################

dir_name = os.getcwd()

left_file = os.path.join(dir_name, r"from_trader1.csv")
right_file = os.path.join(dir_name, r"from_trader2.csv")

left = pd.read_csv(left_file, skiprows=1, delimiter=";")
right = pd.read_csv(right_file, skiprows=1, delimiter=";")

###############
# Data Clean-up
###############

# replace NaN by ""
left = left.fillna("")
right = right.fillna("")

#####################
# Merge the two files
#####################

merged = pd.merge(left, right, on=["Instrument"], how="outer", suffixes=["_trader1", "_trader2"], indicator=True)

# interchange last to first column
cols = merged.columns.tolist()
cols = cols[-1:] + cols[:-1]
merged = merged[cols]

#################
# Save everything
#################

merged.to_csv(os.path.join(dir_name, "FileRecon.csv"), sep=";", index=False)

import code
code.interact(local=dict(globals(), **locals()))
