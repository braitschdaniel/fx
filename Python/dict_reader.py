"""______________________________________________
MODULE 
    dict_reader.py

PROJECT
    snipplets
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    import code
    code.interact(local=dict(globals(),**locals()))

HISTORY
    24.03.18 Daniel Braitsch
______________________________________________"""

import os
import csv
import code


def create_dict_list(power=(2, 3)):
    dict_list = []
    for x in range(1, 11):
        my_dict = {"first": x, "power[0]": power[0] ** x, "power[1]": power[1] ** x}
        dict_list.append(my_dict)
    return dict_list


def create_csv(dict_list, file_name="my_csv_file.csv"):
    if dict_list:
        dir_name = os.getcwd()
        file_name = os.path.join(dir_name, file_name)
        with open(file_name, "w") as csv_file:
            csv_file = csv.DictWriter(csv_file, dict_list[0].keys(), delimiter=";")
            csv_file.writeheader()
            for line in dict_list:
                csv_file.writerow(line)


create_csv(create_dict_list(power=(2, 3)), "my_csv_file_1.csv")
create_csv(create_dict_list(power=(4, 5)), "my_csv_file_2.csv")

code.interact(local=dict(globals(), **locals()))
