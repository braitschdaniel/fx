"""______________________________________________
MODULE 
    mult_merge

PROJECT
    snipplets
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    31.03.18 Daniel Braitsch
______________________________________________"""

import os
import csv
import pandas as pd
import code
import logging as log
import functools
import collections


def create_dict_list(basis=(2, 3), iterator=range(1, 11)):
    """Returns a dictionary giving the power of the resp. basis."""

    dict_list = []

    basis_0 = 'Basis_' + str(basis[0])
    basis_1 = 'Basis_' + str(basis[1])

    for exp in iterator:
        my_dict = dict(
            (key, round(basis ** exp, 0)) for key, basis in
            zip(['Exponent', basis_0, basis_1], [exp ** (1 / exp), basis[0], basis[1]]))
        my_dict["Notes"] = "string:" + str(exp)
        dict_list.append(my_dict)

    return dict_list


def create_csv(dict_list, dir_name, file_name):
    """Creates a CSV file."""

    if dict_list:

        file_name = os.path.join(dir_name, file_name)

        with open(file_name, 'w') as csv_file:

            csv_file = csv.DictWriter(csv_file, dict_list[0].keys(), delimiter=';')
            csv_file.writeheader()

            for line in dict_list:
                csv_file.writerow(line)


def merge_multiple_files(dir_name, file_names, merge_on, file_name_merged='file_name_merged', drop_dup=False):
    """Merge multiple files."""

    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 10000)

    dict_files = collections.defaultdict(lambda: {})

    for file in file_names:
        dict_files[file]['path'] = os.path.join(dir_name, file)
        dict_files[file]['content'] = pd.read_csv(dict_files[file]['path'], delimiter=';')
        dict_files[file]['content'] = dict_files[file]['content'].fillna('')

    if len(file_names) != 2:
        for file in file_names:
            if 'Basis_2' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_2' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_2')
            if 'Basis_3' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_3' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_3')
            if 'Basis_4' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_4' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_4')
            if 'Basis_5' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_5' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_5')
            if 'Basis_6' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_6' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_6')
            if 'Basis_7' in dict_files[file]['content']:
                dict_files[file]['content']['Basis_7' + '_' + file[7:13]] = dict_files[file]['content'].pop('Basis_7')
            if 'Notes' in dict_files[file]['content']:
                dict_files[file]['content']['Notes' + '_' + file[7:13]] = dict_files[file]['content'].pop('Notes')

    path_merged_file = os.path.join(dir_name, file_name_merged)

    dfs = [dict_files[file]['content'] for file in dict_files]

    if len(dfs) == 2:
        merged = functools.reduce(
            lambda left, right: pd.merge(left, right, on=[merge_on], how='outer', suffixes=["_left", "_right"],
                                         indicator=True), dfs)

        # interchange first and last column
        cols = merged.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        merged = merged[cols]

    else:
        merged = functools.reduce(
            lambda left, right: pd.merge(left, right, on=[merge_on], how='outer', indicator=False),
            dfs)

    if drop_dup:
        merged = merged.drop_duplicates(keep='first')

    # drop lines which contain the "1" in the column "Notes"
    # modify entries where a key satisfy a certain condition
    if 'Notes_left' in merged:
        merged = merged[merged['Notes_left'].str.contains('1') == False]

        # code.interact(local=dict(globals(), **locals()))

        merged.loc[merged['Exponent'] == 2, 'Basis_3_left'] = 'modified'
        merged.loc[merged['Exponent'] == 2, 'Basis_3_right'] = 'also modified'

    # save everything
    merged.to_csv(os.path.join(dir_name, path_merged_file), index=False)


################################################################################
################################################################################
################################################################################

if __name__ == '__main__':
    params = {
        'dir_name': os.getcwd(),
        'file_name_1': 'my_csv_file_1.csv',
        'file_name_2': 'my_csv_file_2.csv',
        'file_name_3': 'my_csv_file_3.csv',
        'file_name_4': 'my_csv_file_4.csv',
        'file_name_merged_multiple': 'my_merged_file_multiple.csv',
        'file_name_merged_two': 'my_merged_file_two.csv',
        'file_name_merged_drop_dup': 'my_merged_file_drop_dup.csv',
        'merge_on': 'Exponent',
        'basis1': (2, 3), 'iter1': [1, 2, 2, 3, 4, 5, 6],
        'basis2': (2, 3), 'iter2': range(1, 10, 1),
        'basis3': (4, 5), 'iter3': range(2, 20, 1),
        'basis4': (6, 7), 'iter4': range(1, 18, 2)

    }

    print(create_dict_list.__doc__)
    dict_list_1 = create_dict_list(basis=params['basis1'], iterator=params['iter1'])
    dict_list_2 = create_dict_list(basis=params['basis2'], iterator=params['iter2'])
    dict_list_3 = create_dict_list(basis=params['basis3'], iterator=params['iter3'])
    dict_list_4 = create_dict_list(basis=params['basis4'], iterator=params['iter4'])

    print(create_csv.__doc__)
    create_csv(dict_list_1, params['dir_name'], params['file_name_1'])
    create_csv(dict_list_2, params['dir_name'], params['file_name_2'])
    create_csv(dict_list_3, params['dir_name'], params['file_name_3'])
    create_csv(dict_list_4, params['dir_name'], params['file_name_4'])

    list_file_names_multiple = [params['file_name_1'], params['file_name_2'], params['file_name_3'],
                                params['file_name_4']]
    list_file_names_two = [params['file_name_1'], params['file_name_2']]

    print(merge_multiple_files.__doc__)
    merge_multiple_files(params['dir_name'], list_file_names_multiple, params['merge_on'],
                         params['file_name_merged_multiple'])

    merge_multiple_files(params['dir_name'], list_file_names_two, params['merge_on'], params['file_name_merged_two'])

    merge_multiple_files(params['dir_name'], list_file_names_two, params['merge_on'],
                         params['file_name_merged_drop_dup'], drop_dup=True)

