import textwrap

from collections import namedtuple

import acm
import ael

TableRef = namedtuple("TableRef", ["table_name", "id_col", "ref_col"])

def main():
    # Specify object and underlying SQL (not ADM) table
    object = acm.FInstrument["BMWG.DE"]
    object_sql_table = "instrument"
    
    object_oid = object.Oid()

    possible_refs = get_table_references(object_sql_table)
    
    for ref in possible_refs:
        qry_for_refs = textwrap.dedent("""\
            select
                {id_col}
            from {table}
            where {ref_col} = {obj_oid}
            """.format(
                table=ref.table_name,
                id_col=ref.id_col,
                ref_col=ref.ref_col,
                obj_oid=object_oid
            )
        )
        try:
            results = [row[0] for row in ael.dbsql(qry_for_refs)[0]]
        except:
            print ref
            raise
        if results:
            print ("'{table}' objects with the following '{id_col}' refer to {name} "
                   "in the column '{ref_col}'").format(
                        table=ref.table_name,
                        id_col=ref.id_col,
                        name=object.Name(),
                        ref_col=ref.ref_col
                        )
            print results
            
def get_table_references(table_name="instrument"):
    """
    Get all possible *non-parent* references to the given table and return
    these as a list of TableRef objects
    
    Will not return parent references such as from an instrument to its leg
    """
    qry_table_references = textwrap.dedent("""\
        select
          col_ins_ref.table_name 'table'
        , col_id.column_name 'id_col'
        , col_ins_ref.column_name 'ref_col'
        , case col_ins_ref.column_type
          when 'ref({table_name})' then 'reference'
          else 'parent'
          end 'ref_type'
        from ds_tables col_ins_ref
        left outer join ds_tables col_id
          on col_ins_ref.table_name = col_id.table_name
          and col_id.column_type = 'id'
         where 1=1
         and col_ins_ref.column_type = 'ref({table_name})'
         and col_ins_ref.column_name not like '%curr%'
         """.format(
            table_name=table_name
        )
    )
         
    nested_results = ael.dbsql(qry_table_references)
    result_rows = [row for result in nested_results for row in result]
    return [TableRef(t[0], t[1], t[2]) for t in result_rows]

main()
