"""______________________________________________
MODULE 
    nice_py27_snips

HISTORY
    16.02.20 Daniel Braitsch
______________________________________________"""

Text = raw_input('Please type in some text:')  # hello there! 
print Text
print type(Text)

"""
python skripte sollen grundsätzlich mit zwei zeilen beginnen,
die den pfad zum interpretor ("shebang-line") bzw die zeichenkodierung ("magicline") enthalten.
# in unix you find the correct path to the interpretor with
which python
unter windows ist der pfad gleich dem Installationsverzeichnis
#!/usr/local/bin/python
or
#!c:/Python/python.exe
# -*- coding: utf-8 -*-
"""
