"""______________________________________________
MODULE 
    TODO work with defaultdict.py

PROJECT
    local_repo
    
DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -
    
INTERACT
    code.interact(local=dict(globals(),**locals()))

HISTORY
    01.03.2020 Daniel Braitsch
______________________________________________"""



from collections import defaultdict

"""______________________________________________
MODULE 
    data_types.py

PROJECT
    snipplets

DESCRIPTION
    -

ADDITIONAL INFORMATION ON PARAMETERS
    -

INTERACT
    import code
    code.interact(local=dict(globals(),**locals()))

HISTORY
    24.03.18 Daniel Braitsch

______________________________________________"""

import code

import collections

# OrderedDict, which remembers the order in which the elements have been inserted:

d = {2: 3, 1: 89, 4: 5, 3: 0}
od = collections.OrderedDict(sorted(d.items()))

############
# dictionaries
############


dict(sorted(d.items()))

for key, value in d.items(): print(key, value)

from collections import OrderedDict

d = {'banana': 3, 'apple': 4, 'pear': 1, 'orange': 2}
# dictionary sorted by key
OrderedDict(sorted(d.items(), reverse=False, key=lambda t: t[0]))
# dictionary sorted by value
OrderedDict(sorted(d.items(), key=lambda t: t[1]))
# dictionary sorted by length of the key string
OrderedDict(sorted(d.items(), key=lambda t: len(t[0])))

code.interact(local=dict(globals(), **locals()))

from collections import defaultdict

dict_portfolio = defaultdict(lambda: {})
dict_portfolio["dir1"]["dir2"] = 1
# code.interact(local=dict(globals(), **locals()))

my = dict(("key"))

my_dict = {"double_input": lambda x: 2 * x,
           "multiply_em": lambda x, y: x * y}
my_dict["double_input"](2)
my_dict["multiply_em"](2, 5)

my_d = defaultdict(lambda: {})

# dict comprehension
my_dict_comprehension = ((my_d["1"][str(2 * x)]) for x in range(5))

code.interact(local=dict(globals(), **locals()))

code.interact(local=dict(globals(), **locals()))
my_dict_comprehension = dict((str(x), y) for x, y in zip(["a", "b", "c", "d", "e"], range(5)))

############
# strings
############

# unpack a string
my_long_string = "bla1_bla2_bla3"
a, b, c = my_long_string.split("_")

# look for a string in a list
my_str = "hi"
str_list = ["hii", "hhi", "hi"]
if any([x.find(my_str) != -1 for x in str_list]):
    print("found", my_str)

import code

code.interact(local=dict(globals(), **locals()))









