

import os
import code
import logging as log

import logging



# create logger with 'spam_application'
logger = logging.getLogger('spam_application')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('spam.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


# logging.error("hallo")

Log = logging.getLogger('myLogger')
level = logging.getLevelName('INFO')
Log.setLevel(level)


Log.info("hallo")
Log.error("error")
Log.info("hallo")

# code.interact(local=dict(globals(), ** locals()))

import logging
logging.basicConfig(filename='example.log',level=logging.DEBUG)
logging.debug('This message should go to the log file')
logging.info('So should this')
logging.warning('And this, too')

# import logging_snip



log.error("")
log.error("my warning {first:10} {trying:10} helps me.".format(first="as test", trying="try"))
log.error("")

logging_snip.debug('This message should go to the log file')
logging_snip.info('So should this')
logging_snip.warning('And this, too')


code.interact(local=dict(globals(), ** locals()))
"""
