# Open files always using "with open"
import os
import csv
import code

file_name = os.path.join(os.getcwd(), "inputfile1.csv")

for input_file_name in os.listdir(os.getcwd()):
    if input_file_name.endswith(".csv"):
        input_file = os.path.join(os.getcwd(), input_file_name)
        print input_file

file_name = os.path.join(os.getcwd(), "inputfile1.csv")
with open(file_name, "rb") as file_name:
    # TODO: Work on Decode and Encode
    # data = file_name.read().decode("utf-8-sig")
    # print data
    # data = file_name.read().decode("utf-8-sig").encode("utf-8")
    # print data
    dict_reader = csv.DictReader(file_name, delimiter=";")
    for row in dict_reader:
        print "Name of row is: \t", row["Name"]
        print row
        break

output_file_name = os.path.join(os.getcwd(), "outputfile1.csv")
with open(output_file_name, "wb") as file_name:
    dict_input = {
        "Key1": "Hans",
        "Key2": "Peter",
    }
    csv_headers = dict_input.keys()
    file_name = csv.DictWriter(file_name, csv_headers, delimiter=";")
    file_name.writeheader()
    file_name.writerow(dict_input)

"""
if os.path.exists(output_file_name):
    print "Output file exists, was thus created", output_file_name
    if output_file_name.startswith(r"C:\Users"):
        print "Output filename starts with the 'C:\Users'"

# Check if object is file
print "object is file:", os.path.isfile(output_file_name)

output_file_name = os.path.join(os.getcwd(), "output_nested.csv")
with open(output_file_name, "wb") as file_name:
    dict_input = {
        "Tokyo 12pm": {"City": "JPTO", "Timezone": "Japan"},
                  "London 10am": {"City": "GBLO", "Timezone": "Europe/London"}
    }
    csv_headers = ["MarketName"] + dict_input[dict_input.keys()[0]].keys()
    file_name = csv.DictWriter(file_name, csv_headers, delimiter=";")
    file_name.writeheader()
    for key in dict_input:
        dict_input[key]["MarketName"] = key
        file_name.writerow(dict_input[key])
"""
code.interact(local=dict(globals(), **locals()))
