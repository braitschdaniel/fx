list_of_dict = []

if list_of_dict:
	file_name = r"myfolder\my_csv_file.csv"
	# or file_name = os.path.join(folder_name, file_name)
	with open(file_name, "wb") as csv_file:
		csv_file = csv.DictWriter(csv_file, list_of_dict[0].keys(), delimiter=";")
		csv_file.writerheader()
		for line in list_of_dict:
			csv_file.writerow(line)




def write_data_to_file(dir_name, file_name, dict_reader, ana1):
    with open(os.path.join(dir_name, file_name), "wb") as f:
        dict_writer = csv.DictWriter(f, delimiter=";", fieldnames=dict_reader.fieldnames)
        dict_writer.writeheader()
        dict_writer.writerows(ana1)




def read_data_from_file(dir_name, file_name):
    with open(os.path.join(dir_name, file_name)) as f:
        reader = csv.reader(f, delimiter=";")
        return list(reader)


def write_data_to_file(dir_name, file_name, data):
    with open(os.path.join(dir_name, file_name), "wb") as f:
        dict_writer = csv.DictWriter(f, delimiter=";", fieldnames=data[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(data)

